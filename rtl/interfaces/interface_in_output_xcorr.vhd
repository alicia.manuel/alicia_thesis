--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in_output_xcorr.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! This block does the same as the interface_in_output but the output data does
--! not have a clock  period difference from the input data.
--!
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity interface_in_output_xcorr is
generic (
    gen_mem_addr_size : integer := 10;              --! Bit length of the maximum address signal
    gen_mem_addr_size_decimated : integer := 10;    --! Length in bits of the address signal required for RAM
    gen_mem_data_size : integer := 32);             --! Bit length of the data   
port (
    i_clk       : in std_logic;     --! System clock
    i_nrst      : in std_logic;     --! System reset
    i_w_req     : in std_logic;     --! Asserts with the arrival of a new input data
    i_d_in      : in std_logic_vector(gen_mem_data_size-1 downto 0);    --! Input data
    i_addr_max  : in std_logic_vector(gen_mem_addr_size-1 downto 0);    --! Maximum address
    o_d_out     : out std_logic_vector(gen_mem_data_size-1 downto 0);   --! Output data
    o_WE        : out std_logic;    --! Write enable signal for the RAM
    o_finish    : out std_logic;    --! Indicates when all the data has been written
    o_w_address : out std_logic_vector(gen_mem_addr_size_decimated - 1 downto 0)    --! Write address for the RAM
);
end interface_in_output_xcorr;

architecture behavioral of interface_in_output_xcorr is

-- Input signals
signal clk      : std_logic;
signal nrst     : std_logic;
signal w_req    : std_logic;
signal d_in     : std_logic_vector(gen_mem_data_size-1 downto 0);
signal addr_max : std_logic_vector(gen_mem_addr_size-1 downto 0);

-- Output signals
signal d_out                    : std_logic_vector(gen_mem_data_size-1 downto 0);
signal w_address, w_address_reg : integer range 0 to 1000;
signal finish                   : std_logic;
signal WE                       : std_logic;

-- FSM state definition
type t_states is (s_idle, s_w_mem);
signal state, next_state : t_states;
--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
d_in        <= i_d_in;
w_req       <= i_w_req;
addr_max    <= i_addr_max;
o_WE        <= WE;
o_d_out     <= d_out;
o_w_address <= std_logic_vector(to_unsigned(w_address, o_w_address'length));

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- sync_nrst
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);
--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential signal synchronisation process
p_seq : process(nrst, clk)
begin
    -- async active low reset
    if not nrst then
        state <= s_idle;
        w_address_reg <= 0;
    elsif rising_edge(clk) then
        state <= next_state;
        w_address_reg <= w_address;
    end if;
end process p_seq;

-- Flip flop
p_finish : process(clk)
begin
    if (rising_edge(clk)) then
        o_finish  <= finish;
    end if;
end process;

-- Combinational process
p_comb : process(state, d_in, w_address_reg, w_req, addr_max)
begin
    -- Default state condition: preserve previous state
    w_address <= w_address_reg;
    d_out <= d_in;
    next_state <= state;  
    -- Default state condition: tied to zero  
    WE <= '0';
    finish <= '0';
    case(state) is
        when s_idle => 
            w_address   <= 0;   -- Reset counter to zero
            if (w_req) then     -- New input data arrives
                WE <= '1';      -- Write enable asserts to write new data to RAM
                next_state <= s_w_mem;
            end if;
        
        when s_w_mem => 
            if (std_logic_vector(to_unsigned(w_address_reg, addr_max'length)) = (addr_max - 1)) then -- All data has been written to the RAM
                finish <= '1';  -- Indicates all data has been written
                next_state <= s_idle;
            else    -- There is still more data to write
                if (i_w_req) then -- New input data arrives
                    w_address   <= w_address_reg + 1;   -- Address counter
                    WE          <= '1';                 --  Write enable asserts to write new data to RAM
                end if;
            end if;
        when others => next_state <= s_idle;
    end case;

end process;

end behavioral;
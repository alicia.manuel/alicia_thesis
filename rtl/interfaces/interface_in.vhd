--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in.vhd
-- File history:
--      0.0     : 19/01/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity interface_in is
generic (
    gen_mem_addr_size : integer := 9;
    gen_mem_data_size : integer := 8);    
port (
    i_clk       : in std_logic;
    i_nrst      : in std_logic;
    i_w_req     : in std_logic;
    i_d_in      : in std_logic_vector(gen_mem_data_size-1 downto 0);
    i_addr_max  : in std_logic_vector(gen_mem_addr_size-1 downto 0);
    o_d_out     : out std_logic_vector(gen_mem_data_size-1 downto 0);
    o_WE        : out std_logic;
    o_w_address : out std_logic_vector(gen_mem_addr_size - 1 downto 0) --integer range n_address-1 downto 0
);
end interface_in;

architecture behavioral of interface_in is

-- inputs and outputs signals
signal clk : std_logic;
signal nrst : std_logic;
-- signal WE : std_logic;
-- signal w_req : std_logic_vector(1 downto 0);
signal w_address, w_address_reg : integer range 0 to 2**gen_mem_addr_size-1 ;
signal addr_max : integer range 0 to 2**(gen_mem_addr_size-1);
-- signal d_in, d_in_reg : std_logic_vector(gen_mem_data_size-1 downto 0);
-- signal d_out : std_logic_vector(gen_mem_data_size-1 downto 0);

type t_states is (s_idle, s_w_mem);
signal state, next_state : t_states;
--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk <= i_clk;
addr_max    <= to_integer(unsigned(i_addr_max);)
-- o_w_address <= std_logic_vector(to_unsigned(w_address - 1, o_w_address'length));
-- o_w_address <= std_logic_vector(to_unsigned(w_address - 1, o_w_address'length)) when w_address_reg /= 0 else (others => '0');
o_w_address <= std_logic_vector(to_unsigned(w_address_reg, o_w_address'length));
-- d_in <= i_d_in;
-- w_req <= i_w_req;
-- w_req(0) <= i_w_req;

-- o_d_out <= d_out;
-- o_WE <= WE;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- sync_nrst
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);

-- p_dla : process(clk)
-- begin
--     if rising_edge(clk) then
--         w_req(1) <= w_req(0);
--     end if;
-- end process p_dla;
--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------

p_seq : process(nrst, clk)
begin
    -- async active low reset
    if not nrst then
        state <= s_idle;
        w_address_reg <= 0;
    elsif rising_edge(clk) then
        state <= next_state;
        w_address_reg <= w_address;
    end if;
end process p_seq;

p_comb : process(state, i_d_in, w_address_reg, i_w_req, addr_max)
begin

    w_address <= w_address_reg;
    o_d_out <= i_d_in;
    o_WE <= '0';
    -- default state condition: preserve previous state
    next_state <= state;
    -- FSM
    case(state) is
        when s_idle => 
            if i_w_req then
                
                -- increment w_address value
                w_address <= w_address_reg + 1;
                o_WE <= '1';
                -- go to next state
                next_state <= s_w_mem;
            end if;
        when s_w_mem =>
            if (w_address_reg = addr_max-1) then
                w_address   <= 0;
            end if;
            if not i_w_req then
                next_state <= s_idle;
            end if;
    end case;

end process;


end behavioral;
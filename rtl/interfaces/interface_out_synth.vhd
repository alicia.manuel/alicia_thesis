--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! The purpose of this block is to read data from the RAM memory. The input 
--! signal 'r_req' indicates when to read data in the stored memory, and when
--! this signal is asserted the counter for the 'r_address' output increments and
--! the read enable of the RAM 'RE' is set to '1'. The block also includes the 
--! input signal 'r_addr_max', which indicates the number of data to be read. 
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 2.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity interface_out_synth is
generic (
    gen_mem_addr_size : integer := 9    --! Number of address bits
    gen_mem_data_size : integer := 8);  --! Number of data bits
port (
    i_clk        : in std_logic;                                        --! System clock
    i_nrst       : in std_logic;                                        --! System reset
    i_d_in       : in std_logic_vector(gen_mem_data_size-1 downto 0);   --! Input data
    i_r_req      : in std_logic;                                        --! Indicates when to read the memory
    i_r_addr_max : in std_logic_vector(gen_mem_addr_size - 1 downto 0); --! Signal length
    o_d_out      : out std_logic_vector(gen_mem_data_size-1 downto 0);  --! Output data
    o_RE         : out std_logic;                                       --! Read enable to read data from RAM
    o_ready      : out std_logic;                                       --! Activated when a new data is issued
    o_r_address  : out std_logic_vector(gen_mem_addr_size - 1 downto 0) --! Desired RAM address
);
end interface_out_synth;

architecture behavioral of interface_out_synth is

-- Inputs signals
signal clk          : std_logic;
signal nrst         : std_logic;
signal d_in         : std_logic_vector(gen_mem_data_size-1 downto 0);
signal r_req        : std_logic;
signal r_addr_max   : std_logic_vector(gen_mem_addr_size - 1 downto 0);

-- Output signals
signal ready                    : std_logic;
signal RE                       : std_logic;
signal r_address, r_address_reg : integer range 0 to 2**gen_mem_addr_size-1;
signal d_out, d_out_reg         : std_logic_vector(gen_mem_data_size - 1 downto 0);

-- FSM state definition
type t_states is (s_idle, s_r_mem, s_wait, s_ready);
signal state, next_state : t_states;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
d_in        <= i_d_in;
r_req       <= i_r_req;
r_addr_max  <= i_r_addr_max;
o_r_address <= std_logic_vector(to_unsigned(r_address_reg, o_r_address'length));
o_d_out     <= d_out_reg;
o_ready     <= ready;
o_RE        <= RE;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- sync_nrst
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential process for signal synchronization
p_seq : process(nrst, clk)
begin
    if (not nrst) then      -- async active low reset
        state <= s_idle;    -- set FSM initial state
        -- set registers initial values
        r_address_reg   <= 0;
        d_out_reg       <= (others => '0');
    elsif rising_edge(clk) then
        -- update FSM state
        state <= next_state;
        -- update feedback registers
        r_address_reg   <= r_address;
        d_out_reg       <= d_out;
    end if;
end process p_seq;

-- Combinational process
p_comb : process(state, d_in, r_address_reg, r_req, r_addr_max, d_out_reg)
begin
    -- Default next state definition: preserve previous state
    d_out       <= d_out_reg; 
    r_address   <= r_address_reg;
    next_state  <= state;
    -- Default state definition
    ready   <= '0';
    o_RE    <= r_req;
    -- FSM of it all
    case (state) is
    when s_idle => 
        if (r_req = '1') then -- when a read request condition is detected go to next state
            next_state <= s_r_mem;
        end if;

    when s_r_mem => 
        d_out <= d_in;  -- Load input value
        if (r_address_reg = to_integer(unsigned(r_addr_max))) then -- All the wanted memory addresses have been read
            r_address <= 0; -- Reset counter
            next_state  <= s_ile;
        else
            r_address <= r_address_reg + 1; -- Increment counter
        end if;
        next_state <= s_ready;

    when s_wait =>  -- We wait until a new input is recieved
        if (i_r_req = '1') then -- New input value
            next_state <= s_r_mem;
        end if;
    when s_ready => 
        ready <= '1';   -- Indicates a new output is available
        next_state <= s_wait;
        
    end case;
end process;
end;
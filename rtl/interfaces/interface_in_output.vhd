--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in_output.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! This block is responsible for managing the write address and data flow for 
--! the output RAM. The methodology employed is as follows: it receives the 
--! `data_max_address` signal and a `write_request` signal. The 'WE' signal for 
--! the RAM asserts and counter increments by one until it reaches the 
--! `data_max_address`, indicating that all the data has been received.
--!
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity interface_in_output is
generic (
    gen_mem_addr_size           : integer := 14;    --! Bit length of the maximum address signal
    gen_mem_addr_size_decimated : integer := 14;    --! Length in bits of the address signal required for RAM
    gen_mem_data_size           : integer := 32);   --! Bit length of the data 
port (
    i_clk       : in std_logic;     --! System clock
    i_nrst      : in std_logic;     --! System reset
    i_w_req     : in std_logic;     --! Asserts with the arrival of a new input data
    i_d_in      : in std_logic_vector(gen_mem_data_size-1 downto 0);    --! Input data
    i_addr_max  : in std_logic_vector(gen_mem_addr_size-1 downto 0);    --! Maximum address
    o_d_out     : out std_logic_vector(gen_mem_data_size-1 downto 0);   --! Output data
    o_WE        : out std_logic;    --! Write enable signal for the RAM
    o_finish    : out std_logic;    --! Indicates when all the data has been written
    o_w_address : out std_logic_vector(gen_mem_addr_size_decimated - 1 downto 0)    --! Write address for the RAM
);
end interface_in_output;

architecture behavioral of interface_in_output is

-- Input signals
signal clk      : std_logic;
signal nrst     : std_logic;
signal w_req    : std_logic;
signal d_in     : std_logic_vector(gen_mem_data_size-1 downto 0);
signal addr_max  : std_logic_vector(gen_mem_addr_size-1 downto 0);

-- Output signals
signal w_address    : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal finish : std_logic;
signal WE : std_logic;

-- Auxilliary signals (adaptative counter signals)
signal counter_enable   : std_logic;
signal reset_count      : std_logic;
signal d_out : std_logic_vector(gen_mem_data_size-1 downto 0);

-- FSM state definition
type t_states is (s_idle, s_w_mem, s_wait);
signal state, next_state : t_states;
--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

component adaptative_counter is
    generic(cnt_bit_size : integer := gen_mem_addr_size);
    port (
        i_nrst      :   IN  std_logic;
        i_clk       :   IN  std_logic;
        i_step      :   IN  std_logic;
        o_cnt       :   OUT std_logic_vector(cnt_bit_size - 1 downto 0)
        
    );
end component adaptative_counter;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
addr_max    <= i_addr_max;
d_in        <= i_d_in;
w_req       <= i_w_req;
o_w_address <= w_address;
o_d_out     <= d_out;
o_WE        <= WE;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- sync_nrst
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);

-- Data write counter
counter : adaptative_counter
port map(
    i_nrst      => reset_count,
    i_clk       => clk,
    i_step      => counter_enable,
    o_cnt       => w_address
);
--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential signal synchronisation process
p_seq : process(nrst, clk)
begin
    -- async active low reset
    if not nrst then
        state <= s_idle;
    elsif rising_edge(clk) then
        state <= next_state;
    end if;
end process p_seq;

-- Flip flop
p_finish : process(clk)
begin
    if (rising_edge(clk)) then
        o_finish  <= finish;
    end if;
end process;

-- Combinational process
p_comb : process(state, d_in, w_address, w_req, addr_max)
begin
    -- Default condition: preserve previous state
    d_out       <= d_in;
    next_state  <= state;
    -- Default condition
    WE            <= '0';
    finish          <= '0';
    counter_enable  <= '0';
    reset_count     <= '1';
    -- FSM
    case(state) is
        when s_idle => 
            reset_count <= '0'; -- Reset counter to zero
            if (i_w_req) then   -- New input data arrives
                next_state <= s_w_mem;
            end if;

        when s_w_mem => 
            counter_enable <= '1';  -- Address counter
            WE <= '1';              --  Write enable asserts to write new data to RAM
            if (w_address = (addr_max - 1)) then -- All data has been written to the RAM
                finish <= '1';      -- Indicates all data has been written
                next_state <= s_idle;
            else
                next_state <= s_wait;
            end if;
        
        when s_wait => 
            if (i_w_req) then   -- New input data arrives
                next_state <= s_w_mem; 
            end if; 
        when others => next_state <= s_idle;
    end case;

end process;

end behavioral;
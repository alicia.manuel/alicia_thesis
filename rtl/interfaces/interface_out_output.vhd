--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in.vhd
-- File history:
--      0.0     : 15/03/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! The purpose of this block is to read data from the RAM memory. The input 
--! signal 'start' indicates when all the data to be read is stored in the memory. 
--! When 'start' asserts, the module begins requesting data to be read address by 
--! address. The timing of the read enables depends on the uart_tx block, as all 
--! the received data will be sent to it. When the uart_tx block becomes available, 
--! indicated by 'tx_busy' being tied to zero, the interface_out will request more 
--! data. The block also includes the input signal 'r_addr_max', which indicates 
--! the number of data to be read. Due to the division of data into four parts in 
--! the RAM output, there are four times more addresses to access.
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 2.0
--! @date 2023-03-15
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------


entity interface_out_output is
generic (
    gen_mem_addr_size : integer := 12;  --! Number of address bits
    gen_mem_data_size : integer := 8);  --! Number of data bits
port (
    i_clk       : in std_logic;                                         --! System clock
    i_nrst      : in std_logic;                                         --! System reset
    i_d_in      : in std_logic_vector(gen_mem_data_size-1 downto 0);    --! Input data
    i_start     : in std_logic;                                         --! Indicates when all input data has been written to memory.
    i_tx_busy   : in std_logic;                                         --! It is set to zero when a new byte can be sent
    i_r_addr_max: in std_logic_vector(gen_mem_addr_size - 1 downto 0);  --! Signal length
    o_d_out     : out std_logic_vector(gen_mem_data_size-1 downto 0);   --! Output data
    o_RE        : out std_logic;                                        --! Read enable to read data from RAM
    o_ready     : out std_logic;                                        --! Activated when a new data is issued.
    o_r_address : out std_logic_vector(gen_mem_addr_size - 1 downto 0)  --! Desired RAM address
);
end interface_out_output;

architecture behavioral of interface_out_output is

-- Signal inputs
signal clk      : std_logic;
signal nrst     : std_logic;
signal tx_busy  : std_logic;
signal start    : std_logic;

-- Signal output
signal ready                    : std_logic;
signal r_address, r_address_reg : integer range 0 to 2**gen_mem_addr_size-1;
signal d_out, d_out_reg         : std_logic_vector(gen_mem_data_size - 1 downto 0);

-- Finite State Machine (FSM)
type t_states is (s_idle, s_r_mem, s_tx_busy, s_wait);
signal state, next_state : t_states;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
tx_busy     <= i_tx_busy;
start       <= i_start;
o_r_address <= std_logic_vector(to_unsigned(r_address_reg, o_r_address'length));
o_d_out     <= d_out_reg;
o_ready     <= ready;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- Reset synchronizer
sync_nrst : reset_sync
port map(
	i_clk   => clk,
	i_nrst  => i_nrst,
	o_q     => nrst
);

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential process to synchronize signals
p_seq : process(nrst, clk)
begin
    -- async active low reset
    if not nrst then
        -- Set FSM initial state
        state <= s_idle;
        -- Set registers initial values
        r_address_reg   <= 0;
        d_out_reg       <= (others => '0');
    elsif rising_edge(clk) then
        -- Update FSM state
        state <= next_state;
        -- Update feedback registers
        r_address_reg   <= r_address;
        d_out_reg       <= d_out;
    end if;
end process p_seq;

-- Combinational process
p_comb : process(state, i_d_in, r_address_reg, start, i_r_addr_max, d_out_reg, tx_busy)
begin
    -- Default condition
    d_out       <= d_out_reg; 
    r_address   <= r_address_reg;
    ready       <= '0';
    next_state  <= state;
    o_RE        <= '0';

    -- FSM of it all
    case (state) is
    when s_idle => 
        r_address   <= 0;
        d_out       <= (others => '0');
        if start then   -- Memory ready to be read
            next_state <= s_tx_busy;
        end if;

    when s_tx_busy => 
        if (tx_busy = '0') then     -- Make sure if the tx_module is ready to recieve new data
            o_RE        <= '1';     -- Read data from RAM
            next_state  <= s_r_mem;
        end if;
    when s_r_mem => 
        d_out <= i_d_in;    -- Output register of the RAM data recieved
        ready <= '1';       -- Indicate that a new data has been sent
        if r_address_reg = (to_integer(unsigned(i_r_addr_max))*4) then  -- All RAM data has been read
            next_state <= s_idle;                                       -- Process finished, return to the initial state
        else                                                            -- There is still more data to be read
            r_address <= r_address_reg + 1;                             -- Address counter
            next_state <= s_wait;
        end if;

    when s_wait =>
        if (tx_busy = '1') then     -- Wait until the tx_module recieves the data
            next_state <= s_tx_busy;
        else
            next_state <= s_wait;
        end if;

    when others => next_state <= s_idle;
        
    end case;
end process;
end;
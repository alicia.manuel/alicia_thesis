--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: interface_in_synth.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! This block is responsible for managing the write address and data flow for 
--! the output RAM. The methodology employed is as follows: it receives the 
--! `write_request` signal. The 'WE' signal for the RAM asserts and counter 
--! increments by one. The counter is reset when the nrst signal is recieved.
--!
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity interface_in_synth is
generic (
    gen_mem_addr_size : integer := 16;  -- Bit length of the address signal
    gen_mem_addr_max_size   : integer := 14;
    gen_mem_data_size : integer := 8);  -- Bit length of the data
port (
    i_clk       : in std_logic;                                         --! System clock
    i_nrst      : in std_logic;                                         --! System reset
    i_w_req     : in std_logic;                                         --! Indicates when a new data wants to be writen to the RAM
    i_d_in      : in std_logic_vector(gen_mem_data_size-1 downto 0);    --! Input data
    i_addr_max  : in std_logic_vector(gen_mem_addr_size-1 downto 0);
    o_d_out     : out std_logic_vector(gen_mem_data_size-1 downto 0);   --! Output data
    o_WE        : out std_logic;                                        --! Write enable for the RAM
    o_w_address : out std_logic_vector(gen_mem_addr_size - 1 downto 0)  --! Addres where the data will be written
);
end interface_in_synth;

architecture behavioral of interface_in_synth is

-- Input signals
signal clk      : std_logic;
signal nrst     : std_logic;
signal w_req    : std_logic;
signal d_in     : std_logic_vector(gen_mem_data_size-1 downto 0);
signal addr_max : integer range 0 to 2**gen_mem_addr_size-1 ;

-- Output signals
signal WE                       : std_logic;
signal w_address, w_address_reg : integer range 0 to 4*(2**gen_mem_addr_size-1) ;
signal d_out, d_out_reg         : std_logic_vector(gen_mem_data_size-1 downto 0);

-- Finite State Machine (FSM)
type t_states is (s_idle, s_w_mem, s_enable);
signal state, next_state : t_states;
--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin
--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
d_in        <= i_d_in;
w_req       <= i_w_req;
addr_max    <= to_integer(unsigned(i_addr_max));
o_w_address <= std_logic_vector(to_unsigned(w_address_reg, o_w_address'length));
o_d_out     <= d_out_reg;
o_WE        <= WE;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- Reset synchronizer
sync_nrst : reset_sync
port map(
	i_clk   => clk,
	i_nrst  => i_nrst,
	o_q     => nrst
);

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential process for signal synchronization
p_seq : process(nrst, clk)
begin
    -- async active low reset
    if not nrst then
        state <= s_idle;
        d_out_reg   <= (others => '0');
        w_address_reg <= 0;
    elsif rising_edge(clk) then
        -- Load values
        state <= next_state;
        d_out_reg <= d_out;
        w_address_reg <= w_address;
    end if;
end process p_seq;

-- Combinational process
p_comb : process(state, d_in, w_address_reg, w_req, d_out_reg)
begin
    -- Default condition: preserve previous state
    w_address   <= w_address_reg;
    d_out       <= d_out_reg;
    next_state  <= state;
    -- Default state condition:
    WE          <= '0';
    -- FSM
    case(state) is
        when s_idle => 
            if (w_req) then                         -- New input data arrives
                d_out       <= d_in;                -- Output register
                WE          <= '1';                 -- Indicate that a new address wants to be written   
                next_state <= s_w_mem;              -- go to next state
            end if;
        when s_w_mem =>                         
            if (w_req = '0') then
                if (w_address_reg = (addr_max*4)-1) then    -- All data has been written
                    w_address   <= 0;                   -- Reset counter
                else
                    w_address   <= w_address_reg + 1;   -- Increment the address number
                end if;
                next_state  <= s_idle;
            end if;
        when others => next_state   <= s_idle;
    end case;

end process;

end behavioral;
--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: FIR.vhd
-- File history:
--      0.0     : 19/01/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.fixed_pkg.all;
use IEEE.MATH_REAL.ALL;

--------------------------------------------------------------------------------
--!The module operates by receiving the input signal 'start', which indicates the 
--! arrival of a new sample. The data is registered and then multiplied by the 
--! coefficients. Subsequently, it enters the cascade of flip-flops where the 
--! output of each flip-flop is added to the input of the next flip-flop. The 
--! output data is obtained from the last flip-flop in the cascade. 
--!
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 2.0
--! @date 2023-03-15
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------


entity FIR is
generic (
    gen_integer_size : integer := 2;    -- Number of data integer bits
    gen_decimal_size : integer := 30    -- Number of data decimal bits
    --gen_sum_bits           : integer := 5
    );    
port (
    i_clk	        :	in	std_logic;                                                      --! System clock
    i_nrst	        :	in	std_logic;                                                      --! System reset
    i_start	        :	in	std_logic;                                                      --! Indicates when a new data is recieved
    o_ready	        :	out	std_logic;                                                      --! Activated when the module is available for new data
    o_finish	    :	out	std_logic;                                                      --! Asserts when a new data is output
    o_return_val	:	out	std_logic_vector(gen_integer_size+gen_decimal_size-1 downto 0); --! Output data
    i_input_var	    :	in	std_logic_vector(gen_integer_size+gen_decimal_size-1 downto 0)  --! Input data
);
end FIR;

architecture behavioral of FIR is

-- Input signals
signal clk                      : std_logic;
signal nrst                     : std_logic;
signal start                    : std_logic;
signal input_var, input_var_reg : sfixed(gen_integer_size-1 downto -gen_decimal_size);

-- Output signals
signal ready, ready_reg     : std_logic;
signal finish, finish_reg   : std_logic;
signal return_val           : sfixed(gen_integer_size-1 downto -gen_decimal_size);

-- Auxilliary signals
constant TAPS : integer range 0 to 2000 := 48;          -- Number of coefficients of the filter
signal enable_flipflop : std_logic;                     -- Enable for the flipflop cascade
type t_coefficient_real is array (0 to TAPS-1) of real; -- Filter coefficients
constant coeff_real : t_coefficient_real := (-0.000104727467330348859462715860324522055,
-0.000392074744998403314222290561019690358,
-0.000781715557635020508522871729439884803,
-0.00147668595952192647363654387504539045, 
-0.0024103367236310464793991314991217223,  
-0.003620416280096990305215065575339394854,
-0.005004144859511363424564578394893032964,
-0.006424705176368399224540173264585973811,
-0.00764111056876326522058207046939060092, 
-0.008349346880155098654663348156645952258,
-0.008185117603058469915033867891906993464,
-0.006769503914956541586189775472348628682,
-0.003750602374476772523509415790954335534,
 0.001139480059234831890030426038151745161,
 0.008033174150323582271560063361448555952,
 0.016882754688886700972227927763924526516,
 0.027433523680081089990423492963600438088,
 0.039220331323394204414700681127214920707,
 0.051590887374392463415873066878702957183,
 0.063755750725587256866333518701139837503,
 0.074860229325539123790278495107486378402,
 0.084070156970805984264494270519207930192,
 0.090660968225924981611640873779833782464,
 0.094098184868813189440928113072004634887,
 0.094098184868813189440928113072004634887,
 0.090660968225924981611640873779833782464,
 0.084070156970805984264494270519207930192,
 0.074860229325539123790278495107486378402,
 0.063755750725587256866333518701139837503,
 0.051590887374392463415873066878702957183,
 0.039220331323394204414700681127214920707,
 0.027433523680081089990423492963600438088,
 0.016882754688886700972227927763924526516,
 0.008033174150323582271560063361448555952,
 0.001139480059234831890030426038151745161,
-0.003750602374476772523509415790954335534,
-0.006769503914956541586189775472348628682,
-0.008185117603058469915033867891906993464,
-0.008349346880155098654663348156645952258,
-0.00764111056876326522058207046939060092, 
-0.006424705176368399224540173264585973811,
-0.005004144859511363424564578394893032964,
-0.003620416280096990305215065575339394854,
-0.0024103367236310464793991314991217223,  
-0.00147668595952192647363654387504539045,
-0.000781715557635020508522871729439884803,
-0.000392074744998403314222290561019690358,
-0.000104727467330348859462715860324522055);

type t_coefficient_sfixed is array (0 to TAPS-1) of sfixed(gen_integer_size-1 downto -gen_decimal_size);    -- Filter coefficients in bit type

-- Function to convert the coefficients from real to sfixed
function Real_to_sfixed(Coefficient : t_coefficient_real := coeff_real) 
    return t_coefficient_sfixed is
    variable result : t_coefficient_sfixed;
    begin
        for i in 0 to TAPS-1 loop
            result(i) := to_sfixed(coeff_real(i), result(0)'left, result(0)'right); -- Conversion of real to sfixed
        end loop;
    return result;
end function;

-- Coefficients in sfixed
constant coeff: t_coefficient_sfixed := Real_to_sfixed(Coefficient => coeff_real);
-- Results of the input data and coefficient multiplication
type t_op is array (0 to TAPS-1) of sfixed(gen_integer_size-1 downto -gen_decimal_size);    
signal op : t_op;
-- Results of all the op signals summation
type t_sum is array (0 to TAPS-1) of sfixed(gen_integer_size-1 downto -gen_decimal_size);   
signal sum : t_sum;
-- Flip flop output
type t_flipflop is array (0 to TAPS-1) of std_logic_vector(gen_integer_size+gen_decimal_size-1 downto 0);   
signal q : t_flipflop;
-- Finite State Machine (FSM)
type t_states is (s_idle, s_reg, s_sum, s_wait, s_finish);
signal state, next_state : t_states;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

component flipflop
    generic (gen_data_length : integer := gen_integer_size+gen_decimal_size);
    port(clk,clrn,enable: in std_logic; 
        d : in std_logic_vector(gen_data_length-1 downto 0);
        q : out std_logic_vector(gen_data_length-1 downto 0));
end component;

begin
--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk             <= i_clk;
start           <= i_start;
o_ready         <= ready_reg;
o_finish        <= finish_reg;
o_return_val    <= to_slv(return_val);
return_val      <= sum(TAPS-1);

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
--Reset synchronization
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Cascade flip-flop generation
reg: for i in 0 to TAPS-1 generate
    d0: if (i=0) generate
        -- The input data is the first multiplication
        cmp0: flipflop port map(clk=>clk, clrn=>not(nrst), enable=> enable_flipflop, d => std_logic_vector(resize(op(0), sum(0)'high, sum(0)'low)), q => q(0));
        else generate
        -- The input data is the previous multiplication added with the current multiplication
        cmpi: flipflop port map(clk=>clk, clrn=>not(nrst), enable => enable_flipflop, d => std_logic_vector(resize(sum(i-1)+op(i), sum(0)'high, sum(0)'low)),  q=>q(i));
    end generate;
end generate;

-- Sequential process for signals synchronization
p_seq : process(nrst, clk)
begin
    -- Async active low reset
    if nrst then
        -- Set initial state
        state           <= s_idle;
        input_var_reg   <= (others => '0');
        finish_reg      <= '0';
        ready_reg       <= '1';
    elsif rising_edge(clk) then
        -- Update state
        state           <= next_state;
        input_var_reg   <= input_var;
        finish_reg      <= finish;
        ready_reg       <= ready;
    end if;
end process p_seq;

p_comb : process(state, input_var_reg, start, q, i_input_var, finish_reg, ready_reg)
begin
    -- Signals update
    finish          <= finish_reg;
    ready           <= ready_reg;
    enable_flipflop <= '0';
    input_var       <= input_var_reg;
    next_state      <= state;
    for i in 0 to TAPS-1 loop
        op(i)  <= resize((input_var_reg * coeff(i)), op(0)'high, op(0)'low);    -- Combinational multiplication
    end loop;

    for i in 0 to TAPS-1 loop
        sum(i) <= sfixed(q(i)); -- Combinational sum
    end loop;

    case (state) is
    when s_idle =>
        -- Set initial values
        finish          <= '0';
        ready           <= '1';
        enable_flipflop <= '0';
        input_var       <= (others => '0');
        if start then   -- New input data arrives
            next_state <= s_reg;
        end if;
        -- for i in 0 to TAPS-1 loop
        --     op(i)  <= resize((input_var_reg * coeff(i)), op(0)'high, op(0)'low); -- Input data and coefficients multiplication
        -- end loop;

    when s_reg =>
        ready       <= '0';                     -- Filter not available
        input_var   <= sfixed(i_input_var);     -- Input register with n sfixed conversion since data is decimal
        next_state  <= s_sum;

    when s_sum =>
            enable_flipflop <= '1';     -- Cascade flip-flops activation
            ready           <= '0';     -- Filter not available
            finish          <= '1';     -- New output ready
            next_state      <= s_wait;

    when s_wait =>
        finish  <= '0';             -- Finish set to zero to obtain a one-period activation
        if (start = '1') then       -- New input data arrives
            next_state <= s_reg;    -- Input register state
        else
            next_state <= s_wait;
        end if;
    
    when others => next_state <= s_idle;
        
    end case;
end process;
end;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
--use IEEE.MATH_REAL.ALL;

entity xcorr_fsm is
generic (gen_mem_addr_size : integer := 9;
        gen_mem_w_addr_size : integer := 9);
port (
	    i_clk           :	in	std_logic;                                      --! System clock
	    i_nrst          :	in	std_logic;                                      --! System reset
        i_start         :   in std_logic;                                       --! Indicates when a new input data arrives
        i_signal_length : in std_logic_vector(gen_mem_addr_size-1 downto 0);    --! Total length of the signal
        o_max_addr      :   out std_logic_vector(gen_mem_addr_size-1 downto 0); --! Indicates the length of the input signal
        o_mem_w_req_1   :   out std_logic;                                      --! Shows when to write to RAM
        o_mem_w_req_2   :   out std_logic;
        o_mem_req       :   out std_logic                                       --! Shows when to read RAM data
);
end xcorr_fsm;

architecture behavioral of xcorr_fsm is

-- Signal inputs
signal clk			            : std_logic;
signal nrst			            : std_logic;
signal start                    : std_logic;
signal signal_length            : std_logic_vector(gen_mem_addr_size-1 downto 0);

-- Signal outputs
signal max_addr, max_addr_reg   : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal mem_req, mem_req_reg     : std_logic;
signal mem_w_req_1, mem_w_req_reg_1 : std_logic;
signal mem_w_req_2, mem_w_req_reg_2 : std_logic;

-- Auxilliary signals
signal data_counter                         : std_logic_vector(gen_mem_w_addr_size downto 0);   -- Data read and write counter
signal counter_enable                       : std_logic;                                        -- Counter enable
signal counter_nrst                         : std_logic;                                        -- Counter reset                 
--signal max_addr_write, max_addr_write_reg : std_logic_vector(gen_mem_w_addr_size downto 0);
signal max_addr_write, max_addr_write_reg : integer range 0 to 1000;
-- FSM state definition
type t_state is (s_idle, s_write_1, s_write_2, s_w_wait, s_transition, s_data, s_req, s_wait);
signal current_state : t_state;
signal next_state : t_state;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
port(  
    i_clk   : in std_logic;
    i_nrst     : in std_logic;
    o_q     : out std_logic
);
end component reset_sync;

component adaptative_counter is
generic(cnt_bit_size : integer := gen_mem_w_addr_size+1);
port (
    i_nrst      :   IN  std_logic;
    i_clk       :   IN  std_logic;
    i_step      :   IN  std_logic;
    o_cnt       :   OUT std_logic_vector(cnt_bit_size - 1 downto 0)
    
);
end component adaptative_counter;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk				<= i_clk;
start			<= i_start;
signal_length   <= i_signal_length;
o_max_addr      <= max_addr_reg;
o_mem_req       <= mem_req;
o_mem_w_req_1     <= mem_w_req_reg_1;
o_mem_w_req_2     <= mem_w_req_reg_2;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZER & DELAYERS
--------------------------------------------------------------------------------
-- Reset synchronizer
nrs_sync : reset_sync
port map ( 
  i_clk     => clk,
  i_nrst    => i_nrst,
  o_q       => nrst
);

-- Data read and write counter
counter : adaptative_counter
port map(
    i_nrst      => counter_nrst,
    i_clk       => clk,
    i_step      => counter_enable,
    o_cnt       => data_counter
);

--------------------------------------------------------------------------------
--  FSM
--------------------------------------------------------------------------------
-- Sequential process for signal synchronization
p_seq: process(clk, nrst)
begin
	if (nrst = '0') then
        -- Set initial values
		current_state       <= s_idle;
        mem_req_reg         <= '0';
        mem_w_req_reg_1       <= '0';
        mem_w_req_reg_2       <= '0';
        --max_addr_write_reg  <= (others => '0');
        max_addr_write_reg  <= 0;
        max_addr_reg        <= (others => '0');
    elsif (rising_edge(clk)) then	
        -- Update values
		current_state       <= next_state;
        mem_req_reg         <= mem_req;
        mem_w_req_reg_1       <= mem_w_req_1;
        mem_w_req_reg_2       <= mem_w_req_2;
        max_addr_reg        <= max_addr;
        max_addr_write_reg  <= max_addr_write;
	end if;
end process;

-- Combinational process
p_comb: process(current_state, max_addr_write_reg, mem_req_reg, max_addr_reg, data_counter, start, signal_length, mem_w_req_reg_1, mem_w_req_reg_2) 
begin
    -- Update signals
	next_state      <= current_state;
    mem_req         <= mem_req_reg;
    mem_w_req_1       <= mem_w_req_reg_1;
    mem_w_req_2       <= mem_w_req_reg_2;
    max_addr        <= max_addr_reg;
    counter_nrst    <= '1';
    counter_enable  <= '0';
    max_addr_write  <= max_addr_write_reg;
case current_state is
	when s_idle => 
        -- Set initial values
        max_addr        <= (others => '0');
        mem_req         <= '0';
        mem_w_req_1       <= '0';
        mem_w_req_2       <= '0';
        counter_enable  <= '0';
        counter_nrst    <= '0';
        --max_addr_write  <= (others => '0');
        max_addr_write  <= 0;
        if (start = '1') then   -- New input data arrives
            -- Input register
            max_addr        <= signal_length;
            -- x1 = shift 1 time
            -- max_addr_write(gen_mem_w_addr_size-1 downto 1)  <= signal_length(gen_mem_addr_size-2 downto 0);
            -- max_addr_write( 0)  <= '0';
            max_addr_write <= to_integer(unsigned(signal_length))*2;
            next_state      <= s_write_1;
        else
            next_state <= s_idle;
        end if;

    when s_write_1 => 
        mem_w_req_1       <= '1'; -- Indicates a new data has to be written in the RAM
        counter_enable  <= '1'; -- 
        next_state  <= s_w_wait;

    when s_write_2 => 
        mem_w_req_2       <= '1'; -- Indicates a new data has to be written in the RAM
        counter_enable  <= '1'; -- 
        next_state  <= s_w_wait;

    when s_w_wait =>
        mem_w_req_1       <= '0';
        mem_w_req_2       <= '0';
        max_addr_write  <= max_addr_write_reg;
        if (data_counter = std_logic_vector(to_unsigned(max_addr_write_reg, data_counter'length))) then
            next_state <= s_transition;
        elsif (data_counter < max_addr) then
            if (start = '1') then
                next_state <= s_write_1;
            else
                next_state <= s_w_wait;
            end if;
        else
            if (start = '1') then
                next_state <= s_write_2;
            else
                next_state <= s_w_wait;
            end if;
        end if;

    when s_transition => counter_nrst <= '0'; next_state <= s_data;

	when s_data =>
        counter_enable  <= '1';
        if (data_counter = max_addr) then
            next_state  <= s_idle;
        else
            next_state <= s_req;
        end if;

    when s_req => 
        mem_req <= '1'; 
        next_state <= s_wait;

    when s_wait =>
        mem_req <= '0';
        next_state <= s_data;
		
							
	when others => next_state <= s_idle;

end case;
end process;

end behavioral;

 
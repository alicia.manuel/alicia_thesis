--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: fir_fsm.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: FSM (Finite State Machine) for a FIR (Finite Impulse Response) 
-- filter.
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! This block process the input data and controls the read and write operations 
--! of the RAM based on the start signal, counter values, and FIR filter 
--! readiness.
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity fir_fsm is
generic (gen_mem_addr_size : integer := 14;
        gen_mem_w_addr_size : integer := 16);
port (
	    i_clk           :	in	std_logic;                                      --! System clock
	    i_nrst          :	in	std_logic;                                      --! System reset
        i_start         :   in std_logic;                                       --! Indicates when a new input data arrives
        i_signal_length : in std_logic_vector(gen_mem_addr_size-1 downto 0);    --! Total length of the signal
        i_fir_ready     :   in  std_logic;                                      --! Indicates there is a new FIR filter output
        o_max_addr      :   out std_logic_vector(gen_mem_addr_size-1 downto 0); --! Indicates the length of the input signal
        o_mem_w_req     :   out std_logic;                                      --! Shows when to write to RAM
        o_mem_req       :   out std_logic                                       --! Shows when to read RAM data
);
end fir_fsm;

architecture behavioral of fir_fsm is

-- Signal inputs
signal clk			            : std_logic;
signal nrst			            : std_logic;
signal start                    : std_logic;
signal fir_ready                : std_logic;
signal signal_length            : std_logic_vector(gen_mem_addr_size-1 downto 0);

-- Signal outputs
signal max_addr, max_addr_reg   : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal mem_req, mem_req_reg     : std_logic;
signal mem_w_req, mem_w_req_reg : std_logic;

-- Auxilliary signals
signal data_counter                         : std_logic_vector(gen_mem_w_addr_size-1 downto 0);   -- Data read and write counter
signal counter_enable                       : std_logic;                                        -- Counter enable
signal counter_nrst                         : std_logic;                                        -- Counter reset                 
signal max_addr_write, max_addr_write_reg : std_logic_vector(gen_mem_w_addr_size-1 downto 0);

-- FSM state definition
type t_state is (s_idle, s_write, s_w_wait, s_transition, s_data, s_req, s_wait);
signal current_state : t_state;
signal next_state : t_state;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
port(  
    i_clk   : in std_logic;
    i_nrst     : in std_logic;
    o_q     : out std_logic
);
end component reset_sync;

component adaptative_counter is
generic(cnt_bit_size : integer := gen_mem_w_addr_size);
port (
    i_nrst      :   IN  std_logic;
    i_clk       :   IN  std_logic;
    i_step      :   IN  std_logic;
    o_cnt       :   OUT std_logic_vector(cnt_bit_size - 1 downto 0)
    
);
end component adaptative_counter;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk				<= i_clk;
start			<= i_start;
fir_ready		<= i_fir_ready;
signal_length   <= i_signal_length;
o_max_addr      <= max_addr_reg;
o_mem_req       <= mem_req;
o_mem_w_req     <= mem_w_req_reg;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZER & DELAYERS
--------------------------------------------------------------------------------
-- Reset synchronizer
nrs_sync : reset_sync
port map ( 
  i_clk     => clk,
  i_nrst    => i_nrst,
  o_q       => nrst
);

-- Data read and write counter
counter : adaptative_counter
port map(
    i_nrst      => counter_nrst,
    i_clk       => clk,
    i_step      => counter_enable,
    o_cnt       => data_counter
);

--------------------------------------------------------------------------------
--  FSM
--------------------------------------------------------------------------------
-- Sequential process for signal synchronization
p_seq: process(clk, nrst)
begin
	if (nrst = '0') then
        -- Set initial values
		current_state       <= s_idle;
        mem_req_reg         <= '0';
        mem_w_req_reg       <= '0';
        max_addr_write_reg  <= (others => '0');
        max_addr_reg        <= (others => '0');
    elsif (rising_edge(clk)) then	
        -- Update values
		current_state       <= next_state;
        mem_req_reg         <= mem_req;
        mem_w_req_reg       <= mem_w_req;
        max_addr_reg        <= max_addr;
        max_addr_write_reg  <= max_addr_write;
	end if;
end process;

-- Combinational process
p_comb: process(current_state, max_addr_write_reg, mem_req_reg, max_addr_reg, fir_ready, data_counter, start, signal_length, mem_w_req_reg) 
begin
    -- Update signals
	next_state      <= current_state;
    mem_req         <= mem_req_reg;
    mem_w_req       <= mem_w_req_reg;
    max_addr        <= max_addr_reg;
    counter_nrst    <= '1';
    counter_enable  <= '0';
    max_addr_write  <= max_addr_write_reg;
case current_state is
	when s_idle => 
        -- Set initial values
        max_addr        <= (others => '0');
        mem_req         <= '0';
        mem_w_req       <= '0';
        counter_enable  <= '0';
        counter_nrst    <= '0';
        max_addr_write  <= (others => '0');
        if (start = '1') then   -- New input data arrives
            -- Input register
            max_addr        <= signal_length;
            -- x4 = shift 2 times
            max_addr_write(gen_mem_w_addr_size-1 downto 2)  <= signal_length(gen_mem_addr_size-1 downto 0);
            max_addr_write(1 downto 0)  <= (others => '0');
            next_state      <= s_write;
        else
            max_addr_write  <= (others => '0');
            next_state <= s_idle;
        end if;

    when s_write => 
        mem_w_req       <= '1'; -- Indicates a new data has to be written in the RAM
        counter_enable  <= '1'; -- Counts the number of data written in the RAM
        next_state  <= s_w_wait;

    when s_w_wait =>
        if (data_counter = max_addr_write_reg) then -- All input data has been written
            next_state <= s_transition;             
        else
            if (start = '1') then                   -- New input data arrives
                next_state <= s_write;
            else
                next_state <= s_w_wait;
            end if;
        end if;

    when s_transition => counter_nrst <= '0'; next_state <= s_data; -- The counter is reset

	when s_data =>
        counter_enable  <= '1';             -- Counts the number of data read from the RAM.
        if (data_counter = max_addr) then   -- All data have been read
            next_state  <= s_idle;
        else
            next_state <= s_req;
        end if;

    when s_req => 
        mem_req <= '1';         -- Indicates when to read the RAM
        next_state <= s_wait;

    when s_wait =>              -- This state waits until the fir is ready for a new samples
        mem_req <= '0';
        if (fir_ready = '1') then
            next_state <= s_data;  
        else
            next_state <= s_wait;
        end if;
		
							
	when others => next_state <= s_idle;

end case;
end process;

end behavioral;

 
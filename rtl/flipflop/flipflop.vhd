--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: flipflop.vhd
-- File history:
--      0.0     : 03/02/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: Serial architecture of cross-correlation
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.fixed_pkg.all;


entity flipflop is
    generic (gen_data_length : integer := 32);
port (
    clk,clrn,enable: in std_logic; 
    d : in std_logic_vector(gen_data_length-1 downto 0);
	q : out std_logic_vector(gen_data_length-1 downto 0));
end flipflop;

architecture rtl of flipflop is

    signal r, rin : std_logic_vector(gen_data_length-1 downto 0);

begin
    
    p_comb : process(clrn, enable, d, r)
    variable tmp : std_logic_vector(gen_data_length-1 downto 0);
    begin
        if (clrn = '0') then tmp := (others => '0');
        elsif (enable = '1') then tmp := d;
        else tmp := r;
        end if;
        rin <= tmp;
        q <= r;
    end process;

    p_seq : process(clk)
    begin
        if (rising_edge(clk)) then r <= rin; end if;
    end process;

end rtl;
--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: FIR_system_2.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: The FIR_system module connects the filter and decimator components 
-- together. It establishes the interconnection between the filter and the decimator 
-- to create a complete system.
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
--! This block implements the stages of FIR and decimator desired. The two
--! filters (VHDL and SmartHLS) are instantiated. The desired filter can be 
--! selected by uncommenting the corresponding instantiation.
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity FIR_system_2 is
generic (gen_data_integer_size  : integer := 2;     -- Number of bits representing the integer part
         gen_data_decimal_size  : integer := 30;    -- Number of bits representing the decimal part
         gen_stages             : integer := 2;     -- Number of FIR + decimator stages
         gen_mem_addr_size      : integer := 15;    -- Bit length of the address signal
         gen_decimator_factor   : integer := 10);   -- Determines the rate at which the data is being decimated
port (
    i_clk               :   in std_logic;   -- System clock
    i_nrst              :   in std_logic;   -- System reset
    i_start             :   in std_logic;   -- Indicates when a new input data arrives
    i_data_in           :   in std_logic_vector(gen_data_integer_size+gen_data_decimal_size-1 downto 0);    -- Input data
	o_data_out          :   out std_logic_vector(gen_data_integer_size+gen_data_decimal_size-1 downto 0);   -- Output data
    o_finish_first_fir  :   out std_logic;  -- Asserts when the first stage FIR finish the process
    o_data_ready        :   out std_logic); -- Tied to '1' when the module is ready for new samples
end FIR_system_2;

architecture rtl of FIR_system_2 is

-- Input signals
signal clk		: std_logic;
signal nrst		: std_logic;
signal data_in  : std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal start    : std_logic;

-- Output signals
signal data_ready   : std_logic;

-- Auxilliary signals
signal enable_dec_q : std_logic;

type t_stages is array (0 to gen_stages-1) of std_logic;
signal data_fir_ready, fir_finish, enable_dec  : t_stages;

type t_data is array (0 to gen_stages-1) of std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal fir_output, decimator_output   : t_data;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
------------------------------- VHDL filter ------------------------------------
-- component FIR is
-- generic(gen_integer_size : integer := gen_data_integer_size;
--         gen_decimal_size : integer := gen_data_decimal_size);
--         --gen_sum_bits     : integer := --gen_sum_bits);
-- port(  
--     i_clk	        :	in	std_logic;
--     i_nrst	        :	in	std_logic;
--     i_start	        :	in	std_logic;
--     o_ready	        :	out	std_logic;
--     o_finish	        :	out	std_logic;
--     o_return_val	    :	out	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
--     i_input_var	    :	in	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0)
-- );
-- end component FIR;

----------------------------- SmartHLS filter -----------------------------------
component hw_fir_top
    port (
            clk	    :	in	std_logic;
            reset	:	in	std_logic;
            start	:	in	std_logic;
            ready	:	out	std_logic;
            finish	:	out	std_logic;
        return_val	:	out	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
        input_var	:	in	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0)
    );
    end component;

component decimator
generic (gen_data_length : integer := gen_data_integer_size + gen_data_decimal_size;
        gen_decimation_factor : integer := gen_decimator_factor);
port (
    i_clk               : in std_logic;
    i_nrst              : in std_logic;
    i_enable            : in std_logic; 
    i_d                 : in std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
    o_enable_decimator  : out std_logic;
	o_q                 : out std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0));
end component;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk                 <= i_clk;
nrst                <= i_nrst;
data_in             <= i_data_in;
start               <= i_start;
o_data_out          <= decimator_output(gen_stages-1);
o_data_ready        <= data_ready;
o_finish_first_fir  <= fir_finish(0);

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Falling edge of the enable_dec detection
p_falling_edge : process(clk, nrst)
begin
    if (nrst = '0') then
        enable_dec_q <= '0';
    elsif rising_edge(clk) then
        enable_dec_q <= enable_dec(gen_stages-1);
    end if;
end process;
data_ready <= not(enable_dec(gen_stages-1)) and enable_dec_q; -- Indicates that a new output data is ready

------------------------------- VHDL filter ------------------------------------
-- reg: for i in 0 to gen_stages - 1 generate
--     d0: if (i=0) generate
--         cmp0: FIR port map(
--             i_clk	        =>  clk,
--             i_nrst	        =>	not(nrst),
--             i_start	        =>	start,
--             o_ready	        =>	data_fir_ready(i),
--             o_finish	    =>	fir_finish(i),
--             o_return_val	=>	fir_output(i),
--             i_input_var	    =>   data_in);
--         else generate
--         cmpi: FIR port map(
--             i_clk	        =>  clk,
--             i_nrst	        =>	not(nrst),
--             i_start	        =>	enable_dec(i-1),
--             o_ready	        =>	data_fir_ready(i),
--             o_finish	    =>	fir_finish(i),
--             o_return_val	=>	fir_output(i),
--             i_input_var	    =>  decimator_output(i-1)
--         );
--     end generate;
-- end generate;

----------------------------- SmartHLS filter -----------------------------------
reg: for i in 0 to gen_stages - 1 generate
    d0: if (i=0) generate
        cmp0: hw_fir_top port map(
            clk	        =>  clk,
            reset	        =>	not(nrst),
            start	        =>	start,
            ready	        =>	data_fir_ready(i),
            finish	    =>	fir_finish(i),
            return_val	=>	fir_output(i),
            input_var	    =>   data_in);
        else generate
        cmpi: hw_fir_top port map(
            clk	        =>  clk,
            reset	        =>	not(nrst),
            start	        =>	enable_dec(i-1),
            ready	        =>	data_fir_ready(i),
            finish	    =>	fir_finish(i),
            return_val	=>	fir_output(i),
            input_var	    =>  decimator_output(i-1)
        );
    end generate;
end generate;

-- Decimator
reg_1: for i in 0 to gen_stages - 1 generate
    cmpi: decimator port map(
        i_clk               => clk,
        i_nrst              => nrst,
        i_enable            => fir_finish(i), 
        i_d                 => fir_output(i),
        o_enable_decimator  => enable_dec(i),
        o_q                 => decimator_output(i)
    );
end generate;

end rtl;
--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: FIR_system.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: Serial architecture of cross-correlation
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity FIR_system is
generic (gen_data_integer_size  : integer := 2;
         gen_data_decimal_size  : integer := 30;
         --gen_signal_length      : integer := 20000;
         --gen_sum_bits           : integer := 14,
         gen_stages             : integer := 2;
         gen_mem_addr_size      : integer := 15;
         gen_decimator_factor   : integer := 10);
port (
    i_clk           :   in std_logic;
    i_nrst          :   in std_logic;
    i_enable        :   in std_logic;
    --i_write_request :   in std_logic;
    i_start         :   in std_logic;
    i_data_in       :   in std_logic_vector(gen_data_integer_size+gen_data_decimal_size-1 downto 0);
    i_signal_length :   in std_logic_vector(gen_mem_addr_size-1 downto 0);   
	o_data_out      :   out std_logic_vector(gen_data_integer_size+gen_data_decimal_size-1 downto 0);
    o_data_ready    :   out std_logic);
end FIR_system;

architecture rtl of FIR_system is

-- Input signals
signal clk		: std_logic;
signal nrst		: std_logic;
signal enable   : std_logic;
signal write_request    : std_logic;
signal data_in  : std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal signal_length    : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal start    : std_logic;

-- Output signals
signal data_ready   : std_logic;

-- Auxilliary signals
signal RAM_data_out     : std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal RAM_data_in      : std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal read_request     : std_logic;
signal max_addr         : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal FIR_input        : std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal RAM_RE           : std_logic;
signal RAM_WE           : std_logic;
signal FIR_input_ready  : std_logic;
signal RAM_r_address    : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal RAM_w_address    : std_logic_vector(gen_mem_addr_size-1 downto 0);
signal mem_full         : std_logic;
signal enable_dec_q     : std_logic;

type t_stages is array (0 to gen_stages-1) of std_logic;
signal data_fir_ready, fir_finish, enable_dec  : t_stages;

type t_data is array (0 to gen_stages-1) of std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
signal fir_output, decimator_output   : t_data;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component FIR is
generic(gen_integer_size : integer := gen_data_integer_size;
        gen_decimal_size : integer := gen_data_decimal_size);
        --gen_sum_bits     : integer := --gen_sum_bits);
port(  
    i_clk	        :	in	std_logic;
    i_nrst	        :	in	std_logic;
    i_start	        :	in	std_logic;
    o_ready	        :	out	std_logic;
    o_finish	    :	out	std_logic;
    o_return_val	:	out	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
    i_input_var	    :	in	std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0)
);
end component FIR;

component decimator
generic (gen_data_length : integer := gen_data_integer_size + gen_data_decimal_size;
        gen_decimation_factor : integer := gen_decimator_factor);
port (
    i_clk               : in std_logic;
    i_nrst              : in std_logic;
    i_enable            : in std_logic; 
    i_d                 : in std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0);
    o_enable_decimator  : out std_logic;
	o_q                 : out std_logic_vector(gen_data_integer_size + gen_data_decimal_size-1 downto 0));
end component;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk             <= i_clk;
nrst            <= i_nrst;
enable          <= i_enable;
data_in         <= i_data_in;
signal_length   <= i_signal_length;
start           <= i_start;
o_data_out      <= decimator_output(gen_stages-1);
o_data_ready    <= data_ready;

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
p_fallin_edge : process(clk, nrst)
begin
    if (nrst = '0') then
        enable_dec_q <= '0';
    elsif rising_edge(clk) then
        enable_dec_q <= enable_dec(gen_stages-1);
    end if;
end process;
data_ready <= not(enable_dec(gen_stages-1)) and enable_dec_q;


reg: for i in 0 to gen_stages - 1 generate
    d0: if (i=0) generate
        cmp0: FIR port map(
            i_clk	        =>  clk,
            i_nrst	        =>	not(nrst),
            i_start	        =>	FIR_input_ready,
            o_ready	        =>	data_fir_ready(i),
            o_finish	    =>	fir_finish(i),
            o_return_val	=>	fir_output(i),
            i_input_var	    =>   FIR_input);
        else generate
        cmpi: FIR port map(
            i_clk	        =>  clk,
            i_nrst	        =>	not(nrst),
            i_start	        =>	enable_dec(i-1),
            o_ready	        =>	data_fir_ready(i),
            o_finish	    =>	fir_finish(i),
            o_return_val	=>	fir_output(i),
            i_input_var	    =>  decimator_output(i-1)
        );
    end generate;
end generate;

reg_1: for i in 0 to gen_stages - 1 generate
    cmpi: decimator port map(
        i_clk               => clk,
        i_nrst              => nrst,
        i_enable            => fir_finish(i), 
        i_d                 => fir_output(i),
        o_enable_decimator  => enable_dec(i),
        o_q                 => decimator_output(i)
    );
end generate;

    -- FIR_0 : entity work.FIR
    -- generic map(gen_integer_size => gen_data_integer_size,
    --             gen_decimal_size => gen_data_decimal_size)
    -- port map(
    --         i_clk	        =>  clk,
    --         i_nrst	        =>	not(nrst),
    --         i_start	        =>	FIR_input_ready,
    --         o_ready	        =>	data_fir_ready(0),
    --         o_finish	    =>	fir_finish(0),
    --         o_return_val	=>	fir_output(0),
    --         i_input_var	    =>   FIR_input
    -- );

    -- decimator_0 : entity work.decimator 
    -- generic map(gen_data_length => gen_data_integer_size + gen_data_decimal_size,
    --             gen_decimation_factor => 10)
    -- port map(
    --     i_clk               => clk,
    --     i_nrst              => nrst,
    --     i_enable            => fir_finish(0), 
    --     i_d                 => fir_output(0),
    --     o_enable_decimator  => enable_dec(0),
    --     o_q                 => decimator_output(0)
    -- );


    -- fir_fsm_0 : entity work.fir_fsm
    -- generic map(gen_mem_addr_size => gen_mem_addr_size)
    -- port map(
    --         i_clk           =>  clk,
    --         i_nrst	        =>  nrst,
    --         i_start         =>  start,
    --         i_signal_length =>  signal_length,
    --         i_fir_ready     =>  fir_finish(0),
    --         o_max_addr      =>  max_addr,
    --         o_mem_w_req     =>  write_request,
    --         o_mem_req       =>  read_request
    -- );

    -- RAM_0 : entity work.RAM
    -- generic map(
    --     gen_mem_addr_size => gen_mem_addr_size,
    --     gen_mem_data_size => gen_data_integer_size + gen_data_decimal_size)
    -- port map(
    --     i_clk       =>  clk,
    --     i_w_address =>  RAM_w_address,
    --     i_r_address =>  RAM_r_address,
    --     i_data_in   =>  RAM_data_in,
    --     i_WE        =>  RAM_WE,
    --     i_RE        =>  RAM_RE,
    --     o_data_out  =>  RAM_data_out,
    --     o_mem_full  =>  mem_full
    -- );

    -- interface_in_0 : entity work.interface_in
    -- generic map(
    --     gen_mem_addr_size => gen_mem_addr_size,
    --     gen_mem_data_size => gen_data_integer_size + gen_data_decimal_size)    
    -- port map(
    --     i_clk       => clk,
    --     i_nrst      => nrst,
    --     i_w_req     => write_request,
    --     i_d_in      => data_in,
    --     o_d_out     => RAM_data_in,
    --     o_WE        => RAM_WE,
    --     o_w_address => RAM_w_address
    -- );

    -- interface_out_0 : entity work.interface_out_synth
    -- generic map(
    --     gen_mem_addr_size => gen_mem_addr_size,
    --     gen_mem_data_size => gen_data_integer_size + gen_data_decimal_size)    
    -- port map(
    --     i_clk           => clk,
    --     i_nrst          => nrst,
    --     i_d_in          => RAM_data_out,
    --     i_r_req         => read_request,
    --     i_r_addr_max    => max_addr,
    --     o_d_out         => FIR_input,
    --     o_RE            => RAM_RE,
    --     o_ready         => FIR_input_ready,
    --     o_r_address     => RAM_r_address
    -- );

end rtl;
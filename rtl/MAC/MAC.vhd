--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: MAC.vhd
-- File history:
--      0.0     : 03/02/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: Serial architecture of cross-correlation
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity MAC is
generic(gen_data_size : integer := 8;
        gen_output_size : integer := 13);
port(clk,clrn		: in std_logic;
    enable			: in std_logic;
    data1, data2	: in signed(gen_data_size-1 downto 0);
    r 				: out signed(gen_output_size- 1 downto 0)
);
end MAC;

architecture rtl of MAC is

    signal r_int, r_int_reg    : integer;

begin
    p_seq : process(clk, clrn)
    begin
        if (clrn = '0') then
            r_int_reg   <= 0;
        elsif rising_edge(clk) then
            r_int_reg   <= r_int;
        end if;
    end process;

    p_MAC : process (enable, r_int_reg, data1, data2)
    begin
        r_int   <= r_int_reg;
        if (enable  = '1') then
            r_int <= r_int_reg + to_integer(data1) * to_integer(data2);
        end if;
    end process;
    r   <= to_signed(r_int, r'length);

end rtl;
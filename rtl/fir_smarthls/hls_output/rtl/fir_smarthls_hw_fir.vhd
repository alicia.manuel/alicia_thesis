-- ----------------------------------------------------------------------------
-- Smart High-Level Synthesis Tool Version 2022.2.1
-- Copyright (c) 2015-2022 Microchip Technology Inc. All Rights Reserved.
-- For support, please visit https://microchiptech.github.io/fpga-hls-docs/techsupport.html.
-- Date: Wed May 31 11:43:09 2023
-- ----------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


entity hw_fir_top_vhdl is
port (
	       clk	:	in	std_logic;
	     reset	:	in	std_logic;
	     start	:	in	std_logic;
	     ready	:	out	std_logic;
	    finish	:	out	std_logic;
	return_val	:	out	std_logic_vector(31 downto 0);
	 input_var	:	in	std_logic_vector(31 downto 0)
);

-- Put your code here ... 

end hw_fir_top_vhdl;

architecture behavior of hw_fir_top_vhdl is

component hw_fir_top
port (
	       clk	:	in	std_logic;
	     reset	:	in	std_logic;
	     start	:	in	std_logic;
	     ready	:	out	std_logic;
	    finish	:	out	std_logic;
	return_val	:	out	std_logic_vector(31 downto 0);
	 input_var	:	in	std_logic_vector(31 downto 0)
);
end component;

begin


hw_fir_top_inst : hw_fir_top
port map (
	       clk	=>	clk,
	     reset	=>	reset,
	     start	=>	start,
	     ready	=>	ready,
	    finish	=>	finish,
	return_val	=>	return_val,
	 input_var	=>	input_var
);


end behavior;


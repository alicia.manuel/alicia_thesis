// ----------------------------------------------------------------------------
// Smart High-Level Synthesis Tool Version 2022.2.1
// Copyright (c) 2015-2022 Microchip Technology Inc. All Rights Reserved.
// For support, please visit https://microchiptech.github.io/fpga-hls-docs/techsupport.html.
// Date: Wed May 31 11:43:09 2023
// ----------------------------------------------------------------------------
`define MEMORY_CONTROLLER_ADDR_SIZE 32
//
// NOTE:// If you take this code outside the SmartHLS directory structure
// into your own, then you should adjust this constant accordingly.
// E.g. for simulation on Modelsim:
//		vlog +define+MEM_INIT_DIR=/path/to/rtl/mem_init/ fir_smarthls.v  ...
//
`ifndef MEM_INIT_DIR
`define MEM_INIT_DIR "../hdl/"
`endif


`timescale 1 ns / 1 ns
module hw_fir_top
(
	clk,
	reset,
	start,
	ready,
	finish,
	return_val,
	input_var
);

input  clk;
input  reset;
input  start;
output reg  ready;
output reg  finish;
output reg [31:0] return_val;
input [31:0] input_var;
reg  hw_fir_inst_clk;
reg  hw_fir_inst_reset;
reg  hw_fir_inst_start;
wire  hw_fir_inst_ready;
wire  hw_fir_inst_finish;
wire [31:0] hw_fir_inst_return_val;
reg [31:0] hw_fir_inst_input_var;
reg  hw_fir_inst_finish_reg;
reg [31:0] hw_fir_inst_return_val_reg;


hw_fir_hw_fir hw_fir_inst (
	.clk (hw_fir_inst_clk),
	.reset (hw_fir_inst_reset),
	.start (hw_fir_inst_start),
	.ready (hw_fir_inst_ready),
	.finish (hw_fir_inst_finish),
	.return_val (hw_fir_inst_return_val),
	.input_var (hw_fir_inst_input_var)
);



always @(*) begin
	hw_fir_inst_clk = clk;
end
always @(*) begin
	hw_fir_inst_reset = reset;
end
always @(*) begin
	hw_fir_inst_start = start;
end
always @(*) begin
	hw_fir_inst_input_var = input_var;
end
always @(posedge clk) begin
	if ((reset | hw_fir_inst_start)) begin
		hw_fir_inst_finish_reg <= 1'd0;
	end
	if (hw_fir_inst_finish) begin
		hw_fir_inst_finish_reg <= 1'd1;
	end
end
always @(posedge clk) begin
	if ((reset | hw_fir_inst_start)) begin
		hw_fir_inst_return_val_reg <= 0;
	end
	if (hw_fir_inst_finish) begin
		hw_fir_inst_return_val_reg <= hw_fir_inst_return_val;
	end
end
always @(*) begin
	ready = hw_fir_inst_ready;
end
always @(*) begin
	finish = hw_fir_inst_finish;
end
always @(*) begin
	return_val = hw_fir_inst_return_val;
end

endmodule

`timescale 1 ns / 1 ns
module hw_fir_hw_fir
(
	clk,
	reset,
	start,
	ready,
	finish,
	return_val,
	input_var
);

input  clk;
input  reset;
input  start;
output reg  ready;
output reg  finish;
output reg [31:0] return_val;
input [31:0] input_var;
reg [31:0] input_var_reg;
reg [31:0] hw_fir_init_check_0;
reg [31:0] hw_fir_init_check_1;
reg [31:0] hw_fir_init_check_2;
reg [31:0] hw_fir_init_check_3;
reg [31:0] hw_fir_init_check_4;
reg [31:0] hw_fir_init_check_5;
reg [31:0] hw_fir_init_check_6;
reg [31:0] hw_fir_init_check_7;
reg [31:0] hw_fir_init_check_8;
reg [31:0] hw_fir_init_check_9;
reg [31:0] hw_fir_init_check_10;
reg [31:0] hw_fir_init_check_11;
reg [31:0] hw_fir_init_check_12;
reg [31:0] hw_fir_init_check_13;
reg [31:0] hw_fir_init_check_14;
reg [31:0] hw_fir_init_check_15;
reg [31:0] hw_fir_init_check_16;
reg [31:0] hw_fir_init_check_17;
reg [31:0] hw_fir_init_check_18;
reg [31:0] hw_fir_init_check_19;
reg [31:0] hw_fir_init_check_20;
reg [31:0] hw_fir_init_check_21;
reg [31:0] hw_fir_init_check_22;
reg [31:0] hw_fir_init_check_23;
reg [31:0] hw_fir_init_check_24;
reg [31:0] hw_fir_init_check_25;
reg [31:0] hw_fir_init_check_26;
reg [31:0] hw_fir_init_check_27;
reg [31:0] hw_fir_init_check_28;
reg [31:0] hw_fir_init_check_29;
reg [31:0] hw_fir_init_check_30;
reg [31:0] hw_fir_init_check_31;
reg [31:0] hw_fir_init_check_32;
reg [31:0] hw_fir_init_check_33;
reg [31:0] hw_fir_init_check_34;
reg [31:0] hw_fir_init_check_35;
reg [31:0] hw_fir_init_check_36;
reg [31:0] hw_fir_init_check_37;
reg [31:0] hw_fir_init_check_38;
reg [31:0] hw_fir_init_check_39;
reg [31:0] hw_fir_init_check_40;
reg [31:0] hw_fir_init_check_41;
reg [31:0] hw_fir_init_check_42;
reg [31:0] hw_fir_init_check_43;
reg [31:0] hw_fir_init_check_44;
reg [31:0] hw_fir_init_check_45;
reg [31:0] hw_fir_init_check_46;
reg [31:0] hw_fir_init_check_47;
reg [50:0] hw_fir_init_check_48;
reg [20:0] hw_fir_init_check_bit_select47;
reg [61:0] hw_fir_init_check_49;
reg [52:0] hw_fir_init_check_50;
reg [22:0] hw_fir_init_check_bit_select46;
reg [23:0] hw_fir_init_check_51;
reg [31:0] hw_fir_init_check_52;
reg [53:0] hw_fir_init_check_53;
reg [23:0] hw_fir_init_check_bit_select45;
reg [61:0] hw_fir_init_check_54;
reg [54:0] hw_fir_init_check_55;
reg [24:0] hw_fir_init_check_bit_select44;
reg [25:0] hw_fir_init_check_newEarly_65;
reg [59:0] hw_fir_init_check_56;
reg [55:0] hw_fir_init_check_57;
reg [25:0] hw_fir_init_check_bit_select43;
reg [31:0] hw_fir_init_check_58;
reg [55:0] hw_fir_init_check_59;
reg [25:0] hw_fir_init_check_bit_select42;
reg [60:0] hw_fir_init_check_60;
reg [56:0] hw_fir_init_check_61;
reg [26:0] hw_fir_init_check_bit_select41;
reg [27:0] hw_fir_init_check_newEarly_newEarly_80;
reg [31:0] hw_fir_init_check_62;
reg [56:0] hw_fir_init_check_63;
reg [26:0] hw_fir_init_check_bit_select40;
reg [27:0] hw_fir_init_check_newEarly_newEarly_85;
reg [61:0] hw_fir_init_check_64;
reg [56:0] hw_fir_init_check_65;
reg [26:0] hw_fir_init_check_bit_select39;
reg [31:0] hw_fir_init_check_66;
reg [57:0] hw_fir_init_check_67;
reg [27:0] hw_fir_init_check_bit_select38;
reg [58:0] hw_fir_init_check_68;
reg [57:0] hw_fir_init_check_69;
reg [27:0] hw_fir_init_check_bit_select37;
reg [61:0] hw_fir_init_check_70;
reg [56:0] hw_fir_init_check_71;
reg [26:0] hw_fir_init_check_bit_select36;
reg [31:0] hw_fir_init_check_72;
reg [55:0] hw_fir_init_check_73;
reg [25:0] hw_fir_init_check_bit_select35;
reg [27:0] hw_fir_init_check_newEarly_newEarly_newEarly_110;
reg [31:0] hw_fir_init_check_74;
reg [53:0] hw_fir_init_check_75;
reg [23:0] hw_fir_init_check_bit_select34;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_115;
reg [31:0] hw_fir_init_check_76;
reg [56:0] hw_fir_init_check_77;
reg [26:0] hw_fir_init_check_bit_select33;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_120;
reg [31:0] hw_fir_init_check_78;
reg [57:0] hw_fir_init_check_79;
reg [27:0] hw_fir_init_check_bit_select32;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_125;
reg [31:0] hw_fir_init_check_80;
reg [57:0] hw_fir_init_check_81;
reg [27:0] hw_fir_init_check_bit_select31;
reg [62:0] hw_fir_init_check_82;
reg [58:0] hw_fir_init_check_83;
reg [28:0] hw_fir_init_check_bit_select30;
reg [31:0] hw_fir_init_check_84;
reg [58:0] hw_fir_init_check_85;
reg [28:0] hw_fir_init_check_bit_select29;
reg [55:0] hw_fir_init_check_86;
reg [59:0] hw_fir_init_check_87;
reg [29:0] hw_fir_init_check_bit_select28;
reg [31:0] hw_fir_init_check_88;
reg [59:0] hw_fir_init_check_89;
reg [29:0] hw_fir_init_check_bit_select27;
reg [31:0] hw_fir_init_check_90;
reg [59:0] hw_fir_init_check_91;
reg [29:0] hw_fir_init_check_bit_select26;
reg [31:0] hw_fir_init_check_92;
reg [59:0] hw_fir_init_check_93;
reg [29:0] hw_fir_init_check_bit_select25;
reg [61:0] hw_fir_init_check_94;
reg [59:0] hw_fir_init_check_95;
reg [29:0] hw_fir_init_check_bit_select24;
reg [61:0] hw_fir_init_check_96;
reg [59:0] hw_fir_init_check_97;
reg [29:0] hw_fir_init_check_bit_select23;
reg [30:0] hw_fir_init_check_newEarly_newEarly_220;
reg [31:0] hw_fir_init_check_98;
reg [59:0] hw_fir_init_check_99;
reg [29:0] hw_fir_init_check_bit_select22;
reg [30:0] hw_fir_init_check_newEarly_newEarly_275;
reg [31:0] hw_fir_init_check_100;
reg [59:0] hw_fir_init_check_101;
reg [29:0] hw_fir_init_check_bit_select21;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa;
reg [31:0] hw_fir_init_check_102;
reg [59:0] hw_fir_init_check_103;
reg [29:0] hw_fir_init_check_bit_select20;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_0;
reg [55:0] hw_fir_init_check_104;
reg [59:0] hw_fir_init_check_105;
reg [29:0] hw_fir_init_check_bit_select19;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_1;
reg [31:0] hw_fir_init_check_106;
reg [58:0] hw_fir_init_check_107;
reg [28:0] hw_fir_init_check_bit_select18;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_2;
reg [62:0] hw_fir_init_check_108;
reg [58:0] hw_fir_init_check_109;
reg [28:0] hw_fir_init_check_bit_select17;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_3;
reg [31:0] hw_fir_init_check_110;
reg [57:0] hw_fir_init_check_111;
reg [27:0] hw_fir_init_check_bit_select16;
reg [30:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_4;
reg [31:0] hw_fir_init_check_112;
reg [57:0] hw_fir_init_check_113;
reg [27:0] hw_fir_init_check_bit_select15;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_5;
reg [31:0] hw_fir_init_check_114;
reg [56:0] hw_fir_init_check_115;
reg [26:0] hw_fir_init_check_bit_select14;
reg [27:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_6;
reg [31:0] hw_fir_init_check_116;
reg [53:0] hw_fir_init_check_117;
reg [23:0] hw_fir_init_check_bit_select13;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_7;
reg [31:0] hw_fir_init_check_118;
reg [55:0] hw_fir_init_check_119;
reg [25:0] hw_fir_init_check_bit_select12;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8;
reg [61:0] hw_fir_init_check_120;
reg [56:0] hw_fir_init_check_121;
reg [26:0] hw_fir_init_check_bit_select11;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_9;
reg [58:0] hw_fir_init_check_122;
reg [57:0] hw_fir_init_check_123;
reg [27:0] hw_fir_init_check_bit_select10;
reg [29:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_10;
reg [31:0] hw_fir_init_check_124;
reg [57:0] hw_fir_init_check_125;
reg [27:0] hw_fir_init_check_bit_select9;
reg [29:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_11;
reg [61:0] hw_fir_init_check_126;
reg [56:0] hw_fir_init_check_127;
reg [26:0] hw_fir_init_check_bit_select8;
reg [29:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_12;
reg [31:0] hw_fir_init_check_128;
reg [56:0] hw_fir_init_check_129;
reg [26:0] hw_fir_init_check_bit_select7;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_13;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa;
reg [60:0] hw_fir_init_check_130;
reg [56:0] hw_fir_init_check_131;
reg [26:0] hw_fir_init_check_bit_select6;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_14;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_15;
reg [31:0] hw_fir_init_check_132;
reg [55:0] hw_fir_init_check_133;
reg [25:0] hw_fir_init_check_bit_select5;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_17;
reg [59:0] hw_fir_init_check_134;
reg [55:0] hw_fir_init_check_135;
reg [25:0] hw_fir_init_check_bit_select4;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_18;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_19;
reg [61:0] hw_fir_init_check_136;
reg [54:0] hw_fir_init_check_137;
reg [24:0] hw_fir_init_check_bit_select3;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_20;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_21;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_270;
reg [31:0] hw_fir_init_check_138;
reg [53:0] hw_fir_init_check_139;
reg [23:0] hw_fir_init_check_bit_select2;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_23;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_275;
reg [61:0] hw_fir_init_check_140;
reg [52:0] hw_fir_init_check_141;
reg [22:0] hw_fir_init_check_bit_select1;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_24;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_25;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_280;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_280;
reg [31:0] hw_fir_init_check_142;
reg [50:0] hw_fir_init_check_143;
reg [20:0] hw_fir_init_check_bit_select;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_26;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_27;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_newEarly_285;
reg [31:0] hw_fir_init_check_newCurOp_newEarly_285;
reg [31:0] hw_fir_init_check_newCurOp_285;
reg [31:0] hw_fir_previous_a0_inferred_reg;
reg [31:0] hw_fir_previous_a1_inferred_reg;
reg [31:0] hw_fir_previous_a2_inferred_reg;
reg [31:0] hw_fir_previous_a3_inferred_reg;
reg [31:0] hw_fir_previous_a4_inferred_reg;
reg [31:0] hw_fir_previous_a5_inferred_reg;
reg [31:0] hw_fir_previous_a6_inferred_reg;
reg [31:0] hw_fir_previous_a7_inferred_reg;
reg [31:0] hw_fir_previous_a8_inferred_reg;
reg [31:0] hw_fir_previous_a9_inferred_reg;
reg [31:0] hw_fir_previous_a10_inferred_reg;
reg [31:0] hw_fir_previous_a11_inferred_reg;
reg [31:0] hw_fir_previous_a12_inferred_reg;
reg [31:0] hw_fir_previous_a13_inferred_reg;
reg [31:0] hw_fir_previous_a14_inferred_reg;
reg [31:0] hw_fir_previous_a15_inferred_reg;
reg [31:0] hw_fir_previous_a16_inferred_reg;
reg [31:0] hw_fir_previous_a17_inferred_reg;
reg [31:0] hw_fir_previous_a18_inferred_reg;
reg [31:0] hw_fir_previous_a19_inferred_reg;
reg [31:0] hw_fir_previous_a20_inferred_reg;
reg [31:0] hw_fir_previous_a21_inferred_reg;
reg [31:0] hw_fir_previous_a22_inferred_reg;
reg [31:0] hw_fir_previous_a23_inferred_reg;
reg [31:0] hw_fir_previous_a24_inferred_reg;
reg [31:0] hw_fir_previous_a25_inferred_reg;
reg [31:0] hw_fir_previous_a26_inferred_reg;
reg [31:0] hw_fir_previous_a27_inferred_reg;
reg [31:0] hw_fir_previous_a28_inferred_reg;
reg [31:0] hw_fir_previous_a29_inferred_reg;
reg [31:0] hw_fir_previous_a30_inferred_reg;
reg [31:0] hw_fir_previous_a31_inferred_reg;
reg [31:0] hw_fir_previous_a32_inferred_reg;
reg [31:0] hw_fir_previous_a33_inferred_reg;
reg [31:0] hw_fir_previous_a34_inferred_reg;
reg [31:0] hw_fir_previous_a35_inferred_reg;
reg [31:0] hw_fir_previous_a36_inferred_reg;
reg [31:0] hw_fir_previous_a37_inferred_reg;
reg [31:0] hw_fir_previous_a38_inferred_reg;
reg [31:0] hw_fir_previous_a39_inferred_reg;
reg [31:0] hw_fir_previous_a40_inferred_reg;
reg [31:0] hw_fir_previous_a41_inferred_reg;
reg [31:0] hw_fir_previous_a42_inferred_reg;
reg [31:0] hw_fir_previous_a43_inferred_reg;
reg [31:0] hw_fir_previous_a44_inferred_reg;
reg [31:0] hw_fir_previous_a45_inferred_reg;
reg [31:0] hw_fir_previous_a46_inferred_reg;
reg  hw_fir_valid_bit_0;
wire  hw_fir_state_stall_0;
reg  hw_fir_state_enable_0;
reg  hw_fir_valid_bit_1;
wire  hw_fir_state_stall_1;
reg  hw_fir_state_enable_1;
reg  hw_fir_valid_bit_2;
wire  hw_fir_state_stall_2;
reg  hw_fir_state_enable_2;
reg  hw_fir_valid_bit_3;
wire  hw_fir_state_stall_3;
reg  hw_fir_state_enable_3;
reg  hw_fir_valid_bit_4;
wire  hw_fir_state_stall_4;
reg  hw_fir_state_enable_4;
reg  hw_fir_II_counter;
reg [20:0] hw_fir_init_check_bit_select47_reg_stage4;
reg [23:0] hw_fir_init_check_bit_select45_reg_stage4;
reg [25:0] hw_fir_init_check_bit_select35_reg_stage4;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_115_reg_stage4;
reg [26:0] hw_fir_init_check_bit_select33_reg_stage4;
reg [27:0] hw_fir_init_check_bit_select32_reg_stage4;
reg [27:0] hw_fir_init_check_bit_select31_reg_stage4;
reg [29:0] hw_fir_init_check_bit_select27_reg_stage4;
reg [29:0] hw_fir_init_check_bit_select25_reg_stage4;
reg [29:0] hw_fir_init_check_bit_select22_reg_stage4;
reg [29:0] hw_fir_init_check_bit_select20_reg_stage4;
reg [27:0] hw_fir_init_check_bit_select16_reg_stage4;
reg [27:0] hw_fir_init_check_bit_select15_reg_stage4;
reg [26:0] hw_fir_init_check_bit_select14_reg_stage4;
reg [23:0] hw_fir_init_check_bit_select13_reg_stage4;
reg [28:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8_reg_stage4;
reg [27:0] hw_fir_init_check_bit_select9_reg_stage4;
reg [26:0] hw_fir_init_check_bit_select7_reg_stage4;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16_reg_stage4;
reg [31:0] hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22_reg_stage4;
reg [20:0] hw_fir_init_check_bit_select_reg_stage4;
reg  legup_mult_signed_32_19_3_0_clock;
reg  legup_mult_signed_32_19_3_0_aclr;
reg  legup_mult_signed_32_19_3_0_clken;
reg [31:0] legup_mult_signed_32_19_3_0_dataa;
wire [18:0] legup_mult_signed_32_19_3_0_datab;
wire [50:0] legup_mult_signed_32_19_3_0_result;
reg [50:0] legup_mult_hw_fir_init_check_48_out_actual;
reg [50:0] legup_mult_hw_fir_init_check_48_out;
reg  legup_mult_hw_fir_init_check_48_en;
reg  legup_mult_hw_fir_init_check_48_en_pipeline_cond;
reg [61:0] hw_fir_init_check_48_width_extended;
reg  legup_mult_signed_63_21_4_1_clock;
reg  legup_mult_signed_63_21_4_1_aclr;
reg  legup_mult_signed_63_21_4_1_clken;
reg [62:0] legup_mult_signed_63_21_4_1_dataa;
wire [20:0] legup_mult_signed_63_21_4_1_datab;
wire [83:0] legup_mult_signed_63_21_4_1_result;
reg [83:0] legup_mult_hw_fir_init_check_50_out_actual;
reg [52:0] legup_mult_hw_fir_init_check_50_out;
reg  legup_mult_hw_fir_init_check_50_en;
reg  legup_mult_hw_fir_init_check_50_en_pipeline_cond;
reg [61:0] hw_fir_init_check_50_width_extended;
reg  legup_mult_signed_32_22_3_2_clock;
reg  legup_mult_signed_32_22_3_2_aclr;
reg  legup_mult_signed_32_22_3_2_clken;
reg [31:0] legup_mult_signed_32_22_3_2_dataa;
wire [21:0] legup_mult_signed_32_22_3_2_datab;
wire [53:0] legup_mult_signed_32_22_3_2_result;
reg [53:0] legup_mult_hw_fir_init_check_53_out_actual;
reg [53:0] legup_mult_hw_fir_init_check_53_out;
reg  legup_mult_hw_fir_init_check_53_en;
reg  legup_mult_hw_fir_init_check_53_en_pipeline_cond;
reg [61:0] hw_fir_init_check_53_width_extended;
reg  legup_mult_signed_63_23_4_3_clock;
reg  legup_mult_signed_63_23_4_3_aclr;
reg  legup_mult_signed_63_23_4_3_clken;
reg [62:0] legup_mult_signed_63_23_4_3_dataa;
wire [22:0] legup_mult_signed_63_23_4_3_datab;
wire [85:0] legup_mult_signed_63_23_4_3_result;
reg [85:0] legup_mult_hw_fir_init_check_55_out_actual;
reg [54:0] legup_mult_hw_fir_init_check_55_out;
reg  legup_mult_hw_fir_init_check_55_en;
reg  legup_mult_hw_fir_init_check_55_en_pipeline_cond;
reg [61:0] hw_fir_init_check_55_width_extended;
reg  legup_mult_signed_61_24_4_4_clock;
reg  legup_mult_signed_61_24_4_4_aclr;
reg  legup_mult_signed_61_24_4_4_clken;
reg [60:0] legup_mult_signed_61_24_4_4_dataa;
wire [23:0] legup_mult_signed_61_24_4_4_datab;
wire [84:0] legup_mult_signed_61_24_4_4_result;
reg [84:0] legup_mult_hw_fir_init_check_57_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_57_out;
reg  legup_mult_hw_fir_init_check_57_en;
reg  legup_mult_hw_fir_init_check_57_en_pipeline_cond;
reg [61:0] hw_fir_init_check_57_width_extended;
reg  legup_mult_signed_32_24_3_5_clock;
reg  legup_mult_signed_32_24_3_5_aclr;
reg  legup_mult_signed_32_24_3_5_clken;
reg [31:0] legup_mult_signed_32_24_3_5_dataa;
wire [23:0] legup_mult_signed_32_24_3_5_datab;
wire [55:0] legup_mult_signed_32_24_3_5_result;
reg [55:0] legup_mult_hw_fir_init_check_59_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_59_out;
reg  legup_mult_hw_fir_init_check_59_en;
reg  legup_mult_hw_fir_init_check_59_en_pipeline_cond;
reg [61:0] hw_fir_init_check_59_width_extended;
reg  legup_mult_signed_62_25_4_6_clock;
reg  legup_mult_signed_62_25_4_6_aclr;
reg  legup_mult_signed_62_25_4_6_clken;
reg [61:0] legup_mult_signed_62_25_4_6_dataa;
wire [24:0] legup_mult_signed_62_25_4_6_datab;
wire [86:0] legup_mult_signed_62_25_4_6_result;
reg [86:0] legup_mult_hw_fir_init_check_61_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_61_out;
reg  legup_mult_hw_fir_init_check_61_en;
reg  legup_mult_hw_fir_init_check_61_en_pipeline_cond;
reg [61:0] hw_fir_init_check_61_width_extended;
reg  legup_mult_signed_32_25_3_7_clock;
reg  legup_mult_signed_32_25_3_7_aclr;
reg  legup_mult_signed_32_25_3_7_clken;
reg [31:0] legup_mult_signed_32_25_3_7_dataa;
wire [24:0] legup_mult_signed_32_25_3_7_datab;
wire [56:0] legup_mult_signed_32_25_3_7_result;
reg [56:0] legup_mult_hw_fir_init_check_63_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_63_out;
reg  legup_mult_hw_fir_init_check_63_en;
reg  legup_mult_hw_fir_init_check_63_en_pipeline_cond;
reg [61:0] hw_fir_init_check_63_width_extended;
reg  legup_mult_signed_63_25_4_8_clock;
reg  legup_mult_signed_63_25_4_8_aclr;
reg  legup_mult_signed_63_25_4_8_clken;
reg [62:0] legup_mult_signed_63_25_4_8_dataa;
wire [24:0] legup_mult_signed_63_25_4_8_datab;
wire [87:0] legup_mult_signed_63_25_4_8_result;
reg [87:0] legup_mult_hw_fir_init_check_65_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_65_out;
reg  legup_mult_hw_fir_init_check_65_en;
reg  legup_mult_hw_fir_init_check_65_en_pipeline_cond;
reg [61:0] hw_fir_init_check_65_width_extended;
reg  legup_mult_signed_32_26_3_9_clock;
reg  legup_mult_signed_32_26_3_9_aclr;
reg  legup_mult_signed_32_26_3_9_clken;
reg [31:0] legup_mult_signed_32_26_3_9_dataa;
wire [25:0] legup_mult_signed_32_26_3_9_datab;
wire [57:0] legup_mult_signed_32_26_3_9_result;
reg [57:0] legup_mult_hw_fir_init_check_67_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_67_out;
reg  legup_mult_hw_fir_init_check_67_en;
reg  legup_mult_hw_fir_init_check_67_en_pipeline_cond;
reg [61:0] hw_fir_init_check_67_width_extended;
reg  legup_mult_signed_60_26_4_10_clock;
reg  legup_mult_signed_60_26_4_10_aclr;
reg  legup_mult_signed_60_26_4_10_clken;
reg [59:0] legup_mult_signed_60_26_4_10_dataa;
wire [25:0] legup_mult_signed_60_26_4_10_datab;
wire [85:0] legup_mult_signed_60_26_4_10_result;
reg [85:0] legup_mult_hw_fir_init_check_69_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_69_out;
reg  legup_mult_hw_fir_init_check_69_en;
reg  legup_mult_hw_fir_init_check_69_en_pipeline_cond;
reg [61:0] hw_fir_init_check_69_width_extended;
reg  legup_mult_signed_63_25_4_11_clock;
reg  legup_mult_signed_63_25_4_11_aclr;
reg  legup_mult_signed_63_25_4_11_clken;
reg [62:0] legup_mult_signed_63_25_4_11_dataa;
wire [24:0] legup_mult_signed_63_25_4_11_datab;
wire [87:0] legup_mult_signed_63_25_4_11_result;
reg [87:0] legup_mult_hw_fir_init_check_71_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_71_out;
reg  legup_mult_hw_fir_init_check_71_en;
reg  legup_mult_hw_fir_init_check_71_en_pipeline_cond;
reg [61:0] hw_fir_init_check_71_width_extended;
reg  legup_mult_signed_32_24_3_12_clock;
reg  legup_mult_signed_32_24_3_12_aclr;
reg  legup_mult_signed_32_24_3_12_clken;
reg [31:0] legup_mult_signed_32_24_3_12_dataa;
wire [23:0] legup_mult_signed_32_24_3_12_datab;
wire [55:0] legup_mult_signed_32_24_3_12_result;
reg [55:0] legup_mult_hw_fir_init_check_73_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_73_out;
reg  legup_mult_hw_fir_init_check_73_en;
reg  legup_mult_hw_fir_init_check_73_en_pipeline_cond;
reg [61:0] hw_fir_init_check_73_width_extended;
reg  legup_mult_signed_32_23_3_13_clock;
reg  legup_mult_signed_32_23_3_13_aclr;
reg  legup_mult_signed_32_23_3_13_clken;
reg [31:0] legup_mult_signed_32_23_3_13_dataa;
wire [22:0] legup_mult_signed_32_23_3_13_datab;
wire [54:0] legup_mult_signed_32_23_3_13_result;
reg [54:0] legup_mult_hw_fir_init_check_75_out_actual;
reg [53:0] legup_mult_hw_fir_init_check_75_out;
reg  legup_mult_hw_fir_init_check_75_en;
reg  legup_mult_hw_fir_init_check_75_en_pipeline_cond;
reg [61:0] hw_fir_init_check_75_width_extended;
reg  legup_mult_signed_32_26_3_14_clock;
reg  legup_mult_signed_32_26_3_14_aclr;
reg  legup_mult_signed_32_26_3_14_clken;
reg [31:0] legup_mult_signed_32_26_3_14_dataa;
wire [25:0] legup_mult_signed_32_26_3_14_datab;
wire [57:0] legup_mult_signed_32_26_3_14_result;
reg [57:0] legup_mult_hw_fir_init_check_77_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_77_out;
reg  legup_mult_hw_fir_init_check_77_en;
reg  legup_mult_hw_fir_init_check_77_en_pipeline_cond;
reg [61:0] hw_fir_init_check_77_width_extended;
reg  legup_mult_signed_32_27_3_15_clock;
reg  legup_mult_signed_32_27_3_15_aclr;
reg  legup_mult_signed_32_27_3_15_clken;
reg [31:0] legup_mult_signed_32_27_3_15_dataa;
wire [26:0] legup_mult_signed_32_27_3_15_datab;
wire [58:0] legup_mult_signed_32_27_3_15_result;
reg [58:0] legup_mult_hw_fir_init_check_79_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_79_out;
reg  legup_mult_hw_fir_init_check_79_en;
reg  legup_mult_hw_fir_init_check_79_en_pipeline_cond;
reg [61:0] hw_fir_init_check_79_width_extended;
reg  legup_mult_signed_32_27_3_16_clock;
reg  legup_mult_signed_32_27_3_16_aclr;
reg  legup_mult_signed_32_27_3_16_clken;
reg [31:0] legup_mult_signed_32_27_3_16_dataa;
wire [26:0] legup_mult_signed_32_27_3_16_datab;
wire [58:0] legup_mult_signed_32_27_3_16_result;
reg [58:0] legup_mult_hw_fir_init_check_81_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_81_out;
reg  legup_mult_hw_fir_init_check_81_en;
reg  legup_mult_hw_fir_init_check_81_en_pipeline_cond;
reg [61:0] hw_fir_init_check_81_width_extended;
reg  legup_mult_unsigned_63_27_4_17_clock;
reg  legup_mult_unsigned_63_27_4_17_aclr;
reg  legup_mult_unsigned_63_27_4_17_clken;
reg [62:0] legup_mult_unsigned_63_27_4_17_dataa;
wire [26:0] legup_mult_unsigned_63_27_4_17_datab;
wire [89:0] legup_mult_unsigned_63_27_4_17_result;
reg [89:0] legup_mult_hw_fir_init_check_83_out_actual;
reg [58:0] legup_mult_hw_fir_init_check_83_out;
reg  legup_mult_hw_fir_init_check_83_en;
reg  legup_mult_hw_fir_init_check_83_en_pipeline_cond;
reg [61:0] hw_fir_init_check_83_width_extended;
reg  legup_mult_signed_32_28_3_18_clock;
reg  legup_mult_signed_32_28_3_18_aclr;
reg  legup_mult_signed_32_28_3_18_clken;
reg [31:0] legup_mult_signed_32_28_3_18_dataa;
wire [27:0] legup_mult_signed_32_28_3_18_datab;
wire [59:0] legup_mult_signed_32_28_3_18_result;
reg [59:0] legup_mult_hw_fir_init_check_85_out_actual;
reg [58:0] legup_mult_hw_fir_init_check_85_out;
reg  legup_mult_hw_fir_init_check_85_en;
reg  legup_mult_hw_fir_init_check_85_en_pipeline_cond;
reg [61:0] hw_fir_init_check_85_width_extended;
reg  legup_mult_unsigned_56_28_4_19_clock;
reg  legup_mult_unsigned_56_28_4_19_aclr;
reg  legup_mult_unsigned_56_28_4_19_clken;
reg [55:0] legup_mult_unsigned_56_28_4_19_dataa;
wire [27:0] legup_mult_unsigned_56_28_4_19_datab;
wire [83:0] legup_mult_unsigned_56_28_4_19_result;
reg [83:0] legup_mult_hw_fir_init_check_87_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_87_out;
reg  legup_mult_hw_fir_init_check_87_en;
reg  legup_mult_hw_fir_init_check_87_en_pipeline_cond;
reg [61:0] hw_fir_init_check_87_width_extended;
reg  legup_mult_signed_32_29_3_20_clock;
reg  legup_mult_signed_32_29_3_20_aclr;
reg  legup_mult_signed_32_29_3_20_clken;
reg [31:0] legup_mult_signed_32_29_3_20_dataa;
wire [28:0] legup_mult_signed_32_29_3_20_datab;
wire [60:0] legup_mult_signed_32_29_3_20_result;
reg [60:0] legup_mult_hw_fir_init_check_89_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_89_out;
reg  legup_mult_hw_fir_init_check_89_en;
reg  legup_mult_hw_fir_init_check_89_en_pipeline_cond;
reg [61:0] hw_fir_init_check_89_width_extended;
reg  legup_mult_signed_32_29_3_21_clock;
reg  legup_mult_signed_32_29_3_21_aclr;
reg  legup_mult_signed_32_29_3_21_clken;
reg [31:0] legup_mult_signed_32_29_3_21_dataa;
wire [28:0] legup_mult_signed_32_29_3_21_datab;
wire [60:0] legup_mult_signed_32_29_3_21_result;
reg [60:0] legup_mult_hw_fir_init_check_91_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_91_out;
reg  legup_mult_hw_fir_init_check_91_en;
reg  legup_mult_hw_fir_init_check_91_en_pipeline_cond;
reg [61:0] hw_fir_init_check_91_width_extended;
reg  legup_mult_signed_32_29_3_22_clock;
reg  legup_mult_signed_32_29_3_22_aclr;
reg  legup_mult_signed_32_29_3_22_clken;
reg [31:0] legup_mult_signed_32_29_3_22_dataa;
wire [28:0] legup_mult_signed_32_29_3_22_datab;
wire [60:0] legup_mult_signed_32_29_3_22_result;
reg [60:0] legup_mult_hw_fir_init_check_93_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_93_out;
reg  legup_mult_hw_fir_init_check_93_en;
reg  legup_mult_hw_fir_init_check_93_en_pipeline_cond;
reg [61:0] hw_fir_init_check_93_width_extended;
reg  legup_mult_unsigned_62_28_4_23_clock;
reg  legup_mult_unsigned_62_28_4_23_aclr;
reg  legup_mult_unsigned_62_28_4_23_clken;
reg [61:0] legup_mult_unsigned_62_28_4_23_dataa;
wire [27:0] legup_mult_unsigned_62_28_4_23_datab;
wire [89:0] legup_mult_unsigned_62_28_4_23_result;
reg [89:0] legup_mult_hw_fir_init_check_95_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_95_out;
reg  legup_mult_hw_fir_init_check_95_en;
reg  legup_mult_hw_fir_init_check_95_en_pipeline_cond;
reg [61:0] hw_fir_init_check_95_width_extended;
reg  legup_mult_unsigned_62_28_4_24_clock;
reg  legup_mult_unsigned_62_28_4_24_aclr;
reg  legup_mult_unsigned_62_28_4_24_clken;
reg [61:0] legup_mult_unsigned_62_28_4_24_dataa;
wire [27:0] legup_mult_unsigned_62_28_4_24_datab;
wire [89:0] legup_mult_unsigned_62_28_4_24_result;
reg [89:0] legup_mult_hw_fir_init_check_97_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_97_out;
reg  legup_mult_hw_fir_init_check_97_en;
reg  legup_mult_hw_fir_init_check_97_en_pipeline_cond;
reg [61:0] hw_fir_init_check_97_width_extended;
reg  legup_mult_signed_32_29_3_25_clock;
reg  legup_mult_signed_32_29_3_25_aclr;
reg  legup_mult_signed_32_29_3_25_clken;
reg [31:0] legup_mult_signed_32_29_3_25_dataa;
wire [28:0] legup_mult_signed_32_29_3_25_datab;
wire [60:0] legup_mult_signed_32_29_3_25_result;
reg [60:0] legup_mult_hw_fir_init_check_99_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_99_out;
reg  legup_mult_hw_fir_init_check_99_en;
reg  legup_mult_hw_fir_init_check_99_en_pipeline_cond;
reg [61:0] hw_fir_init_check_99_width_extended;
reg  legup_mult_signed_32_29_3_26_clock;
reg  legup_mult_signed_32_29_3_26_aclr;
reg  legup_mult_signed_32_29_3_26_clken;
reg [31:0] legup_mult_signed_32_29_3_26_dataa;
wire [28:0] legup_mult_signed_32_29_3_26_datab;
wire [60:0] legup_mult_signed_32_29_3_26_result;
reg [60:0] legup_mult_hw_fir_init_check_101_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_101_out;
reg  legup_mult_hw_fir_init_check_101_en;
reg  legup_mult_hw_fir_init_check_101_en_pipeline_cond;
reg [61:0] hw_fir_init_check_101_width_extended;
reg  legup_mult_signed_32_29_3_27_clock;
reg  legup_mult_signed_32_29_3_27_aclr;
reg  legup_mult_signed_32_29_3_27_clken;
reg [31:0] legup_mult_signed_32_29_3_27_dataa;
wire [28:0] legup_mult_signed_32_29_3_27_datab;
wire [60:0] legup_mult_signed_32_29_3_27_result;
reg [60:0] legup_mult_hw_fir_init_check_103_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_103_out;
reg  legup_mult_hw_fir_init_check_103_en;
reg  legup_mult_hw_fir_init_check_103_en_pipeline_cond;
reg [61:0] hw_fir_init_check_103_width_extended;
reg  legup_mult_unsigned_56_28_4_28_clock;
reg  legup_mult_unsigned_56_28_4_28_aclr;
reg  legup_mult_unsigned_56_28_4_28_clken;
reg [55:0] legup_mult_unsigned_56_28_4_28_dataa;
wire [27:0] legup_mult_unsigned_56_28_4_28_datab;
wire [83:0] legup_mult_unsigned_56_28_4_28_result;
reg [83:0] legup_mult_hw_fir_init_check_105_out_actual;
reg [59:0] legup_mult_hw_fir_init_check_105_out;
reg  legup_mult_hw_fir_init_check_105_en;
reg  legup_mult_hw_fir_init_check_105_en_pipeline_cond;
reg [61:0] hw_fir_init_check_105_width_extended;
reg  legup_mult_signed_32_28_3_29_clock;
reg  legup_mult_signed_32_28_3_29_aclr;
reg  legup_mult_signed_32_28_3_29_clken;
reg [31:0] legup_mult_signed_32_28_3_29_dataa;
wire [27:0] legup_mult_signed_32_28_3_29_datab;
wire [59:0] legup_mult_signed_32_28_3_29_result;
reg [59:0] legup_mult_hw_fir_init_check_107_out_actual;
reg [58:0] legup_mult_hw_fir_init_check_107_out;
reg  legup_mult_hw_fir_init_check_107_en;
reg  legup_mult_hw_fir_init_check_107_en_pipeline_cond;
reg [61:0] hw_fir_init_check_107_width_extended;
reg  legup_mult_unsigned_63_27_4_30_clock;
reg  legup_mult_unsigned_63_27_4_30_aclr;
reg  legup_mult_unsigned_63_27_4_30_clken;
reg [62:0] legup_mult_unsigned_63_27_4_30_dataa;
wire [26:0] legup_mult_unsigned_63_27_4_30_datab;
wire [89:0] legup_mult_unsigned_63_27_4_30_result;
reg [89:0] legup_mult_hw_fir_init_check_109_out_actual;
reg [58:0] legup_mult_hw_fir_init_check_109_out;
reg  legup_mult_hw_fir_init_check_109_en;
reg  legup_mult_hw_fir_init_check_109_en_pipeline_cond;
reg [61:0] hw_fir_init_check_109_width_extended;
reg  legup_mult_signed_32_27_3_31_clock;
reg  legup_mult_signed_32_27_3_31_aclr;
reg  legup_mult_signed_32_27_3_31_clken;
reg [31:0] legup_mult_signed_32_27_3_31_dataa;
wire [26:0] legup_mult_signed_32_27_3_31_datab;
wire [58:0] legup_mult_signed_32_27_3_31_result;
reg [58:0] legup_mult_hw_fir_init_check_111_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_111_out;
reg  legup_mult_hw_fir_init_check_111_en;
reg  legup_mult_hw_fir_init_check_111_en_pipeline_cond;
reg [61:0] hw_fir_init_check_111_width_extended;
reg  legup_mult_signed_32_27_3_32_clock;
reg  legup_mult_signed_32_27_3_32_aclr;
reg  legup_mult_signed_32_27_3_32_clken;
reg [31:0] legup_mult_signed_32_27_3_32_dataa;
wire [26:0] legup_mult_signed_32_27_3_32_datab;
wire [58:0] legup_mult_signed_32_27_3_32_result;
reg [58:0] legup_mult_hw_fir_init_check_113_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_113_out;
reg  legup_mult_hw_fir_init_check_113_en;
reg  legup_mult_hw_fir_init_check_113_en_pipeline_cond;
reg [61:0] hw_fir_init_check_113_width_extended;
reg  legup_mult_signed_32_26_3_33_clock;
reg  legup_mult_signed_32_26_3_33_aclr;
reg  legup_mult_signed_32_26_3_33_clken;
reg [31:0] legup_mult_signed_32_26_3_33_dataa;
wire [25:0] legup_mult_signed_32_26_3_33_datab;
wire [57:0] legup_mult_signed_32_26_3_33_result;
reg [57:0] legup_mult_hw_fir_init_check_115_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_115_out;
reg  legup_mult_hw_fir_init_check_115_en;
reg  legup_mult_hw_fir_init_check_115_en_pipeline_cond;
reg [61:0] hw_fir_init_check_115_width_extended;
reg  legup_mult_signed_32_23_3_34_clock;
reg  legup_mult_signed_32_23_3_34_aclr;
reg  legup_mult_signed_32_23_3_34_clken;
reg [31:0] legup_mult_signed_32_23_3_34_dataa;
wire [22:0] legup_mult_signed_32_23_3_34_datab;
wire [54:0] legup_mult_signed_32_23_3_34_result;
reg [54:0] legup_mult_hw_fir_init_check_117_out_actual;
reg [53:0] legup_mult_hw_fir_init_check_117_out;
reg  legup_mult_hw_fir_init_check_117_en;
reg  legup_mult_hw_fir_init_check_117_en_pipeline_cond;
reg [61:0] hw_fir_init_check_117_width_extended;
reg  legup_mult_signed_32_24_3_35_clock;
reg  legup_mult_signed_32_24_3_35_aclr;
reg  legup_mult_signed_32_24_3_35_clken;
reg [31:0] legup_mult_signed_32_24_3_35_dataa;
wire [23:0] legup_mult_signed_32_24_3_35_datab;
wire [55:0] legup_mult_signed_32_24_3_35_result;
reg [55:0] legup_mult_hw_fir_init_check_119_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_119_out;
reg  legup_mult_hw_fir_init_check_119_en;
reg  legup_mult_hw_fir_init_check_119_en_pipeline_cond;
reg [61:0] hw_fir_init_check_119_width_extended;
reg  legup_mult_signed_63_25_4_36_clock;
reg  legup_mult_signed_63_25_4_36_aclr;
reg  legup_mult_signed_63_25_4_36_clken;
reg [62:0] legup_mult_signed_63_25_4_36_dataa;
wire [24:0] legup_mult_signed_63_25_4_36_datab;
wire [87:0] legup_mult_signed_63_25_4_36_result;
reg [87:0] legup_mult_hw_fir_init_check_121_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_121_out;
reg  legup_mult_hw_fir_init_check_121_en;
reg  legup_mult_hw_fir_init_check_121_en_pipeline_cond;
reg [61:0] hw_fir_init_check_121_width_extended;
reg  legup_mult_signed_60_26_4_37_clock;
reg  legup_mult_signed_60_26_4_37_aclr;
reg  legup_mult_signed_60_26_4_37_clken;
reg [59:0] legup_mult_signed_60_26_4_37_dataa;
wire [25:0] legup_mult_signed_60_26_4_37_datab;
wire [85:0] legup_mult_signed_60_26_4_37_result;
reg [85:0] legup_mult_hw_fir_init_check_123_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_123_out;
reg  legup_mult_hw_fir_init_check_123_en;
reg  legup_mult_hw_fir_init_check_123_en_pipeline_cond;
reg [61:0] hw_fir_init_check_123_width_extended;
reg  legup_mult_signed_32_26_3_38_clock;
reg  legup_mult_signed_32_26_3_38_aclr;
reg  legup_mult_signed_32_26_3_38_clken;
reg [31:0] legup_mult_signed_32_26_3_38_dataa;
wire [25:0] legup_mult_signed_32_26_3_38_datab;
wire [57:0] legup_mult_signed_32_26_3_38_result;
reg [57:0] legup_mult_hw_fir_init_check_125_out_actual;
reg [57:0] legup_mult_hw_fir_init_check_125_out;
reg  legup_mult_hw_fir_init_check_125_en;
reg  legup_mult_hw_fir_init_check_125_en_pipeline_cond;
reg [61:0] hw_fir_init_check_125_width_extended;
reg  legup_mult_signed_63_25_4_39_clock;
reg  legup_mult_signed_63_25_4_39_aclr;
reg  legup_mult_signed_63_25_4_39_clken;
reg [62:0] legup_mult_signed_63_25_4_39_dataa;
wire [24:0] legup_mult_signed_63_25_4_39_datab;
wire [87:0] legup_mult_signed_63_25_4_39_result;
reg [87:0] legup_mult_hw_fir_init_check_127_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_127_out;
reg  legup_mult_hw_fir_init_check_127_en;
reg  legup_mult_hw_fir_init_check_127_en_pipeline_cond;
reg [61:0] hw_fir_init_check_127_width_extended;
reg  legup_mult_signed_32_25_3_40_clock;
reg  legup_mult_signed_32_25_3_40_aclr;
reg  legup_mult_signed_32_25_3_40_clken;
reg [31:0] legup_mult_signed_32_25_3_40_dataa;
wire [24:0] legup_mult_signed_32_25_3_40_datab;
wire [56:0] legup_mult_signed_32_25_3_40_result;
reg [56:0] legup_mult_hw_fir_init_check_129_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_129_out;
reg  legup_mult_hw_fir_init_check_129_en;
reg  legup_mult_hw_fir_init_check_129_en_pipeline_cond;
reg [61:0] hw_fir_init_check_129_width_extended;
reg  legup_mult_signed_62_25_4_41_clock;
reg  legup_mult_signed_62_25_4_41_aclr;
reg  legup_mult_signed_62_25_4_41_clken;
reg [61:0] legup_mult_signed_62_25_4_41_dataa;
wire [24:0] legup_mult_signed_62_25_4_41_datab;
wire [86:0] legup_mult_signed_62_25_4_41_result;
reg [86:0] legup_mult_hw_fir_init_check_131_out_actual;
reg [56:0] legup_mult_hw_fir_init_check_131_out;
reg  legup_mult_hw_fir_init_check_131_en;
reg  legup_mult_hw_fir_init_check_131_en_pipeline_cond;
reg [61:0] hw_fir_init_check_131_width_extended;
reg  legup_mult_signed_32_24_3_42_clock;
reg  legup_mult_signed_32_24_3_42_aclr;
reg  legup_mult_signed_32_24_3_42_clken;
reg [31:0] legup_mult_signed_32_24_3_42_dataa;
wire [23:0] legup_mult_signed_32_24_3_42_datab;
wire [55:0] legup_mult_signed_32_24_3_42_result;
reg [55:0] legup_mult_hw_fir_init_check_133_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_133_out;
reg  legup_mult_hw_fir_init_check_133_en;
reg  legup_mult_hw_fir_init_check_133_en_pipeline_cond;
reg [61:0] hw_fir_init_check_133_width_extended;
reg  legup_mult_signed_61_24_4_43_clock;
reg  legup_mult_signed_61_24_4_43_aclr;
reg  legup_mult_signed_61_24_4_43_clken;
reg [60:0] legup_mult_signed_61_24_4_43_dataa;
wire [23:0] legup_mult_signed_61_24_4_43_datab;
wire [84:0] legup_mult_signed_61_24_4_43_result;
reg [84:0] legup_mult_hw_fir_init_check_135_out_actual;
reg [55:0] legup_mult_hw_fir_init_check_135_out;
reg  legup_mult_hw_fir_init_check_135_en;
reg  legup_mult_hw_fir_init_check_135_en_pipeline_cond;
reg [61:0] hw_fir_init_check_135_width_extended;
reg  legup_mult_signed_63_23_4_44_clock;
reg  legup_mult_signed_63_23_4_44_aclr;
reg  legup_mult_signed_63_23_4_44_clken;
reg [62:0] legup_mult_signed_63_23_4_44_dataa;
wire [22:0] legup_mult_signed_63_23_4_44_datab;
wire [85:0] legup_mult_signed_63_23_4_44_result;
reg [85:0] legup_mult_hw_fir_init_check_137_out_actual;
reg [54:0] legup_mult_hw_fir_init_check_137_out;
reg  legup_mult_hw_fir_init_check_137_en;
reg  legup_mult_hw_fir_init_check_137_en_pipeline_cond;
reg [61:0] hw_fir_init_check_137_width_extended;
reg  legup_mult_signed_32_22_3_45_clock;
reg  legup_mult_signed_32_22_3_45_aclr;
reg  legup_mult_signed_32_22_3_45_clken;
reg [31:0] legup_mult_signed_32_22_3_45_dataa;
wire [21:0] legup_mult_signed_32_22_3_45_datab;
wire [53:0] legup_mult_signed_32_22_3_45_result;
reg [53:0] legup_mult_hw_fir_init_check_139_out_actual;
reg [53:0] legup_mult_hw_fir_init_check_139_out;
reg  legup_mult_hw_fir_init_check_139_en;
reg  legup_mult_hw_fir_init_check_139_en_pipeline_cond;
reg [61:0] hw_fir_init_check_139_width_extended;
reg  legup_mult_signed_63_21_4_46_clock;
reg  legup_mult_signed_63_21_4_46_aclr;
reg  legup_mult_signed_63_21_4_46_clken;
reg [62:0] legup_mult_signed_63_21_4_46_dataa;
wire [20:0] legup_mult_signed_63_21_4_46_datab;
wire [83:0] legup_mult_signed_63_21_4_46_result;
reg [83:0] legup_mult_hw_fir_init_check_141_out_actual;
reg [52:0] legup_mult_hw_fir_init_check_141_out;
reg  legup_mult_hw_fir_init_check_141_en;
reg  legup_mult_hw_fir_init_check_141_en_pipeline_cond;
reg [61:0] hw_fir_init_check_141_width_extended;
reg  legup_mult_signed_32_19_3_47_clock;
reg  legup_mult_signed_32_19_3_47_aclr;
reg  legup_mult_signed_32_19_3_47_clken;
reg [31:0] legup_mult_signed_32_19_3_47_dataa;
wire [18:0] legup_mult_signed_32_19_3_47_datab;
wire [50:0] legup_mult_signed_32_19_3_47_result;
reg [50:0] legup_mult_hw_fir_init_check_143_out_actual;
reg [50:0] legup_mult_hw_fir_init_check_143_out;
reg  legup_mult_hw_fir_init_check_143_en;
reg  legup_mult_hw_fir_init_check_143_en_pipeline_cond;
reg [61:0] hw_fir_init_check_143_width_extended;

/*   %48 = mul i64 %47, -112451, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5193, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_19_3_0 (
	.clock (legup_mult_signed_32_19_3_0_clock),
	.aclr (legup_mult_signed_32_19_3_0_aclr),
	.clken (legup_mult_signed_32_19_3_0_clken),
	.dataa (legup_mult_signed_32_19_3_0_dataa),
	.datab (legup_mult_signed_32_19_3_0_datab),
	.result (legup_mult_signed_32_19_3_0_result)
);

defparam
	legup_mult_signed_32_19_3_0.widtha = 32,
	legup_mult_signed_32_19_3_0.widthb = 19,
	legup_mult_signed_32_19_3_0.widthp = 51,
	legup_mult_signed_32_19_3_0.pipeline = 3,
	legup_mult_signed_32_19_3_0.representation = "SIGNED";

/*   %50 = mul i64 %49, -420988, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5198, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_21_4_1 (
	.clock (legup_mult_signed_63_21_4_1_clock),
	.aclr (legup_mult_signed_63_21_4_1_aclr),
	.clken (legup_mult_signed_63_21_4_1_clken),
	.dataa (legup_mult_signed_63_21_4_1_dataa),
	.datab (legup_mult_signed_63_21_4_1_datab),
	.result (legup_mult_signed_63_21_4_1_result)
);

defparam
	legup_mult_signed_63_21_4_1.widtha = 63,
	legup_mult_signed_63_21_4_1.widthb = 21,
	legup_mult_signed_63_21_4_1.widthp = 84,
	legup_mult_signed_63_21_4_1.pipeline = 4,
	legup_mult_signed_63_21_4_1.representation = "SIGNED";

/*   %53 = mul i64 %52, -839361, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5205, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_22_3_2 (
	.clock (legup_mult_signed_32_22_3_2_clock),
	.aclr (legup_mult_signed_32_22_3_2_aclr),
	.clken (legup_mult_signed_32_22_3_2_clken),
	.dataa (legup_mult_signed_32_22_3_2_dataa),
	.datab (legup_mult_signed_32_22_3_2_datab),
	.result (legup_mult_signed_32_22_3_2_result)
);

defparam
	legup_mult_signed_32_22_3_2.widtha = 32,
	legup_mult_signed_32_22_3_2.widthb = 22,
	legup_mult_signed_32_22_3_2.widthp = 54,
	legup_mult_signed_32_22_3_2.pipeline = 3,
	legup_mult_signed_32_22_3_2.representation = "SIGNED";

/*   %55 = mul i64 %54, -1585580, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5206, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_23_4_3 (
	.clock (legup_mult_signed_63_23_4_3_clock),
	.aclr (legup_mult_signed_63_23_4_3_aclr),
	.clken (legup_mult_signed_63_23_4_3_clken),
	.dataa (legup_mult_signed_63_23_4_3_dataa),
	.datab (legup_mult_signed_63_23_4_3_datab),
	.result (legup_mult_signed_63_23_4_3_result)
);

defparam
	legup_mult_signed_63_23_4_3.widtha = 63,
	legup_mult_signed_63_23_4_3.widthb = 23,
	legup_mult_signed_63_23_4_3.widthp = 86,
	legup_mult_signed_63_23_4_3.pipeline = 4,
	legup_mult_signed_63_23_4_3.representation = "SIGNED";

/*   %57 = mul i64 %56, -2588080, !dbg !5192, !MSB !5191, !LSB !5199, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_61_24_4_4 (
	.clock (legup_mult_signed_61_24_4_4_clock),
	.aclr (legup_mult_signed_61_24_4_4_aclr),
	.clken (legup_mult_signed_61_24_4_4_clken),
	.dataa (legup_mult_signed_61_24_4_4_dataa),
	.datab (legup_mult_signed_61_24_4_4_datab),
	.result (legup_mult_signed_61_24_4_4_result)
);

defparam
	legup_mult_signed_61_24_4_4.widtha = 61,
	legup_mult_signed_61_24_4_4.widthb = 24,
	legup_mult_signed_61_24_4_4.widthp = 85,
	legup_mult_signed_61_24_4_4.pipeline = 4,
	legup_mult_signed_61_24_4_4.representation = "SIGNED";

/*   %59 = mul i64 %58, -3887393, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_24_3_5 (
	.clock (legup_mult_signed_32_24_3_5_clock),
	.aclr (legup_mult_signed_32_24_3_5_aclr),
	.clken (legup_mult_signed_32_24_3_5_clken),
	.dataa (legup_mult_signed_32_24_3_5_dataa),
	.datab (legup_mult_signed_32_24_3_5_datab),
	.result (legup_mult_signed_32_24_3_5_result)
);

defparam
	legup_mult_signed_32_24_3_5.widtha = 32,
	legup_mult_signed_32_24_3_5.widthb = 24,
	legup_mult_signed_32_24_3_5.widthp = 56,
	legup_mult_signed_32_24_3_5.pipeline = 3,
	legup_mult_signed_32_24_3_5.representation = "SIGNED";

/*   %61 = mul i64 %60, -5373160, !dbg !5192, !MSB !5191, !LSB !5194, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_62_25_4_6 (
	.clock (legup_mult_signed_62_25_4_6_clock),
	.aclr (legup_mult_signed_62_25_4_6_aclr),
	.clken (legup_mult_signed_62_25_4_6_clken),
	.dataa (legup_mult_signed_62_25_4_6_dataa),
	.datab (legup_mult_signed_62_25_4_6_datab),
	.result (legup_mult_signed_62_25_4_6_result)
);

defparam
	legup_mult_signed_62_25_4_6.widtha = 62,
	legup_mult_signed_62_25_4_6.widthb = 25,
	legup_mult_signed_62_25_4_6.widthp = 87,
	legup_mult_signed_62_25_4_6.pipeline = 4,
	legup_mult_signed_62_25_4_6.representation = "SIGNED";

/*   %63 = mul i64 %62, -6898475, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_25_3_7 (
	.clock (legup_mult_signed_32_25_3_7_clock),
	.aclr (legup_mult_signed_32_25_3_7_aclr),
	.clken (legup_mult_signed_32_25_3_7_clken),
	.dataa (legup_mult_signed_32_25_3_7_dataa),
	.datab (legup_mult_signed_32_25_3_7_datab),
	.result (legup_mult_signed_32_25_3_7_result)
);

defparam
	legup_mult_signed_32_25_3_7.widtha = 32,
	legup_mult_signed_32_25_3_7.widthb = 25,
	legup_mult_signed_32_25_3_7.widthp = 57,
	legup_mult_signed_32_25_3_7.pipeline = 3,
	legup_mult_signed_32_25_3_7.representation = "SIGNED";

/*   %65 = mul i64 %64, -8204580, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_25_4_8 (
	.clock (legup_mult_signed_63_25_4_8_clock),
	.aclr (legup_mult_signed_63_25_4_8_aclr),
	.clken (legup_mult_signed_63_25_4_8_clken),
	.dataa (legup_mult_signed_63_25_4_8_dataa),
	.datab (legup_mult_signed_63_25_4_8_datab),
	.result (legup_mult_signed_63_25_4_8_result)
);

defparam
	legup_mult_signed_63_25_4_8.widtha = 63,
	legup_mult_signed_63_25_4_8.widthb = 25,
	legup_mult_signed_63_25_4_8.widthp = 88,
	legup_mult_signed_63_25_4_8.pipeline = 4,
	legup_mult_signed_63_25_4_8.representation = "SIGNED";

/*   %67 = mul i64 %66, -8965043, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_26_3_9 (
	.clock (legup_mult_signed_32_26_3_9_clock),
	.aclr (legup_mult_signed_32_26_3_9_aclr),
	.clken (legup_mult_signed_32_26_3_9_clken),
	.dataa (legup_mult_signed_32_26_3_9_dataa),
	.datab (legup_mult_signed_32_26_3_9_datab),
	.result (legup_mult_signed_32_26_3_9_result)
);

defparam
	legup_mult_signed_32_26_3_9.widtha = 32,
	legup_mult_signed_32_26_3_9.widthb = 26,
	legup_mult_signed_32_26_3_9.widthp = 58,
	legup_mult_signed_32_26_3_9.pipeline = 3,
	legup_mult_signed_32_26_3_9.representation = "SIGNED";

/*   %69 = mul i64 %68, -8788704, !dbg !5192, !MSB !5191, !LSB !5217, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_60_26_4_10 (
	.clock (legup_mult_signed_60_26_4_10_clock),
	.aclr (legup_mult_signed_60_26_4_10_aclr),
	.clken (legup_mult_signed_60_26_4_10_clken),
	.dataa (legup_mult_signed_60_26_4_10_dataa),
	.datab (legup_mult_signed_60_26_4_10_datab),
	.result (legup_mult_signed_60_26_4_10_result)
);

defparam
	legup_mult_signed_60_26_4_10.widtha = 60,
	legup_mult_signed_60_26_4_10.widthb = 26,
	legup_mult_signed_60_26_4_10.widthp = 86,
	legup_mult_signed_60_26_4_10.pipeline = 4,
	legup_mult_signed_60_26_4_10.representation = "SIGNED";

/*   %71 = mul i64 %70, -7268700, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_25_4_11 (
	.clock (legup_mult_signed_63_25_4_11_clock),
	.aclr (legup_mult_signed_63_25_4_11_aclr),
	.clken (legup_mult_signed_63_25_4_11_clken),
	.dataa (legup_mult_signed_63_25_4_11_dataa),
	.datab (legup_mult_signed_63_25_4_11_datab),
	.result (legup_mult_signed_63_25_4_11_result)
);

defparam
	legup_mult_signed_63_25_4_11.widtha = 63,
	legup_mult_signed_63_25_4_11.widthb = 25,
	legup_mult_signed_63_25_4_11.widthp = 88,
	legup_mult_signed_63_25_4_11.pipeline = 4,
	legup_mult_signed_63_25_4_11.representation = "SIGNED";

/*   %73 = mul i64 %72, -4027179, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_24_3_12 (
	.clock (legup_mult_signed_32_24_3_12_clock),
	.aclr (legup_mult_signed_32_24_3_12_aclr),
	.clken (legup_mult_signed_32_24_3_12_clken),
	.dataa (legup_mult_signed_32_24_3_12_dataa),
	.datab (legup_mult_signed_32_24_3_12_datab),
	.result (legup_mult_signed_32_24_3_12_result)
);

defparam
	legup_mult_signed_32_24_3_12.widtha = 32,
	legup_mult_signed_32_24_3_12.widthb = 24,
	legup_mult_signed_32_24_3_12.widthp = 56,
	legup_mult_signed_32_24_3_12.pipeline = 3,
	legup_mult_signed_32_24_3_12.representation = "SIGNED";

/*   %75 = mul i64 %74, 1223507, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5205, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_23_3_13 (
	.clock (legup_mult_signed_32_23_3_13_clock),
	.aclr (legup_mult_signed_32_23_3_13_aclr),
	.clken (legup_mult_signed_32_23_3_13_clken),
	.dataa (legup_mult_signed_32_23_3_13_dataa),
	.datab (legup_mult_signed_32_23_3_13_datab),
	.result (legup_mult_signed_32_23_3_13_result)
);

defparam
	legup_mult_signed_32_23_3_13.widtha = 32,
	legup_mult_signed_32_23_3_13.widthb = 23,
	legup_mult_signed_32_23_3_13.widthp = 55,
	legup_mult_signed_32_23_3_13.pipeline = 3,
	legup_mult_signed_32_23_3_13.representation = "SIGNED";

/*   %77 = mul i64 %76, 8625555, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_26_3_14 (
	.clock (legup_mult_signed_32_26_3_14_clock),
	.aclr (legup_mult_signed_32_26_3_14_aclr),
	.clken (legup_mult_signed_32_26_3_14_clken),
	.dataa (legup_mult_signed_32_26_3_14_dataa),
	.datab (legup_mult_signed_32_26_3_14_datab),
	.result (legup_mult_signed_32_26_3_14_result)
);

defparam
	legup_mult_signed_32_26_3_14.widtha = 32,
	legup_mult_signed_32_26_3_14.widthb = 26,
	legup_mult_signed_32_26_3_14.widthp = 58,
	legup_mult_signed_32_26_3_14.pipeline = 3,
	legup_mult_signed_32_26_3_14.representation = "SIGNED";

/*   %79 = mul i64 %78, 18127719, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_27_3_15 (
	.clock (legup_mult_signed_32_27_3_15_clock),
	.aclr (legup_mult_signed_32_27_3_15_aclr),
	.clken (legup_mult_signed_32_27_3_15_clken),
	.dataa (legup_mult_signed_32_27_3_15_dataa),
	.datab (legup_mult_signed_32_27_3_15_datab),
	.result (legup_mult_signed_32_27_3_15_result)
);

defparam
	legup_mult_signed_32_27_3_15.widtha = 32,
	legup_mult_signed_32_27_3_15.widthb = 27,
	legup_mult_signed_32_27_3_15.widthp = 59,
	legup_mult_signed_32_27_3_15.pipeline = 3,
	legup_mult_signed_32_27_3_15.representation = "SIGNED";

/*   %81 = mul i64 %80, 29456521, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_27_3_16 (
	.clock (legup_mult_signed_32_27_3_16_clock),
	.aclr (legup_mult_signed_32_27_3_16_aclr),
	.clken (legup_mult_signed_32_27_3_16_clken),
	.dataa (legup_mult_signed_32_27_3_16_dataa),
	.datab (legup_mult_signed_32_27_3_16_datab),
	.result (legup_mult_signed_32_27_3_16_result)
);

defparam
	legup_mult_signed_32_27_3_16.widtha = 32,
	legup_mult_signed_32_27_3_16.widthb = 27,
	legup_mult_signed_32_27_3_16.widthp = 59,
	legup_mult_signed_32_27_3_16.pipeline = 3,
	legup_mult_signed_32_27_3_16.representation = "SIGNED";

/*   %83 = mul i64 %82, 42112510, !dbg !5192, !MSB !5191, !LSB !5220, !extendFrom !5216, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_63_27_4_17 (
	.clock (legup_mult_unsigned_63_27_4_17_clock),
	.aclr (legup_mult_unsigned_63_27_4_17_aclr),
	.clken (legup_mult_unsigned_63_27_4_17_clken),
	.dataa (legup_mult_unsigned_63_27_4_17_dataa),
	.datab (legup_mult_unsigned_63_27_4_17_datab),
	.result (legup_mult_unsigned_63_27_4_17_result)
);

defparam
	legup_mult_unsigned_63_27_4_17.widtha = 63,
	legup_mult_unsigned_63_27_4_17.widthb = 27,
	legup_mult_unsigned_63_27_4_17.widthp = 90,
	legup_mult_unsigned_63_27_4_17.pipeline = 4,
	legup_mult_unsigned_63_27_4_17.representation = "UNSIGNED";

/*   %85 = mul i64 %84, 55395293, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5216, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_28_3_18 (
	.clock (legup_mult_signed_32_28_3_18_clock),
	.aclr (legup_mult_signed_32_28_3_18_aclr),
	.clken (legup_mult_signed_32_28_3_18_clken),
	.dataa (legup_mult_signed_32_28_3_18_dataa),
	.datab (legup_mult_signed_32_28_3_18_datab),
	.result (legup_mult_signed_32_28_3_18_result)
);

defparam
	legup_mult_signed_32_28_3_18.widtha = 32,
	legup_mult_signed_32_28_3_18.widthb = 28,
	legup_mult_signed_32_28_3_18.widthp = 60,
	legup_mult_signed_32_28_3_18.pipeline = 3,
	legup_mult_signed_32_28_3_18.representation = "SIGNED";

/*   %87 = mul i64 %86, 68457216, !dbg !5192, !MSB !5191, !LSB !5221, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_56_28_4_19 (
	.clock (legup_mult_unsigned_56_28_4_19_clock),
	.aclr (legup_mult_unsigned_56_28_4_19_aclr),
	.clken (legup_mult_unsigned_56_28_4_19_clken),
	.dataa (legup_mult_unsigned_56_28_4_19_dataa),
	.datab (legup_mult_unsigned_56_28_4_19_datab),
	.result (legup_mult_unsigned_56_28_4_19_result)
);

defparam
	legup_mult_unsigned_56_28_4_19.widtha = 56,
	legup_mult_unsigned_56_28_4_19.widthb = 28,
	legup_mult_unsigned_56_28_4_19.widthp = 84,
	legup_mult_unsigned_56_28_4_19.pipeline = 4,
	legup_mult_unsigned_56_28_4_19.representation = "UNSIGNED";

/*   %89 = mul i64 %88, 80380559, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_20 (
	.clock (legup_mult_signed_32_29_3_20_clock),
	.aclr (legup_mult_signed_32_29_3_20_aclr),
	.clken (legup_mult_signed_32_29_3_20_clken),
	.dataa (legup_mult_signed_32_29_3_20_dataa),
	.datab (legup_mult_signed_32_29_3_20_datab),
	.result (legup_mult_signed_32_29_3_20_result)
);

defparam
	legup_mult_signed_32_29_3_20.widtha = 32,
	legup_mult_signed_32_29_3_20.widthb = 29,
	legup_mult_signed_32_29_3_20.widthp = 61,
	legup_mult_signed_32_29_3_20.pipeline = 3,
	legup_mult_signed_32_29_3_20.representation = "SIGNED";

/*   %91 = mul i64 %90, 90269643, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_21 (
	.clock (legup_mult_signed_32_29_3_21_clock),
	.aclr (legup_mult_signed_32_29_3_21_aclr),
	.clken (legup_mult_signed_32_29_3_21_clken),
	.dataa (legup_mult_signed_32_29_3_21_dataa),
	.datab (legup_mult_signed_32_29_3_21_datab),
	.result (legup_mult_signed_32_29_3_21_result)
);

defparam
	legup_mult_signed_32_29_3_21.widtha = 32,
	legup_mult_signed_32_29_3_21.widthb = 29,
	legup_mult_signed_32_29_3_21.widthp = 61,
	legup_mult_signed_32_29_3_21.pipeline = 3,
	legup_mult_signed_32_29_3_21.representation = "SIGNED";

/*   %93 = mul i64 %92, 97346473, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_22 (
	.clock (legup_mult_signed_32_29_3_22_clock),
	.aclr (legup_mult_signed_32_29_3_22_aclr),
	.clken (legup_mult_signed_32_29_3_22_clken),
	.dataa (legup_mult_signed_32_29_3_22_dataa),
	.datab (legup_mult_signed_32_29_3_22_datab),
	.result (legup_mult_signed_32_29_3_22_result)
);

defparam
	legup_mult_signed_32_29_3_22.widtha = 32,
	legup_mult_signed_32_29_3_22.widthb = 29,
	legup_mult_signed_32_29_3_22.widthp = 61,
	legup_mult_signed_32_29_3_22.pipeline = 3,
	legup_mult_signed_32_29_3_22.representation = "SIGNED";

/*   %95 = mul i64 %94, 101037156, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_62_28_4_23 (
	.clock (legup_mult_unsigned_62_28_4_23_clock),
	.aclr (legup_mult_unsigned_62_28_4_23_aclr),
	.clken (legup_mult_unsigned_62_28_4_23_clken),
	.dataa (legup_mult_unsigned_62_28_4_23_dataa),
	.datab (legup_mult_unsigned_62_28_4_23_datab),
	.result (legup_mult_unsigned_62_28_4_23_result)
);

defparam
	legup_mult_unsigned_62_28_4_23.widtha = 62,
	legup_mult_unsigned_62_28_4_23.widthb = 28,
	legup_mult_unsigned_62_28_4_23.widthp = 90,
	legup_mult_unsigned_62_28_4_23.pipeline = 4,
	legup_mult_unsigned_62_28_4_23.representation = "UNSIGNED";

/*   %97 = mul i64 %96, 101037156, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_62_28_4_24 (
	.clock (legup_mult_unsigned_62_28_4_24_clock),
	.aclr (legup_mult_unsigned_62_28_4_24_aclr),
	.clken (legup_mult_unsigned_62_28_4_24_clken),
	.dataa (legup_mult_unsigned_62_28_4_24_dataa),
	.datab (legup_mult_unsigned_62_28_4_24_datab),
	.result (legup_mult_unsigned_62_28_4_24_result)
);

defparam
	legup_mult_unsigned_62_28_4_24.widtha = 62,
	legup_mult_unsigned_62_28_4_24.widthb = 28,
	legup_mult_unsigned_62_28_4_24.widthp = 90,
	legup_mult_unsigned_62_28_4_24.pipeline = 4,
	legup_mult_unsigned_62_28_4_24.representation = "UNSIGNED";

/*   %99 = mul i64 %98, 97346473, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_25 (
	.clock (legup_mult_signed_32_29_3_25_clock),
	.aclr (legup_mult_signed_32_29_3_25_aclr),
	.clken (legup_mult_signed_32_29_3_25_clken),
	.dataa (legup_mult_signed_32_29_3_25_dataa),
	.datab (legup_mult_signed_32_29_3_25_datab),
	.result (legup_mult_signed_32_29_3_25_result)
);

defparam
	legup_mult_signed_32_29_3_25.widtha = 32,
	legup_mult_signed_32_29_3_25.widthb = 29,
	legup_mult_signed_32_29_3_25.widthp = 61,
	legup_mult_signed_32_29_3_25.pipeline = 3,
	legup_mult_signed_32_29_3_25.representation = "SIGNED";

/*   %101 = mul i64 %100, 90269643, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_26 (
	.clock (legup_mult_signed_32_29_3_26_clock),
	.aclr (legup_mult_signed_32_29_3_26_aclr),
	.clken (legup_mult_signed_32_29_3_26_clken),
	.dataa (legup_mult_signed_32_29_3_26_dataa),
	.datab (legup_mult_signed_32_29_3_26_datab),
	.result (legup_mult_signed_32_29_3_26_result)
);

defparam
	legup_mult_signed_32_29_3_26.widtha = 32,
	legup_mult_signed_32_29_3_26.widthb = 29,
	legup_mult_signed_32_29_3_26.widthp = 61,
	legup_mult_signed_32_29_3_26.pipeline = 3,
	legup_mult_signed_32_29_3_26.representation = "SIGNED";

/*   %103 = mul i64 %102, 80380559, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_29_3_27 (
	.clock (legup_mult_signed_32_29_3_27_clock),
	.aclr (legup_mult_signed_32_29_3_27_aclr),
	.clken (legup_mult_signed_32_29_3_27_clken),
	.dataa (legup_mult_signed_32_29_3_27_dataa),
	.datab (legup_mult_signed_32_29_3_27_datab),
	.result (legup_mult_signed_32_29_3_27_result)
);

defparam
	legup_mult_signed_32_29_3_27.widtha = 32,
	legup_mult_signed_32_29_3_27.widthb = 29,
	legup_mult_signed_32_29_3_27.widthp = 61,
	legup_mult_signed_32_29_3_27.pipeline = 3,
	legup_mult_signed_32_29_3_27.representation = "SIGNED";

/*   %105 = mul i64 %104, 68457216, !dbg !5192, !MSB !5191, !LSB !5221, !extendFrom !5209, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_56_28_4_28 (
	.clock (legup_mult_unsigned_56_28_4_28_clock),
	.aclr (legup_mult_unsigned_56_28_4_28_aclr),
	.clken (legup_mult_unsigned_56_28_4_28_clken),
	.dataa (legup_mult_unsigned_56_28_4_28_dataa),
	.datab (legup_mult_unsigned_56_28_4_28_datab),
	.result (legup_mult_unsigned_56_28_4_28_result)
);

defparam
	legup_mult_unsigned_56_28_4_28.widtha = 56,
	legup_mult_unsigned_56_28_4_28.widthb = 28,
	legup_mult_unsigned_56_28_4_28.widthp = 84,
	legup_mult_unsigned_56_28_4_28.pipeline = 4,
	legup_mult_unsigned_56_28_4_28.representation = "UNSIGNED";

/*   %107 = mul i64 %106, 55395293, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5216, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_28_3_29 (
	.clock (legup_mult_signed_32_28_3_29_clock),
	.aclr (legup_mult_signed_32_28_3_29_aclr),
	.clken (legup_mult_signed_32_28_3_29_clken),
	.dataa (legup_mult_signed_32_28_3_29_dataa),
	.datab (legup_mult_signed_32_28_3_29_datab),
	.result (legup_mult_signed_32_28_3_29_result)
);

defparam
	legup_mult_signed_32_28_3_29.widtha = 32,
	legup_mult_signed_32_28_3_29.widthb = 28,
	legup_mult_signed_32_28_3_29.widthp = 60,
	legup_mult_signed_32_28_3_29.pipeline = 3,
	legup_mult_signed_32_28_3_29.representation = "SIGNED";

/*   %109 = mul i64 %108, 42112510, !dbg !5192, !MSB !5191, !LSB !5220, !extendFrom !5216, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_unsigned_63_27_4_30 (
	.clock (legup_mult_unsigned_63_27_4_30_clock),
	.aclr (legup_mult_unsigned_63_27_4_30_aclr),
	.clken (legup_mult_unsigned_63_27_4_30_clken),
	.dataa (legup_mult_unsigned_63_27_4_30_dataa),
	.datab (legup_mult_unsigned_63_27_4_30_datab),
	.result (legup_mult_unsigned_63_27_4_30_result)
);

defparam
	legup_mult_unsigned_63_27_4_30.widtha = 63,
	legup_mult_unsigned_63_27_4_30.widthb = 27,
	legup_mult_unsigned_63_27_4_30.widthp = 90,
	legup_mult_unsigned_63_27_4_30.pipeline = 4,
	legup_mult_unsigned_63_27_4_30.representation = "UNSIGNED";

/*   %111 = mul i64 %110, 29456521, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_27_3_31 (
	.clock (legup_mult_signed_32_27_3_31_clock),
	.aclr (legup_mult_signed_32_27_3_31_aclr),
	.clken (legup_mult_signed_32_27_3_31_clken),
	.dataa (legup_mult_signed_32_27_3_31_dataa),
	.datab (legup_mult_signed_32_27_3_31_datab),
	.result (legup_mult_signed_32_27_3_31_result)
);

defparam
	legup_mult_signed_32_27_3_31.widtha = 32,
	legup_mult_signed_32_27_3_31.widthb = 27,
	legup_mult_signed_32_27_3_31.widthp = 59,
	legup_mult_signed_32_27_3_31.pipeline = 3,
	legup_mult_signed_32_27_3_31.representation = "SIGNED";

/*   %113 = mul i64 %112, 18127719, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_27_3_32 (
	.clock (legup_mult_signed_32_27_3_32_clock),
	.aclr (legup_mult_signed_32_27_3_32_aclr),
	.clken (legup_mult_signed_32_27_3_32_clken),
	.dataa (legup_mult_signed_32_27_3_32_dataa),
	.datab (legup_mult_signed_32_27_3_32_datab),
	.result (legup_mult_signed_32_27_3_32_result)
);

defparam
	legup_mult_signed_32_27_3_32.widtha = 32,
	legup_mult_signed_32_27_3_32.widthb = 27,
	legup_mult_signed_32_27_3_32.widthp = 59,
	legup_mult_signed_32_27_3_32.pipeline = 3,
	legup_mult_signed_32_27_3_32.representation = "SIGNED";

/*   %115 = mul i64 %114, 8625555, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_26_3_33 (
	.clock (legup_mult_signed_32_26_3_33_clock),
	.aclr (legup_mult_signed_32_26_3_33_aclr),
	.clken (legup_mult_signed_32_26_3_33_clken),
	.dataa (legup_mult_signed_32_26_3_33_dataa),
	.datab (legup_mult_signed_32_26_3_33_datab),
	.result (legup_mult_signed_32_26_3_33_result)
);

defparam
	legup_mult_signed_32_26_3_33.widtha = 32,
	legup_mult_signed_32_26_3_33.widthb = 26,
	legup_mult_signed_32_26_3_33.widthp = 58,
	legup_mult_signed_32_26_3_33.pipeline = 3,
	legup_mult_signed_32_26_3_33.representation = "SIGNED";

/*   %117 = mul i64 %116, 1223507, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5205, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_23_3_34 (
	.clock (legup_mult_signed_32_23_3_34_clock),
	.aclr (legup_mult_signed_32_23_3_34_aclr),
	.clken (legup_mult_signed_32_23_3_34_clken),
	.dataa (legup_mult_signed_32_23_3_34_dataa),
	.datab (legup_mult_signed_32_23_3_34_datab),
	.result (legup_mult_signed_32_23_3_34_result)
);

defparam
	legup_mult_signed_32_23_3_34.widtha = 32,
	legup_mult_signed_32_23_3_34.widthb = 23,
	legup_mult_signed_32_23_3_34.widthp = 55,
	legup_mult_signed_32_23_3_34.pipeline = 3,
	legup_mult_signed_32_23_3_34.representation = "SIGNED";

/*   %119 = mul i64 %118, -4027179, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_24_3_35 (
	.clock (legup_mult_signed_32_24_3_35_clock),
	.aclr (legup_mult_signed_32_24_3_35_aclr),
	.clken (legup_mult_signed_32_24_3_35_clken),
	.dataa (legup_mult_signed_32_24_3_35_dataa),
	.datab (legup_mult_signed_32_24_3_35_datab),
	.result (legup_mult_signed_32_24_3_35_result)
);

defparam
	legup_mult_signed_32_24_3_35.widtha = 32,
	legup_mult_signed_32_24_3_35.widthb = 24,
	legup_mult_signed_32_24_3_35.widthp = 56,
	legup_mult_signed_32_24_3_35.pipeline = 3,
	legup_mult_signed_32_24_3_35.representation = "SIGNED";

/*   %121 = mul i64 %120, -7268700, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_25_4_36 (
	.clock (legup_mult_signed_63_25_4_36_clock),
	.aclr (legup_mult_signed_63_25_4_36_aclr),
	.clken (legup_mult_signed_63_25_4_36_clken),
	.dataa (legup_mult_signed_63_25_4_36_dataa),
	.datab (legup_mult_signed_63_25_4_36_datab),
	.result (legup_mult_signed_63_25_4_36_result)
);

defparam
	legup_mult_signed_63_25_4_36.widtha = 63,
	legup_mult_signed_63_25_4_36.widthb = 25,
	legup_mult_signed_63_25_4_36.widthp = 88,
	legup_mult_signed_63_25_4_36.pipeline = 4,
	legup_mult_signed_63_25_4_36.representation = "SIGNED";

/*   %123 = mul i64 %122, -8788704, !dbg !5192, !MSB !5191, !LSB !5217, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_60_26_4_37 (
	.clock (legup_mult_signed_60_26_4_37_clock),
	.aclr (legup_mult_signed_60_26_4_37_aclr),
	.clken (legup_mult_signed_60_26_4_37_clken),
	.dataa (legup_mult_signed_60_26_4_37_dataa),
	.datab (legup_mult_signed_60_26_4_37_datab),
	.result (legup_mult_signed_60_26_4_37_result)
);

defparam
	legup_mult_signed_60_26_4_37.widtha = 60,
	legup_mult_signed_60_26_4_37.widthb = 26,
	legup_mult_signed_60_26_4_37.widthp = 86,
	legup_mult_signed_60_26_4_37.pipeline = 4,
	legup_mult_signed_60_26_4_37.representation = "SIGNED";

/*   %125 = mul i64 %124, -8965043, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5215, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_26_3_38 (
	.clock (legup_mult_signed_32_26_3_38_clock),
	.aclr (legup_mult_signed_32_26_3_38_aclr),
	.clken (legup_mult_signed_32_26_3_38_clken),
	.dataa (legup_mult_signed_32_26_3_38_dataa),
	.datab (legup_mult_signed_32_26_3_38_datab),
	.result (legup_mult_signed_32_26_3_38_result)
);

defparam
	legup_mult_signed_32_26_3_38.widtha = 32,
	legup_mult_signed_32_26_3_38.widthb = 26,
	legup_mult_signed_32_26_3_38.widthp = 58,
	legup_mult_signed_32_26_3_38.pipeline = 3,
	legup_mult_signed_32_26_3_38.representation = "SIGNED";

/*   %127 = mul i64 %126, -8204580, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_25_4_39 (
	.clock (legup_mult_signed_63_25_4_39_clock),
	.aclr (legup_mult_signed_63_25_4_39_aclr),
	.clken (legup_mult_signed_63_25_4_39_clken),
	.dataa (legup_mult_signed_63_25_4_39_dataa),
	.datab (legup_mult_signed_63_25_4_39_datab),
	.result (legup_mult_signed_63_25_4_39_result)
);

defparam
	legup_mult_signed_63_25_4_39.widtha = 63,
	legup_mult_signed_63_25_4_39.widthb = 25,
	legup_mult_signed_63_25_4_39.widthp = 88,
	legup_mult_signed_63_25_4_39.pipeline = 4,
	legup_mult_signed_63_25_4_39.representation = "SIGNED";

/*   %129 = mul i64 %128, -6898475, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_25_3_40 (
	.clock (legup_mult_signed_32_25_3_40_clock),
	.aclr (legup_mult_signed_32_25_3_40_aclr),
	.clken (legup_mult_signed_32_25_3_40_clken),
	.dataa (legup_mult_signed_32_25_3_40_dataa),
	.datab (legup_mult_signed_32_25_3_40_datab),
	.result (legup_mult_signed_32_25_3_40_result)
);

defparam
	legup_mult_signed_32_25_3_40.widtha = 32,
	legup_mult_signed_32_25_3_40.widthb = 25,
	legup_mult_signed_32_25_3_40.widthp = 57,
	legup_mult_signed_32_25_3_40.pipeline = 3,
	legup_mult_signed_32_25_3_40.representation = "SIGNED";

/*   %131 = mul i64 %130, -5373160, !dbg !5192, !MSB !5191, !LSB !5194, !extendFrom !5212, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_62_25_4_41 (
	.clock (legup_mult_signed_62_25_4_41_clock),
	.aclr (legup_mult_signed_62_25_4_41_aclr),
	.clken (legup_mult_signed_62_25_4_41_clken),
	.dataa (legup_mult_signed_62_25_4_41_dataa),
	.datab (legup_mult_signed_62_25_4_41_datab),
	.result (legup_mult_signed_62_25_4_41_result)
);

defparam
	legup_mult_signed_62_25_4_41.widtha = 62,
	legup_mult_signed_62_25_4_41.widthb = 25,
	legup_mult_signed_62_25_4_41.widthp = 87,
	legup_mult_signed_62_25_4_41.pipeline = 4,
	legup_mult_signed_62_25_4_41.representation = "SIGNED";

/*   %133 = mul i64 %132, -3887393, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_24_3_42 (
	.clock (legup_mult_signed_32_24_3_42_clock),
	.aclr (legup_mult_signed_32_24_3_42_aclr),
	.clken (legup_mult_signed_32_24_3_42_clken),
	.dataa (legup_mult_signed_32_24_3_42_dataa),
	.datab (legup_mult_signed_32_24_3_42_datab),
	.result (legup_mult_signed_32_24_3_42_result)
);

defparam
	legup_mult_signed_32_24_3_42.widtha = 32,
	legup_mult_signed_32_24_3_42.widthb = 24,
	legup_mult_signed_32_24_3_42.widthp = 56,
	legup_mult_signed_32_24_3_42.pipeline = 3,
	legup_mult_signed_32_24_3_42.representation = "SIGNED";

/*   %135 = mul i64 %134, -2588080, !dbg !5192, !MSB !5191, !LSB !5199, !extendFrom !5210, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_61_24_4_43 (
	.clock (legup_mult_signed_61_24_4_43_clock),
	.aclr (legup_mult_signed_61_24_4_43_aclr),
	.clken (legup_mult_signed_61_24_4_43_clken),
	.dataa (legup_mult_signed_61_24_4_43_dataa),
	.datab (legup_mult_signed_61_24_4_43_datab),
	.result (legup_mult_signed_61_24_4_43_result)
);

defparam
	legup_mult_signed_61_24_4_43.widtha = 61,
	legup_mult_signed_61_24_4_43.widthb = 24,
	legup_mult_signed_61_24_4_43.widthp = 85,
	legup_mult_signed_61_24_4_43.pipeline = 4,
	legup_mult_signed_61_24_4_43.representation = "SIGNED";

/*   %137 = mul i64 %136, -1585580, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5206, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_23_4_44 (
	.clock (legup_mult_signed_63_23_4_44_clock),
	.aclr (legup_mult_signed_63_23_4_44_aclr),
	.clken (legup_mult_signed_63_23_4_44_clken),
	.dataa (legup_mult_signed_63_23_4_44_dataa),
	.datab (legup_mult_signed_63_23_4_44_datab),
	.result (legup_mult_signed_63_23_4_44_result)
);

defparam
	legup_mult_signed_63_23_4_44.widtha = 63,
	legup_mult_signed_63_23_4_44.widthb = 23,
	legup_mult_signed_63_23_4_44.widthp = 86,
	legup_mult_signed_63_23_4_44.pipeline = 4,
	legup_mult_signed_63_23_4_44.representation = "SIGNED";

/*   %139 = mul i64 %138, -839361, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5205, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_22_3_45 (
	.clock (legup_mult_signed_32_22_3_45_clock),
	.aclr (legup_mult_signed_32_22_3_45_aclr),
	.clken (legup_mult_signed_32_22_3_45_clken),
	.dataa (legup_mult_signed_32_22_3_45_dataa),
	.datab (legup_mult_signed_32_22_3_45_datab),
	.result (legup_mult_signed_32_22_3_45_result)
);

defparam
	legup_mult_signed_32_22_3_45.widtha = 32,
	legup_mult_signed_32_22_3_45.widthb = 22,
	legup_mult_signed_32_22_3_45.widthp = 54,
	legup_mult_signed_32_22_3_45.pipeline = 3,
	legup_mult_signed_32_22_3_45.representation = "SIGNED";

/*   %141 = mul i64 %140, -420988, !dbg !5192, !MSB !5191, !LSB !5197, !extendFrom !5198, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5199, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_63_21_4_46 (
	.clock (legup_mult_signed_63_21_4_46_clock),
	.aclr (legup_mult_signed_63_21_4_46_aclr),
	.clken (legup_mult_signed_63_21_4_46_clken),
	.dataa (legup_mult_signed_63_21_4_46_dataa),
	.datab (legup_mult_signed_63_21_4_46_datab),
	.result (legup_mult_signed_63_21_4_46_result)
);

defparam
	legup_mult_signed_63_21_4_46.widtha = 63,
	legup_mult_signed_63_21_4_46.widthb = 21,
	legup_mult_signed_63_21_4_46.widthp = 84,
	legup_mult_signed_63_21_4_46.pipeline = 4,
	legup_mult_signed_63_21_4_46.representation = "SIGNED";

/*   %143 = mul i64 %142, -112451, !dbg !5192, !MSB !5191, !LSB !5186, !extendFrom !5193, !legup.pipeline.start_time !5186, !legup.pipeline.avail_time !5194, !legup.pipeline.stage !5186*/
hw_fir_legup_mult legup_mult_signed_32_19_3_47 (
	.clock (legup_mult_signed_32_19_3_47_clock),
	.aclr (legup_mult_signed_32_19_3_47_aclr),
	.clken (legup_mult_signed_32_19_3_47_clken),
	.dataa (legup_mult_signed_32_19_3_47_dataa),
	.datab (legup_mult_signed_32_19_3_47_datab),
	.result (legup_mult_signed_32_19_3_47_result)
);

defparam
	legup_mult_signed_32_19_3_47.widtha = 32,
	legup_mult_signed_32_19_3_47.widthb = 19,
	legup_mult_signed_32_19_3_47.widthp = 51,
	legup_mult_signed_32_19_3_47.pipeline = 3,
	legup_mult_signed_32_19_3_47.representation = "SIGNED";


always @(posedge clk) begin
	if ((start & ready)) begin
		input_var_reg <= input_var;
	end
end
always @(*) begin
		hw_fir_init_check_0 = hw_fir_previous_a46_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_1 = hw_fir_previous_a45_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_2 = hw_fir_previous_a44_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_3 = hw_fir_previous_a43_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_4 = hw_fir_previous_a42_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_5 = hw_fir_previous_a41_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_6 = hw_fir_previous_a40_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_7 = hw_fir_previous_a39_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_8 = hw_fir_previous_a38_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_9 = hw_fir_previous_a37_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_10 = hw_fir_previous_a36_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_11 = hw_fir_previous_a35_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_12 = hw_fir_previous_a34_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_13 = hw_fir_previous_a33_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_14 = hw_fir_previous_a32_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_15 = hw_fir_previous_a31_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_16 = hw_fir_previous_a30_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_17 = hw_fir_previous_a29_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_18 = hw_fir_previous_a28_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_19 = hw_fir_previous_a27_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_20 = hw_fir_previous_a26_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_21 = hw_fir_previous_a25_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_22 = hw_fir_previous_a24_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_23 = hw_fir_previous_a23_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_24 = hw_fir_previous_a22_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_25 = hw_fir_previous_a21_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_26 = hw_fir_previous_a20_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_27 = hw_fir_previous_a19_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_28 = hw_fir_previous_a18_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_29 = hw_fir_previous_a17_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_30 = hw_fir_previous_a16_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_31 = hw_fir_previous_a15_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_32 = hw_fir_previous_a14_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_33 = hw_fir_previous_a13_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_34 = hw_fir_previous_a12_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_35 = hw_fir_previous_a11_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_36 = hw_fir_previous_a10_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_37 = hw_fir_previous_a9_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_38 = hw_fir_previous_a8_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_39 = hw_fir_previous_a7_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_40 = hw_fir_previous_a6_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_41 = hw_fir_previous_a5_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_42 = hw_fir_previous_a4_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_43 = hw_fir_previous_a3_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_44 = hw_fir_previous_a2_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_45 = hw_fir_previous_a1_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_46 = hw_fir_previous_a0_inferred_reg;
end
always @(*) begin
		hw_fir_init_check_47 = $signed(input_var_reg);
end
always @(*) begin
	hw_fir_init_check_48 = legup_mult_hw_fir_init_check_48_out;
end
always @(*) begin
		hw_fir_init_check_bit_select47 = hw_fir_init_check_48_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_49 = $signed(hw_fir_init_check_46);
end
always @(*) begin
	hw_fir_init_check_50 = legup_mult_hw_fir_init_check_50_out;
end
always @(*) begin
		hw_fir_init_check_bit_select46 = hw_fir_init_check_50_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_51 = ($signed({{1{hw_fir_init_check_bit_select46[22]}},hw_fir_init_check_bit_select46}) + $signed({{3{hw_fir_init_check_bit_select47_reg_stage4[20]}},hw_fir_init_check_bit_select47_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_52 = $signed(hw_fir_init_check_45);
end
always @(*) begin
	hw_fir_init_check_53 = legup_mult_hw_fir_init_check_53_out;
end
always @(*) begin
		hw_fir_init_check_bit_select45 = hw_fir_init_check_53_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_54 = $signed(hw_fir_init_check_44);
end
always @(*) begin
	hw_fir_init_check_55 = legup_mult_hw_fir_init_check_55_out;
end
always @(*) begin
		hw_fir_init_check_bit_select44 = hw_fir_init_check_55_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_65 = ($signed({{2{hw_fir_init_check_bit_select45_reg_stage4[23]}},hw_fir_init_check_bit_select45_reg_stage4}) + $signed({{1{hw_fir_init_check_bit_select44[24]}},hw_fir_init_check_bit_select44}));
end
always @(*) begin
		hw_fir_init_check_56 = $signed(hw_fir_init_check_43);
end
always @(*) begin
	hw_fir_init_check_57 = legup_mult_hw_fir_init_check_57_out;
end
always @(*) begin
		hw_fir_init_check_bit_select43 = hw_fir_init_check_57_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_58 = $signed(hw_fir_init_check_42);
end
always @(*) begin
	hw_fir_init_check_59 = legup_mult_hw_fir_init_check_59_out;
end
always @(*) begin
		hw_fir_init_check_bit_select42 = hw_fir_init_check_59_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_60 = $signed(hw_fir_init_check_41);
end
always @(*) begin
	hw_fir_init_check_61 = legup_mult_hw_fir_init_check_61_out;
end
always @(*) begin
		hw_fir_init_check_bit_select41 = hw_fir_init_check_61_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_80 = ($signed({{2{hw_fir_init_check_bit_select43[25]}},hw_fir_init_check_bit_select43}) + $signed({{1{hw_fir_init_check_bit_select41[26]}},hw_fir_init_check_bit_select41}));
end
always @(*) begin
		hw_fir_init_check_62 = $signed(hw_fir_init_check_40);
end
always @(*) begin
	hw_fir_init_check_63 = legup_mult_hw_fir_init_check_63_out;
end
always @(*) begin
		hw_fir_init_check_bit_select40 = hw_fir_init_check_63_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_85 = ($signed({{2{hw_fir_init_check_bit_select42[25]}},hw_fir_init_check_bit_select42}) + $signed({{1{hw_fir_init_check_bit_select40[26]}},hw_fir_init_check_bit_select40}));
end
always @(*) begin
		hw_fir_init_check_64 = $signed(hw_fir_init_check_39);
end
always @(*) begin
	hw_fir_init_check_65 = legup_mult_hw_fir_init_check_65_out;
end
always @(*) begin
		hw_fir_init_check_bit_select39 = hw_fir_init_check_65_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_66 = $signed(hw_fir_init_check_38);
end
always @(*) begin
	hw_fir_init_check_67 = legup_mult_hw_fir_init_check_67_out;
end
always @(*) begin
		hw_fir_init_check_bit_select38 = hw_fir_init_check_67_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_68 = $signed(hw_fir_init_check_37);
end
always @(*) begin
	hw_fir_init_check_69 = legup_mult_hw_fir_init_check_69_out;
end
always @(*) begin
		hw_fir_init_check_bit_select37 = hw_fir_init_check_69_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_70 = $signed(hw_fir_init_check_36);
end
always @(*) begin
	hw_fir_init_check_71 = legup_mult_hw_fir_init_check_71_out;
end
always @(*) begin
		hw_fir_init_check_bit_select36 = hw_fir_init_check_71_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_72 = $signed(hw_fir_init_check_35);
end
always @(*) begin
	hw_fir_init_check_73 = legup_mult_hw_fir_init_check_73_out;
end
always @(*) begin
		hw_fir_init_check_bit_select35 = hw_fir_init_check_73_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_110 = ($signed({{1{hw_fir_init_check_bit_select39[26]}},hw_fir_init_check_bit_select39}) + $signed({{2{hw_fir_init_check_bit_select35_reg_stage4[25]}},hw_fir_init_check_bit_select35_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_74 = $signed(hw_fir_init_check_34);
end
always @(*) begin
	hw_fir_init_check_75 = legup_mult_hw_fir_init_check_75_out;
end
always @(*) begin
		hw_fir_init_check_bit_select34 = hw_fir_init_check_75_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_115 = ($signed({{1{hw_fir_init_check_bit_select38[27]}},hw_fir_init_check_bit_select38}) + $signed({{5{hw_fir_init_check_bit_select34[23]}},hw_fir_init_check_bit_select34}));
end
always @(*) begin
		hw_fir_init_check_76 = $signed(hw_fir_init_check_33);
end
always @(*) begin
	hw_fir_init_check_77 = legup_mult_hw_fir_init_check_77_out;
end
always @(*) begin
		hw_fir_init_check_bit_select33 = hw_fir_init_check_77_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_120 = ($signed({{1{hw_fir_init_check_bit_select37[27]}},hw_fir_init_check_bit_select37}) + $signed({{2{hw_fir_init_check_bit_select33_reg_stage4[26]}},hw_fir_init_check_bit_select33_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_78 = $signed(hw_fir_init_check_32);
end
always @(*) begin
	hw_fir_init_check_79 = legup_mult_hw_fir_init_check_79_out;
end
always @(*) begin
		hw_fir_init_check_bit_select32 = hw_fir_init_check_79_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_125 = ($signed({{2{hw_fir_init_check_bit_select36[26]}},hw_fir_init_check_bit_select36}) + $signed({{1{hw_fir_init_check_bit_select32_reg_stage4[27]}},hw_fir_init_check_bit_select32_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_80 = $signed(hw_fir_init_check_31);
end
always @(*) begin
	hw_fir_init_check_81 = legup_mult_hw_fir_init_check_81_out;
end
always @(*) begin
		hw_fir_init_check_bit_select31 = hw_fir_init_check_81_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_82 = $signed(hw_fir_init_check_30);
end
always @(*) begin
	hw_fir_init_check_83 = legup_mult_hw_fir_init_check_83_out;
end
always @(*) begin
		hw_fir_init_check_bit_select30 = hw_fir_init_check_83_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_84 = $signed(hw_fir_init_check_29);
end
always @(*) begin
	hw_fir_init_check_85 = legup_mult_hw_fir_init_check_85_out;
end
always @(*) begin
		hw_fir_init_check_bit_select29 = hw_fir_init_check_85_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_86 = $signed(hw_fir_init_check_28);
end
always @(*) begin
	hw_fir_init_check_87 = legup_mult_hw_fir_init_check_87_out;
end
always @(*) begin
		hw_fir_init_check_bit_select28 = hw_fir_init_check_87_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_88 = $signed(hw_fir_init_check_27);
end
always @(*) begin
	hw_fir_init_check_89 = legup_mult_hw_fir_init_check_89_out;
end
always @(*) begin
		hw_fir_init_check_bit_select27 = hw_fir_init_check_89_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_90 = $signed(hw_fir_init_check_26);
end
always @(*) begin
	hw_fir_init_check_91 = legup_mult_hw_fir_init_check_91_out;
end
always @(*) begin
		hw_fir_init_check_bit_select26 = hw_fir_init_check_91_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_92 = $signed(hw_fir_init_check_25);
end
always @(*) begin
	hw_fir_init_check_93 = legup_mult_hw_fir_init_check_93_out;
end
always @(*) begin
		hw_fir_init_check_bit_select25 = hw_fir_init_check_93_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_94 = $signed(hw_fir_init_check_24);
end
always @(*) begin
	hw_fir_init_check_95 = legup_mult_hw_fir_init_check_95_out;
end
always @(*) begin
		hw_fir_init_check_bit_select24 = hw_fir_init_check_95_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_96 = $signed(hw_fir_init_check_23);
end
always @(*) begin
	hw_fir_init_check_97 = legup_mult_hw_fir_init_check_97_out;
end
always @(*) begin
		hw_fir_init_check_bit_select23 = hw_fir_init_check_97_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_220 = ($signed({{3{hw_fir_init_check_bit_select31_reg_stage4[27]}},hw_fir_init_check_bit_select31_reg_stage4}) + $signed({{1{hw_fir_init_check_bit_select23[29]}},hw_fir_init_check_bit_select23}));
end
always @(*) begin
		hw_fir_init_check_98 = $signed(hw_fir_init_check_22);
end
always @(*) begin
	hw_fir_init_check_99 = legup_mult_hw_fir_init_check_99_out;
end
always @(*) begin
		hw_fir_init_check_bit_select22 = hw_fir_init_check_99_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_275 = ($signed({{2{hw_fir_init_check_bit_select30[28]}},hw_fir_init_check_bit_select30}) + $signed({{1{hw_fir_init_check_bit_select22_reg_stage4[29]}},hw_fir_init_check_bit_select22_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_100 = $signed(hw_fir_init_check_21);
end
always @(*) begin
	hw_fir_init_check_101 = legup_mult_hw_fir_init_check_101_out;
end
always @(*) begin
		hw_fir_init_check_bit_select21 = hw_fir_init_check_101_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa = ($signed({{2{hw_fir_init_check_bit_select29[28]}},hw_fir_init_check_bit_select29}) + $signed({{1{hw_fir_init_check_bit_select21[29]}},hw_fir_init_check_bit_select21}));
end
always @(*) begin
		hw_fir_init_check_102 = $signed(hw_fir_init_check_20);
end
always @(*) begin
	hw_fir_init_check_103 = legup_mult_hw_fir_init_check_103_out;
end
always @(*) begin
		hw_fir_init_check_bit_select20 = hw_fir_init_check_103_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_0 = ($signed({{1{hw_fir_init_check_bit_select28[29]}},hw_fir_init_check_bit_select28}) + $signed({{1{hw_fir_init_check_bit_select20_reg_stage4[29]}},hw_fir_init_check_bit_select20_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_104 = $signed(hw_fir_init_check_19);
end
always @(*) begin
	hw_fir_init_check_105 = legup_mult_hw_fir_init_check_105_out;
end
always @(*) begin
		hw_fir_init_check_bit_select19 = hw_fir_init_check_105_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_1 = ($signed({{1{hw_fir_init_check_bit_select27_reg_stage4[29]}},hw_fir_init_check_bit_select27_reg_stage4}) + $signed({{1{hw_fir_init_check_bit_select19[29]}},hw_fir_init_check_bit_select19}));
end
always @(*) begin
		hw_fir_init_check_106 = $signed(hw_fir_init_check_18);
end
always @(*) begin
	hw_fir_init_check_107 = legup_mult_hw_fir_init_check_107_out;
end
always @(*) begin
		hw_fir_init_check_bit_select18 = hw_fir_init_check_107_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_2 = ($signed({{1{hw_fir_init_check_bit_select26[29]}},hw_fir_init_check_bit_select26}) + $signed({{2{hw_fir_init_check_bit_select18[28]}},hw_fir_init_check_bit_select18}));
end
always @(*) begin
		hw_fir_init_check_108 = $signed(hw_fir_init_check_17);
end
always @(*) begin
	hw_fir_init_check_109 = legup_mult_hw_fir_init_check_109_out;
end
always @(*) begin
		hw_fir_init_check_bit_select17 = hw_fir_init_check_109_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_3 = ($signed({{1{hw_fir_init_check_bit_select25_reg_stage4[29]}},hw_fir_init_check_bit_select25_reg_stage4}) + $signed({{2{hw_fir_init_check_bit_select17[28]}},hw_fir_init_check_bit_select17}));
end
always @(*) begin
		hw_fir_init_check_110 = $signed(hw_fir_init_check_16);
end
always @(*) begin
	hw_fir_init_check_111 = legup_mult_hw_fir_init_check_111_out;
end
always @(*) begin
		hw_fir_init_check_bit_select16 = hw_fir_init_check_111_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_4 = ($signed({{1{hw_fir_init_check_bit_select24[29]}},hw_fir_init_check_bit_select24}) + $signed({{3{hw_fir_init_check_bit_select16_reg_stage4[27]}},hw_fir_init_check_bit_select16_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_112 = $signed(hw_fir_init_check_15);
end
always @(*) begin
	hw_fir_init_check_113 = legup_mult_hw_fir_init_check_113_out;
end
always @(*) begin
		hw_fir_init_check_bit_select15 = hw_fir_init_check_113_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_5 = ($signed({{5{hw_fir_init_check_51[23]}},hw_fir_init_check_51}) + $signed({{1{hw_fir_init_check_bit_select15_reg_stage4[27]}},hw_fir_init_check_bit_select15_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_114 = $signed(hw_fir_init_check_14);
end
always @(*) begin
	hw_fir_init_check_115 = legup_mult_hw_fir_init_check_115_out;
end
always @(*) begin
		hw_fir_init_check_bit_select14 = hw_fir_init_check_115_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_6 = ($signed({{2{hw_fir_init_check_newEarly_65[25]}},hw_fir_init_check_newEarly_65}) + $signed({{1{hw_fir_init_check_bit_select14_reg_stage4[26]}},hw_fir_init_check_bit_select14_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_116 = $signed(hw_fir_init_check_13);
end
always @(*) begin
	hw_fir_init_check_117 = legup_mult_hw_fir_init_check_117_out;
end
always @(*) begin
		hw_fir_init_check_bit_select13 = hw_fir_init_check_117_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_7 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_80[27]}},hw_fir_init_check_newEarly_newEarly_80}) + $signed({{5{hw_fir_init_check_bit_select13_reg_stage4[23]}},hw_fir_init_check_bit_select13_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_118 = $signed(hw_fir_init_check_12);
end
always @(*) begin
	hw_fir_init_check_119 = legup_mult_hw_fir_init_check_119_out;
end
always @(*) begin
		hw_fir_init_check_bit_select12 = hw_fir_init_check_119_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_85[27]}},hw_fir_init_check_newEarly_newEarly_85}) + $signed({{3{hw_fir_init_check_bit_select12[25]}},hw_fir_init_check_bit_select12}));
end
always @(*) begin
		hw_fir_init_check_120 = $signed(hw_fir_init_check_11);
end
always @(*) begin
	hw_fir_init_check_121 = legup_mult_hw_fir_init_check_121_out;
end
always @(*) begin
		hw_fir_init_check_bit_select11 = hw_fir_init_check_121_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_9 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_110[27]}},hw_fir_init_check_newEarly_newEarly_newEarly_110}) + $signed({{2{hw_fir_init_check_bit_select11[26]}},hw_fir_init_check_bit_select11}));
end
always @(*) begin
		hw_fir_init_check_122 = $signed(hw_fir_init_check_10);
end
always @(*) begin
	hw_fir_init_check_123 = legup_mult_hw_fir_init_check_123_out;
end
always @(*) begin
		hw_fir_init_check_bit_select10 = hw_fir_init_check_123_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_10 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_115_reg_stage4[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_115_reg_stage4}) + $signed({{2{hw_fir_init_check_bit_select10[27]}},hw_fir_init_check_bit_select10}));
end
always @(*) begin
		hw_fir_init_check_124 = $signed(hw_fir_init_check_9);
end
always @(*) begin
	hw_fir_init_check_125 = legup_mult_hw_fir_init_check_125_out;
end
always @(*) begin
		hw_fir_init_check_bit_select9 = hw_fir_init_check_125_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_11 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_120[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_120}) + $signed({{2{hw_fir_init_check_bit_select9_reg_stage4[27]}},hw_fir_init_check_bit_select9_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_126 = $signed(hw_fir_init_check_8);
end
always @(*) begin
	hw_fir_init_check_127 = legup_mult_hw_fir_init_check_127_out;
end
always @(*) begin
		hw_fir_init_check_bit_select8 = hw_fir_init_check_127_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_12 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_125[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_125}) + $signed({{3{hw_fir_init_check_bit_select8[26]}},hw_fir_init_check_bit_select8}));
end
always @(*) begin
		hw_fir_init_check_128 = $signed(hw_fir_init_check_7);
end
always @(*) begin
	hw_fir_init_check_129 = legup_mult_hw_fir_init_check_129_out;
end
always @(*) begin
		hw_fir_init_check_bit_select7 = hw_fir_init_check_129_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_13 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_220[30]}},hw_fir_init_check_newEarly_newEarly_220}) + $signed({{5{hw_fir_init_check_bit_select7_reg_stage4[26]}},hw_fir_init_check_bit_select7_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa = ($signed({{3{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_5[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_5}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_13);
end
always @(*) begin
		hw_fir_init_check_130 = $signed(hw_fir_init_check_6);
end
always @(*) begin
	hw_fir_init_check_131 = legup_mult_hw_fir_init_check_131_out;
end
always @(*) begin
		hw_fir_init_check_bit_select6 = hw_fir_init_check_131_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_14 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_275[30]}},hw_fir_init_check_newEarly_newEarly_275}) + $signed({{5{hw_fir_init_check_bit_select6[26]}},hw_fir_init_check_bit_select6}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_15 = ($signed({{4{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_6[27]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_6}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_14);
end
always @(*) begin
		hw_fir_init_check_132 = $signed(hw_fir_init_check_5);
end
always @(*) begin
	hw_fir_init_check_133 = legup_mult_hw_fir_init_check_133_out;
end
always @(*) begin
		hw_fir_init_check_bit_select5 = hw_fir_init_check_133_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa}) + $signed({{6{hw_fir_init_check_bit_select5[25]}},hw_fir_init_check_bit_select5}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_17 = ($signed({{3{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_7[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_7}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16_reg_stage4);
end
always @(*) begin
		hw_fir_init_check_134 = $signed(hw_fir_init_check_4);
end
always @(*) begin
	hw_fir_init_check_135 = legup_mult_hw_fir_init_check_135_out;
end
always @(*) begin
		hw_fir_init_check_bit_select4 = hw_fir_init_check_135_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_18 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_0[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_0}) + $signed({{6{hw_fir_init_check_bit_select4[25]}},hw_fir_init_check_bit_select4}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_19 = ($signed({{3{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8_reg_stage4[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8_reg_stage4}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_18);
end
always @(*) begin
		hw_fir_init_check_136 = $signed(hw_fir_init_check_3);
end
always @(*) begin
	hw_fir_init_check_137 = legup_mult_hw_fir_init_check_137_out;
end
always @(*) begin
		hw_fir_init_check_bit_select3 = hw_fir_init_check_137_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_20 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_1[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_1}) + $signed({{7{hw_fir_init_check_bit_select3[24]}},hw_fir_init_check_bit_select3}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_21 = ($signed({{3{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_9[28]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_9}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_20);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_270 = (hw_fir_init_check_newCurOp_newEarly_newEarly_newEa + hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_21);
end
always @(*) begin
		hw_fir_init_check_138 = $signed(hw_fir_init_check_2);
end
always @(*) begin
	hw_fir_init_check_139 = legup_mult_hw_fir_init_check_139_out;
end
always @(*) begin
		hw_fir_init_check_bit_select2 = hw_fir_init_check_139_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_2[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_2}) + $signed({{8{hw_fir_init_check_bit_select2[23]}},hw_fir_init_check_bit_select2}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_23 = ($signed({{2{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_10[29]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_10}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22_reg_stage4);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_275 = (hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_15 + hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_23);
end
always @(*) begin
		hw_fir_init_check_140 = $signed(hw_fir_init_check_1);
end
always @(*) begin
	hw_fir_init_check_141 = legup_mult_hw_fir_init_check_141_out;
end
always @(*) begin
		hw_fir_init_check_bit_select1 = hw_fir_init_check_141_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_24 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_3[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_3}) + $signed({{9{hw_fir_init_check_bit_select1[22]}},hw_fir_init_check_bit_select1}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_25 = ($signed({{2{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_11[29]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_11}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_24);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_280 = (hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_17 + hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_25);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_280 = (hw_fir_init_check_newCurOp_newEarly_newEarly_270 + hw_fir_init_check_newCurOp_newEarly_newEarly_280);
end
always @(*) begin
		hw_fir_init_check_142 = $signed(hw_fir_init_check_0);
end
always @(*) begin
	hw_fir_init_check_143 = legup_mult_hw_fir_init_check_143_out;
end
always @(*) begin
		hw_fir_init_check_bit_select = hw_fir_init_check_143_width_extended[61:30];
end
always @(*) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_26 = ($signed({{1{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_4[30]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_4}) + $signed({{11{hw_fir_init_check_bit_select_reg_stage4[20]}},hw_fir_init_check_bit_select_reg_stage4}));
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_27 = ($signed({{2{hw_fir_init_check_newEarly_newEarly_newEarly_newEa_12[29]}},hw_fir_init_check_newEarly_newEarly_newEarly_newEa_12}) + hw_fir_init_check_newEarly_newEarly_newEarly_newEa_26);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_newEarly_285 = (hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_19 + hw_fir_init_check_newCurOp_newEarly_newEarly_newEa_27);
end
always @(*) begin
		hw_fir_init_check_newCurOp_newEarly_285 = (hw_fir_init_check_newCurOp_newEarly_newEarly_275 + hw_fir_init_check_newCurOp_newEarly_newEarly_285);
end
always @(*) begin
		hw_fir_init_check_newCurOp_285 = (hw_fir_init_check_newCurOp_newEarly_280 + hw_fir_init_check_newCurOp_newEarly_285);
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a0_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a0_inferred_reg <= input_var_reg;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a1_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a1_inferred_reg <= hw_fir_init_check_46;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a2_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a2_inferred_reg <= hw_fir_init_check_45;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a3_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a3_inferred_reg <= hw_fir_init_check_44;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a4_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a4_inferred_reg <= hw_fir_init_check_43;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a5_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a5_inferred_reg <= hw_fir_init_check_42;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a6_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a6_inferred_reg <= hw_fir_init_check_41;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a7_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a7_inferred_reg <= hw_fir_init_check_40;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a8_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a8_inferred_reg <= hw_fir_init_check_39;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a9_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a9_inferred_reg <= hw_fir_init_check_38;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a10_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a10_inferred_reg <= hw_fir_init_check_37;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a11_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a11_inferred_reg <= hw_fir_init_check_36;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a12_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a12_inferred_reg <= hw_fir_init_check_35;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a13_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a13_inferred_reg <= hw_fir_init_check_34;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a14_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a14_inferred_reg <= hw_fir_init_check_33;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a15_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a15_inferred_reg <= hw_fir_init_check_32;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a16_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a16_inferred_reg <= hw_fir_init_check_31;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a17_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a17_inferred_reg <= hw_fir_init_check_30;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a18_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a18_inferred_reg <= hw_fir_init_check_29;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a19_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a19_inferred_reg <= hw_fir_init_check_28;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a20_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a20_inferred_reg <= hw_fir_init_check_27;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a21_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a21_inferred_reg <= hw_fir_init_check_26;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a22_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a22_inferred_reg <= hw_fir_init_check_25;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a23_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a23_inferred_reg <= hw_fir_init_check_24;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a24_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a24_inferred_reg <= hw_fir_init_check_23;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a25_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a25_inferred_reg <= hw_fir_init_check_22;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a26_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a26_inferred_reg <= hw_fir_init_check_21;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a27_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a27_inferred_reg <= hw_fir_init_check_20;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a28_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a28_inferred_reg <= hw_fir_init_check_19;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a29_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a29_inferred_reg <= hw_fir_init_check_18;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a30_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a30_inferred_reg <= hw_fir_init_check_17;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a31_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a31_inferred_reg <= hw_fir_init_check_16;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a32_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a32_inferred_reg <= hw_fir_init_check_15;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a33_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a33_inferred_reg <= hw_fir_init_check_14;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a34_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a34_inferred_reg <= hw_fir_init_check_13;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a35_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a35_inferred_reg <= hw_fir_init_check_12;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a36_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a36_inferred_reg <= hw_fir_init_check_11;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a37_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a37_inferred_reg <= hw_fir_init_check_10;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a38_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a38_inferred_reg <= hw_fir_init_check_9;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a39_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a39_inferred_reg <= hw_fir_init_check_8;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a40_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a40_inferred_reg <= hw_fir_init_check_7;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a41_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a41_inferred_reg <= hw_fir_init_check_6;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a42_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a42_inferred_reg <= hw_fir_init_check_5;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a43_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a43_inferred_reg <= hw_fir_init_check_4;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a44_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a44_inferred_reg <= hw_fir_init_check_3;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a45_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a45_inferred_reg <= hw_fir_init_check_2;
	end
end
always @(posedge clk) begin
	if (reset) begin
		hw_fir_previous_a46_inferred_reg <= 32'd0;
	end
	if (hw_fir_state_enable_0) begin
		hw_fir_previous_a46_inferred_reg <= hw_fir_init_check_1;
	end
end
always @(posedge clk) begin
	if (~(hw_fir_state_stall_0)) begin
		hw_fir_valid_bit_0 <= (hw_fir_II_counter & start);
	end
	if (reset) begin
		hw_fir_valid_bit_0 <= 1'd0;
	end
end
assign hw_fir_state_stall_0 = 1'd0;
always @(*) begin
	hw_fir_state_enable_0 = (hw_fir_valid_bit_0 & ~(hw_fir_state_stall_0));
end
always @(posedge clk) begin
	if (~(hw_fir_state_stall_1)) begin
		hw_fir_valid_bit_1 <= hw_fir_state_enable_0;
	end
	if (reset) begin
		hw_fir_valid_bit_1 <= 1'd0;
	end
end
assign hw_fir_state_stall_1 = 1'd0;
always @(*) begin
	hw_fir_state_enable_1 = (hw_fir_valid_bit_1 & ~(hw_fir_state_stall_1));
end
always @(posedge clk) begin
	if (~(hw_fir_state_stall_2)) begin
		hw_fir_valid_bit_2 <= hw_fir_state_enable_1;
	end
	if (reset) begin
		hw_fir_valid_bit_2 <= 1'd0;
	end
end
assign hw_fir_state_stall_2 = 1'd0;
always @(*) begin
	hw_fir_state_enable_2 = (hw_fir_valid_bit_2 & ~(hw_fir_state_stall_2));
end
always @(posedge clk) begin
	if (~(hw_fir_state_stall_3)) begin
		hw_fir_valid_bit_3 <= hw_fir_state_enable_2;
	end
	if (reset) begin
		hw_fir_valid_bit_3 <= 1'd0;
	end
end
assign hw_fir_state_stall_3 = 1'd0;
always @(*) begin
	hw_fir_state_enable_3 = (hw_fir_valid_bit_3 & ~(hw_fir_state_stall_3));
end
always @(posedge clk) begin
	if (~(hw_fir_state_stall_4)) begin
		hw_fir_valid_bit_4 <= hw_fir_state_enable_3;
	end
	if (reset) begin
		hw_fir_valid_bit_4 <= 1'd0;
	end
end
assign hw_fir_state_stall_4 = 1'd0;
always @(*) begin
	hw_fir_state_enable_4 = (hw_fir_valid_bit_4 & ~(hw_fir_state_stall_4));
end
always @(posedge clk) begin
	hw_fir_II_counter <= 1'd1;
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select47_reg_stage4 <= hw_fir_init_check_bit_select47;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select45_reg_stage4 <= hw_fir_init_check_bit_select45;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select35_reg_stage4 <= hw_fir_init_check_bit_select35;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_115_reg_stage4 <= hw_fir_init_check_newEarly_newEarly_newEarly_115;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select33_reg_stage4 <= hw_fir_init_check_bit_select33;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select32_reg_stage4 <= hw_fir_init_check_bit_select32;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select31_reg_stage4 <= hw_fir_init_check_bit_select31;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select27_reg_stage4 <= hw_fir_init_check_bit_select27;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select25_reg_stage4 <= hw_fir_init_check_bit_select25;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select22_reg_stage4 <= hw_fir_init_check_bit_select22;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select20_reg_stage4 <= hw_fir_init_check_bit_select20;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select16_reg_stage4 <= hw_fir_init_check_bit_select16;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select15_reg_stage4 <= hw_fir_init_check_bit_select15;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select14_reg_stage4 <= hw_fir_init_check_bit_select14;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select13_reg_stage4 <= hw_fir_init_check_bit_select13;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8_reg_stage4 <= hw_fir_init_check_newEarly_newEarly_newEarly_newEa_8;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select9_reg_stage4 <= hw_fir_init_check_bit_select9;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select7_reg_stage4 <= hw_fir_init_check_bit_select7;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16_reg_stage4 <= hw_fir_init_check_newEarly_newEarly_newEarly_newEa_16;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22_reg_stage4 <= hw_fir_init_check_newEarly_newEarly_newEarly_newEa_22;
	end
end
always @(posedge clk) begin
	if (hw_fir_state_enable_3) begin
		hw_fir_init_check_bit_select_reg_stage4 <= hw_fir_init_check_bit_select;
	end
end
always @(*) begin
	legup_mult_signed_32_19_3_0_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_19_3_0_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_19_3_0_clken = legup_mult_hw_fir_init_check_48_en;
end
always @(*) begin
	legup_mult_signed_32_19_3_0_dataa = hw_fir_init_check_47;
end
assign legup_mult_signed_32_19_3_0_datab = -64'd112451;
always @(*) begin
	legup_mult_hw_fir_init_check_48_out_actual = legup_mult_signed_32_19_3_0_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_48_out = $signed(legup_mult_hw_fir_init_check_48_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_48_en = legup_mult_hw_fir_init_check_48_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_48_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_48_width_extended = {{11{hw_fir_init_check_48[50]}},hw_fir_init_check_48};
end
always @(*) begin
	legup_mult_signed_63_21_4_1_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_21_4_1_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_21_4_1_clken = legup_mult_hw_fir_init_check_50_en;
end
always @(*) begin
	legup_mult_signed_63_21_4_1_dataa = {1'd0,hw_fir_init_check_49};
end
assign legup_mult_signed_63_21_4_1_datab = -64'd420988;
always @(*) begin
	legup_mult_hw_fir_init_check_50_out_actual = legup_mult_signed_63_21_4_1_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_50_out = legup_mult_hw_fir_init_check_50_out_actual[52:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_50_en = legup_mult_hw_fir_init_check_50_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_50_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_50_width_extended = {{9{hw_fir_init_check_50[52]}},hw_fir_init_check_50};
end
always @(*) begin
	legup_mult_signed_32_22_3_2_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_22_3_2_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_22_3_2_clken = legup_mult_hw_fir_init_check_53_en;
end
always @(*) begin
	legup_mult_signed_32_22_3_2_dataa = hw_fir_init_check_52;
end
assign legup_mult_signed_32_22_3_2_datab = -64'd839361;
always @(*) begin
	legup_mult_hw_fir_init_check_53_out_actual = legup_mult_signed_32_22_3_2_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_53_out = $signed(legup_mult_hw_fir_init_check_53_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_53_en = legup_mult_hw_fir_init_check_53_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_53_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_53_width_extended = {{8{hw_fir_init_check_53[53]}},hw_fir_init_check_53};
end
always @(*) begin
	legup_mult_signed_63_23_4_3_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_23_4_3_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_23_4_3_clken = legup_mult_hw_fir_init_check_55_en;
end
always @(*) begin
	legup_mult_signed_63_23_4_3_dataa = {1'd0,hw_fir_init_check_54};
end
assign legup_mult_signed_63_23_4_3_datab = -64'd1585580;
always @(*) begin
	legup_mult_hw_fir_init_check_55_out_actual = legup_mult_signed_63_23_4_3_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_55_out = legup_mult_hw_fir_init_check_55_out_actual[54:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_55_en = legup_mult_hw_fir_init_check_55_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_55_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_55_width_extended = {{7{hw_fir_init_check_55[54]}},hw_fir_init_check_55};
end
always @(*) begin
	legup_mult_signed_61_24_4_4_clock = clk;
end
always @(*) begin
	legup_mult_signed_61_24_4_4_aclr = reset;
end
always @(*) begin
	legup_mult_signed_61_24_4_4_clken = legup_mult_hw_fir_init_check_57_en;
end
always @(*) begin
	legup_mult_signed_61_24_4_4_dataa = {1'd0,hw_fir_init_check_56};
end
assign legup_mult_signed_61_24_4_4_datab = -64'd2588080;
always @(*) begin
	legup_mult_hw_fir_init_check_57_out_actual = legup_mult_signed_61_24_4_4_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_57_out = legup_mult_hw_fir_init_check_57_out_actual[55:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_57_en = legup_mult_hw_fir_init_check_57_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_57_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_57_width_extended = {{6{hw_fir_init_check_57[55]}},hw_fir_init_check_57};
end
always @(*) begin
	legup_mult_signed_32_24_3_5_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_24_3_5_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_24_3_5_clken = legup_mult_hw_fir_init_check_59_en;
end
always @(*) begin
	legup_mult_signed_32_24_3_5_dataa = hw_fir_init_check_58;
end
assign legup_mult_signed_32_24_3_5_datab = -64'd3887393;
always @(*) begin
	legup_mult_hw_fir_init_check_59_out_actual = legup_mult_signed_32_24_3_5_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_59_out = $signed(legup_mult_hw_fir_init_check_59_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_59_en = legup_mult_hw_fir_init_check_59_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_59_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_59_width_extended = {{6{hw_fir_init_check_59[55]}},hw_fir_init_check_59};
end
always @(*) begin
	legup_mult_signed_62_25_4_6_clock = clk;
end
always @(*) begin
	legup_mult_signed_62_25_4_6_aclr = reset;
end
always @(*) begin
	legup_mult_signed_62_25_4_6_clken = legup_mult_hw_fir_init_check_61_en;
end
always @(*) begin
	legup_mult_signed_62_25_4_6_dataa = {1'd0,hw_fir_init_check_60};
end
assign legup_mult_signed_62_25_4_6_datab = -64'd5373160;
always @(*) begin
	legup_mult_hw_fir_init_check_61_out_actual = legup_mult_signed_62_25_4_6_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_61_out = legup_mult_hw_fir_init_check_61_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_61_en = legup_mult_hw_fir_init_check_61_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_61_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_61_width_extended = {{5{hw_fir_init_check_61[56]}},hw_fir_init_check_61};
end
always @(*) begin
	legup_mult_signed_32_25_3_7_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_25_3_7_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_25_3_7_clken = legup_mult_hw_fir_init_check_63_en;
end
always @(*) begin
	legup_mult_signed_32_25_3_7_dataa = hw_fir_init_check_62;
end
assign legup_mult_signed_32_25_3_7_datab = -64'd6898475;
always @(*) begin
	legup_mult_hw_fir_init_check_63_out_actual = legup_mult_signed_32_25_3_7_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_63_out = $signed(legup_mult_hw_fir_init_check_63_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_63_en = legup_mult_hw_fir_init_check_63_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_63_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_63_width_extended = {{5{hw_fir_init_check_63[56]}},hw_fir_init_check_63};
end
always @(*) begin
	legup_mult_signed_63_25_4_8_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_25_4_8_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_25_4_8_clken = legup_mult_hw_fir_init_check_65_en;
end
always @(*) begin
	legup_mult_signed_63_25_4_8_dataa = {1'd0,hw_fir_init_check_64};
end
assign legup_mult_signed_63_25_4_8_datab = -64'd8204580;
always @(*) begin
	legup_mult_hw_fir_init_check_65_out_actual = legup_mult_signed_63_25_4_8_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_65_out = legup_mult_hw_fir_init_check_65_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_65_en = legup_mult_hw_fir_init_check_65_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_65_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_65_width_extended = {{5{hw_fir_init_check_65[56]}},hw_fir_init_check_65};
end
always @(*) begin
	legup_mult_signed_32_26_3_9_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_26_3_9_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_26_3_9_clken = legup_mult_hw_fir_init_check_67_en;
end
always @(*) begin
	legup_mult_signed_32_26_3_9_dataa = hw_fir_init_check_66;
end
assign legup_mult_signed_32_26_3_9_datab = -64'd8965043;
always @(*) begin
	legup_mult_hw_fir_init_check_67_out_actual = legup_mult_signed_32_26_3_9_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_67_out = $signed(legup_mult_hw_fir_init_check_67_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_67_en = legup_mult_hw_fir_init_check_67_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_67_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_67_width_extended = {{4{hw_fir_init_check_67[57]}},hw_fir_init_check_67};
end
always @(*) begin
	legup_mult_signed_60_26_4_10_clock = clk;
end
always @(*) begin
	legup_mult_signed_60_26_4_10_aclr = reset;
end
always @(*) begin
	legup_mult_signed_60_26_4_10_clken = legup_mult_hw_fir_init_check_69_en;
end
always @(*) begin
	legup_mult_signed_60_26_4_10_dataa = {1'd0,hw_fir_init_check_68};
end
assign legup_mult_signed_60_26_4_10_datab = -64'd8788704;
always @(*) begin
	legup_mult_hw_fir_init_check_69_out_actual = legup_mult_signed_60_26_4_10_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_69_out = legup_mult_hw_fir_init_check_69_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_69_en = legup_mult_hw_fir_init_check_69_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_69_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_69_width_extended = {{4{hw_fir_init_check_69[57]}},hw_fir_init_check_69};
end
always @(*) begin
	legup_mult_signed_63_25_4_11_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_25_4_11_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_25_4_11_clken = legup_mult_hw_fir_init_check_71_en;
end
always @(*) begin
	legup_mult_signed_63_25_4_11_dataa = {1'd0,hw_fir_init_check_70};
end
assign legup_mult_signed_63_25_4_11_datab = -64'd7268700;
always @(*) begin
	legup_mult_hw_fir_init_check_71_out_actual = legup_mult_signed_63_25_4_11_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_71_out = legup_mult_hw_fir_init_check_71_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_71_en = legup_mult_hw_fir_init_check_71_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_71_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_71_width_extended = {{5{hw_fir_init_check_71[56]}},hw_fir_init_check_71};
end
always @(*) begin
	legup_mult_signed_32_24_3_12_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_24_3_12_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_24_3_12_clken = legup_mult_hw_fir_init_check_73_en;
end
always @(*) begin
	legup_mult_signed_32_24_3_12_dataa = hw_fir_init_check_72;
end
assign legup_mult_signed_32_24_3_12_datab = -64'd4027179;
always @(*) begin
	legup_mult_hw_fir_init_check_73_out_actual = legup_mult_signed_32_24_3_12_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_73_out = $signed(legup_mult_hw_fir_init_check_73_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_73_en = legup_mult_hw_fir_init_check_73_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_73_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_73_width_extended = {{6{hw_fir_init_check_73[55]}},hw_fir_init_check_73};
end
always @(*) begin
	legup_mult_signed_32_23_3_13_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_23_3_13_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_23_3_13_clken = legup_mult_hw_fir_init_check_75_en;
end
always @(*) begin
	legup_mult_signed_32_23_3_13_dataa = hw_fir_init_check_74;
end
assign legup_mult_signed_32_23_3_13_datab = 64'd1223507;
always @(*) begin
	legup_mult_hw_fir_init_check_75_out_actual = legup_mult_signed_32_23_3_13_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_75_out = legup_mult_hw_fir_init_check_75_out_actual[53:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_75_en = legup_mult_hw_fir_init_check_75_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_75_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_75_width_extended = {{8{hw_fir_init_check_75[53]}},hw_fir_init_check_75};
end
always @(*) begin
	legup_mult_signed_32_26_3_14_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_26_3_14_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_26_3_14_clken = legup_mult_hw_fir_init_check_77_en;
end
always @(*) begin
	legup_mult_signed_32_26_3_14_dataa = hw_fir_init_check_76;
end
assign legup_mult_signed_32_26_3_14_datab = 64'd8625555;
always @(*) begin
	legup_mult_hw_fir_init_check_77_out_actual = legup_mult_signed_32_26_3_14_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_77_out = legup_mult_hw_fir_init_check_77_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_77_en = legup_mult_hw_fir_init_check_77_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_77_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_77_width_extended = {{5{hw_fir_init_check_77[56]}},hw_fir_init_check_77};
end
always @(*) begin
	legup_mult_signed_32_27_3_15_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_27_3_15_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_27_3_15_clken = legup_mult_hw_fir_init_check_79_en;
end
always @(*) begin
	legup_mult_signed_32_27_3_15_dataa = hw_fir_init_check_78;
end
assign legup_mult_signed_32_27_3_15_datab = 64'd18127719;
always @(*) begin
	legup_mult_hw_fir_init_check_79_out_actual = legup_mult_signed_32_27_3_15_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_79_out = legup_mult_hw_fir_init_check_79_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_79_en = legup_mult_hw_fir_init_check_79_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_79_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_79_width_extended = {{4{hw_fir_init_check_79[57]}},hw_fir_init_check_79};
end
always @(*) begin
	legup_mult_signed_32_27_3_16_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_27_3_16_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_27_3_16_clken = legup_mult_hw_fir_init_check_81_en;
end
always @(*) begin
	legup_mult_signed_32_27_3_16_dataa = hw_fir_init_check_80;
end
assign legup_mult_signed_32_27_3_16_datab = 64'd29456521;
always @(*) begin
	legup_mult_hw_fir_init_check_81_out_actual = legup_mult_signed_32_27_3_16_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_81_out = legup_mult_hw_fir_init_check_81_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_81_en = legup_mult_hw_fir_init_check_81_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_81_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_81_width_extended = {{4{hw_fir_init_check_81[57]}},hw_fir_init_check_81};
end
always @(*) begin
	legup_mult_unsigned_63_27_4_17_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_17_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_17_clken = legup_mult_hw_fir_init_check_83_en;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_17_dataa = hw_fir_init_check_82;
end
assign legup_mult_unsigned_63_27_4_17_datab = 64'd42112510;
always @(*) begin
	legup_mult_hw_fir_init_check_83_out_actual = legup_mult_unsigned_63_27_4_17_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_83_out = legup_mult_hw_fir_init_check_83_out_actual[58:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_83_en = legup_mult_hw_fir_init_check_83_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_83_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_83_width_extended = {{3{hw_fir_init_check_83[58]}},hw_fir_init_check_83};
end
always @(*) begin
	legup_mult_signed_32_28_3_18_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_28_3_18_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_28_3_18_clken = legup_mult_hw_fir_init_check_85_en;
end
always @(*) begin
	legup_mult_signed_32_28_3_18_dataa = hw_fir_init_check_84;
end
assign legup_mult_signed_32_28_3_18_datab = 64'd55395293;
always @(*) begin
	legup_mult_hw_fir_init_check_85_out_actual = legup_mult_signed_32_28_3_18_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_85_out = legup_mult_hw_fir_init_check_85_out_actual[58:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_85_en = legup_mult_hw_fir_init_check_85_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_85_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_85_width_extended = {{3{hw_fir_init_check_85[58]}},hw_fir_init_check_85};
end
always @(*) begin
	legup_mult_unsigned_56_28_4_19_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_19_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_19_clken = legup_mult_hw_fir_init_check_87_en;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_19_dataa = hw_fir_init_check_86;
end
assign legup_mult_unsigned_56_28_4_19_datab = 64'd68457216;
always @(*) begin
	legup_mult_hw_fir_init_check_87_out_actual = legup_mult_unsigned_56_28_4_19_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_87_out = legup_mult_hw_fir_init_check_87_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_87_en = legup_mult_hw_fir_init_check_87_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_87_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_87_width_extended = {{2{hw_fir_init_check_87[59]}},hw_fir_init_check_87};
end
always @(*) begin
	legup_mult_signed_32_29_3_20_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_20_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_20_clken = legup_mult_hw_fir_init_check_89_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_20_dataa = hw_fir_init_check_88;
end
assign legup_mult_signed_32_29_3_20_datab = 64'd80380559;
always @(*) begin
	legup_mult_hw_fir_init_check_89_out_actual = legup_mult_signed_32_29_3_20_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_89_out = legup_mult_hw_fir_init_check_89_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_89_en = legup_mult_hw_fir_init_check_89_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_89_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_89_width_extended = {{2{hw_fir_init_check_89[59]}},hw_fir_init_check_89};
end
always @(*) begin
	legup_mult_signed_32_29_3_21_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_21_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_21_clken = legup_mult_hw_fir_init_check_91_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_21_dataa = hw_fir_init_check_90;
end
assign legup_mult_signed_32_29_3_21_datab = 64'd90269643;
always @(*) begin
	legup_mult_hw_fir_init_check_91_out_actual = legup_mult_signed_32_29_3_21_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_91_out = legup_mult_hw_fir_init_check_91_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_91_en = legup_mult_hw_fir_init_check_91_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_91_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_91_width_extended = {{2{hw_fir_init_check_91[59]}},hw_fir_init_check_91};
end
always @(*) begin
	legup_mult_signed_32_29_3_22_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_22_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_22_clken = legup_mult_hw_fir_init_check_93_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_22_dataa = hw_fir_init_check_92;
end
assign legup_mult_signed_32_29_3_22_datab = 64'd97346473;
always @(*) begin
	legup_mult_hw_fir_init_check_93_out_actual = legup_mult_signed_32_29_3_22_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_93_out = legup_mult_hw_fir_init_check_93_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_93_en = legup_mult_hw_fir_init_check_93_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_93_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_93_width_extended = {{2{hw_fir_init_check_93[59]}},hw_fir_init_check_93};
end
always @(*) begin
	legup_mult_unsigned_62_28_4_23_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_23_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_23_clken = legup_mult_hw_fir_init_check_95_en;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_23_dataa = hw_fir_init_check_94;
end
assign legup_mult_unsigned_62_28_4_23_datab = 64'd101037156;
always @(*) begin
	legup_mult_hw_fir_init_check_95_out_actual = legup_mult_unsigned_62_28_4_23_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_95_out = legup_mult_hw_fir_init_check_95_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_95_en = legup_mult_hw_fir_init_check_95_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_95_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_95_width_extended = {{2{hw_fir_init_check_95[59]}},hw_fir_init_check_95};
end
always @(*) begin
	legup_mult_unsigned_62_28_4_24_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_24_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_24_clken = legup_mult_hw_fir_init_check_97_en;
end
always @(*) begin
	legup_mult_unsigned_62_28_4_24_dataa = hw_fir_init_check_96;
end
assign legup_mult_unsigned_62_28_4_24_datab = 64'd101037156;
always @(*) begin
	legup_mult_hw_fir_init_check_97_out_actual = legup_mult_unsigned_62_28_4_24_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_97_out = legup_mult_hw_fir_init_check_97_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_97_en = legup_mult_hw_fir_init_check_97_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_97_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_97_width_extended = {{2{hw_fir_init_check_97[59]}},hw_fir_init_check_97};
end
always @(*) begin
	legup_mult_signed_32_29_3_25_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_25_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_25_clken = legup_mult_hw_fir_init_check_99_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_25_dataa = hw_fir_init_check_98;
end
assign legup_mult_signed_32_29_3_25_datab = 64'd97346473;
always @(*) begin
	legup_mult_hw_fir_init_check_99_out_actual = legup_mult_signed_32_29_3_25_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_99_out = legup_mult_hw_fir_init_check_99_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_99_en = legup_mult_hw_fir_init_check_99_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_99_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_99_width_extended = {{2{hw_fir_init_check_99[59]}},hw_fir_init_check_99};
end
always @(*) begin
	legup_mult_signed_32_29_3_26_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_26_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_26_clken = legup_mult_hw_fir_init_check_101_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_26_dataa = hw_fir_init_check_100;
end
assign legup_mult_signed_32_29_3_26_datab = 64'd90269643;
always @(*) begin
	legup_mult_hw_fir_init_check_101_out_actual = legup_mult_signed_32_29_3_26_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_101_out = legup_mult_hw_fir_init_check_101_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_101_en = legup_mult_hw_fir_init_check_101_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_101_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_101_width_extended = {{2{hw_fir_init_check_101[59]}},hw_fir_init_check_101};
end
always @(*) begin
	legup_mult_signed_32_29_3_27_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_29_3_27_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_29_3_27_clken = legup_mult_hw_fir_init_check_103_en;
end
always @(*) begin
	legup_mult_signed_32_29_3_27_dataa = hw_fir_init_check_102;
end
assign legup_mult_signed_32_29_3_27_datab = 64'd80380559;
always @(*) begin
	legup_mult_hw_fir_init_check_103_out_actual = legup_mult_signed_32_29_3_27_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_103_out = legup_mult_hw_fir_init_check_103_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_103_en = legup_mult_hw_fir_init_check_103_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_103_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_103_width_extended = {{2{hw_fir_init_check_103[59]}},hw_fir_init_check_103};
end
always @(*) begin
	legup_mult_unsigned_56_28_4_28_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_28_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_28_clken = legup_mult_hw_fir_init_check_105_en;
end
always @(*) begin
	legup_mult_unsigned_56_28_4_28_dataa = hw_fir_init_check_104;
end
assign legup_mult_unsigned_56_28_4_28_datab = 64'd68457216;
always @(*) begin
	legup_mult_hw_fir_init_check_105_out_actual = legup_mult_unsigned_56_28_4_28_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_105_out = legup_mult_hw_fir_init_check_105_out_actual[59:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_105_en = legup_mult_hw_fir_init_check_105_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_105_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_105_width_extended = {{2{hw_fir_init_check_105[59]}},hw_fir_init_check_105};
end
always @(*) begin
	legup_mult_signed_32_28_3_29_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_28_3_29_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_28_3_29_clken = legup_mult_hw_fir_init_check_107_en;
end
always @(*) begin
	legup_mult_signed_32_28_3_29_dataa = hw_fir_init_check_106;
end
assign legup_mult_signed_32_28_3_29_datab = 64'd55395293;
always @(*) begin
	legup_mult_hw_fir_init_check_107_out_actual = legup_mult_signed_32_28_3_29_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_107_out = legup_mult_hw_fir_init_check_107_out_actual[58:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_107_en = legup_mult_hw_fir_init_check_107_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_107_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_107_width_extended = {{3{hw_fir_init_check_107[58]}},hw_fir_init_check_107};
end
always @(*) begin
	legup_mult_unsigned_63_27_4_30_clock = clk;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_30_aclr = reset;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_30_clken = legup_mult_hw_fir_init_check_109_en;
end
always @(*) begin
	legup_mult_unsigned_63_27_4_30_dataa = hw_fir_init_check_108;
end
assign legup_mult_unsigned_63_27_4_30_datab = 64'd42112510;
always @(*) begin
	legup_mult_hw_fir_init_check_109_out_actual = legup_mult_unsigned_63_27_4_30_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_109_out = legup_mult_hw_fir_init_check_109_out_actual[58:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_109_en = legup_mult_hw_fir_init_check_109_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_109_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_109_width_extended = {{3{hw_fir_init_check_109[58]}},hw_fir_init_check_109};
end
always @(*) begin
	legup_mult_signed_32_27_3_31_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_27_3_31_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_27_3_31_clken = legup_mult_hw_fir_init_check_111_en;
end
always @(*) begin
	legup_mult_signed_32_27_3_31_dataa = hw_fir_init_check_110;
end
assign legup_mult_signed_32_27_3_31_datab = 64'd29456521;
always @(*) begin
	legup_mult_hw_fir_init_check_111_out_actual = legup_mult_signed_32_27_3_31_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_111_out = legup_mult_hw_fir_init_check_111_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_111_en = legup_mult_hw_fir_init_check_111_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_111_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_111_width_extended = {{4{hw_fir_init_check_111[57]}},hw_fir_init_check_111};
end
always @(*) begin
	legup_mult_signed_32_27_3_32_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_27_3_32_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_27_3_32_clken = legup_mult_hw_fir_init_check_113_en;
end
always @(*) begin
	legup_mult_signed_32_27_3_32_dataa = hw_fir_init_check_112;
end
assign legup_mult_signed_32_27_3_32_datab = 64'd18127719;
always @(*) begin
	legup_mult_hw_fir_init_check_113_out_actual = legup_mult_signed_32_27_3_32_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_113_out = legup_mult_hw_fir_init_check_113_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_113_en = legup_mult_hw_fir_init_check_113_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_113_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_113_width_extended = {{4{hw_fir_init_check_113[57]}},hw_fir_init_check_113};
end
always @(*) begin
	legup_mult_signed_32_26_3_33_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_26_3_33_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_26_3_33_clken = legup_mult_hw_fir_init_check_115_en;
end
always @(*) begin
	legup_mult_signed_32_26_3_33_dataa = hw_fir_init_check_114;
end
assign legup_mult_signed_32_26_3_33_datab = 64'd8625555;
always @(*) begin
	legup_mult_hw_fir_init_check_115_out_actual = legup_mult_signed_32_26_3_33_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_115_out = legup_mult_hw_fir_init_check_115_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_115_en = legup_mult_hw_fir_init_check_115_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_115_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_115_width_extended = {{5{hw_fir_init_check_115[56]}},hw_fir_init_check_115};
end
always @(*) begin
	legup_mult_signed_32_23_3_34_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_23_3_34_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_23_3_34_clken = legup_mult_hw_fir_init_check_117_en;
end
always @(*) begin
	legup_mult_signed_32_23_3_34_dataa = hw_fir_init_check_116;
end
assign legup_mult_signed_32_23_3_34_datab = 64'd1223507;
always @(*) begin
	legup_mult_hw_fir_init_check_117_out_actual = legup_mult_signed_32_23_3_34_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_117_out = legup_mult_hw_fir_init_check_117_out_actual[53:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_117_en = legup_mult_hw_fir_init_check_117_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_117_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_117_width_extended = {{8{hw_fir_init_check_117[53]}},hw_fir_init_check_117};
end
always @(*) begin
	legup_mult_signed_32_24_3_35_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_24_3_35_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_24_3_35_clken = legup_mult_hw_fir_init_check_119_en;
end
always @(*) begin
	legup_mult_signed_32_24_3_35_dataa = hw_fir_init_check_118;
end
assign legup_mult_signed_32_24_3_35_datab = -64'd4027179;
always @(*) begin
	legup_mult_hw_fir_init_check_119_out_actual = legup_mult_signed_32_24_3_35_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_119_out = $signed(legup_mult_hw_fir_init_check_119_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_119_en = legup_mult_hw_fir_init_check_119_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_119_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_119_width_extended = {{6{hw_fir_init_check_119[55]}},hw_fir_init_check_119};
end
always @(*) begin
	legup_mult_signed_63_25_4_36_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_25_4_36_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_25_4_36_clken = legup_mult_hw_fir_init_check_121_en;
end
always @(*) begin
	legup_mult_signed_63_25_4_36_dataa = {1'd0,hw_fir_init_check_120};
end
assign legup_mult_signed_63_25_4_36_datab = -64'd7268700;
always @(*) begin
	legup_mult_hw_fir_init_check_121_out_actual = legup_mult_signed_63_25_4_36_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_121_out = legup_mult_hw_fir_init_check_121_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_121_en = legup_mult_hw_fir_init_check_121_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_121_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_121_width_extended = {{5{hw_fir_init_check_121[56]}},hw_fir_init_check_121};
end
always @(*) begin
	legup_mult_signed_60_26_4_37_clock = clk;
end
always @(*) begin
	legup_mult_signed_60_26_4_37_aclr = reset;
end
always @(*) begin
	legup_mult_signed_60_26_4_37_clken = legup_mult_hw_fir_init_check_123_en;
end
always @(*) begin
	legup_mult_signed_60_26_4_37_dataa = {1'd0,hw_fir_init_check_122};
end
assign legup_mult_signed_60_26_4_37_datab = -64'd8788704;
always @(*) begin
	legup_mult_hw_fir_init_check_123_out_actual = legup_mult_signed_60_26_4_37_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_123_out = legup_mult_hw_fir_init_check_123_out_actual[57:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_123_en = legup_mult_hw_fir_init_check_123_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_123_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_123_width_extended = {{4{hw_fir_init_check_123[57]}},hw_fir_init_check_123};
end
always @(*) begin
	legup_mult_signed_32_26_3_38_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_26_3_38_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_26_3_38_clken = legup_mult_hw_fir_init_check_125_en;
end
always @(*) begin
	legup_mult_signed_32_26_3_38_dataa = hw_fir_init_check_124;
end
assign legup_mult_signed_32_26_3_38_datab = -64'd8965043;
always @(*) begin
	legup_mult_hw_fir_init_check_125_out_actual = legup_mult_signed_32_26_3_38_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_125_out = $signed(legup_mult_hw_fir_init_check_125_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_125_en = legup_mult_hw_fir_init_check_125_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_125_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_125_width_extended = {{4{hw_fir_init_check_125[57]}},hw_fir_init_check_125};
end
always @(*) begin
	legup_mult_signed_63_25_4_39_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_25_4_39_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_25_4_39_clken = legup_mult_hw_fir_init_check_127_en;
end
always @(*) begin
	legup_mult_signed_63_25_4_39_dataa = {1'd0,hw_fir_init_check_126};
end
assign legup_mult_signed_63_25_4_39_datab = -64'd8204580;
always @(*) begin
	legup_mult_hw_fir_init_check_127_out_actual = legup_mult_signed_63_25_4_39_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_127_out = legup_mult_hw_fir_init_check_127_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_127_en = legup_mult_hw_fir_init_check_127_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_127_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_127_width_extended = {{5{hw_fir_init_check_127[56]}},hw_fir_init_check_127};
end
always @(*) begin
	legup_mult_signed_32_25_3_40_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_25_3_40_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_25_3_40_clken = legup_mult_hw_fir_init_check_129_en;
end
always @(*) begin
	legup_mult_signed_32_25_3_40_dataa = hw_fir_init_check_128;
end
assign legup_mult_signed_32_25_3_40_datab = -64'd6898475;
always @(*) begin
	legup_mult_hw_fir_init_check_129_out_actual = legup_mult_signed_32_25_3_40_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_129_out = $signed(legup_mult_hw_fir_init_check_129_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_129_en = legup_mult_hw_fir_init_check_129_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_129_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_129_width_extended = {{5{hw_fir_init_check_129[56]}},hw_fir_init_check_129};
end
always @(*) begin
	legup_mult_signed_62_25_4_41_clock = clk;
end
always @(*) begin
	legup_mult_signed_62_25_4_41_aclr = reset;
end
always @(*) begin
	legup_mult_signed_62_25_4_41_clken = legup_mult_hw_fir_init_check_131_en;
end
always @(*) begin
	legup_mult_signed_62_25_4_41_dataa = {1'd0,hw_fir_init_check_130};
end
assign legup_mult_signed_62_25_4_41_datab = -64'd5373160;
always @(*) begin
	legup_mult_hw_fir_init_check_131_out_actual = legup_mult_signed_62_25_4_41_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_131_out = legup_mult_hw_fir_init_check_131_out_actual[56:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_131_en = legup_mult_hw_fir_init_check_131_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_131_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_131_width_extended = {{5{hw_fir_init_check_131[56]}},hw_fir_init_check_131};
end
always @(*) begin
	legup_mult_signed_32_24_3_42_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_24_3_42_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_24_3_42_clken = legup_mult_hw_fir_init_check_133_en;
end
always @(*) begin
	legup_mult_signed_32_24_3_42_dataa = hw_fir_init_check_132;
end
assign legup_mult_signed_32_24_3_42_datab = -64'd3887393;
always @(*) begin
	legup_mult_hw_fir_init_check_133_out_actual = legup_mult_signed_32_24_3_42_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_133_out = $signed(legup_mult_hw_fir_init_check_133_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_133_en = legup_mult_hw_fir_init_check_133_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_133_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_133_width_extended = {{6{hw_fir_init_check_133[55]}},hw_fir_init_check_133};
end
always @(*) begin
	legup_mult_signed_61_24_4_43_clock = clk;
end
always @(*) begin
	legup_mult_signed_61_24_4_43_aclr = reset;
end
always @(*) begin
	legup_mult_signed_61_24_4_43_clken = legup_mult_hw_fir_init_check_135_en;
end
always @(*) begin
	legup_mult_signed_61_24_4_43_dataa = {1'd0,hw_fir_init_check_134};
end
assign legup_mult_signed_61_24_4_43_datab = -64'd2588080;
always @(*) begin
	legup_mult_hw_fir_init_check_135_out_actual = legup_mult_signed_61_24_4_43_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_135_out = legup_mult_hw_fir_init_check_135_out_actual[55:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_135_en = legup_mult_hw_fir_init_check_135_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_135_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_135_width_extended = {{6{hw_fir_init_check_135[55]}},hw_fir_init_check_135};
end
always @(*) begin
	legup_mult_signed_63_23_4_44_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_23_4_44_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_23_4_44_clken = legup_mult_hw_fir_init_check_137_en;
end
always @(*) begin
	legup_mult_signed_63_23_4_44_dataa = {1'd0,hw_fir_init_check_136};
end
assign legup_mult_signed_63_23_4_44_datab = -64'd1585580;
always @(*) begin
	legup_mult_hw_fir_init_check_137_out_actual = legup_mult_signed_63_23_4_44_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_137_out = legup_mult_hw_fir_init_check_137_out_actual[54:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_137_en = legup_mult_hw_fir_init_check_137_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_137_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_137_width_extended = {{7{hw_fir_init_check_137[54]}},hw_fir_init_check_137};
end
always @(*) begin
	legup_mult_signed_32_22_3_45_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_22_3_45_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_22_3_45_clken = legup_mult_hw_fir_init_check_139_en;
end
always @(*) begin
	legup_mult_signed_32_22_3_45_dataa = hw_fir_init_check_138;
end
assign legup_mult_signed_32_22_3_45_datab = -64'd839361;
always @(*) begin
	legup_mult_hw_fir_init_check_139_out_actual = legup_mult_signed_32_22_3_45_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_139_out = $signed(legup_mult_hw_fir_init_check_139_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_139_en = legup_mult_hw_fir_init_check_139_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_139_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_139_width_extended = {{8{hw_fir_init_check_139[53]}},hw_fir_init_check_139};
end
always @(*) begin
	legup_mult_signed_63_21_4_46_clock = clk;
end
always @(*) begin
	legup_mult_signed_63_21_4_46_aclr = reset;
end
always @(*) begin
	legup_mult_signed_63_21_4_46_clken = legup_mult_hw_fir_init_check_141_en;
end
always @(*) begin
	legup_mult_signed_63_21_4_46_dataa = {1'd0,hw_fir_init_check_140};
end
assign legup_mult_signed_63_21_4_46_datab = -64'd420988;
always @(*) begin
	legup_mult_hw_fir_init_check_141_out_actual = legup_mult_signed_63_21_4_46_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_141_out = legup_mult_hw_fir_init_check_141_out_actual[52:0];
end
always @(*) begin
	legup_mult_hw_fir_init_check_141_en = legup_mult_hw_fir_init_check_141_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_141_en_pipeline_cond = ~(hw_fir_state_stall_4);
end
always @(*) begin
	hw_fir_init_check_141_width_extended = {{9{hw_fir_init_check_141[52]}},hw_fir_init_check_141};
end
always @(*) begin
	legup_mult_signed_32_19_3_47_clock = clk;
end
always @(*) begin
	legup_mult_signed_32_19_3_47_aclr = reset;
end
always @(*) begin
	legup_mult_signed_32_19_3_47_clken = legup_mult_hw_fir_init_check_143_en;
end
always @(*) begin
	legup_mult_signed_32_19_3_47_dataa = hw_fir_init_check_142;
end
assign legup_mult_signed_32_19_3_47_datab = -64'd112451;
always @(*) begin
	legup_mult_hw_fir_init_check_143_out_actual = legup_mult_signed_32_19_3_47_result;
end
always @(*) begin
	legup_mult_hw_fir_init_check_143_out = $signed(legup_mult_hw_fir_init_check_143_out_actual);
end
always @(*) begin
	legup_mult_hw_fir_init_check_143_en = legup_mult_hw_fir_init_check_143_en_pipeline_cond;
end
always @(*) begin
	legup_mult_hw_fir_init_check_143_en_pipeline_cond = ~(hw_fir_state_stall_3);
end
always @(*) begin
	hw_fir_init_check_143_width_extended = {{11{hw_fir_init_check_143[50]}},hw_fir_init_check_143};
end
always @(*) begin
	ready = ~(hw_fir_state_stall_0);
end
always @(posedge clk) begin
	finish <= hw_fir_state_enable_4;
end
always @(posedge clk) begin
	if (hw_fir_state_enable_4) begin
		return_val <= hw_fir_init_check_newCurOp_285;
	end
end

endmodule



// ©2022 Microchip Technology Inc. and its subsidiaries
//
// Subject to your compliance with these terms, you may use this Microchip
// software and any derivatives exclusively with Microchip products. You are
// responsible for complying with third party license terms applicable to your
// use of third party software (including open source software) that may
// accompany this Microchip software. SOFTWARE IS “AS IS.” NO WARRANTIES,
// WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING
// ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR
// A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY
// INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST
// OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
// EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
// FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP’S TOTAL
// LIABILITY ON ALL CLAIMS LATED TO THE SOFTWARE WILL NOT EXCEED AMOUNT OF
// FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. MICROCHIP
// OFFERS NO SUPPORT FOR THE SOFTWARE. YOU MAY CONTACT MICROCHIP AT
// https://www.microchip.com/en-us/support-and-training/design-help/client-support-services
// TO INQUIRE ABOUT SUPPORT SERVICES AND APPLICABLE FEES, IF AVAILABLE.

`timescale 1ns / 1ns
module hw_fir_legup_mult # (
  parameter widtha = 32,
  parameter widthb = 32,
  parameter widthp = 64,
  parameter pipeline = 3,
  parameter representation = "UNSIGNED",
  parameter pipeline_stallable = 0 
) (
  input clock,
  input aclr,
  input clken,
  input [widtha-1:0] dataa,
  input [widthb-1:0] datab,
  output [widthp-1:0] result
);

generate 
if (pipeline == 0) begin
  // If the number of pipeline stages is 0, 
  // instantiate the combinational multiplier
  hw_fir_legup_mult_core legup_mult_core_inst(
      .dataa(dataa),
      .datab(datab),
      .result(result) 
  );
  defparam legup_mult_core_inst.widtha = widtha;
  defparam legup_mult_core_inst.widthb = widthb;
  defparam legup_mult_core_inst.widthp = widthp;
  defparam legup_mult_core_inst.representation = representation;

end else if (pipeline_stallable == 0) begin
  // If the datapath that uses the multiplier is not a pipeline or 
  // is a pipeline but is not stallable, or if the number of pipeline stages
  // is 1 or less,
  // simply instantiate the normal multiplier
  hw_fir_legup_mult_pipelined legup_mult_pipelined_inst(
      .clock(clock),
      .aclr(aclr),
      .clken(clken),
      .dataa(dataa),
      .datab(datab),
      .result(result) 
  );
  defparam legup_mult_pipelined_inst.widtha = widtha;
  defparam legup_mult_pipelined_inst.widthb = widthb;
  defparam legup_mult_pipelined_inst.widthp = widthp;
  defparam legup_mult_pipelined_inst.pipeline = pipeline;
  defparam legup_mult_pipelined_inst.representation = representation;

end 
endgenerate

endmodule


// ©2022 Microchip Technology Inc. and its subsidiaries
//
// Subject to your compliance with these terms, you may use this Microchip
// software and any derivatives exclusively with Microchip products. You are
// responsible for complying with third party license terms applicable to your
// use of third party software (including open source software) that may
// accompany this Microchip software. SOFTWARE IS “AS IS.” NO WARRANTIES,
// WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING
// ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR
// A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY
// INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST
// OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
// EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
// FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP’S TOTAL
// LIABILITY ON ALL CLAIMS LATED TO THE SOFTWARE WILL NOT EXCEED AMOUNT OF
// FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. MICROCHIP
// OFFERS NO SUPPORT FOR THE SOFTWARE. YOU MAY CONTACT MICROCHIP AT
// https://www.microchip.com/en-us/support-and-training/design-help/client-support-services
// TO INQUIRE ABOUT SUPPORT SERVICES AND APPLICABLE FEES, IF AVAILABLE.

// combinational generic multiplier
`timescale 1ns / 1ns

module hw_fir_legup_mult_core(
    dataa,
    datab,
    result  
);

parameter widtha = 32;
parameter widthb = 32;
parameter widthp = 64;
parameter representation = "UNSIGNED";

input [widtha-1:0] dataa;
input [widthb-1:0] datab;
output [widthp-1:0] result;

generate
if (representation == "UNSIGNED")
begin

    wire [widtha-1:0] dataa_in = dataa;
    wire [widthb-1:0] datab_in = datab;
    assign result = dataa_in * datab_in;

end else begin

    wire signed [widtha-1:0] dataa_in = dataa;
    wire signed [widthb-1:0] datab_in = datab;
    assign result = dataa_in * datab_in;

end
endgenerate

endmodule

// ©2022 Microchip Technology Inc. and its subsidiaries
//
// Subject to your compliance with these terms, you may use this Microchip
// software and any derivatives exclusively with Microchip products. You are
// responsible for complying with third party license terms applicable to your
// use of third party software (including open source software) that may
// accompany this Microchip software. SOFTWARE IS “AS IS.” NO WARRANTIES,
// WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING
// ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR
// A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY
// INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST
// OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
// EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
// FORESEEABLE.  TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP’S TOTAL
// LIABILITY ON ALL CLAIMS LATED TO THE SOFTWARE WILL NOT EXCEED AMOUNT OF
// FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. MICROCHIP
// OFFERS NO SUPPORT FOR THE SOFTWARE. YOU MAY CONTACT MICROCHIP AT
// https://www.microchip.com/en-us/support-and-training/design-help/client-support-services
// TO INQUIRE ABOUT SUPPORT SERVICES AND APPLICABLE FEES, IF AVAILABLE.

// generic multiplier with parameterizable pipeline stages
`timescale 1ns / 1ns
module hw_fir_legup_mult_pipelined(
    clock,
    aclr,
    clken, 
    dataa,
    datab,
    result  
)/* synthesis syn_hier = fixed */;

parameter widtha = 32;
parameter widthb = 32;
parameter widthp = 64;
parameter pipeline = 3;
parameter representation = "UNSIGNED";
localparam num_input_pipelines = pipeline >> 1;
localparam num_output_pipelines = pipeline - num_input_pipelines;

input clock;
input aclr;
input clken; 

input [widtha-1:0] dataa;
input [widthb-1:0] datab;
output [widthp-1:0] result;

`define PIPELINED_MULTIPLIER_CORE                                                                                \
    integer input_stage;                                                                                         \
    always @(*)                                                                                                  \
    begin                                                                                                        \
      dataa_reg[0] <= dataa;                                                                                     \
      datab_reg[0] <= datab;                                                                                     \
    end                                                                                                          \
    always @(posedge clock)                                                                                      \
    begin                                                                                                        \
      for (input_stage=0; input_stage<num_input_pipelines; input_stage=input_stage+1) begin                      \
        if (aclr) begin                                                                                          \
          dataa_reg[input_stage+1] <= 'd0;                                                                       \
          datab_reg[input_stage+1] <= 'd0;                                                                       \
        end else if (clken) begin                                                                                \
          dataa_reg[input_stage+1] <= dataa_reg[input_stage];                                                    \
          datab_reg[input_stage+1] <= datab_reg[input_stage];                                                    \
        end                                                                                                      \
      end                                                                                                        \
    end                                                                                                          \
    integer output_stage;                                                                                        \
    always @(*)                                                                                                  \
    begin                                                                                                        \
      result_reg[0] <= dataa_reg[num_input_pipelines] * datab_reg[num_input_pipelines];                          \
    end                                                                                                          \
    always @(posedge clock)                                                                                      \
    begin                                                                                                        \
      for (output_stage=0; output_stage<num_output_pipelines; output_stage=output_stage+1) begin                 \
        if (aclr) begin                                                                                          \
           result_reg[output_stage+1] <= 'd0;                                                                    \
        end else if (clken) begin                                                                                \
           result_reg[output_stage+1] <= result_reg[output_stage];                                               \
        end                                                                                                      \
      end                                                                                                        \
    end                                                                                                          \
    assign result = result_reg[num_output_pipelines];

generate
if (representation == "UNSIGNED")
begin
    reg [widtha-1:0] dataa_reg [num_input_pipelines:0];
    reg [widthb-1:0] datab_reg [num_input_pipelines:0];
    reg [widthp-1:0] result_reg [num_output_pipelines:0];

    `PIPELINED_MULTIPLIER_CORE

end else begin

    reg signed [widtha-1:0] dataa_reg [num_input_pipelines:0];
    reg signed [widthb-1:0] datab_reg [num_input_pipelines:0];
    reg signed [widthp-1:0] result_reg [num_output_pipelines:0];

    `PIPELINED_MULTIPLIER_CORE

end
endgenerate

endmodule


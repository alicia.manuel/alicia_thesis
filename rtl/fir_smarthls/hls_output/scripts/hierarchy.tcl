add wave -hex -group "hw_fir_top" -group "ports"  {*}[lsort [find nets -ports [lindex [find instances -bydu hw_fir_top] 0]/*]]
add wave -hex -group "hw_fir_top" -group "hw_fir" -group "ports"  {*}[lsort [find nets -ports [lindex [find instances -r /hw_fir_inst] 0]/*]]

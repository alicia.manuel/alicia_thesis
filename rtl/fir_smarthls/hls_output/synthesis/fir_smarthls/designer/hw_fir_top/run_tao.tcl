set_device -family {PolarFire} -die {MPF300TS} -speed {-1}
read_verilog -mode system_verilog {C:\Users\Alicia\Documents\lira_fpga\rtl\fir_smarthls\hls_output\rtl\fir_smarthls_hw_fir.v}
set_top_level {hw_fir_top}
map_netlist
read_sdc {C:\Users\Alicia\Documents\lira_fpga\rtl\fir_smarthls\hls_output\synthesis\fir_smarthls.sdc}
check_constraints {C:\Users\Alicia\Documents\lira_fpga\rtl\fir_smarthls\hls_output\synthesis\fir_smarthls\constraint\synthesis_sdc_errors.log}
write_fdc {C:\Users\Alicia\Documents\lira_fpga\rtl\fir_smarthls\hls_output\synthesis\fir_smarthls\designer\hw_fir_top\synthesis.fdc}

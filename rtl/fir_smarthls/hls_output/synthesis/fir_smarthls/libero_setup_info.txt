# Microsemi Corp.
# Date: 2023-May-31 11:35:02
Libero Release : v2022.2
Libero Version : 2022.2.0.10
Operating System Name : Windows 8
Ram Size : 7.93 GB (usable)
OS Type : 64-bit

Environment Variables : 
---------------------
	Please enable the option from 'Preferences => Startup => Export environment variables' to get this info.

mkdir -p hls_output/rtl hls_output/scripts hls_output/simulation
if [ "fir_smarthls" = "main_tb" ] ; then \
	echo "Error: The project name cannot be "main_tb" because this name is reserved by SmartHLS. Please use a different project name." && false; \
fi;
# Set the variable to zero if not set.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -legup-startup /dev/null -o /dev/null
mkdir -p hls_output/.hls
mkdir -p hls_output/.hls/./
mkdir -p hls_output/reports
mkdir -p hls_output/reports/dot_graphs
mkdir -p hls_output/rtl/mem_init
# If HYBRID isn't specified, set this variable to 0. We need to default to 0, since when this variable
# is passed in an argument into an opt pass (see legup-parallel-api), giving it an empty value will also set the argument to true..
# produces pre-link time optimization binary bitcode: hls_output/.hls/fir_smarthls.prelto.bc
# need to put noinling, to preserve function boundaries first
# 2. Runs frontend (choose Clang or Dragonegg depending on whether OpenMP is used)
# to create hls_output/.hls/name.bc
# Frontend also uses llvm-link to link all the .bc files (without optimizations)
# to produce fir_smarthls.00.prelto.1.bc
# If not using OpenMP, use Clang
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/clang fir.cpp -emit-llvm -c -fno-exceptions -Wall -Wno-strict-aliasing -Wno-unused-label -Wno-unknown-pragmas -Wno-attributes -O1 -I/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/smarthls-library -D__SYNTHESIS__  -m32 -I /usr/include/i386-linux-gnu -D LEGUP_DEFAULT_FIFO_DEPTH=2 -O3 -fno-builtin -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/lib/include/ -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/smarthls-library/ -std=c++11 -std=gnu++11 -target i386-unknown-linux-gnu -fno-vectorize -fno-slp-vectorize -Werror=implicit-function-declaration -Wno-ignored-attributes -D_GLIBCXX_USE_CXX11_ABI=1  -D __SYNTHESIS__ -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/dependencies/include/ -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/dependencies/usr/include/ -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/dependencies/gcc/include/c++/5.4.0/ -I C:/Microchip/SmartHLS-2022.2.1/SmartHLS/dependencies/gcc/include/c++/5.4.0/x86_64-unknown-linux-gnu/ -g -mllvm -inline-threshold=-9999 -o hls_output/.hls/fir.bc;    
# Then uses llvm-link to link all the .bc file (without optimizations)
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/llvm-link  hls_output/.hls/fir.bc -o hls_output/.hls/fir_smarthls.00.prelto.1.bc;
#		
# Generate the AXI slave mmap source code hls_output/.hls/axi_slave.mmap.tmp for axi slave when axi_s is specified without -custom_impl flag.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -axi-interface -axi-gen-code-only hls_output/.hls/fir_smarthls.00.prelto.1.bc -o hls_output/.hls/fir_smarthls.axi.00.prelto.1.bc
make compile_and_link_axi_slave_mmap_src
# Remove debug info if the TCL parameter REMOVE_DEBUG_INFO is set.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -remove-debug-info hls_output/.hls/fir_smarthls.00.prelto.1.bc -o hls_output/.hls/fir_smarthls.00.prelto.1.bc
# Set the variable to zero if not set.
#---------------------------------------------------------------------------#
#--------------------- IR TRANSFORMATIONS START HERE -----------------------#
#---------------------------------------------------------------------------#
# perform any pre-processing that has to be done, before any optimization passes are run.
# This needs to run first before running any optimizations so that functions boundaries don't disappear
# for pipelined functions, set_custom_top_level, and functions marked with noinline tcl, add the noinline attribute
# also flatten/inline any functions marked as flatten_function/inline_function.
# Run always-inline pass right afterwards to inline any functions marked with AlwaysInline attribute
# Also modify the IRs to support axi slave interface, which includes
#  1. insert FIFOs to the beginning of main()
#  2. create a new top level function
#  3. call the original top level function + 2 slave functions (read and write) within the new top level
# We also run parallel-api here to lower thread APIs. After this, all calls should be normal function calls and they should not be any function pointers.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -before-removing-tb=1 -lower-bitcasted-function-calls -legup-preprocessing -legup-preprocess-thread-lib -legup-lower-thread-structs -always-inline -axi-interface -loop-rotate -legup-unroll -unroll-threads=1 -break-constgeps -legup-parallel-api -deadargelim hls_output/.hls/fir_smarthls.00.prelto.1.bc -o hls_output/.hls/fir_smarthls.parallel.bc 
# Annotate top-level pointer and global variables. This needs to run before internalize (and std-link-opts which includes internalize)
# Lower external global variables to top-level arguments.
# Then lower global variables to arguments and run SystemC parser
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -before-removing-tb=1 -break-constgeps -annotate-toplevel-ptrargs -ANALYZE_WHOLE_PROGRAM=1 -break-constgeps -lower-globals-to-arguments -std-link-opts -break-constgeps -loweratomic -legup-sc-parser hls_output/.hls/fir_smarthls.parallel.bc -o hls_output/.hls/fir_smarthls.preprocessing.bc
# Remove sw testbench
# Run -break-constgeps before -removeswtestbench
# Expand structs to remove extractvalue/insertvalue 
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -before-removing-tb=1 -break-constgeps -removeswtestbench -globaldce -expand-struct-regs hls_output/.hls/fir_smarthls.preprocessing.bc -o hls_output/.hls/fir_smarthls.swremoved.bc
# check for unsupported C features (recursion, malloc) and C++ features (STL) used in HW functions
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -legup-code-support-checking hls_output/.hls/fir_smarthls.swremoved.bc -o /dev/null
# AP Int Global Initializer.
# -inline-global-apint-ctor only runs inline on the global initialization section and not the whole IR (if run on the whole IR, it might lead to some unintended effect e.g. increased II)
# We need -simplifycfg and a second -instcombine because there might still be branches without these, preventing the global initialization section of the input IR to -extract-global-ap-int-initializer to only have simple store instructions
# -simplifycfg gets rid of the branch, and the second -instcombine combine instructions of the now-single-BB
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -arbitrary-bitwidth -inline-global-apint-ctor -instcombine -simplifycfg -instcombine -extract-global-ap-int-initializer hls_output/.hls/fir_smarthls.swremoved.bc -o hls_output/.hls/fir_smarthls.ap_int_global_init.bc
# lower special types
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  hls_output/.hls/fir_smarthls.ap_int_global_init.bc -o hls_output/.hls/fir_smarthls.mempromo.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -arbitrary-bitwidth -legup-lower-sc hls_output/.hls/fir_smarthls.mempromo.bc -o hls_output/.hls/fir_smarthls.lower.int.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -break-constgeps -legup-lower-single-element-struct hls_output/.hls/fir_smarthls.lower.int.bc -o hls_output/.hls/fir_smarthls.lower.ap.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -data-pack -split-legup-force-reg hls_output/.hls/fir_smarthls.lower.ap.bc -o hls_output/.hls/fir_smarthls.lower.datapack.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -legup-lower-fifos hls_output/.hls/fir_smarthls.lower.datapack.bc -o hls_output/.hls/fir_smarthls.lower.fifo.bc
# inline pass must be run after to inline attribute functions
# globaldce is run to get rid of function definitions that are no longer necessary due to all call sites being inlined
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -basiccg -inline-cost -inline -globalopt -globaldce hls_output/.hls/fir_smarthls.lower.fifo.bc -o hls_output/.hls/fir_smarthls.lower.bc
# run standard optimizations now to optimize the program
# std-link-opts also includes argument promotion, which is where we want struct of FIFOs to be promoted to
# individual arguments of FIFOs
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -std-link-opts hls_output/.hls/fir_smarthls.lower.bc -o hls_output/.hls/fir_smarthls.stdlinkopts.bc
# run std-compile-opts to optimize the program more. This reduced the cycle count for many pipelining benchmarks
# pipeline/correlation, pipeline/accumulate, pipeline/two_loops, pipeline/unroll_loop_nest_depth_*
# We must run std-link-opts then std-compile-opts afterwards. Running std-compile-opts actually broke the IR for some
# hybrid benchmarks: dfadd_shift64RightJamming, dfsin_shift64RightJamming, dfmul_mul64to128
# produced incorrect results for: motion_motion_vectors
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -std-compile-opts hls_output/.hls/fir_smarthls.stdlinkopts.bc -o hls_output/.hls/fir_smarthls.stdcompileopts.bc
# perform link-time optimizations
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -std-link-opts hls_output/.hls/fir_smarthls.stdcompileopts.bc -o hls_output/.hls/fir_smarthls.stdlinkopts2.bc
# Lower math functions (fmod/fmodf) into frem instructions
# This pass needs to run before linking in the math library (libm) so that we use our pre-generated cores for these, instead of compiling the math functions
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-lower-math-for-pipeline hls_output/.hls/fir_smarthls.stdlinkopts2.bc -o hls_output/.hls/fir_smarthls.lowermath.bc
# link libraries
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/llvm-link  hls_output/.hls/fir_smarthls.lowermath.bc /cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/lib/llvm/liblegup_32.bc /cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/lib/llvm/liblegupParallel_32.bc /cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/lib/llvm/libm_32.bc -o hls_output/.hls/fir_smarthls.postlto.6.bc
# performs intrinsic lowering
# We need to run legup-prelto after all libraries have been linked in, but before running internalize-public-api and std-link-opts such that the libraries functions are not all optimized away
# We need to run mp-preprocess after legup-prelto but before any inlining run
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-prelto -mp-preprocess hls_output/.hls/fir_smarthls.postlto.6.bc -o hls_output/.hls/fir_smarthls.legupprelto.bc
# remove all unused functions from linking with liblegup and libm
# running -internalize allows to remove unused functions
# when NO_INLINE=1, std-link-opts doesn't remove functions even when they're unused
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -globaldce -std-link-opts -internalize hls_output/.hls/fir_smarthls.legupprelto.bc -o hls_output/.hls/fir_smarthls.postlto.8.bc
# now that linking is done change any 64 bit frem instructions in IR to 32 bit frem instructions so that
# they match up with the generic frem core we provide.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -frem-shrinking hls_output/.hls/fir_smarthls.postlto.8.bc -o hls_output/.hls/fir_smarthls.postlto.9.bc
# arbitrary bitwidth
# need to run this after legup-prelto, as legup-prelto replaces some functions which causes some bitwidth data to be lost
# but this pass also needs to be run sufficiently early, so that variables can be optimized when we remove llvm.annotation
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-process-bw -legup-prop-bw hls_output/.hls/fir_smarthls.postlto.9.bc -o hls_output/.hls/fir_smarthls.ab.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -indvars2 -mem2reg -simplifycfg -loops -lcssa -loop-simplify -loop-rotate hls_output/.hls/fir_smarthls.ab.bc -o hls_output/.hls/fir_smarthls.looprotate.bc
# inline any necessary functions
# this must be run right before legup-unroll, since for pipelined BBs, we only inline calls in them
# if all of the loops in the descendant functions can also be unrolled. So we check in this pass whether they can be
# fully inlined and unrolled, then performing inlining of the call sites, and the actual unrolling is done
# in the legup-unroll pass.
# The inliner pass also handles other inlining cases outside of pipelining
# run globaldce here to delete the unused functions after inlining
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-inliner -always-inline hls_output/.hls/fir_smarthls.looprotate.bc -o hls_output/.hls/fir_smarthls.inline.1.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -globaldce hls_output/.hls/fir_smarthls.inline.1.bc -o hls_output/.hls/fir_smarthls.inline.bc
# unroll all loops inside function pipeline, or if it calls pthread_create inside the loop
# this has to run before legup-parallel-api
# we MUST run lcssa before the unrolling pass, otherwise the unrolled IR could be broken (examples/pipeline/inline_and_unroll_loop_nest_dept_2)
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -lcssa -rotation-max-header-size=1000  -loop-rotate -legup-unroll hls_output/.hls/fir_smarthls.inline.bc -o hls_output/.hls/fir_smarthls.unroll.bc
# after unrolling, we can now merge loop nests.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -loops -lcssa -loop-simplify -indvars2 -legup-loop-nest-merge -simplifycfg -dce hls_output/.hls/fir_smarthls.unroll.bc -o hls_output/.hls/fir_smarthls.loop_nest_merged.bc
# run this to break up constant GEPs and bitcasts, such as
# store i32 43, i32* getelementptr inbounds ([32 x i32]* @key, i32 0, i32 0), align 4, !tbaa !1
# these are part of another instruction, and thus hard to handle correctly
# this pass break it up to separate instructions again
# this needs to run before voidptr-argpromotion pass
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -break-constgeps hls_output/.hls/fir_smarthls.loop_nest_merged.bc -o hls_output/.hls/fir_smarthls.breakgeps.bc
# Replicate functions as necessary
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -replicate-functions hls_output/.hls/fir_smarthls.breakgeps.bc -o hls_output/.hls/fir_smarthls.replicate.bc
# promote void* arguments (pthread arguments) to typed arguments
# this needs to run after legup-parallel-api pass
# run globaldce here to delete the old functions
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -voidptr-argpromotion -dce -globaldce hls_output/.hls/fir_smarthls.replicate.bc -o hls_output/.hls/fir_smarthls.voidptr.bc
# need to run instcome and std-link-opts before mem-partition so that it removes indirection of a fifo ptr
# being stored to a struct of fifo ptr ptr and loading from the struct to pass into function as fifo ptr (used for pthreads)
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -resolve-bitcast -instcombine -std-link-opts -break-constgeps hls_output/.hls/fir_smarthls.voidptr.bc -o hls_output/.hls/fir_smarthls.voidptropt.bc
# run mem-partition to split up memory if possible (mainly targeting structs variables and arguments)
# run dce + globaldce to clean up leftover instructions and global variables
# this needs to run after voidptr-argpromotion pass, so that void* arguments have been promoted already
# Make sure DCE runs after axi-interface pass so that the pass can still find the dead global variable.
# break-constgeps needs to run before memory partition pass in order for the pass to work properly
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -loop-simplify -indvars -mem-partition -instcombine -axi-interface -axi-rm-unused-mem-only -dce -deadargelim -dce -globaldce -break-constgeps hls_output/.hls/fir_smarthls.voidptropt.bc -o hls_output/.hls/fir_smarthls.mp.bc
# run struct memory packing after memory partitioning
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -bit-pack-struct -byte-pack-struct -abi-pack-struct -llvm-pack-struct hls_output/.hls/fir_smarthls.mp.bc -o hls_output/.hls/fir_smarthls.pack.bc
# run select transform to encourage register promotion, then mem2reg to actually promote those memories to registers
# select-transform targets single element memories, so it could be beneficial to run it after memory partitioning.
# select-transform will take phi's and select's of pointers that get loaded from into phi's and selects of the
# actual underlying values by hosting loads before the phis and selects. This transformation is only applied where
# it would actually allow mem2reg to turn the memories into registers.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -instcombine -break-constgeps -select-transform -mem2reg hls_output/.hls/fir_smarthls.pack.bc -o hls_output/.hls/fir_smarthls.memlowered.bc
# annotate fifo function arguments with their widths and as input/output FIFOs
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-sc-fsm-generator -annotate-toplevel-ptrargs hls_output/.hls/fir_smarthls.memlowered.bc -o hls_output/.hls/fir_smarthls.fifo.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-handle-static-objects -dce hls_output/.hls/fir_smarthls.fifo.bc -o hls_output/.hls/fir_smarthls.static.bc
#
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -mergereturn -legup-ga2l -dce -mem2reg hls_output/.hls/fir_smarthls.static.bc -o hls_output/.hls/fir_smarthls.ga2l.1.bc
cp hls_output/.hls/fir_smarthls.ga2l.1.bc hls_output/.hls/fir_smarthls.ga2l.bc
# /cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -break-crit-edges -legup-ga2l -break-exit-stores -simplifycfg hls_output/.hls/fir_smarthls.ga2l.1.bc -o hls_output/.hls/fir_smarthls.ga2l.bc
# perform link-time optimizations
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -instcombine -std-link-opts hls_output/.hls/fir_smarthls.ga2l.bc -o hls_output/.hls/fir_smarthls.postlto.bc
# lower any ap_int/ap_uint/ap_num that have not been lowered at this point. Generally means they exist as top level arguments
# /cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -break-constgeps -legup-lower-single-element-struct hls_output/.hls/fir_smarthls.postlto.bc -o hls_output/.hls/fir_smarthls.lower.ap.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -break-constgeps -legup-struct-support-checking hls_output/.hls/fir_smarthls.postlto.bc -o hls_output/.hls/fir_smarthls.check.struct.bc
# At this stage all AP types at the top level have been lowered to iX pointers; we can now promote AP arguments that should be passed by value
# and AP return values that should be returned by value into iX at the top level. Run mem2reg directly after, because it should be able to
# handle the loads and stores introduced through promote-ap.
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-promote-ap -mem2reg hls_output/.hls/fir_smarthls.check.struct.bc -o hls_output/.hls/fir_smarthls.promoted.bc
cp hls_output/.hls/fir_smarthls.promoted.bc hls_output/.hls/fir_smarthls.ap.1.bc
# iterative modulo scheduling (with instcombine, removing instcombine makes pipeline/accumulate produce wrong result)
# inst-combine needs to run before loop pipelining, since there are sometimes phi's with only one incoming block
# in which case loop pipelining crashes, inst-combine get rid of these phi's
# Noticed a bug that when -basicaa and -gvn run together in one command, the GVN pass can incorrectly remove a redandunt load instruction.
#  - The load is from a return value pointer (used to be a pointer to ap_int struct but lowered to a pointer to int)
#  - So we separate the following into two commands:
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -basicaa -loop-simplify -indvars2 -instcombine -axi-initiator-fifo-creation hls_output/.hls/fir_smarthls.ap.1.bc -o hls_output/.hls/fir_smarthls.preifconv.1.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -gvn -legup-strength-reduction -legup-expr-tree-balance -legup-store-merge hls_output/.hls/fir_smarthls.preifconv.1.bc -o hls_output/.hls/fir_smarthls.preifconv.2.bc
# These passes are run before COMBINE_BB_PASS as a preprocessing
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -simplifycfg hls_output/.hls/fir_smarthls.preifconv.2.bc -o hls_output/.hls/fir_smarthls.preifconv.3.bc
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-if-conversion hls_output/.hls/fir_smarthls.preifconv.3.bc -o hls_output/.hls/fir_smarthls.ifconv.bc
# slow to invoke another make on cygwin. Add to dependency instead.
#make PostIfconv_IRtransformations
# Set the variable to zero if not set.
#---------------------------------------------------------------------------#
#-----------------Post ifconv TRANSFORMATIONS START HERE -------------------#
#---------------------------------------------------------------------------#
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -simplifycfg hls_output/.hls/fir_smarthls.ifconv.bc -o hls_output/.hls/fir_smarthls.ifconv.1.bc
#/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -instcombine -gvn -legup-strength-reduction hls_output/.hls/fir_smarthls.ifconv.bc -o hls_output/.hls/fir_smarthls.ifconv.1.bc
# GVN sometimes replaces the predicate instruction with another instruction, even with legup_preserve (legup_preserve holds the replaced instruction).
# When the instruction is replaced, the metadata doesn't follow, so LegUp crashes. BugZilla #174
# instcombine was removed from this line (was run right before GVN),
# since it was also replacing instructions with metadata, and causing circuits
# (benchmarks/LegUp/pthreads/loop_pipelining/k-means/single) to produce wrong result.
#
# Running argument promotion after if-conversion allows some FIFO** to be promoted to FIFO* (network_stack/arp_server)
# Re-run annotate fifo arguments pass after promoting FIFO pointers
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -loop-simplify -indvars2 -instcombine -gvn -legup-strength-reduction -argpromotion -annotate-fifoarguments hls_output/.hls/fir_smarthls.ifconv.1.bc -o hls_output/.hls/fir_smarthls.ifconv.2.bc
# Run final code support checking
# This pass checks:
# 1. Whether there are any undefined functions at this point
# 2. If there are any supported features used for function pipelines
# This pass needs to run after linking in all libraires (libm), after memory partitioning, and after annotate-fifoarguments pass
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-final-code-support-checking hls_output/.hls/fir_smarthls.ifconv.2.bc -o /dev/null
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -legup-reduce-latency -legup-reduce-latency-annotate-depth -fuse-ops -bit-manipulation -globaldce -adce -annotate-legup-force-reg hls_output/.hls/fir_smarthls.ifconv.2.bc -o hls_output/.hls/fir_smarthls.finaloptimizedir.bc
# Restore predicate metadata with restore-metadata
rm -f hls_output/reports/pipelining.hls.rpt
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=   -restore-metadata -basicaa -loop-pipeline -function-pipeline hls_output/.hls/fir_smarthls.finaloptimizedir.bc -o hls_output/.hls/fir_smarthls.bc
# NOTE: no passes which modify the IR should be run after -loop-pipeline
# and -function-pipeline, as this will cause the pipelining schedule
# to break!
#---------------------------------------------------------------------------#
#--------------------- IR TRANSFORMATIONS END HERE -------------------------#
#---------------------------------------------------------------------------#
# Set the variable to zero if not set.
rm -f hls_output/rtl/fir_smarthls*.v
rm -f hls_output/.hls/Makefile.local_pragma_config
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/llc -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -PURE_HW_FLOW= -LEGUP_ACCELERATOR_FILENAME=fir_smarthls -march=v hls_output/.hls/fir_smarthls.bc	
# Make the generated Verilog file read-only.
chmod a-w hls_output/rtl/fir_smarthls*.v
# Make the Makefile.local_pragma_config file read-only.
chmod a-w hls_output/.hls/Makefile.local_pragma_config
mkdir -p hls_output/.hls/sw/
/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/../llvm/Release+Asserts/bin/opt -legup-config=/cygdrive/c/Microchip/SmartHLS-2022.2.1/SmartHLS/examples/legup.tcl -legup-config=config.tcl -vectorizer-min-trip-count=1000000 -slp-threshold=1000000 -force-vector-width=1  -simplifycfg-disable-switchtolookup -instcombine-disable-foldphiload  -after-clang=1 -PURE_HW_FLOW=  -generate-accelerator-driver -LEGUP_ACCELERATOR_FILENAME=fir_smarthls hls_output/.hls/fir_smarthls.finaloptimizedir.bc -o /dev/null
# Copy the accelerator_driver code to the directory `drivers` so users have access to it
if [[ -f hls_output/.hls/sw//fir_smarthls_accelerator_driver.h || -f hls_output/.hls/sw//fir_smarthls_accelerator_driver.cpp ]]; then \
	mkdir -p hls_output/accelerator_drivers; \
	cp hls_output/.hls/sw//fir_smarthls_accelerator_driver.{h,cpp} hls_output/accelerator_drivers; \
fi

#include "hls/ap_fixpt.hpp"
#include "hls/streaming.hpp"
#include <math.h>
#include <stdio.h>


#define TAPS 48
#define INPUT_SIZE 20000
#define SUM_BIT 0
#define INT_SIZE 2
#define DEC_SIZE 30

typedef hls::ap_fixpt<INT_SIZE+DEC_SIZE, INT_SIZE, hls::AP_RND_CONV, hls::AP_SAT> fixpt_t;
typedef hls::ap_fixpt<INT_SIZE+DEC_SIZE, INT_SIZE> fixpt_w_t;
typedef hls::ap_fixpt<INT_SIZE+DEC_SIZE, INT_SIZE> fixpt_s_t;

/**
 * A hardware model for a streaming ap_fixpt FIR filter.
 */
fixpt_w_t hw_fir(fixpt_t input) {
    #pragma HLS function top pipeline

    static fixpt_t previous[TAPS] = {0};
    const fixpt_s_t coefficients[TAPS] = {-0.000104727467330348859462715860324522055,
    		-0.000392074744998403314222290561019690358,
    		-0.000781715557635020508522871729439884803,
    		-0.00147668595952192647363654387504539045,
    		-0.0024103367236310464793991314991217223,
    		-0.003620416280096990305215065575339394854,
    		-0.005004144859511363424564578394893032964,
    		-0.006424705176368399224540173264585973811,
    		-0.00764111056876326522058207046939060092,
    		-0.008349346880155098654663348156645952258,
    		-0.008185117603058469915033867891906993464,
    		-0.006769503914956541586189775472348628682,
    		-0.003750602374476772523509415790954335534,
    		 0.001139480059234831890030426038151745161,
    		 0.008033174150323582271560063361448555952,
    		 0.016882754688886700972227927763924526516,
    		 0.027433523680081089990423492963600438088,
    		 0.039220331323394204414700681127214920707,
    		 0.051590887374392463415873066878702957183,
    		 0.063755750725587256866333518701139837503,
    		 0.074860229325539123790278495107486378402,
    		 0.084070156970805984264494270519207930192,
    		 0.090660968225924981611640873779833782464,
    		 0.094098184868813189440928113072004634887,
    		 0.094098184868813189440928113072004634887,
    		 0.090660968225924981611640873779833782464,
    		 0.084070156970805984264494270519207930192,
    		 0.074860229325539123790278495107486378402,
    		 0.063755750725587256866333518701139837503,
    		 0.051590887374392463415873066878702957183,
    		 0.039220331323394204414700681127214920707,
    		 0.027433523680081089990423492963600438088,
    		 0.016882754688886700972227927763924526516,
    		 0.008033174150323582271560063361448555952,
    		 0.001139480059234831890030426038151745161,
    		-0.003750602374476772523509415790954335534,
    		-0.006769503914956541586189775472348628682,
    		-0.008185117603058469915033867891906993464,
    		-0.008349346880155098654663348156645952258,
    		-0.00764111056876326522058207046939060092,
    		-0.006424705176368399224540173264585973811,
    		-0.005004144859511363424564578394893032964,
    		-0.003620416280096990305215065575339394854,
    		-0.0024103367236310464793991314991217223,
    		-0.00147668595952192647363654387504539045,
    		-0.000781715557635020508522871729439884803,
    		-0.000392074744998403314222290561019690358,
    		-0.000104727467330348859462715860324522055};
    // The constant coefficients use fixpt_s_t which is exactly as
    // wide as it needs to be to represent each of the coefficients.
    // This reduces the width required for the multiply.

    for (int i = (TAPS - 1); i > 0; --i) {
        previous[i] = previous[i - 1];
    }

    previous[0] = input;

    fixpt_w_t out = 0;
#pragma unroll
    for (int i = 0; i < TAPS; ++i) {
        // Here we use fixpt_w_t, which is wider
        // than fixpt_t but allows wrapping and
        // truncation while accumulating the result.
        //
        // This means that the result can overflow,
        // but we save a lot of pipeline stages against
        // checking for overflow at every step using
        // fixpt_t, which uses the AP_SAT saturation mode.
        //
        // See the userguide for an example of an FIR filter
        // that accumulates while checking for saturation, but
        // can still be turned into an efficient pipeline.
        out += previous[i] * coefficients[i];
    }

    return out;
}

float sw_fir(float input) {
	static float previous[TAPS] = {0};
	    const float coefficients[TAPS] = {-4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5,
	                                              0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4};

	    for (int i = (TAPS - 1); i > 0; --i) {
	        previous[i] = previous[i - 1];
	    }

	    previous[0] = input;

	    float out = 0;
	    for (int i = 0; i < TAPS; ++i) {
	        out += previous[i] * coefficients[i];
	    }

	    return out;
}

float test_input_injector() {
    const float mag = 30.0;
    const float increment = 3.14159 / 64.0;
    static float in = 0.0;

    in += increment;

    return mag * cos(in);
}

int main() {
	fixpt_t hw_out = 0;
		fixpt_t hw_in;
	    float sw_out = 0.0;
		float sw_in;
	    float cum_abs_difference = 0.0, avg_abs_difference = 0.0;

	    //fixpt_t hw_output[TAPS] = {0};
	    //float sw_output[TAPS] = {0};

	    for (int i = 0; i < INPUT_SIZE; ++i) {
	        sw_in = test_input_injector();
	        hw_in = fixpt_t(sw_in);

	        hw_out = hw_fir(hw_in);
	        sw_out = sw_fir(sw_in);

	        cum_abs_difference += fabs(hw_out.to_double() - sw_out); // absolute value calculation

	        if (fabs(hw_out.to_double() - sw_out) > 0.1) {
				// The input has been chosen such that the results between
				// hardware and software differ for one input, because
				// saturation occurs in the fixed point module.
				printf("%d: %f != %f\n", i, hw_out.to_double(), sw_out);
			}
		}

	    avg_abs_difference = cum_abs_difference / INPUT_SIZE;

		printf("Average Absolute Difference: %f\n", avg_abs_difference);

		// returns 0 if avg_abs_difference is less than 0.1
		return avg_abs_difference > 0.1;
}

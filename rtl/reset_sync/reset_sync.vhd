--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: sync1b.vhd
-- File history:
--      0.0.0   : 25/08/2022  : creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- reset synchronizer.
--
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity reset_sync is
port (
    --<port_name> : <direction> <type>;
	i_clk   :   in  std_logic;
	i_nrst	:   in std_logic;
    o_q     :   out std_logic
);
end reset_sync;
architecture beh of reset_sync is
    
    signal clk  : std_logic;
	signal nrst : std_logic;
    signal q1 : std_logic;
	--signal q2 : std_logic;

begin

-----------------------------------------------------------------
--  PORT CONNECTIONS
-----------------------------------------------------------------

    clk <= i_clk;
	nrst <= i_nrst;
	
-----------------------------------------------------------------
--  FLIP FLOP PROCESSES
-----------------------------------------------------------------

    p_sync_0 : process(clk, nrst)
    -- variable v_temp : std_logic_vector(sync_len - 1 downto 0);
    begin
        if (nrst='0') then
				q1 <= '0';
		elsif (clk'event and clk='1') then
			q1 <= '1';
        end if;
    end process;
	
	p_sync_1: process(clk, nrst)
	begin
		if (nrst='0') then
				o_q <= '0';
		elsif (clk'event and clk='1') then
			o_q<=q1;
		end if;
	end process;

    --o_q <= q2;
end beh;

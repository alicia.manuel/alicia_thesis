--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: adaptative_counter.vhd
-- File history:
--      0.0     :   02/02/2022   :  Creation. TEST PASSED
--
-- Description: 
--
-- Counter unit. when step signal is asserted, cnt increments. When nrst is asserted (='0') 
-- cnt = 0. 
-- Use generic to define cnt width, which defines max cnt value. 
--
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity adaptative_counter is
generic( 
    cnt_bit_size : integer := 32 );
port (
	i_nrst      :   IN  std_logic;
    i_clk       :   IN  std_logic;
    i_step      :   IN  std_logic;
    o_cnt       :   OUT std_logic_vector(cnt_bit_size-1 downto 0)
    
);
end adaptative_counter;
architecture architecture_adaptative_counter of adaptative_counter is
    
    signal nrst         :   std_logic;
    signal clk          :   std_logic;
    signal step         :   std_logic;
    -- signal step_q    :   std_logic;

    signal cnt_reg      :   std_logic_vector(cnt_bit_size - 1 downto 0);
    signal cnt_reg_p    :   std_logic_vector(cnt_bit_size - 1 downto 0);

    -- signal do_count     :   std_logic;

begin
    clk <= i_clk;
    nrst <= i_nrst;
    o_cnt <= cnt_reg;

    -- detect step rising edge
    p_step  :   process(clk, i_step)
    begin 
        if clk'event and clk = '1' then
            step <= i_step;
        end if;
    end process p_step;
    
    -- p_step_q : process(clk, step)
    -- begin
    --     if clk'event and clk = '1' then
    --         step_q <= step;
    --     end if;
    -- end process p_step_q;

    
    -- do_count <= step;-- and not step_q;

    -- do counting action
    p_counter   :   process(nrst, clk, cnt_reg_p)
    begin
        if nrst = '0' then cnt_reg <= (others => '0');
        elsif clk'event and clk = '1' then
            if step = '1' then 
                cnt_reg <= std_logic_vector(unsigned(cnt_reg_p) + 1);
            end if;
        end if;

    end process p_counter;
    cnt_reg_p <= cnt_reg;    

end architecture_adaptative_counter;

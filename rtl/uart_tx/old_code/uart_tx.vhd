--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: uart_tx_old.vhd
-- File history:
--      0.0     :   26/10/2022      :   creation
-- Description: 
--
-- This block is built out of a counter, a set-reset register and a 10 bit shift register, 
-- and some D flip flops. 
--
-- 
-- The 10 bit shift content is as follows:
--   Bit 9 | Bit 8 | Bit 7 | Bit 6 | Bit 5 | Bit 4 | Bit 3 | Bit 2 | Bit 1 | Bit 0 |
--     D7      D6      D5      D4      D3      D2      D1      D0      0       1
--
-- BIT 0 acts as serial output. At reset bit 0 must be '1', hence the forced '1'. The start 
-- signal rising edge indicates beginning of transmission. This edge sets the SR register,
-- which acts as 'busy'flag externally and, internally, an 'enable' signal. The counter is
-- activated by the enable signal and the count action is triggered by the clock itself. 
-- Every 16 clock rising edges a 'new_byte' flag is asserted for a clock cycle, which in time
-- triggers shift operation on the shift register. 
-- The end of shifting will happen after:
--      1 bit '1' as time delay for start condition
--      1 bit '0' as start condition
--      8 data bits
--    + 1 stop bit                          
--    --------------------------------------
--      11 bits * (16 clock cycles/1 bit) = |176 clock cycles|
--                                          __________________

-- 176 clock cycles imply count value = 175 = 0xAF. That's when the counter and SR register
-- are reset to restore the flags to their initial value. 
-- Targeted device: <Family::PolarFire> <Die::MPF300T> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity uart_tx_old is
port (
    i_clk       : in std_logic;
    i_nrst      : in std_logic;
    i_tx_par    : in std_logic_vector(7 downto 0);
    i_tx_start  : in std_logic;

    o_tx_ser    : out std_logic;
    o_tx_busy   : out std_logic
    );
end uart_tx_old;
architecture rtl of uart_tx_old is

    constant NEW_BIT_CNT : integer := 15;-- 31;--15;
    constant FULL_TX_CNT : integer := 175;--350;--175;
    signal clk          : std_logic;
    signal nrst         : std_logic;
    signal tx_par       : std_logic_vector(7 downto 0);
    signal tx_start     : std_logic;

    signal tx_start_q   : std_logic;
    signal tx_par_reg   : std_logic_vector(9 downto 0);
    signal tx_par_reg_q : std_logic_vector(9 downto 0);

    signal clk_cnt_nrst:   std_logic;
    signal new_bit           :   std_logic;
    signal clk_cnt, clk_cnt_q : unsigned(8 downto 0);
    signal enable : std_logic;
--------------------------------------------------------------------------------
--  COMPONENT DECLARATION
--------------------------------------------------------------------------------    
    

    component reset_sync
    port( 
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
    end component reset_sync;

begin

--------------------------------------------------------------------------------
--  PORT CONNECTIONS
--------------------------------------------------------------------------------    
          
    clk <= i_clk;
    tx_par <= i_tx_par;
    o_tx_ser <= tx_par_reg(0);    
    o_tx_busy <= enable;  

--------------------------------------------------------------------------------
--  SYNCHRONIZERS & DELAYS
--------------------------------------------------------------------------------    
    nrst_sync : reset_sync
    port map( 
        i_clk   => clk,
        i_nrst     => i_nrst,
        o_q     => nrst
    );

    p_tx_start_reg   :   process(clk, i_tx_start)
    begin
        if rising_edge(clk) then
            tx_start <= i_tx_start;
        end if;
    end process p_tx_start_reg;

    p_tx_start_reg_q    :   process(clk, tx_start) 
    begin
        if rising_edge(clk) then
            tx_start_q <= tx_start;
        end if;
    end process p_tx_start_reg_q;
    
    
--------------------------------------------------------------------------------
--  COUNTER PROCESS
--------------------------------------------------------------------------------    

    p_clk_counter : process(nrst, clk_cnt_nrst,  clk)--, clk_cnt_q, enable)
    begin
        if nrst = '0' or clk_cnt_nrst = '0' then
            clk_cnt <= (others => '0');
        elsif rising_edge(clk) then
            if enable then
                clk_cnt <= clk_cnt_q + 1;
            else clk_cnt <= clk_cnt_q;
            end if;
        end if;
    end process;  
    clk_cnt_q <= clk_cnt; -- counter feedback

--------------------------------------------------------------------------------
--  FLAGS GENERATION
--------------------------------------------------------------------------------    
    -- new bit flag
    new_bit <= '1' when clk_cnt(3 downto 0) = to_unsigned(NEW_BIT_CNT, 4) else '0';
    -- counter/SR reg reset signal
    p_clk_cnt_nrst : process(clk)
    begin
        if rising_edge(clk) then
            clk_cnt_nrst <= '0' when clk_cnt = to_unsigned(FULL_TX_CNT, clk_cnt'length) else '1';
        end if;
    end process p_clk_cnt_nrst;

    -- sr register. 
    p_enable : process(nrst, clk, tx_start, tx_start_q, clk_cnt_nrst)
    begin
        if not nrst or not clk_cnt_nrst then
            enable <= '0';
        elsif rising_edge(clk) then
            if tx_start and not tx_start_q then -- rising edge
                enable <= '1';
            end if;                 
        end if;
    end process p_enable;
--------------------------------------------------------------------------------
--  SHIFT REGISTER
--------------------------------------------------------------------------------    
    p_tx_par_reg    :   process(nrst, clk)--, tx_start, tx_start_q, tx_par, tx_par_reg_q)
    begin
        if nrst = '0' then
            -- tx_busy <= '0';
            tx_par_reg(9 downto 0) <= (others => '1');
            -- tx_par_reg(0) <= '1';
        elsif clk'event and clk = '1' then
            if tx_start = '1' and enable = '0' then-- tx_start_q = '0' then
                tx_par_reg(9 downto 2) <= tx_par; -- register input byte
                tx_par_reg(1) <= '0'; --start condition bit

            elsif new_bit then
                tx_par_reg <= '1' & tx_par_reg(9 downto 1);      -- shift operation. tx_par_reg(0) is serial output       
            else

                tx_par_reg <= tx_par_reg_q;
            end if;
        end if;
    end process p_tx_par_reg;
    tx_par_reg_q <= tx_par_reg; -- register feedback
 
end rtl;

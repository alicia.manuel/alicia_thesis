--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: uart_tx.vhd
-- File history:
--      0.0     : 11/1/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: In this module a first operation is performed to know the number of clock rising edges in a bit, this signal is called max. 
-- There is a counter that goes from zero to max in order to controll the bit period and it activates a second counter,that controlls the 
-- data bits that goes to the output register.
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity uart_tx is
generic ( baudrate : integer := 500000;
		clk_freq : integer := 100e6);
port (
    i_clk       : in std_logic;
	i_enable 	: in std_logic;
    i_nrst      : in std_logic;
    i_tx_par    : in std_logic_vector(7 downto 0);
    i_tx_start  : in std_logic;

    o_tx_ser    : out std_logic;
    o_tx_busy   : out std_logic
    
);
end uart_tx;
architecture architecture_lira_uart_tx of uart_tx is
--------------------------------------------------------------------------------
--  SIGNAL DECLARATION
--------------------------------------------------------------------------------

    signal clk          			: std_logic; 
    signal nrst         			: std_logic;
    signal enable       			: std_logic;
    signal tx_par					: std_logic_vector(7 downto 0);
	signal start					: std_logic_vector(2 downto 0);
	signal tx_ser					: std_logic;

	signal in_process				: std_logic; 
	signal enable_bit				: std_logic; -- enable nivell alt del comptador de bits
	signal end_frame				: std_logic;
	signal reset					: std_logic;
	
	
	signal bit_count : integer range 0 to 10;   -- bit count within the received data word
	--constant max : integer range 0 to 7 := 4;		-- set to 4 only for simulation purposes
	constant max : integer range 0 to 524288 := clk_freq/baudrate;  -- 50M/5208~9600, /1302~38400, /892~56000, etc.
	-- El valor de max és Fclk/baudrate
	signal baud : integer range 0 to max+1; -- rx counter at the baud rate
	 
--------------------------------------------------------------------------------
--  COMPONENT DECLARATION
-------------------------------------------------------------------------------- 	 

component reset_sync is
    port( 
        i_clk       :   in std_logic;
        i_nrst         :   in std_logic;
        o_q         :   out std_logic
    );
end component reset_sync;


begin

	reset <= end_frame or not(nrst); -- module reset, is activated in case the frame runs out or the external reset is activated.

--------------------------------------------------------------------------------
--  PORT CONNECTIONS
--------------------------------------------------------------------------------    

    clk <= i_clk;          
    enable <= i_enable;
	o_tx_ser <= tx_ser;
	o_tx_busy <= in_process;
	 
--------------------------------------------------------------------------------
--  SYNC NRST  PROCESSES
--------------------------------------------------------------------------------    
    
    nrst_sync : reset_sync
    port map(  
        i_clk => clk, 
        i_nrst   => i_nrst,
        o_q   => nrst
    );

--------------------------------------------------------------------------------
--  MSIC PROCESSES
--------------------------------------------------------------------------------

-- Start synchronization
p_tx_start_reg   :   process(clk, i_tx_start)
    begin
        if clk'event and clk = '1' then
            start(0) <= i_tx_start;
			start(1) <= start(0);
			start(2) <= start(1);
        end if;
    end process p_tx_start_reg;


p_in_process: process (clk, reset, start)
begin
-- When start is activated, de flip flop loads the value '1'. Since start acts as an enable and is only
-- active during one clock period, the rest of the time the in_process signal will remain '1' until a reset is done
	if (rising_edge(clk)) then
		if (reset = '1') then
			in_process <= '0';
		elsif ((start(1) and not(start(2)))= '1') then -- rising edge
			in_process <= '1';
		end if;
	end if;
end process;

-- Save the input data
p_input_reg: process (clk, start, nrst)
begin
	if (clk'event and clk='1') then
		if (nrst = '0') then
			tx_par <= (others => '0');
		elsif ((start(1) and not(start(2))) = '1') then
			tx_par <= i_tx_par;
		end if;
	end if;
end process;

-- The rising edges of the clock occupying a bit are counted.
p_baud_count: process (clk, reset)
begin
	if (clk'event and clk='1') then
		if (reset = '1') then
			-- Initial values 
			baud <= 0;
			enable_bit <= '0';
		elsif (in_process = '1') then
			if (baud = max-1) then	-- Bit period has ended
				baud <= 0;
				enable_bit <= '1';	-- Output register enabled 
			else
				baud <=baud + 1;	-- Bit period counter
				enable_bit <= '0';
			end if;
		end if;
	end if;
end process;

p_end_frame : process (clk, bit_count)
begin
	if (rising_edge(clk)) then
		end_frame <= '0';
		if (bit_count = 10) then -- All bits have passed through the output register
			end_frame <= '1';
		end if;
	end if;
end process;


-- Counter that controlls the data bits that goes to the output register
p_bit_count: process (clk, reset, enable_bit)
begin
	if (clk'event and clk='1') then
		if (reset = '1') then
			bit_count <= 0;
		elsif (enable_bit = '1') then
			bit_count <= bit_count + 1;
		end if;
	end if;
end process;


-- Output register
p_output_register : process (clk,  bit_count)
begin
	if (clk'event and clk='1') then
		if (reset = '1' or in_process = '0') then
			tx_ser <= '1';
			--end_frame <= '0';
		else
			--end_frame <= '0';
			tx_ser <= '1';
			if (bit_count= 0) then
				tx_ser <= '0';	-- Start bit
			elsif (bit_count >= 1 and bit_count <= 8) then
				tx_ser <= tx_par(bit_count-1);	-- Data bits
			elsif (bit_count = 9) then
				--end_frame <= '1';	-- The frame has ended
				tx_ser <= '1';	-- Stop bit
			end if;
		end if;
	end if;
end process;
	

end ;

--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: CrossCorrelation.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 

-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg_array.all;

--------------------------------------------------------------------------------
--! The purpose of this block is to sequentially calculate the positions of the 
--! cross-correlation vector. The module incorporates two counters, 'i' and 'j', 
--! which help indicate the positions of the input arrays needed at the input of 
--! the MAC block. As this block can be used with other equal blocks in parallel, 
--! the counter 'i' starts at the assigned block number and increments by the 
--! total number of blocks involved in each cross-correlation calculation. This 
--! allows different items to be processed independently and simultaneously.
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 2.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------

entity CrossCorrelation is
    generic (
        N                   : integer := 0;         --! Block number
        N_total             : integer := 1;         --! Total number of blocks
        gen_data_length     : integer := 8;         --! Longitud de los datos
        gen_output_length   : integer := 40;        --! Number of bits of the output samples
        gen_samples         : integer := 500        --! Number of signal samples
    );
    port (
        i_clk           : in std_logic;     --! System clock
        i_nrst          : in std_logic;     --! System reset
        i_x             : in t_array(0 to gen_samples-1)(gen_data_length-1 downto 0);   --! Input data 
        i_y             : in t_array(0 to gen_samples-1)(gen_data_length-1 downto 0);   --! Input data
        i_data_ready    : in std_logic;     --! Indicates when new samples arrive
        o_finish        : out std_logic;    --! Asserts when a result is ready
        o_rxy           : out std_logic_vector(gen_output_length - 1 downto 0)  --! Results of the cross-correlation
    );
end entity CrossCorrelation;

architecture Behavioral of CrossCorrelation is

-- Input signals
signal clk                  : std_logic;
signal nrst                 : std_logic;
signal data_ready           : std_logic;
signal x_buff, x_buff_reg   : t_array(0 to gen_samples-1)(gen_data_length-1 downto 0);
signal y_buff, y_buff_reg   : t_array(0 to gen_samples-1)(gen_data_length-1 downto 0);

-- Output signals
signal finish   : std_logic;
signal rxy      : signed(gen_output_length - 1 downto 0);

-- Auxilliary signals
signal  i, i_reg        : integer range 0 to gen_samples*2-1;   -- Indicates the calculated possition of the cross-correlation. 
signal j, j_reg         : integer range 0 to gen_samples-1;     -- Counter used to calculate the possition of the arrays desired
signal x_index, y_index : integer range 0 to gen_samples-1;     -- Possition of the arrays to send to the MAC module
signal send, send_reg   : std_logic;    -- Enable of the MAC module
signal reset_accum      : std_logic;    -- Reset of the MAC module
signal x_data           : std_logic_vector(gen_data_length-1 downto 0); -- Sample sent to the MAC module
signal y_data           : std_logic_vector(gen_data_length-1 downto 0); -- Sample sent to the MAC module

-- FSM state definition
type t_states is (s_idle, s_index, s_reset);
signal state, next_state : t_states;


begin
--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk         <= i_clk;
nrst        <= i_nrst;
data_ready  <= i_data_ready;
o_finish    <= finish;
o_rxy       <= std_logic_vector(rxy) when finish = '1' else (others => '0');


--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential process to synchronize signals
p_seq: process(nrst, clk) 
begin
    if (nrst = '0') then
        -- Set registers initial values
        i_reg       <= N;   -- The first item to be calculated is the number of block assigned
        j_reg       <= 0;
        send_reg    <= '0';
        state       <= s_idle;
        x_buff_reg  <= (others => (others => '0'));
        y_buff_reg  <= (others => (others => '0'));
    elsif rising_edge(clk) then
        -- Update feedback registers
        i_reg       <= i;
        j_reg       <= j;
        send_reg    <= send;
        state       <= next_state;
        x_buff_reg  <= x_buff;
        y_buff_reg  <= y_buff;
    end if;
end process;

-- Combinational process
p_states: process(state, i_reg, j_reg, x_buff_reg, y_buff_reg, i_x, i_y, data_ready, send_reg)
begin
    -- Default condition
    next_state  <= state;
    i           <= i_reg;
    j           <= j_reg;
    x_index     <= 0;
    y_index     <= 0;
    x_buff      <= x_buff_reg;
    y_buff      <= y_buff_reg;
    finish      <= '0';
    reset_accum <= '1';
    send        <= send_reg;
    case state is
        when s_idle =>
            -- Idle condition
            x_buff      <= (others => (others => '0'));
            y_buff      <= (others => (others => '0'));
            finish      <= '0';
            x_index     <= 0;
            y_index     <= 0;
            reset_accum <= '0';
            i           <= N;
            j           <= 0;
            send        <= '0';
            if (data_ready = '1') then  -- Input data arrives
                -- Register values
                x_buff  <= i_x;
                y_buff  <= i_y;
                send    <= '1'; -- Send first samples to MAC
                next_state  <= s_index;
            end if;

        when s_index    => 
                reset_accum <= '1';             -- Reset MAC to calculate the next cross-correlation position
                x_index     <= j_reg;           -- Position of the first input array desired for the calculation of the cross-correlation
                y_index     <= i_reg - j_reg;   -- Position of the second input array desired for the calculation of the cross-correlation
                send        <= '1';             -- Send samples to MAC
                if (j_reg < i_reg and j_reg < gen_samples-1) then 
                    j   <= j_reg + 1;           -- Increment counter
                else    -- The cross-correlation value calculated is finished
                    send        <= '0';
                    j           <= 0;   -- Reset the counter
                    finish      <= '1'; -- Indicate that there is a new cross-correlation value
                    next_state  <= s_reset;
                end if;

            when s_reset    => 
                if (i_reg + N_total < gen_samples*2-1) then -- The next possition cross-correlation exists (The maximum is 2*number_of_samples)
                    i           <= i_reg + N_total; -- The next position to calculate is the one that comes after all the other blocks calculations
                    reset_accum <= '0';             -- The MAC module is reset to calculate the next value
                    if (i_reg + N_total > gen_samples - 1) then
                        -- If the next possition is greater than the number of samples, the calculation would have to be with zeros
                        -- This calculations are not done and starts directly with the possitions available in the arrays
                        j   <= i_reg + N_total - (gen_samples - 1); -- Firts possition available in the array
                    end if;
                    next_state   <= s_index;
                else
                    next_state  <= s_idle;
                end if;
                send    <= '1';
            
            when others => next_state   <= s_idle;
        end case;
end process;
 
x_data  <= x_buff(x_index);
y_data  <= y_buff(y_index);

-- Multiply and accumulate block
MAC_0 : entity work.MAC
generic map(gen_data_size => gen_data_length,
            gen_output_size => gen_output_length)
port map(
    clk => clk,
    clrn    => reset_accum,
    enable	=> send_reg,
    data1   => signed(x_data), 
    data2	=> signed(y_data),
    r 		=> rxy
);

end Behavioral;
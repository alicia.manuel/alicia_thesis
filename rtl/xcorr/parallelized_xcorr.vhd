--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: parallelized_xcorr.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: Serial architecture of cross-correlation
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.pkg_array.all;

--------------------------------------------------------------------------------
--! The purpose of this module is to parallelize the calculation of cross-
--! correlation by instantiating multiple CrossCorrelation blocks. It receives 
--! input data samples one by one and stores them in an array. The arrays are then 
--! sent to the blocks, and the results are outputted. A counter is used to control 
--! from which block the result is obtained. Since the final results of all blocks 
--! are obtained simultaneously, they are stored in an array and outputted one by one.
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 2.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------


entity parallelized_xcorr is
generic (gen_data_size          : integer := 500;   -- Number of input samples
        gen_data_length         : integer := 8;     -- Bit length of the samples
        gen_output_length       : integer := 32;    -- Bit length of the output data
        gen_number_of_blocks    : integer := 2);    -- Number of CrossCorrelation blocks
port (
    i_clk           : in std_logic;     -- System clock
    i_nrst          : in std_logic;     -- System reset
    i_data1         : in std_logic_vector(gen_data_length-1 downto 0);      -- Input data of the first signal
    i_data2         : in std_logic_vector(gen_data_length-1 downto 0);      -- Input data of the second signal
    i_data_ready    : in std_logic;     -- Asserts when a new input data arrives
    o_finish        : out std_logic;    -- Asserts when a new cross-correlation value is ready
    o_rxy           : out std_logic_vector(gen_output_length - 1 downto 0)  --  Output value
);
end parallelized_xcorr;

architecture rtl of parallelized_xcorr is

-- Input signals
signal clk		    : std_logic;
signal nrst		    : std_logic;
signal data_ready   : std_logic;
signal data1        : std_logic_vector(gen_data_length-1 downto 0);
signal data2        : std_logic_vector(gen_data_length-1 downto 0);

-- Output signals
signal rxy, rxy_reg                 : std_logic_vector(gen_output_length - 1 downto 0);
signal finish_rxy, finish_rxy_reg   : std_logic;

-- Auxilliary signals
type t_output is array (0 to gen_number_of_blocks-1) of std_logic_vector(gen_output_length - 1 downto 0);
type t_finish is array (0 to gen_number_of_blocks-1) of std_logic;
signal rxy_array                            : t_output; -- Array of the results of each correlation block
signal rxy_out, rxy_out_reg                 : t_output; -- Register of rxy_array
signal d1_array, d1_array_reg               : t_array(0 to gen_data_size-1)(gen_data_length-1 downto 0); -- Array of the input data
signal d2_array, d2_array_reg               : t_array(0 to gen_data_size-1)(gen_data_length-1 downto 0); -- Array of the input data
signal finish                               : t_finish; -- Array with the finish signals of each correlation block
signal counter, counter_reg                 : integer range 0 to gen_number_of_blocks-1;    -- Counter to select the array possition of rxy_array
signal samples_counter, samples_counter_reg : integer range 0 to gen_data_size*2;   -- Counter of the cross-correlation results
signal send                                 : std_logic;    -- Indicates when the input arrays are sent to the CrossCorrelation

-- FSM state defintion
type t_states is (s_idle, s_reg, s_wait, s_output);
signal next_state, state: t_states;

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst  : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

begin
--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk             <= i_clk;
data_ready      <= i_data_ready;
data1           <= i_data1;
data2           <= i_data2;
o_finish        <= finish_rxy_reg;
o_rxy           <= rxy_reg;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
-- sync_nrst
sync_nrst : reset_sync
port map(
	i_clk => clk,
	i_nrst => i_nrst,
	o_q => nrst
);


--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Instantiation of the CrossCorrelation blocks
reg: for i in 0 to gen_number_of_blocks - 1 generate
    cmpi: entity work.CrossCorrelation
            generic map(
                N => i, -- Numero de bloque
                N_total => gen_number_of_blocks,
                gen_data_length => gen_data_length, -- Longitud de los datos
                gen_output_length   => gen_output_length,
                gen_samples => gen_data_size
            )
            port map(
                i_clk           => clk,
                i_nrst          => nrst,
                i_x             => d1_array_reg,
                i_y             => d2_array_reg,
                i_data_ready    => send,
                o_finish        => finish(i),
                o_rxy           => rxy_array(i)
            );
end generate; 

-- Sequential process to synchronize signals
p_seq: process(nrst, clk)
begin
    if (nrst = '0') then
        -- Set registers initial values
        state       <= s_idle;
        counter_reg <= 0;
        samples_counter_reg <= 0;
        rxy_reg     <= (others => '0');
        rxy_out_reg <= (others => (others => '0'));
        finish_rxy_reg  <= '0';
        d1_array_reg    <= (others => (others => '0'));
        d2_array_reg    <= (others => (others => '0'));
    elsif rising_edge(clk) then
        -- Update feedback registers
        state   <= next_state;
        counter_reg <= counter;
        samples_counter_reg <= samples_counter;
        rxy_reg     <= rxy;
        rxy_out_reg <= rxy_out;
        d1_array_reg    <= d1_array;
        d2_array_reg    <= d2_array;
        finish_rxy_reg  <= finish_rxy;
    end if;
end process;

-- Combinational process
p_states: process(finish, counter_reg, rxy_reg, rxy_array, rxy_out_reg, samples_counter_reg, d1_array_reg, d2_array_reg, state, data1, data2, data_ready)
begin
    -- Default  condition
    samples_counter <= samples_counter_reg;
    finish_rxy      <= '0';
    rxy_out         <= rxy_out_reg;
    counter         <= counter_reg;
    rxy             <= rxy_reg;
    send            <= '0';
    next_state      <= state;
    d1_array        <= d1_array_reg;
    d2_array        <= d2_array_reg;
    case state is 
        when s_idle =>
            -- Initial condition
            d1_array        <= (others => (others => '0'));
            d2_array        <= (others => (others => '0'));
            rxy_out         <= (others => (others => '0'));
            counter         <= 0;
            samples_counter <= 0;
            if (data_ready = '1') then  -- New samples arrive
                next_state  <= s_reg;
            end if;

        when s_reg  =>
            d1_array(samples_counter_reg)                   <= data1;   -- Save sample in array
            d2_array(gen_data_size-1 - samples_counter_reg) <= data2;   -- Save sample in array
            if (samples_counter_reg = gen_data_size - 1) then           -- All samples have been received
                samples_counter <= 0;   -- Reset counter
                next_state  <= s_output;
            else    -- There are still more samples to recieve
                samples_counter <= samples_counter_reg + 1; -- Increment counter
                next_state  <= s_wait;
            end if;

        when s_wait => 
            -- Wait until next sample is received
            if (data_ready = '1') then
                next_state  <= s_reg;
            end if;

        when s_output   =>  -- State where the cross-correlation results are received
            send    <= '1'; -- Send input arrays to the CrossCorrelation modules
            if (finish(counter_reg) = '1') then                     -- Cross-correlation samples received
                finish_rxy    <= '1';                               -- Indicate the arrival of a new result
                samples_counter <= samples_counter_reg + 1;         -- Increment the results counter
                rxy <= rxy_array(counter_reg);                      -- Output the result
                if (counter_reg < gen_number_of_blocks - 1) then    -- If there are still more blocks to obtain results
                    counter <= counter_reg + 1;                     -- Increment counter to see the ressults of the next block
                else                                                -- Results from all the blocks were obtained
                    counter <= 0;                                   -- Reset counter
                end if;
                if (samples_counter_reg = (gen_data_size*2 - gen_number_of_blocks)) then    -- Last result of each block
                    -- The last result of all blocks are obtained at the same time 
                    rxy_out <= rxy_array;   -- We save the results
                end if;
            else
                if (samples_counter_reg >= (gen_data_size*2 - gen_number_of_blocks)) then   -- All results were obtained
                    if (counter_reg < gen_number_of_blocks - 1) then    -- Not all results from the blocks were outputed
                        finish_rxy  <= '1';                             -- Indicate there is a new result
                        rxy <= rxy_out_reg(counter_reg);                -- Output last result from the selected block
                        counter <= counter_reg + 1;                     -- Increment counter
                    else    -- Every result has been obtained and outputed
                        next_state  <= s_idle;
                    end if;
                end if;
            end if;

        when others => next_state <= s_idle;
    end case;
end process;


end rtl;
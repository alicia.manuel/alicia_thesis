--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: decimator.vhd
-- File history:
--      0.0     : xx/xx/xxxx : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: Decimator block to reduce the sampling rate
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.fixed_pkg.all;

--------------------------------------------------------------------------------
--! This block takes an input signal and reduces its sample rate based on the 
--! provided decimation factor. It employs internal counters and state machines 
--! to control the decimation process and synchronize the input signals. The 
--! decimated output and the enable_decimator signal are provided as outputs from
--! the module. 
--! 
--! @author Alicia Manuel (manuel@ieec.cat)
--! @version 1.0
--! @date xxxx-xx-xx
--!
--! @par Company
--! IEEC
--------------------------------------------------------------------------------


entity decimator is
generic (gen_data_length : integer := 32;       -- Number of bits of the input data
        gen_decimation_factor : integer := 2);  -- Determines the rate at which the data is being decimated
port (
    i_clk       : in std_logic;                             --! System clock
    i_nrst      : in std_logic;                             --! System reset
    i_enable    : in std_logic;                             --! System enable
    i_d : in std_logic_vector(gen_data_length-1 downto 0);  --! Input data
    o_enable_decimator : out std_logic;                     --! Indicates when a new output data is ready
	o_q : out std_logic_vector(gen_data_length-1 downto 0));--! Output data
end decimator;

architecture rtl of decimator is

-- Input signals
signal clk      : std_logic;
signal nrst     : std_logic;
signal enable   : std_logic;

-- Output signals
signal enable_decimator     : std_logic;
signal q                    : std_logic_vector(gen_data_length-1 downto 0);

-- Auxilliary signals
signal data_sync            : std_logic_vector(gen_data_length-1 downto 0); -- Syncrhonised input data
signal counter, counter_reg : integer range 0 to gen_decimation_factor;     -- Signals used for counting and managing the decimation process

-- FSM state definition
type state is (s_idle, s_active, s_wait, s_enable);
signal current_state : state;
signal next_state : state; 

--------------------------------------------------------------------------------
--  COMPONENTS
--------------------------------------------------------------------------------
component reset_sync is
    port(  
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
end component reset_sync;

component flipflop
    generic (gen_data_length : integer := gen_data_length);
    port(clk,clrn,enable: in std_logic; 
        d : in std_logic_vector(gen_data_length-1 downto 0);
        q : out std_logic_vector(gen_data_length-1 downto 0));
end component;

begin

--------------------------------------------------------------------------------
--  PORT ASSIGNS
--------------------------------------------------------------------------------
clk                 <= i_clk;
enable              <= i_enable;
o_q                 <= q;
o_enable_decimator  <= enable_decimator;

--------------------------------------------------------------------------------
--  INPUT SYNCHRONIZERS
--------------------------------------------------------------------------------
    -- Reset synchronizer
    sync_nrst : reset_sync
    port map(
        i_clk => clk,
        i_nrst => i_nrst,
        o_q => nrst
    );

    -- Input syncrhonizer
    flipflop_0  : flipflop
    port map(
        clk     => clk,
        clrn    => nrst,
        enable  => enable,
        d       =>  i_d,
        q       =>  data_sync
    );

    -- Output syncrhonizer
    flipflop_1  : flipflop
    port map(
        clk     => clk,
        clrn    => nrst,
        enable  => enable_decimator,
        d       =>  data_sync,
        q       =>  q
    );

--------------------------------------------------------------------------------
--  PROCESSES
--------------------------------------------------------------------------------
-- Sequential signal synchronisation process
p_seq : process (clk, nrst)
begin
    if (nrst = '0') then            -- Asyncrhon reset
        -- Reset signals to their initial value
        counter_reg     <= 0;
        current_state   <= s_idle;
    elsif (rising_edge(clk)) then
        -- Load values
        counter_reg     <= counter;
        current_state   <= next_state;
    end if;
end process;

-- Combinational process
p_com : process(current_state, nrst, enable, counter_reg)
begin
    -- Load values
    counter             <= counter_reg;
    next_state          <= current_state;
    enable_decimator    <= '0'; -- It remains at zero
    case current_state is
        when s_idle =>  -- Idle state
            counter <= 0;
            if (enable = '1') then
                next_state <= s_enable;
            end if;

        when s_enable =>    -- State in which the input values are outputed
            enable_decimator <= '1';    --  The output data (data_sync) is updated and subsequently passed to the output signal (o_q)
            next_state <= s_wait;
        
        when s_wait =>     -- State where the counter is evaluated to determine whether the data should be outputted
            if (enable = '1') then
                if (counter_reg = gen_decimation_factor - 1) then   -- The desired number of clock cycles for decimation has been completed
                    counter <= 0;                                   -- The counter is reset
                    next_state  <= s_enable;
                end if;
                next_state <= s_active;
            end if;

        when s_active =>
            counter <= counter_reg + 1;     -- The counter increments
            next_state  <= s_wait;
        
        when others => next_state <= s_idle;

    end case;
end process;

end rtl;
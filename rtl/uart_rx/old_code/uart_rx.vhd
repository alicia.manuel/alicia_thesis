--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: uart_rx.vhd
-- File history:
--      0.0     :   28/10/2022      :   creation
-- Description [CURRENTLY TX BLOCK, UPDATE WITH ACTUAL RX DESCRIPTION]: 
--
-- This block is built out of a counter, a set-reset register and a 10 bit shift register, 
-- and some D flip flops. 
--
-- 
-- The 10 bit shift content is as follows:
--   Bit 9 | Bit 8 | Bit 7 | Bit 6 | Bit 5 | Bit 4 | Bit 3 | Bit 2 | Bit 1 | Bit 0 |
--     D7      D6      D5      D4      D3      D2      D1      D0      0       1
--
-- BIT 0 acts as serial output. At reset bit 0 must be '1', hence the forced '1'. The start 
-- signal rising edge indicates beginning of transmission. This edge sets the SR register,
-- which acts as 'busy'flag externally and, internally, an 'enable' signal. The counter is
-- activated by the enable signal and the count action is triggered by the clock itself. 
-- Every 16 clock rising edges a 'new_byte' flag is asserted for a clock cycle, which in time
-- triggers shift operation on the shift register. 
-- The end of shifting will happen after:
--      1 bit '1' as time delay for start condition
--      1 bit '0' as start condition
--      8 data bits
--    + 1 stop bit                          
--    --------------------------------------
--      11 bits * (16 clock cycles/1 bit) = |176 clock cycles|
--                                          __________________

-- 176 clock cycles imply count value = 175 = 0xAF. That's when the counter and SR register
-- are reset to restore the flags to their initial value. 
-- Targeted device: <Family::PolarFire> <Die::MPF300T> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity uart_rx is
port (
    i_clk           : in std_logic;
    i_nrst          : in std_logic;
    i_enable        : in std_logic;
    i_rx_ser        : in std_logic;

    o_rx_par        : out std_logic_vector(7 downto 0);
    o_rx_par_valid  : out std_logic
    );
end uart_rx;
architecture rtl of uart_rx is

    constant NEW_BIT_CNT : integer := 7;--15;--7;
    constant FULL_TX_CNT : integer := 159; --318;--159;
    signal clk          : std_logic;
    signal nrst         : std_logic;
    signal rx_par       : std_logic_vector(9 downto 0);
    signal rx_par_valid : std_logic;
    signal rx_ser       : std_logic;

    signal rx_ser_q   : std_logic;

    signal clk_cnt_nrst:   std_logic;
    signal new_bit           :   std_logic;
    signal clk_cnt, clk_cnt_q : unsigned(8 downto 0);
    signal enable, enable_q : std_logic;
--------------------------------------------------------------------------------
--  COMPONENT DECLARATION
--------------------------------------------------------------------------------    

    component sync1b
    port( 
        i_clk   : in std_logic;
        i_d     : in std_logic;
        o_q     : out std_logic    
    );
    end component sync1b;

    component reset_sync
    port( 
        i_clk   : in std_logic;
        i_nrst     : in std_logic;
        o_q     : out std_logic
    );
    end component reset_sync;



begin

--------------------------------------------------------------------------------
--  PORT CONNECTIONS
--------------------------------------------------------------------------------    
          
    clk <= i_clk;
    o_rx_par_valid <= rx_par_valid;

--------------------------------------------------------------------------------
--  SYNCHRONIZERS & DELAYS
--------------------------------------------------------------------------------    
    nrst_sync : reset_sync
    port map( 
        i_clk   => clk,
        i_nrst  => i_nrst,
        o_q     => nrst
    );

    rx_ser_sync : sync1b
    port map( 
        i_clk   => clk,
        i_d     => i_rx_ser,
        o_q     => rx_ser
    );
    
    p_rx_ser_q   :   process(clk)
    begin
        if rising_edge(clk) then
            rx_ser_q <= rx_ser;
        end if;
    end process p_rx_ser_q;
    
    
--------------------------------------------------------------------------------
--  COUNTER PROCESS
--------------------------------------------------------------------------------    

    p_clk_counter : process(nrst, clk_cnt_nrst,  clk)
    begin
        if nrst = '0' or clk_cnt_nrst = '0' then
            clk_cnt <= (others => '0');
        elsif rising_edge(clk) then
            if enable then
                clk_cnt <= clk_cnt_q + 1;
            else clk_cnt <= clk_cnt_q;
            end if;
        end if;
    end process;  
    clk_cnt_q <= clk_cnt; -- counter feedback

--------------------------------------------------------------------------------
--  FLAGS GENERATION
--------------------------------------------------------------------------------    
    -- new bit flag
    new_bit <= '1' when clk_cnt(3 downto 0) = to_unsigned(NEW_BIT_CNT, 4) else '0';

    -- counter/SR reg reset signal
    p_clk_cnt_nrst : process(clk)
    begin
        if rising_edge(clk) then
            clk_cnt_nrst <= '0' when clk_cnt = to_unsigned(FULL_TX_CNT, clk_cnt'length) else '1';
        end if;
    end process p_clk_cnt_nrst;

    -- sr register. 
    p_enable : process(nrst, clk, rx_ser, rx_ser_q, clk_cnt_nrst)
    begin
        if not nrst or not clk_cnt_nrst then
            enable <= '0';
        elsif rising_edge(clk) then
            if rx_ser_q and not rx_ser then -- falling edge
                enable <= '1';
            end if;                 
        end if;
    end process p_enable;
    p_enable_q : process(clk)
    begin
        if rising_edge(clk) then
            enable_q <= enable;
        end if;
    end process p_enable_q;

    -- output valid
    p_rx_par_valid : process(nrst, clk)
    begin
        if not nrst then
            rx_par_valid <= '0';
        elsif rising_edge(clk) then
            if enable_q and not enable then
                if rx_par(9) = '1' and rx_par(0) = '0' then
                    rx_par_valid <= '1';
                else rx_par_valid <= '0';
                end if;
            else rx_par_valid <= '0';
            end if;
        end if;
    end process p_rx_par_valid;
--------------------------------------------------------------------------------
--  SHIFT REGISTER
--------------------------------------------------------------------------------    

    p_rx_par_reg    : process(nrst, clk)
    begin
        if not nrst then
            rx_par <= (others => '0');
        elsif rising_edge(clk) then
            if new_bit then
                rx_par <= rx_ser & rx_par(9 downto 1);
            end if;
        end if;
    end process p_rx_par_reg;

--------------------------------------------------------------------------------
--  OUTPUT REGISTER
--------------------------------------------------------------------------------    
    p_o_rx_par : process(nrst, clk)
    begin
        if not nrst then 
            o_rx_par <= (others => '0');
        elsif rising_edge(clk) then
            if enable_q and not enable then
                o_rx_par <= rx_par(8 downto 1);
            end if;
        end if;
    end process p_o_rx_par;

    

end rtl;

--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: uart_rx.vhd
-- File history:
--      0.0     : 10/1/2023 : Creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: In this module a first operation is performed to know the number of clock rising edges in a bit, this signal is called max. 
-- There is a counter that goes from zero to max and at half of max, which is when we are in the middle of the bit, an enable signal 
-- is activated, which activates a second counter. This counter is the bit counter. When it is between 1 and 8, which is when there 
-- are the data bits, it will activate the shift register to store the data. Finally, when the bit counter reaches 9, it means that 
-- the frame has finished and a reset is made.
-- 
-- Targeted device: <Family::PolarFire> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------

--hola 

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity uart_rx is
generic ( baudrate : integer := 500000;
		clk_freq : integer := 100e6);
port (
    i_clk           :	in std_logic;
    i_nrst          :	in std_logic;
    i_enable        :	in std_logic;
    i_rx_ser        :	in std_logic;

    o_rx_par_valid  :   out std_logic;
    o_rx_par        :   out std_logic_vector(7 downto 0)
    
);
end uart_rx;

architecture architecture_lira_uart_rx of uart_rx is

--------------------------------------------------------------------------------
--  SIGNAL DECLARATION
--------------------------------------------------------------------------------

    signal clk          	: std_logic; 
    signal nrst         	: std_logic;
    signal enable       	: std_logic;
    signal rx_ser       	: std_logic;
	signal rx_par			: std_logic_vector(7 downto 0);	-- Final result of 
	signal rx_par_aux		: std_logic_vector(7 downto 0); -- Shift register output
	
	signal end_frame 		: std_logic_vector(2 downto 0);	-- Indicates the end of the frame
	signal in_process		: std_logic;					-- Indicates when the module is capturing the bits
	signal reset_uart		: std_logic;					-- Module reset
	signal rx_ser_sync		: std_logic_vector(1 downto 0);	-- Syncrhonized data
	signal new_bit			: std_logic;					-- Indicates when a new bit appears
	signal data_bits		: std_logic;					-- Indicates when we are situated in the data bits
	signal sampling			: std_logic;					-- Enable of the sample register
	signal end_sampling		: std_logic;					-- Indicates when the sampling is over
	signal rx_bit			: std_logic;					-- Final value of the bit
	signal one_counter		: integer range 0 to 4;			-- Counter of '1s' obtained from the bit 
	signal zero_counter		: integer range 0 to 4;			-- Counter of '0s' obtained from the bit



	
	signal bit_count : integer range 0 to 10;   -- bit count within the received data word + start_bit + stop_bit
	--constant max : integer range 0 to 7 := 4;		-- set to 4 only for simulation purposes
	constant max : integer range 0 to 524288 := clk_freq/baudrate;  -- 50M/110~454545, 50M/115200~434, etc.
	signal baud : integer range 0 to max; -- rx counter at the baud rate

	 
--------------------------------------------------------------------------------
--  COMPONENT DECLARATION
-------------------------------------------------------------------------------- 	 
-- Reset synchronization
component reset_sync is
    port( 
        i_clk       :   in std_logic;
        i_nrst         :   in std_logic;
        o_q         :   out std_logic
    );
end component reset_sync;

begin

reset_uart <= end_frame(0) or not(nrst); -- module reset, is activated in case the frame runs out or the external reset is activated.

--------------------------------------------------------------------------------
--  PORT CONNECTIONS
--------------------------------------------------------------------------------    

    clk <= i_clk;          
    enable <= i_enable;
    rx_ser <= i_rx_ser;
	o_rx_par_valid <= end_frame(2);
	o_rx_par <= rx_par;
	 
--------------------------------------------------------------------------------
--  SYNC NRST  PROCESSES
--------------------------------------------------------------------------------    
    
    nrst_sync : reset_sync
    port map(  
        i_clk => clk, 
        i_nrst   => i_nrst,

        o_q   => nrst
    );

--------------------------------------------------------------------------------
--  MSIC PROCESSES
--------------------------------------------------------------------------------
	-- Data syncrhronization
	p_data_sync : process(clk) 
	begin
		if (rising_edge(clk)) then
			rx_ser_sync(0) <= rx_ser;
			rx_ser_sync(1) <= rx_ser_sync(0);
		end if;
	end process;
	
	p_in_process : process(clk, reset_uart, bit_count)
	begin
	-- When there is a falling edge of rx_ser_sync, the flip flop loads the value '1'. Since the falling edge acts as an 
	-- enable and is only active during one clock period, the rest of the time the in_process signal will remain '1' until 
	-- a reset is done
		if (rising_edge(clk)) then
			if(reset_uart = '1') then -- Module reset
				in_process <= '0';
			elsif (enable = '1') then
				if (bit_count = 0) then -- Start activated
					if (rx_ser_sync(1) and not(rx_ser_sync(0))) then -- falling edge detection 
						in_process <= '1';
					end if;
				end if;
			end if;
		end if;
	end process;


	p_baud_counter : process(clk, in_process)
	begin
		if (rising_edge(clk)) then
			if (reset_uart = '1') then
				baud <= 0;					-- Initial value
			elsif (enable = '1') then
				if (in_process = '1') then	-- The frame is being captured	
					if (baud = max-1) then		-- max-1 since the counter starts with zero
						baud <= 0;				-- Returns to the initial value
					else
						baud <= baud + 1;		-- Bit period counter
					end if;
				end if;
			end if;
		end if;
	end process;

	new_bit <= '1' when (baud = max-1) else '0'; -- Bit detection

	-- Bit counter 
	p_bit_count : process(clk, reset_uart, in_process, new_bit)
	begin
		if (rising_edge(clk)) then
			if (reset_uart = '1') then
				-- Initial value
				bit_count <= 0;
			elsif (new_bit='1' and in_process = '1') then	-- There is a new bit
				bit_count <= bit_count + 1;					-- Bit counter
				if (bit_count = 9) then						-- Stop bit
					bit_count <= 0;							-- We reset the counter
				end if;
			end if;
		end if;
	end process;

	-- Detects the end of the frame
	p_end_frame : process(clk, bit_count, baud)
	begin
		if (rising_edge(clk)) then
			if (nrst = '0') then
				-- Initial value
				end_frame <= (others=>'0');
			elsif (bit_count=9 and baud=max-3) then	-- The frame has ended
				end_frame(0) <= '1';
			else
				end_frame(0) <= '0';
			end if;
			-- Flip-flops to delay the signal
			end_frame(1) <= end_frame(0);
			end_frame(2) <= end_frame(1);	-- End_frame(2) is activated when rx_par is updated
		end if;
	end process;		
	
	-- Enable of the catch bit register
	p_sampling : process(clk, data_bits, baud)
	begin
		if (rising_edge(clk)) then
			if (nrst='0') then
				-- Initial value
				sampling <= '0';
				end_sampling <= '0';
			elsif (data_bits = '1') then
				end_sampling <= '0';
				if (baud = ((max-1)/2)-1) then		-- A rising edge of clk before we get to the center of the bit
					sampling <= '1';				-- Sampling condition begins
				elsif (baud = ((max-1)/2)+2) then	-- Two rising edges of clk after the center of the bit
					-- Four samples have been obtained from the bit therefore sampling condition stops
					sampling <= '0';	
					end_sampling <= '1';			
				end if;
			end if;
		end if;
	end process;

	-- We classify the samples obtained from bit
	p_catch_bits : process (clk, sampling)
	begin
		if (rising_edge(clk)) then
			if (sampling = '1') then					-- Sampling condition enabled
				if (rx_ser_sync(1)='1') then
					one_counter <= one_counter + 1;		-- Counter of '1s' obtained from the bit
				else
					zero_counter <= zero_counter + 1;	-- Counter of '0s' obtained from the bit
				end if;
			else										-- Counters are reset
					one_counter <= 0;
					zero_counter <= 0;
			end if;
		end if;
	end process;

	-- The final bit chosen depends on the counters
	rx_bit <= '1' when (end_sampling = '1' and one_counter > zero_counter) else '0';


	data_bits <= '1' when (bit_count >= 1 and bit_count <= 8) else '0';


	-- Shift register
	p_shift_reg : process(clk, end_sampling, nrst)
	begin
		if (rising_edge(clk)) then
			if (nrst = '0') then
				rx_par_aux <= (others=>'0');	
			elsif (end_sampling = '1') then
				rx_par_aux <= rx_bit & rx_par_aux(7 downto 1);
				if (bit_count = 9 and rx_bit = '0') then -- if the stop bit is not detected
				-- the output would be null
 					rx_par_aux <= (others => '0');
				end if;
			end if;
		end if;
	end process;


	-- Output register
	p_rx_par : process(clk, nrst, end_frame(1))
	begin
		if (rising_edge(clk)) then
			if (nrst = '0') then
				rx_par <= (others => '0');
			elsif (end_frame(1) = '1') then
				rx_par <= rx_par_aux;
			end if;
		end if;
	end process;

end ;

set BASE_DIR "./../.."

if {[file exists presynh/_info]} {
    echo "INFO: Simulation library presynth already exists"
} else {
   file delete -force presynth 
   vlib presynth
}
vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/adaptative_counter/tb_adaptative_counter.vhd"


vsim -L presynth -t 1ps presynth.tb_adaptative_counter

add wave /tb_adaptative_counter/*

run 1us
set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/fir_fsm/fir_fsm.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/fir_fsm/tb_fir_fsm.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_fir_fsm

add wave -radix hexadecimal /tb_fir_fsm/*
add wave -radix hexadecimal /tb_fir_fsm/fir_fsm_0/*

run 0.5 ms
set BASE_DIR "./../.."

if {[file exists postsynth/tb_fir_fsm]} { 								
	echo "INFO: Simulation library postsynth already exists" 
} else {
	file delete -force postsynth
	vlib postsynth
}

vmap postsynth postsynth
#vmap PolarFire "C:/Microsemi/Libero_SoC_v2021.3/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"
vmap PolarFire "C:/Microchip/Libero_SoC_v2022.2/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/prj/synth/synthesis/fir_fsm.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/tb/fir_fsm/tb_fir_fsm.vhd"

vsim -debugDB -L PolarFire -L postsynth -t 1ps postsynth.tb_fir_fsm

add wave -radix hexadecimal /tb_fir_fsm/*
add wave -radix hexadecimal /tb_fir_fsm/fir_fsm_0/*

run 0.2 ms
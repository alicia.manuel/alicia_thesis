set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/flipflop/flipflop.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/FIR_vhd/FIR.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/decimator/decimator.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/FIR_system/FIR_system_2.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/fir_smarthls/hls_output/rtl/fir_smarthls_hw_fir.vhd"
vlog -sv -work presynth "${BASE_DIR}/rtl/fir_smarthls/hls_output/rtl/fir_smarthls_hw_fir.v"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/FIR/tb_FIR.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_FIR

add wave -radix hexadecimal /tb_FIR/*
add wave -radix hexadecimal /tb_FIR/FIR_0/*
add wave -radix hexadecimal /tb_FIR/FIR_0/reg(0)/d0/cmp0/*
add wave -radix hexadecimal /tb_FIR/FIR_0/reg(1)/d0/cmpi/*
#add wave -radix hexadecimal /tb_FIR/hw_fir_top_inst/*

run 1000 ms
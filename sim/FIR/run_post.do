set BASE_DIR "./../.."

if {[file exists postsynth/tb_FIR]} { 								
	echo "INFO: Simulation library postsynth already exists" 
} else {
	file delete -force postsynth
	vlib postsynth
}

vmap postsynth postsynth
#vmap PolarFire "C:/Microsemi/Libero_SoC_v2021.3/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"
vmap PolarFire "C:/Microchip/Libero_SoC_v2022.2/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

#vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
#vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
#vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/flipflop/flipflop.vhd"
#vcom -2008 -explicit -work postsynth "${BASE_DIR}/rtl/fir/hls_output/rtl/fir_hw_fir.vhd"
#vlog -sv -work postsynth "${BASE_DIR}/prj/FIR_smarthls/synthesis/hw_fir_top.v"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/prj/FIR_synth/synthesis/FIR.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/tb/FIR/tb_FIR.vhd"

vsim -debugDB -L PolarFire -L postsynth -t 1ps postsynth.tb_FIR

add wave -radix hexadecimal /tb_FIR/*
add wave -radix hexadecimal /tb_FIR/FIR_0/*
#add wave -radix hexadecimal /tb_FIR/hw_fir_top_inst/*

run 12 ms

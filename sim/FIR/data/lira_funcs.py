#!/usr/bin/env python
# coding: utf-8

import numpy as np
from numpy import pi

from scipy import signal, fft
import scipy.integrate as integrate

import sys

# sys.path.append(r'D:\repos\ieec\lisa\SW\pydiag')
# sys.path.append(r'E:\repos\LISA_SW')
# from pydiag.filters.fir import fir_lpf_design, fir_apply_filter
# from pydiag.dataObjects.common_functions import asd, find_nearest, plot_asd

# from multiprocessing import Pool, shared_memory
import itertools


def sinc(x):
    # a = np.pi*x
    a = x
    # Will raise a warning due to division by 0
    res = np.sin(a) / a
    res[np.where(x == 0)] = 1.0
    return res


def V_mod_ramp_func(beta=10e3, t_start=-0.5e-3, t_end=0.5e-3, n_points=1e4):
    t_vec = np.linspace(t_start, t_end, int(n_points))
    return beta * t_vec, t_vec


def quantize_amplitude_array(array, n_bits):
    remove_n_bits = np.ceil(np.log2(array.max())) - n_bits
    if remove_n_bits <= 0:
        return array
    else:
        return (2 ** remove_n_bits) * np.round(array / (2 ** remove_n_bits))


def quantize_time_array(array, decimation_factor):
    if decimation_factor <= 1:
        return array
    else:
        ds_array = array[::decimation_factor]
        return np.repeat(ds_array, decimation_factor)


def V_mod_quantified_ramp(beta=10e3, t_start=-0.5e-3, t_end=0.5e-3, n_points=1e4, n_bits=16, time_decimation=1):
    '''

    beta = Delta_T_max*f_s_mod
    n_points=f_s_dac/f_s_mod
    time_decimation=int(f_s/f_s_dac)

    '''
    if time_decimation <= 1:
        time_decimation = 1
    V_vec, t_vec = V_mod_ramp_func(beta=beta, t_start=t_start, t_end=t_end, n_points=n_points * time_decimation)
    V_vec = quantize_amplitude_array(V_vec, n_bits)
    V_vec = quantize_time_array(V_vec, time_decimation)
    return V_vec, t_vec


def I_pd_analytical_func(V, lambda_1=1.4e-6, lambda_2=1.6e-6, L_1=2e-3, L_2=1e-3, L_3=86e-3, n_1=1.00027316,
                         n_2=1.00027316, e_V=100e-6 / 150, alpha_T=23.1e-6, delta_T=0, A=1, B=1, P_opt=None):
    """
    V: piezo modulator voltage signal [V]
    lambda_1: lower limit of the light incomming on the PD (laser, filter, etc) [m]
    lambda_2: higher limit of the light incomming on the PD [m]
    L_1: length of the cavity of the interferometer with the piezo modulator [m]
    L_2: length of the cavity of the interferometer with the temperature sensor [m]
    L_3: size of the metal layer of the sensor [m]
    n_1: refraction index of first interferometer
    n_2: refraction index of second interferometer
    e_V: transfer function of the piezo modulator [m/V]
    alpha_T: sensitivity of the temperature sensor [K^-1]

    """

    k_1 = 2 * pi / lambda_1
    k_2 = 2 * pi / lambda_2
    Lambda_1 = n_1 * L_1 + n_1 * e_V * V
    Lambda_2 = n_2 * (2 * L_2 + L_3 * alpha_T * delta_T)
    delta_Lambda = Lambda_1 - Lambda_2

    I_0 = (A ** 2) * (B ** 2) * 2 * pi * 4 / (8 * (k_1 + k_2) ** 2)

    I_pd = I_0 * (k_1 * sinc(k_1 * delta_Lambda) - k_2 * sinc(k_2 * delta_Lambda))
    I_pd += (A ** 2) * (B ** 2) * (lambda_2 - lambda_1) / 4

    if P_opt is not None:
        I_pd = I_pd * P_opt / I_pd.max()

    return I_pd


def i_pd_bw_integral(x, Lambda_1, Lambda_2, A, B):
    return np.cos(2 * pi * (Lambda_1 - Lambda_2) / x)


def I_pd_integral_bw_func(V_vec, lambda_1=1.4e-6, lambda_2=1.6e-6, L_1=1e-3, L_2=1e-3, L_3=86e-3, n_1=1.00027316,
                          n_2=1.00027316,
                          e_V=10e-6, alpha_T=50e-6, delta_T=0, A=1, B=1):
    Lambda_2 = n_2 * (2 * L_2 + L_3 * alpha_T * delta_T)
    i_pd_vec = np.empty(V_vec.size)

    Lambda_1_vec = n_1 * (L_1 + e_V * V_vec)
    for i, Lambda_1 in enumerate(Lambda_1_vec):
        i_pd_vec[i] = integrate.quad(i_pd_bw_integral, lambda_1, lambda_2, args=(Lambda_1, Lambda_2, A, B),
                                     epsabs=1e-13)[0]

    I_0 = (A ** 2) * (B ** 2) / 8

    return I_0 * i_pd_vec


def V_pd_integral_bw_func(V_vec, pd_resp=1.0, r_gain=1e3, **kwargs):
    return pd_resp * r_gain * I_pd_integral_bw_func(V_vec, kwargs)


def i_pd_full_integral(x, Lambda_1, Lambda_2, A, B):
    return ((A * np.cos(pi * Lambda_1 / x) + (1 - A)) ** 2) * ((B * np.cos(pi * Lambda_2 / x) + (1 - B)) ** 2)
    # return ((A * np.cos(x*Lambda_1) + (1-A))**2)  * ((B * np.cos(x*Lambda_2) + (1-B))**2)


def I_pd_integral_trapz_func(V_vec, lambda_1=1.4e-6, lambda_2=1.6e-6, L_1=1e-3, L_2=1e-3, L_3=86e-3, n_1=1.00027316,
                             n_2=1, P_opt=None,
                             e_V=10e-6, alpha_T=23.1e-6, delta_T=0, A=1, B=1, dlambda=1e-12):
    Lambda_2 = n_2 * (2 * L_2 + L_3 * alpha_T * delta_T)
    i_pd_vec = np.empty(V_vec.size)
    lambda_vec = np.arange(lambda_1, lambda_2, dlambda)

    Lambda_1_vec = n_1 * (L_1 + e_V * V_vec)
    for i, Lambda_1 in enumerate(Lambda_1_vec):
        y = i_pd_full_integral(lambda_vec, Lambda_1, Lambda_2, A, B)
        # y = i_pd_bw_integral(lambda_vec, Lambda_1, Lambda_2, A, B)

        i_pd_vec[i] = integrate.trapz(y, lambda_vec)

    if P_opt is not None:
        i_pd_vec = i_pd_vec * P_opt / (i_pd_vec.max() - i_pd_vec.mean())

    return i_pd_vec


def get_carrier_freq(lambda_1=1.4e-6, lambda_2=1.6e-6, n_1=1.00027316, e_V=15e-6, beta=10e3):
    return (1 / 2) * n_1 * e_V * beta * (lambda_2 + lambda_1) / (lambda_2 * lambda_1)


def get_sync_freq(lambda_1=1.4e-6, lambda_2=1.6e-6, n_1=1.00027316, e_V=15e-6, beta=10e3):
    return (1 / 2) * n_1 * e_V * beta * (lambda_2 - lambda_1) / (lambda_2 * lambda_1)


def sensitivity(L_3, alpha_T=23.1e-6, e_V=100e-6 / 150, beta=150 * 1e3, n_1=1.00027316, n_2=1.00027316):
    return n_2 * 2* L_3 * alpha_T / (n_1 * e_V * beta)


def get_relative_T0_phase(n_2=1.00027316, L_3=86e-3, alpha_T=50e-6, delta_T=10, lambda_1=1.4e-6, lambda_2=1.6e-6):
    k_1 = 2 * pi / lambda_1
    k_2 = 2 * pi / lambda_2
    return (- n_2 * L_3 * alpha_T * delta_T) * (k_2 - k_1) / 2


def delay_2_phase(delay, sync_freq):
    return delay * (2 * pi * sync_freq)


def envelope_phase_2_temp(delta_phi, n_2=1.00027316, L_3=86e-3, alpha_T=23.1e-6, lambda_1=1.5e-6, lambda_2=1.6e-6):
    return delta_phi * lambda_1 * lambda_2 / ((pi * n_2 * L_3 * alpha_T) * (lambda_2 - lambda_1))


def carrier_phase_2_temp(delta_phi, n_2=1.00027316, L_3=86e-3, alpha_T=23.1e-6, lambda_1=1.5e-6, lambda_2=1.6e-6):
    return delta_phi * lambda_1 * lambda_2 / ((pi * n_2 * L_3 * alpha_T) * (lambda_2 + lambda_1))


def delay_2_temp(delay, n_2=1.0, L_3=86e-3, alpha_T=23.1e-6, n_1=1.00027316, e_V=100e-6 / 150, beta=150 * 1e3):
    return delay * (n_1 * e_V * beta) / (n_2 * L_3 * alpha_T)


def old_temp_to_delay(sync_freq, delta_T=1, n_2=1.00027316, L_3=86e-3, alpha_T=23.1e-6, lambda_1=1.5e-6,
                      lambda_2=1.6e-6):
    return delta_T * n_2 * L_3 * alpha_T * (lambda_2 - lambda_1) / (2 * sync_freq * lambda_1 * lambda_2)


def temp_to_delay(delta_T, n_2=1.00027316, L_3=86e-3, alpha_T=23.1e-6, n_1=1.00027316, e_V=100e-6 / 150,
                  beta=150 * 1e3):
    return delta_T * n_2 * L_3 * alpha_T / (n_1 * e_V * beta)


def find_integer_displacement(I_pd_ref, I_pd_sensor, method='same', min_pos=None, max_pos=None, pos_range=None):
    """


    """
    if method == "subwindow" or method == "subwindow-ssd":
        if pos_range is None:
            if min_pos is not None and max_pos is not None:
                pos_range = max_pos - min_pos
            else:
                pos_range = int(I_pd_ref.size * 0.75)
                # Ensure even numbers
                pos_range += pos_range % 2
        if min_pos is None or max_pos is None:
            min_pos = int(np.argmax(I_pd_ref) - pos_range // 2)
            min_pos += min_pos % 2
            max_pos = int(np.argmax(I_pd_ref) + pos_range // 2)
            max_pos += max_pos % 2

    if method == 'same':
        corr = signal.correlate(I_pd_ref, I_pd_sensor, mode='same')
        int_disp = (corr.size // 2) - np.argmax(corr)
        if (I_pd_ref.size // 2 != I_pd_sensor.size / 2):
            print("ERROR, windows size should be even")
    elif method == "same-weighted":
        # corr = direct_same_time_correlation(I_pd_ref, I_pd_sensor, weighted=True)
        corr = same_time_weighted_correlation(I_pd_ref, I_pd_sensor)
        int_disp = (corr.size // 2) - np.argmax(corr)
    elif method == "same-norm":
        # corr = direct_same_time_correlation(I_pd_ref, I_pd_sensor, normalized=True)
        corr = same_time_normalized_correlation(I_pd_ref, I_pd_sensor)
        int_disp = (corr.size // 2) - np.argmax(corr)
    elif method == "same-norm-par":
        # Don't use, slower than serial version
        corr = direct_same_time_correlation_parallel(I_pd_ref, I_pd_sensor)
        int_disp = (corr.size // 2) - np.argmax(corr)
    elif method == 'subwindow':
        corr = signal.correlate(I_pd_ref[min_pos:max_pos], I_pd_sensor, mode='valid')
        int_disp = (corr.size // 2) - np.argmax(corr)
    elif method == 'subwindow-same':
        corr = signal.correlate(I_pd_ref, I_pd_sensor, mode='same')
        int_disp = (corr.size // 2) - np.argmax(corr)
    elif method == 'subwindow-ssd':
        # TODO Needs testing
        corr = direct_subwindow_correlation(I_pd_ref, I_pd_sensor, A_min=min_pos, A_max=max_pos, method="ssd")
        int_disp = (corr.size // 2) - np.argmin(corr)
    elif method == 'subwindow-sad':
        # TODO Needs testing
        corr = direct_subwindow_correlation(I_pd_ref, I_pd_sensor, A_min=min_pos, A_max=max_pos, method="sad")
        int_disp = (corr.size // 2) - np.argmin(corr)
    return corr, int_disp


def matched_iir_time_diff(I_pd_Tref, I_pd_Tsensor, fs, bw_min=62e3, bw_max=67e3,
                          order=2, sos=None):
    if sos is None:
        sos = signal.butter(order, (bw_min / (fs / 2), bw_max / (fs / 2)),
                            output='sos', analog=False, btype='bandpass')
    I_pd_Tref_flt = signal.sosfiltfilt(sos, I_pd_Tref)
    I_pd_Tsensor_flt = signal.sosfiltfilt(sos, I_pd_Tsensor)
    delay_ref = data_maximum(I_pd_Tref_flt)
    delay_sensor = data_maximum(I_pd_Tsensor_flt)

    return (delay_sensor - delay_ref) / fs


def bandpass_iir_time_diff(I_pd_Tref, I_pd_Tsensor, fs, bw_min=62e3, bw_max=67e3,
                           order=2, sos=None):
    if sos is None:
        sos = signal.butter(order, (bw_min / (fs / 2), bw_max / (fs / 2)),
                            output='sos', analog=False, btype='bandpass')
    I_pd_Tref_flt = signal.sosfilt(sos, I_pd_Tref)
    I_pd_Tsensor_flt = signal.sosfilt(sos, I_pd_Tsensor)

    delay_ref = data_maximum(I_pd_Tref_flt)
    delay_sensor = data_maximum(I_pd_Tsensor_flt)

    return (delay_sensor - delay_ref) / fs


def matched_fir_time_diff(I_pd_Tref, I_pd_Tsensor, fs, bw_min=62e3, bw_max=67e3,
                          n_taps=2049, taps=None):
    if taps is None:
        taps = signal.firwin(n_taps, (bw_min / (fs / 2), bw_max / (fs / 2)),
                             pass_zero=False)
    I_pd_Tref_flt = signal.filtfilt(taps, 1.0, I_pd_Tref)
    I_pd_Tsensor_flt = signal.filtfilt(taps, 1.0, I_pd_Tsensor)
    delay_ref = data_maximum(I_pd_Tref_flt)
    delay_sensor = data_maximum(I_pd_Tsensor_flt)

    return (delay_sensor - delay_ref) / fs


def direct_maximum(I_pd_Tref, I_pd_Tsensor, fs):
    delay_ref = data_maximum(I_pd_Tref)
    delay_sensor = data_maximum(I_pd_Tsensor)

    return (delay_sensor - delay_ref) / fs


def data_maximum(y_flt):
    int_disp_flt = np.argmax(y_flt)

    paraboloid_max, z, (x, y), res = fit_parabola(y_flt)

    return int_disp_flt + paraboloid_max


def same_time_weighted_correlation(A, B):
    """

    TODO: https://github.com/2b-t/SciPy-stereo-sgm/blob/master/stereo_matching.py
    @jit(nopython = True, parallel = True, cache = True)

    """
    w = list(range(A.size // 2, A.size + 1))
    w_rev = w[1:-1]
    w_rev.reverse()
    w.extend(w_rev)
    corr = signal.correlate(A, B, mode='same')
    return corr / w


def same_time_normalized_correlation(A, B):
    w = calculate_normalization_terms(A, B)
    corr = signal.correlate(A, B, mode='same')
    return corr / w


def single_normalization_terms(A):
    w = np.empty(A.size)

    for i in range(0, w.size // 2):
        A_corr = A[:i + A.size // 2]
        w[i] = np.sqrt(np.sum(A_corr ** 2) ** 2)

    w[w.size // 2] = np.sqrt(np.sum(A ** 2) ** 2)

    for i in range(1, w.size // 2):
        j = A.size - i
        A_corr = A[j - A.size // 2:]
        w[-i] = np.sqrt(np.sum(A_corr ** 2) ** 2)

    return w


def calculate_normalization_terms(A, B):
    w = np.empty(B.size)

    for i in range(0, w.size // 2):
        j = A.size - i
        A_corr = A[:i + B.size // 2]
        B_corr = B[j - B.size // 2:]
        w[i] = np.sqrt(np.sum(A_corr ** 2) * np.sum(B_corr ** 2))

    w[w.size // 2] = np.sqrt(np.sum(A ** 2) * np.sum(B ** 2))

    for i in range(1, w.size // 2):
        j = A.size - i
        A_corr = A[j - B.size // 2:]
        B_corr = B[:i + B.size // 2]
        w[-i] = np.sqrt(np.sum(A_corr ** 2) * np.sum(B_corr ** 2))

    return w


def fft_correlate(A, B, weighted=False):
    """
    ### Correlation and convolution is the same but with time reversed
    signal.correlate(I_pd_T0, I_pd_Tsense, mode='same', method='fft') == signal.fftconvolve(I_pd_T0, I_pd_T0, mode='same')[::-1]

    ### The full mode requires adding padding to the input
    ffcorr = signal.correlate(I_pd_T0, I_pd_Tsense, mode='full', method='fft')
    I_pd_T0_padded = np.zeros(I_pd_T0.size*2)
    I_pd_T0_padded[:I_pd_T0.size] = I_pd_T0
    # The second vector needs to be either the conjugate or the input time reversed
    # The imaginary part of the result can be directly discarded
    fft_res_padded = scipy.fft.ifft(scipy.fft.fft(I_pd_T0_padded) * scipy.fft.fft(I_pd_T0_padded[::-1])).real
    (fft_res_padded.real[:] - ffcorr)

    #Applying only output padding:

    fft_res_outpadded = scipy.fft.ifft(scipy.fft.fft(I_pd_T0, n=2*fft_res.size) * scipy.fft.fft(I_pd_T0[::-1], n=2*fft_res.size)).real
    (fft_res_outpadded.real[:-1] - ffcorr)

    # Without zero-padding it can be also done, but requires resorting the output
    fft_res = scipy.fft.ifft(scipy.fft.fft(I_pd_T0) * scipy.fft.fft(I_pd_T0[::-1])).real
    fft_res_tmp = fft_res.copy()
    fft_res[:fft_res.size//2] = fft_res_tmp[fft_res.size//2:]
    fft_res[fft_res.size//2:] = fft_res_tmp[:fft_res.size//2]

    # Try ignoring FFT phase
    """
    # TODO: Tests this function
    correlation_res = fft.ifft(fft.fft(A, n=2 * B.size) * fft.fft(B[::-1], n=2 * B.size)).real
    if weighted:
        w = calculate_normalization_terms(A, B)
        correlation_res /= w

    return correlation_res


def find_integer_displacement_partial_ref(I_pd_ref, I_pd_sensor, ref_corr_min=250, ref_corr_max=750):
    # TODO: Not working
    corr = signal.correlate(I_pd_ref[ref_corr_min:ref_corr_max], I_pd_sensor, mode='same')
    # int_disp = (I_pd_ref.size // 2) - np.argmax(np.abs(corr))
    int_disp = (ref_corr_max - ref_corr_min) // 2 - np.argmax(corr)
    return corr, int_disp


def envelope_detector(array, filter_coeff):
    array_abs = np.abs(array)


#    return fir_apply_filter(array_abs, filter_coeff)[int(filter_coeff.size // 2):-int(filter_coeff.size // 2)]


def direct_full_time_correlation(A, B):
    '''

    signal.correlate(I_pd_T0, I_pd_T0, mode='valid') = np.sum(I_pd_T0*I_pd_T0)
    signal.correlate(I_pd_T0, I_pd_T0, mode='full')[I_pd_T0.size - 1] = np.sum(I_pd_T0*I_pd_T0)
    signal.correlate(I_pd_T0, I_pd_T0, mode='full')[I_pd_T0.size] = np.sum(I_pd_T0[1:]*I_pd_T0[:-1])

    signal.correlate(I_pd_T0, I_pd_T0, mode='same')[I_pd_T0.size] = np.sum(I_pd_T0[1:]*I_pd_T0[:-1])

    %timeit signal.correlate(I_pd_T0, I_pd_T0, mode='valid', method='fft')
    3.24 ms ± 91.1 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
    %timeit signal.correlate(I_pd_T0, I_pd_T0, mode='valid', method='direct')
    7.06 µs ± 54.5 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)

    %timeit signal.correlate(I_pd_T0, I_pd_T0, mode='full', method='fft')
    3.23 ms ± 63.1 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
    %timeit signal.correlate(I_pd_T0, I_pd_T0, mode='full', method='direct')
    178 ms ± 1.81 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)
    '''
    dcorr = np.empty(2 * B.size - 1)
    for i in range(1, B.size):
        j = A.size - i
        dcorr[i - 1] = np.sum(A[:i] * B[j:])

    dcorr[B.size - 1] = np.sum(A * B)
    for i in range(1, B.size):
        j = A.size - i
        dcorr[-i] = np.sum(A[j:] * B[:i])

    return dcorr


def direct_same_time_correlation(A, B, normalized=True, weighted=False):
    '''
    signal.correlate(I_pd_T0, I_pd_T0, mode='valid') = np.sum(I_pd_T0*I_pd_T0)
    signal.correlate(I_pd_T0, I_pd_T0, mode='full')[I_pd_T0.size - 1] = np.sum(I_pd_T0*I_pd_T0)
    signal.correlate(I_pd_T0, I_pd_T0, mode='full')[I_pd_T0.size] = np.sum(I_pd_T0[1:]*I_pd_T0[:-1])
    signal.correlate(I_pd_T0, I_pd_T0, mode='same')[I_pd_T0.size//2] = np.sum(I_pd_T0*I_pd_T0)
    '''

    dcorr = np.empty(B.size)

    for i in range(0, dcorr.size // 2):
        j = A.size - i
        A_corr = A[:i + B.size // 2]
        B_corr = B[j - B.size // 2:]
        if normalized:
            norm = np.sqrt(np.sum(A_corr ** 2) * np.sum(B_corr ** 2))
        elif weighted:
            norm = A_corr.size
        else:
            norm = 1

        dcorr[i] = np.sum(A_corr * B_corr) / norm

    if normalized:
        norm = np.sqrt(np.sum(A ** 2) * np.sum(B ** 2))
    elif weighted:
        norm = A.size
    else:
        norm = 1
    dcorr[dcorr.size // 2] = np.sum(A * B) / norm

    for i in range(1, dcorr.size // 2):
        j = A.size - i
        A_corr = A[j - B.size // 2:]
        B_corr = B[:i + B.size // 2]
        if normalized:
            norm = np.sqrt(np.sum(A_corr ** 2) * np.sum(B_corr ** 2))
        elif weighted:
            norm = A.size
        else:
            norm = 1
        dcorr[-i] = np.sum(A_corr * B_corr) / norm

    return dcorr


def Fun1(foo):
    i, A_shape, B_shape = foo
    sh_A = shared_memory.SharedMemory(name='A')
    A = np.ndarray(A_shape, dtype=np.dtype('float64'), buffer=sh_A.buf)
    sh_B = shared_memory.SharedMemory(name='B')
    B = np.ndarray(B_shape, dtype=np.dtype('float64'), buffer=sh_B.buf)

    j = A.size - i
    A_corr = A[:i + B.size // 2]
    B_corr = B[j - B.size // 2:]
    norm = A_corr.size * np.std(A_corr) * np.std(B_corr)
    return np.sum(A_corr * B_corr) / norm


def Fun2(foo):
    i, A_shape, B_shape = foo
    sh_A = shared_memory.SharedMemory(name='A')
    A = np.ndarray(A_shape, dtype=np.dtype('float64'), buffer=sh_A.buf)
    sh_B = shared_memory.SharedMemory(name='B')
    B = np.ndarray(B_shape, dtype=np.dtype('float64'), buffer=sh_B.buf)

    j = A.size - i
    A_corr = A[j - B.size // 2:]
    B_corr = B[:i + B.size // 2]
    norm = A_corr.size * np.std(A_corr) * np.std(B_corr)
    return np.sum(A_corr * B_corr) / norm


def direct_same_time_correlation_parallel(A, B):
    shm_A = shared_memory.SharedMemory(create=True, size=A.nbytes, name='A')
    shm_A_np = np.ndarray(A.shape, dtype=A.dtype, buffer=shm_A.buf)
    shm_A_np[:] = A[:]
    shm_B = shared_memory.SharedMemory(create=True, size=B.nbytes, name='B')
    shm_B_np = np.ndarray(B.shape, dtype=B.dtype, buffer=shm_B.buf)
    shm_B_np[:] = B[:]

    dcorr = np.empty(B.size)

    pool = Pool()  # defaults to number of available CPU's
    pool_it = pool.map(Fun1, zip(range(0, dcorr.size // 2), itertools.repeat(A), itertools.repeat(B)))
    for ind, res in enumerate(pool_it):
        dcorr[ind] = res

    norm = A.size * np.std(A) * np.std(B)
    dcorr[dcorr.size // 2] = np.sum(A * B) / norm

    pool_it = pool.map(Fun2, zip(range(0, dcorr.size // 2), itertools.repeat(A), itertools.repeat(B)))
    for ind, res in enumerate(pool_it):
        dcorr[-ind] = res

    shm_A.close()
    shm_A.unlink()
    shm_B.close()
    shm_B.unlink()

    # pool.join()
    pool.close()

    return dcorr


def direct_subwindow_correlation(A, B, A_min=None, A_max=None, method="correlation"):
    """

    signal.correlate(A[A_min:A_max], B, mode='valid')
    """
    if A_min is None or A_max is None:
        A_min = int(A.size / 4)
        A_max = int(A.size * 3 / 4)
    if B.size <= (A_max - A_min):
        raise RuntimeError("B size needs to be bigger than (A_max - A_min)")
    corr_size = B.size - (A_max - A_min) + 1
    dcorr = np.empty(corr_size)

    A_corr = A[A_min:A_max]

    for i in range(0, corr_size):
        if method == "correlation":
            dcorr[i] = np.sum(A_corr * B[i:i + corr_size - 1])
        elif method == "ssd":
            dcorr[i] = np.sum((A_corr - B[i:i + corr_size - 1]) ** 2)
        elif method == "sad":
            dcorr[i] = np.sum(np.abs(A_corr - B[i:i + corr_size - 1]))
    return dcorr


def fit_parabola(corr_vector, fit_n_points=4):
    y = corr_vector[np.argmax(corr_vector) - int(fit_n_points / 2):
                    np.argmax(corr_vector) + int(fit_n_points / 2 + 1)]
    # x = np.arange(0, (parab_fit_points + 1)/f_s_adc, 1/f_s_adc)
    x = np.arange(0, (fit_n_points + 1), 1) - fit_n_points / 2

    # y = a*x^2 + b*x + c
    z, res = np.polyfit(x, y, 2, cov=True)

    a = z[0]
    b = z[1]
    paraboloid_max = -b / (2 * a)

    return paraboloid_max, z, (x, y), np.sqrt(res.diagonal().sum())


def fit_parabola_direct(corr_vector):
    x1 = -1;
    x2 = 0;
    x3 = 1
    x = np.array([x1, x2, x3])
    y1 = corr_vector[np.argmax(corr_vector) - 1]
    y2 = corr_vector[np.argmax(corr_vector)]
    y3 = corr_vector[np.argmax(corr_vector) + 1]
    y = np.array([y1, y2, y3])

    # denom = (x1 - x2)*(x1 - x3)*(x2 - x3)
    # a = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom
    # b = (x3 ** 2 * (y1 - y2) + x2 ** 2 * (y3 - y1) + x1 ** 2 * (y2 - y3)) / denom
    # c = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom
    #
    # c = x3 * x1 * (x3 - x1) * y2  / denom
    #
    # # y = a*x^2 + b*x + c
    # paraboloid_max = -b / (2 * a)

    paraboloid_max = (1 / 2) * (-y3 + y1) / (y3 - 2 * y2 + y1)

    return paraboloid_max, None, (x, y), None, None


def brute_force_maximum_arb_order(corr_vector, poly_order, fit_n_points=None, f_s=10e6):
    if fit_n_points is None:
        fit_n_points = 2 * poly_order

    y = corr_vector[np.argmax(corr_vector) - int(fit_n_points / 2):
                    np.argmax(corr_vector) + int(fit_n_points / 2 + 1)]

    x = np.arange(0, (fit_n_points + 1), 1) - fit_n_points / 2

    z, res = np.polyfit(x, y, poly_order, cov=True)
    p = np.poly1d(z)

    req_res = 10 * 2e9 / f_s
    new_x = np.arange(-1, 1, 1 / req_res)
    new_y = p(new_x)
    paraboloid_max = new_x[np.argmax(new_y)]

    return paraboloid_max, z, (x, y), np.sqrt(res.diagonal().sum()), (new_x, new_y)

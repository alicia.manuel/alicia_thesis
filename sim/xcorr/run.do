set BASE_DIR "./../.."


if {[file exists presynth/_info]} {
   echo "INFO: Simulation library presynth already exists"
} else {
   file delete -force presynth 
   vlib presynth
}

vmap presynth presynth

vlog -sv -work presynth "${BASE_DIR}/rtl/xcorr_smarthls/hls_output/rtl/xcorr_smarthls_top_xcorr.v"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/xcorr_smarthls/hls_output/rtl/xcorr_smarthls_top_xcorr.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/MAC/MAC.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/pkg_array/pkg_array.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/xcorr/CrossCorrelation.vhd"
#vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/xcorr/parallelized_xcorr.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/xcorr/CrossCorrelation_TB.vhd"

vsim -debugDB -L presynth -t 1ps presynth.CrossCorrelation_TB
add wave -radix decimal /CrossCorrelation_TB/*
add wave -radix decimal /CrossCorrelation_TB/top_xcorr_top_vhdl_0/*
#add wave -radix decimal /CrossCorrelation_TB/CrossCorrelation_0/*
#add wave -radix decimal /CrossCorrelation_TB/CrossCorrelation_0/MAC_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(0)/cmpi/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(0)/cmpi/MAC_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(1)/cmpi/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(1)/cmpi/MAC_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(2)/cmpi/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(2)/cmpi/MAC_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(3)/cmpi/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(3)/cmpi/MAC_0/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(4)/cmpi/*
#add wave -radix decimal /CrossCorrelation_TB/parallelized_xcorr_0/reg(4)/cmpi/MAC_0/*

run 1000 us
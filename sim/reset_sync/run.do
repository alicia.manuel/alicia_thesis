set BASE_DIR "./../.."

if {[file exists presynth/_info/]} { 								
	echo "INFO: Simulation library presynth already exists" 
} else {
	file delete -force presynth
	vlib presynth
}

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/reset_sync/tb_reset_sync.vhd"


vsim -L presynth -t 1ps presynth.tb_reset_sync

add wave /tb_reset_sync/*
add wave /tb_reset_sync/reset_sync_inst/*
#add wave /tb_reset_sync/reset_sync_inst/q2

run 500 ns
set BASE_DIR "./../.."

if {[file exists postsynth/tb_FIR_system]} { 								
	echo "INFO: Simulation library postsynth already exists" 
} else {
	file delete -force postsynth
	vlib postsynth
}

vmap postsynth postsynth
#vmap PolarFire "C:/Microsemi/Libero_SoC_v2021.3/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"
vmap PolarFire "C:/Microchip/Libero_SoC_v2022.2/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

#vlog -sv -work postsynth "${BASE_DIR}/prj/FIR_smarthls/synthesis/hw_fir_top.v"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/prj/FIR_system/synthesis/FIR_system.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/tb/FIR_system/tb_FIR_system.vhd"

vsim -debugDB -L PolarFire -L postsynth -t 1ps postsynth.tb_FIR_system

add wave -radix hexadecimal /tb_FIR_system/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/*
#add wave -radix hexadecimal /tb_FIR_system/hw_fir_top_inst/*

run 17 ms
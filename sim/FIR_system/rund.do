set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/flipflop/flipflop.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_sim/uart_sim_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_rx/uart_rx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_tx/uart_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/byte_joiner/byte_joiner.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/data_to_bytes/data_to_bytes.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/RAM/RAM.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/interfaces/interface_in.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/interfaces/interface_out_synth.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/fir_fsm/fir_fsm.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/FIR_vhd/FIR.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/decimator/decimator.vhd"
#vlog -sv -work presynth "${BASE_DIR}/rtl/fir/hls_output/rtl/fir_hw_fir.v"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/FIR_system/FIR_system.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/FIR_system/tb_FIR_system.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_FIR_system

add wave -radix hexadecimal /tb_FIR_system/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/fir_fsm_0/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/interface_in_0/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/interface_out_0/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/RAM_0/*
add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/RAM_0/mem
#add wave -radix hexadecimal /tb_FIR_system/uart_sim_tx_0/*
#add wave -radix hexadecimal /tb_FIR_system/uart_rx_0/*
add wave -radix hexadecimal /tb_FIR_system/byte_joiner_0/*
#add wave -radix hexadecimal /tb_FIR_system/FIR_system_0/reg(0)/d0/cmp0/*

run 2000 ms
set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/flipflop/flipflop.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/decimator/decimator.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/decimator/tb_decimator.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_decimator

add wave -radix hexadecimal /tb_decimator/*
add wave -radix hexadecimal /tb_decimator/decimator_0/*
add wave -radix hexadecimal /tb_decimator/decimator_0/flipflop_0/*
add wave -radix hexadecimal /tb_decimator/decimator_0/flipflop_1/*

run 10 us
set BASE_DIR "./../.."

if {[file exists postsynth/_info]} {
    echo "INFO: Simulation library postsynth already exists"
    file delete -force postsynth 
}    
vlib postsynth

vmap postsynth postsynth

vcom -2008 -explicit -work postsynth "${BASE_DIR}/prj/decimator/synthesis/decimator.vhd"
vcom -2008 -explicit -work postsynth "${BASE_DIR}/tb/decimator/tb_decimator.vhd"

vsim -debugDB -L postsynth -t 1ps postsynth.tb_decimator

add wave -radix hexadecimal /tb_decimator/*
add wave -radix hexadecimal /tb_decimator/decimator_0/*

run 10 us
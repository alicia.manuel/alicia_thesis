set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/adaptative_counter/adaptative_counter.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/interfaces/interface_in_output.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/interfaces/tb_interface_in_output.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_interface_in_output

add wave -radix hexadecimal /tb_interface_in_output/*
add wave -radix hexadecimal /tb_interface_in_output/interface_in_output_0/*

run 1 ms
set BASE_DIR "./../.."

if {[file exists presynth/tb_uart_tx]} { 								
	echo "INFO: Simulation library presynth already exists" 
} else {
	file delete -force presynth
	vlib presynth
}

vmap presynth presynth
vmap PolarFire "C:/Microchip/Libero_SoC_v2022.2/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_tx/uart_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/uart_tx/tb_uart_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_sim/uart_sim_rx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/../LIRA/libero/lira_fpga/proba_uart/synthesis/uart_tx.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_uart_tx



add wave /tb_uart_tx/*
#add wave /tb_uart_tx/uart_rx_0/*
#add wave /tb_uart_tx/uart_sim_rx_0/*
add wave /tb_uart_tx/uart_tx_0/*


run 10 ms
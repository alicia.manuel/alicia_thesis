set BASE_DIR "./../.."

if {[file exists presynth/_info]} {
    echo "INFO: Simulation library presynth already exists"
    file delete -force presynth 
}    
vlib presynth

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/reset_sync/reset_sync.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_sim/uart_sim_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_rx/uart_rx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/uart_rx/tb_uart_rx.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_uart_rx

add wave /tb_uart_rx/*
add wave /tb_uart_rx/uart_rx_0/*
add wave /tb_uart_rx/uart_sim_tx_0/*


run 1 ms
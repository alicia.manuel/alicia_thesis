set BASE_DIR "./../.."

if {[file exists presynth/tb_uart_rx]} { 								
	echo "INFO: Simulation library presynth already exists" 
} else {
	file delete -force presynth
	vlib presynth
}

vmap presynth presynth
vmap PolarFire "C:/Microchip/Libero_SoC_v2022.2/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/uart_sim/uart_sim_tx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/prj/proba_uart/synthesis/uart_rx.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/uart_rx/tb_uart_rx.vhd"

vsim -debugDB -L presynth -L PolarFire -t 1ps presynth.tb_uart_rx

add wave /tb_uart_rx/*
add wave /tb_uart_rx/uart_rx_0/*
add wave /tb_uart_rx/uart_sim_tx_0/*


run 1 ms

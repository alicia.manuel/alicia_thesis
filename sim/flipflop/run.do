set BASE_DIR "./../.."


if {[file exists presynth/_info]} {
   echo "INFO: Simulation library presynth already exists"
} else {
   file delete -force presynth 
   vlib presynth
}

vmap presynth presynth

vcom -2008 -explicit -work presynth "${BASE_DIR}/rtl/flipflop/flipflop.vhd"
vcom -2008 -explicit -work presynth "${BASE_DIR}/tb/flipflop/tb_flipflop.vhd"

vsim -debugDB -L presynth -t 1ps presynth.tb_flipflop
add wave -radix decimal /tb_flipflop/*
add wave -radix decimal /tb_flipflop/flipflop_0/*


run 10 us
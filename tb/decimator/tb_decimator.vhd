--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_decimator.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
use std.textio.all;


entity tb_decimator is
end tb_decimator;

architecture behavioral of tb_decimator is

    constant SYSCLK_PERIOD      : time := 100 ns; -- 10MHZ
    constant LENGTH_DATA        : integer := 3;
    constant DECIMATOR_FACTOR   : integer := 2;
    constant N_POINTS          : integer := 10;
    
	-- inputs
    signal SYSCLK			:   std_logic := '0';
    signal NSYSRESET		:   std_logic := '0';
    signal uut_enable       :   std_logic := '0';
    signal uut_data_in      :   std_logic_vector(LENGTH_DATA-1 downto 0) := (others => '0');

    -- outputs
    signal uut_data_out     :   std_logic_vector(LENGTH_DATA-1 downto 0);
    signal uut_enable_decimator :   std_logic;

    -- auxilliary
    type t_buff is array (0 to N_POINTS - 1) of integer;
    signal data : t_buff := (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

begin

    p_reg : process
    
    begin
		-- Assert Reset
		NSYSRESET <= '0';
		wait for ( SYSCLK_PERIOD * 2);
        wait until rising_edge(SYSCLK);
		NSYSRESET <= '1';
		for i in 0 to N_POINTS-1 loop
            wait until rising_edge(SYSCLK);
            uut_enable  <= '1';
            uut_data_in <= std_logic_vector(to_unsigned(data(i), uut_data_in'length));
            wait for (SYSCLK_PERIOD);
        end loop;
        uut_enable  <= '0';
        wait;
    end process;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    decimator_0 : entity work.decimator
    -- generic map(
    --     gen_data_length     => LENGTH_DATA,
    --     gen_decimation_factor   => DECIMATOR_FACTOR
    -- )
    port map(
        i_clk       =>  SYSCLK,
        i_nrst      =>  NSYSRESET,
        i_enable    =>  uut_enable, 
        i_d         =>  uut_data_in,
        o_enable_decimator  => uut_enable_decimator,
        o_q         =>  uut_data_out
    );

end behavioral;
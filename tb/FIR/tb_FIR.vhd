--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_FIR.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.fixed_pkg.all;
use IEEE.MATH_REAL.ALL;



entity tb_FIR is
end tb_FIR;

architecture behavioral of tb_FIR is

    constant SYSCLK_PERIOD : time := 10 ns; -- 100MHZ
	constant CONST_MEM_ADDR_SIZE  : integer := 15;
	constant CONST_SIGNAL_LENGTH  : integer := 2000;
    constant CONS_INTEGER_SIZE : integer := 2;
    constant CONS_DECIMAL_SIZE : integer := 30;


	-- inputs
    signal SYSCLK			: std_logic := '0';
    signal NSYSRESET		: std_logic := '0';
    signal uut_start            : std_logic := '0';
	signal uut_fir_finish, uut_fir_finish_1	: std_logic	:= '0';
	signal uut_data_in_ready    : std_logic := '0';
	-- outputs
	signal uut_fir_output   : std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0);

    -- auxilliary
    signal uut_data_fir_ready  : std_logic;
    signal FIR_input           : std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0);

	-- filer
	file Fin_d1 : text;
	file Fin_d2 : text;

    -- component hw_fir_top
    -- port (
    --         clk	:	in	std_logic;
    --         reset	:	in	std_logic;
    --         start	:	in	std_logic;
    --         ready	:	out	std_logic;
    --         finish	:	out	std_logic;
    --     return_val	:	out	std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0);
    --     input_var	:	in	std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0)
    -- );
    -- end component;

begin

    p_read : process
        
		--C:\Users\Alicia\Documents\lira_fpga\sim\xcorr
		file Fin_d1	: TEXT open READ_MODE is "./data/noisy_signal.txt";
		variable text_line_1 : line;
		
		--variable in_d1	: real;
        variable in_d1 : std_logic_vector(31 downto 0);
        variable int_val : sfixed(CONS_INTEGER_SIZE-1 downto -CONS_DECIMAL_SIZE);
		
    begin
		-- Assert Reset
		NSYSRESET <= '0';
        file_close(Fin_d1);
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);

		file_open(Fin_d1, "./data/noisy_signal.txt", read_mode);
		
		while (not endfile(Fin_d1)) loop
			readline(Fin_d1, text_line_1);
			read(text_line_1, in_d1);
			wait until rising_edge(SYSCLK);
			uut_data_in_ready <= '1';
            --int_val := to_sfixed(in_d1, int_val'left, int_val'right);
            --FIR_input <= std_logic_vector(int_val);
            FIR_input <= in_d1;
			wait for (SYSCLK_PERIOD);
            uut_data_in_ready <= '0';
            -- wait until rising_edge(uut_fir_finish) for 1 ms;
            -- if (not(uut_fir_finish)) then
            --     report "FIR is not ready on time";
            -- end if;
            wait until rising_edge(uut_data_fir_ready) for 1 ms;
            if (not(uut_data_fir_ready)) then
                report "FIR is not ready on time";
            end if;
		end loop;
		file_close(Fin_d1);
        uut_start <= '1';
        wait for SYSCLK_PERIOD*5;
        uut_start <= '0';
        wait for SYSCLK_PERIOD;
		wait;
    end process;
	
	p_write : process
        
		file Fout	: TEXT open WRITE_MODE is "./data/output_file.txt";
		variable text_line : line;
        variable fir_output : sfixed(CONS_INTEGER_SIZE-1 downto -CONS_DECIMAL_SIZE);
		
	begin
        for i in 0 to CONST_SIGNAL_LENGTH/100 -1 loop
            wait until rising_edge(uut_fir_finish); --for 1 ms;
            --if (uut_fir_finish) then
                --fir_output := sfixed(uut_fir_output);
                --write(text_line, to_string(to_real(fir_output)));
                write(text_line, std_logic_vector(uut_fir_output));
                writeline(Fout, text_line);
            -- else
            --    report "Did not recieve FIR output";
            -- end if;
        end loop;
        wait;
	end process;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );
    

    -- hw_fir_top_inst : hw_fir_top
    -- port map (
    --         clk	    =>  SYSCLK,
    --         reset	=>	not(NSYSRESET),
    --         start	=>	uut_data_in_ready,
    --         ready	=>	uut_data_fir_ready,
    --         finish	=>	uut_fir_finish,
    --         return_val	=>	uut_fir_output,
    --         input_var	=>   FIR_input
    -- );

    -- hw_fir_top_vhdl_0 : entity work.hw_fir_top_vhdl
    -- port map (
    --         clk	    =>  SYSCLK,
    --         reset	=>	not(NSYSRESET),
    --         start	=>	uut_data_in_ready,
    --         ready	=>	uut_data_fir_ready,
    --         finish	=>	uut_fir_finish,
    --         return_val	=>	uut_fir_output,
    --         input_var	=>   FIR_input
    -- );

    -- FIR_0 : entity work.FIR
    -- -- generic map(
    -- --     gen_integer_size => CONS_INTEGER_SIZE,
    -- --     gen_decimal_size => CONS_DECIMAL_SIZE
    -- --     --gen_sum_bits           => SUM_BITS
    -- -- )
    -- port map(
    --     i_clk	    =>  SYSCLK,
    --     i_nrst	=>	not(NSYSRESET),
    --     i_start	=>	uut_data_in_ready,
    --     o_ready	=>	uut_data_fir_ready,
    --     o_finish	=>	uut_fir_finish,
    --     o_return_val	=>	uut_fir_output,
    --     i_input_var	=>   FIR_input
    -- );

    FIR_0 : entity work.FIR_system_2
    -- generic map(
    --     gen_integer_size => CONS_INTEGER_SIZE,
    --     gen_decimal_size => CONS_DECIMAL_SIZE
    --     --gen_sum_bits           => SUM_BITS
    -- )
    port map(
        i_clk           => SYSCLK,
        i_nrst          => NSYSRESET,
        i_start         => uut_data_in_ready,
        i_data_in       => FIR_input,
        o_data_out      => uut_fir_output,
        o_finish_first_fir  => uut_data_fir_ready,
        o_data_ready    => uut_fir_finish
    );

    

end behavioral;
--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: tb_uart_tx.vhd
-- File history:
--      0.0.0 : 26/10/2022 : creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300T> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_uart_rx is
end tb_uart_rx;

architecture beh of tb_uart_rx is


    constant LOGGING : boolean := FALSE;
    constant SYSCLK_PERIOD : time := 5 ns;
    constant N_TEST_POINTS : integer := 50;
    signal sysclk : std_logic := '0';
    signal nrst : std_logic := '1'; 
    signal uart_tx_par, uut_rx_par : std_logic_vector(7 downto 0);
    signal uart_tx_start : std_logic := '0';
    signal uut_rx_ser, uart_tx_busy, uut_rx_par_valid, uut_enable : std_logic;
    
    signal testing : boolean := FALSE;
    
    type t_array is array (0 to N_TEST_POINTS - 1) of std_logic_vector(7 downto 0);
    signal test_vector : t_array := (others => (others => '0'));

    component uart_rx 
    port(
        i_clk           : in std_logic;
        i_nrst          : in std_logic;
        i_enable        : in std_logic;
        i_rx_ser        : in std_logic;
    
        o_rx_par        : out std_logic_vector(7 downto 0);
        o_rx_par_valid  : out std_logic
        );
    end component uart_rx;

    component uart_tx
    port( 
        i_clk       : in std_logic;
        i_nrst      : in std_logic;
        i_tx_par    : in std_logic_vector(7 downto 0);
        i_tx_start  : in std_logic;

        o_tx_ser    : out std_logic;
        o_tx_busy   : out std_logic
    );
    end component uart_tx;

begin

    p_set_input_vector : process
    begin
        for ii in 0 to N_TEST_POINTS -1 loop
            test_vector(ii) <= std_logic_vector(to_unsigned(ii + 1, 8));
        end loop;
        wait;
    end process p_set_input_vector;
    p_excite : process
    begin
        -- reset pulse
        wait for SYSCLK_PERIOD * 10;
        nrst <= '0';
        wait for SYSCLK_PERIOD * 10;
        nrst <= '1';
        wait for SYSCLK_PERIOD * 10;
        uut_enable <= '1';

        for ii in 0 to N_TEST_POINTS -1 loop
            uart_tx_par <= test_vector(ii);
            uart_tx_start <= '1';
            wait until uart_tx_busy = '1';
            uart_tx_start <= '0';
            wait until uart_tx_busy = '0';
        end loop;
        wait;

    end process p_excite;
    

    p_test_output : process
    variable v_err_cnt : integer := 0;
    begin
        for ii in 0 to N_TEST_POINTS - 1 loop
            wait until uut_rx_par_valid = '1';
            if test_vector(ii) /= uut_rx_par then
                v_err_cnt := v_err_cnt + 1;
            end if;
        end loop;
        report to_string(N_TEST_POINTS) & " bytes received of wich " & to_string(v_err_cnt) & " were faulty";

    end process p_test_output;

    -- generate clock signal
    sysclk <= not sysclk after SYSCLK_PERIOD/2.0;


    -- instantiate uut
    uart_rx_0 : uart_rx
    port map( 
        i_clk           => sysclk,
        i_nrst          => nrst,
        i_enable        => uut_enable,
        i_rx_ser        => uut_rx_ser,
        
        o_rx_par        => uut_rx_par,
        o_rx_par_valid  => uut_rx_par_valid
    );

    -- instantiate uart transmitting module
    uart_tx_0 : uart_tx
    port map( 
        i_clk => sysclk,
        i_nrst => nrst,
        i_tx_par => uart_tx_par,
        i_tx_start => uart_tx_start,
        
        o_tx_ser => uut_rx_ser,
        o_tx_busy => uart_tx_busy
    );

end beh;
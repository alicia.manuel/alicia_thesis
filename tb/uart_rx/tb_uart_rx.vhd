library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity tb_uart_rx is
end tb_uart_rx;

architecture test of tb_uart_rx is 


constant BAUD_RATE : integer := 115200;
constant SYSCLK_PERIOD : time := 20 ns; -- 50MHz
-- declaration of the uart_rx as component

constant logging : std_logic := '1';
constant N_POINTS : integer :=256;

type t_tx_buff is array (0 to N_POINTS - 1) of std_logic_vector(7 downto 0);
signal tx_buff : t_tx_buff := (others => (others => '0'));

-- declaration of the uart_rx input signals, with initial values
signal clk : std_logic :='0';
signal nreset : std_logic := '0';
signal uut_rx_ser : std_logic;
signal uut_rx_par : std_logic_vector(7 downto 0);
signal uut_rx_par_valid : std_logic;

signal uart_tx_start : std_logic := '0';
signal uart_tx_par : std_logic_vector(7 downto 0);
signal uart_tx_busy : std_logic;


component uart_rx
generic(
	clk_freq : integer := 100e6;	
	baudrate : integer := BAUD_RATE);
port (
	i_clk           :   in std_logic;
    i_nrst          :   in std_logic;
    i_enable        :   in std_logic;

    i_rx_ser        :   in std_logic;

    o_rx_par_valid  :   out std_logic;
    o_rx_par  		: 	out std_logic_vector(7 downto 0)
	);
end component;

component uart_sim_tx
generic( baud_rate : integer := BAUD_RATE);
port (
	i_tx_start : in std_logic;
	i_tx_par : in  std_logic_vector(7 downto 0); -- example
    o_tx_ser : out std_logic;
    o_tx_busy : out std_logic
	);
end component;


begin

	clk <= not clk after SYSCLK_PERIOD/2.0; -- generate clk signal

	nreset <= '1' after 10 ns; -- reset pulse

	-- fill tx_buff
	GEN_0 : for kk in 0 to N_POINTS - 1 generate
		tx_buff(kk) <= std_logic_vector(to_unsigned(kk, tx_buff(0)'length));
	end generate;
	
	p_send : process 
	begin
		wait for 50 ns;
		for ii in 0 to N_POINTS -1 loop
			
			uart_tx_par <= tx_buff(ii); -- set byte to send
			uart_tx_start <= '1'; -- begin start pulse
			
			wait until uart_tx_busy = '1';
			uart_tx_start <= '0'; -- end start pulse
			wait until uart_tx_busy = '0';
		end loop;
		wait;
	end process;
	
	p_recieve : process
	variable v_err_cnt : integer := 0;
	begin
		for jj in 0 to N_POINTS -1 loop
			wait until uut_rx_par_valid = '1'; -- wait until data received
			wait for 1 ps;
			-- inform user if requested
			if logging then
				report "jj = " & to_string(jj);
				report "rx_par = " & to_hstring(uut_rx_par);
				report "tx_buff(jj) = " & to_hstring(tx_buff(jj));
			end if;
			-- evaluate received byte against sent one
			if (uut_rx_par /= tx_buff(jj)) then
				v_err_cnt := v_err_cnt + 1; -- increment error counting variable
			end if;
			wait until uut_rx_par_valid = '0';
			
		end loop;
		report to_string(v_err_cnt) & " faulty points out of " & to_string(N_POINTS); -- inform of the final results
		wait;
	end process;
	
	uart_rx_0 : uart_rx
	-- generic map( 
    --     baudrate => BAUD_RATE
    -- )
    port map( 
        i_clk           => clk,
		i_nrst          => nreset,
		i_enable        => '1',
		i_rx_ser        => uut_rx_ser,
		o_rx_par        => uut_rx_par,
		o_rx_par_valid  => uut_rx_par_valid
    );
	
	uart_sim_tx_0 : uart_sim_tx
	generic map( 
        baud_rate => BAUD_RATE
    )
    port map( 
        i_tx_start		=> uart_tx_start,
		i_tx_par		=> uart_tx_par,
		o_tx_ser		=> uut_rx_ser,
		o_tx_busy		=> uart_tx_busy
    );

end test;

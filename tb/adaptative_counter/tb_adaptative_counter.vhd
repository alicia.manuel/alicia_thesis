----------------------------------------------------------------------
-- Created by Microsemi SmartDesign Wed Feb  2 12:50:33 2022
-- Testbench Template
-- This is a basic testbench that instantiates your design with basic 
-- clock and reset pins connected.  If your design has special
-- clock/reset or testbench driver requirements then you should 
-- copy this file and modify it. 
----------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: tb_adaptative_counter.vhd
-- File history:
--      1.0     : 02/02/2022    : creation. Test defined. 
--    
--
-- Description: 
--
-- Testbench for the adaptative_counter module. Instantiate module, perform a reset
-- cycle and trigger count for  STEPS multiple. Alternate enable assertion and reset 
-- pulse. 
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use work.lira_globals_pkg.all;

entity tb_adaptative_counter is
end tb_adaptative_counter;

architecture behavioral of tb_adaptative_counter is

    constant SYSCLK_PERIOD        :   time        := 100 ns; -- 10MHZ
    --constant COUNTER_BIT_SIZE   :   integer     := 8;
    --constant STEPS              :   integer     := 15;
    signal step                 :   std_logic   := '0';
    signal nrst                 :   std_logic   := '1';
    signal clk                  :   std_logic   := '1';


    signal cnt :    std_logic_vector( FRAME_CNT_BIT_SIZE -1 downto 0);
    component adaptative_counter
        generic(cnt_bit_size : integer);
        port( 
            -- Inputs
            i_nrst      :   in std_logic;
            i_clk       :   in std_logic;
            i_step      :   in std_logic;

            -- Outputs
            o_cnt       :   out std_logic_vector(FRAME_CNT_BIT_SIZE -1 downto 0)
        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Reset Pulse
            wait for ( SYSCLK_PERIOD * 10 );
            nrst <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            nrst <= '1';
            wait for ( SYSCLK_PERIOD * 10 );
           
            wait for 1 ns; 
            step <= '1';

            wait for ( 8 us);
            step <= '0';

            wait for (8 us);
            step <= '1';
            wait for (8 us);
            step <= '0';
            
            wait for 8 us;
            -- Reset Pulse
            wait for ( SYSCLK_PERIOD * 10 );
            nrst <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            nrst <= '1';
            wait;
        end if;
    end process;

    clk <= not clk after SYSCLK_PERIOD/2.0;

    -- Instantiate Unit Under Test:  adaptative_counter
    adaptative_counter_0 : adaptative_counter
        
        generic map(cnt_bit_size => FRAME_CNT_BIT_SIZE)
        port map( 
            -- Inputs
            i_nrst  => nrst,
            i_clk   => clk,
            i_step  => step,
            -- Outputs
            o_cnt => cnt

            -- Inouts

        );

end behavioral;


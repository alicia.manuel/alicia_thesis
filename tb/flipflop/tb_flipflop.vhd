--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_flipflop.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;


entity tb_flipflop is
end tb_flipflop;

architecture behavioral of tb_flipflop is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ
    
	-- inputs
    signal SYSCLK			: std_logic := '0';
    signal NSYSRESET		: std_logic := '0';
    signal uut_d            : signed(13 downto 0);
    signal uut_enable       : std_logic := '0';

    -- outputs
    signal uut_q            : signed(13 downto 0);
   
    component flipflop is
    port (
        clk,clrn,enable: in std_logic; 
        d : in signed(13 downto 0);
        q : out signed(13 downto 0));
    end component;

begin

    p_reg : process
    
    begin
		-- Assert Reset
		NSYSRESET <= '0';
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;

        for i in 0 to 9 loop
            wait until rising_edge(SYSCLK);
            uut_enable <= '1';
            uut_d <= to_signed(i, uut_d'length);
            wait until rising_edge(SYSCLK);
            uut_enable <= '0';
        end loop;
        wait;
    end process;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  statistics
    flipflop_0 : flipflop
        port map( 
            clk     => SYSCLK,
            clrn    => NSYSRESET,
            enable  => uut_enable, 
            d       => uut_d,
            q       => uut_q

        );

end behavioral;
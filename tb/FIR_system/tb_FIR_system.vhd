--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_FIR_system.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.fixed_pkg.all;
use IEEE.MATH_REAL.ALL;



entity tb_FIR_system is
end tb_FIR_system;

architecture behavioral of tb_FIR_system is

    constant SYSCLK_PERIOD : time := 10 ns; -- 100MHZ
	constant CONST_MEM_ADDR_SIZE  : integer := 14;
	constant CONST_SIGNAL_LENGTH  : integer := 10000;
    constant CONS_INTEGER_SIZE : integer := 2;
    constant CONS_DECIMAL_SIZE : integer := 30;
    constant CONS_STAGES        : integer := 2;
    constant CONS_DECIMATOR_FACTOR  : integer := 10;

	-- inputs
    signal SYSCLK			: std_logic := '0';
    signal NSYSRESET		: std_logic := '0';
    signal uut_start        : std_logic := '0';
    signal d1               : std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0) := (others => '0');
    signal write_request    : std_logic := '0';
    signal uut_signal_length: std_logic_vector(CONST_MEM_ADDR_SIZE-1 downto 0) := (others => '0');
	
	-- outputs
	signal uut_data_out     : std_logic_vector(CONS_INTEGER_SIZE+CONS_DECIMAL_SIZE-1 downto 0);
    signal uut_data_ready   : std_logic;
    signal uut_FIR_ser      : std_logic;
    signal uut_FIR_par      : std_logic_vector(7 downto 0);
    signal uut_FIR_par_ready    : std_logic;


    -- auxilliary
    signal rx_ready         : std_logic;
    signal rx_data_out      : std_logic_vector(7 downto 0);
    signal tx_busy          : std_logic := '0';
    signal tx_data_in       : std_logic_vector(7 downto 0);
    signal tx_in_ready      : std_logic;
    signal uart_sim_ser     : std_logic;
    signal tx_ser           : std_logic;
    signal uart_sim_start   : std_logic := '0';
    signal text_data        : std_logic_vector(7 downto 0);
    signal uart_sim_busy    : std_logic;
    signal shift_reg : std_logic_vector(CONS_INTEGER_SIZE + CONS_DECIMAL_SIZE - 1 downto 0) := (others => '0');

    signal count    : integer range 0 to 50000 := 0;

	-- filer
	file Fin_d1 : text;
	file Fin_d2 : text;

    -- component hw_fir_top
    -- port (
    --         clk	:	in	std_logic;
    --         reset	:	in	std_logic;
    --         start	:	in	std_logic;
    --         ready	:	out	std_logic;
    --         finish	:	out	std_logic;
    --     return_val	:	out	std_logic_vector(CONS_DATA_SIZE_OUT-1 downto 0);
    --     input_var	:	in	std_logic_vector(CONS_DATA_SIZE_IN-1 downto 0)
    -- );
    -- end component;

begin

    p_read : process
        
		--C:\Users\Alicia\Documents\lira_fpga\sim\xcorr
		file Fin_d1	: TEXT open READ_MODE is "./data/noisy_signal.txt";
		variable text_line_1 : line;
		
		--variable in_d1	: real;
        --variable in_d1  : sfixed(CONS_INTEGER_SIZE - 1 downto -CONS_DECIMAL_SIZE);
        --variable int_val : sfixed(CONS_INTEGER_SIZE - 1 downto -CONS_DECIMAL_SIZE);
        variable in_d1  : std_logic_vector(7 downto 0);
		
    begin
		-- Assert Reset
		NSYSRESET <= '0';
        file_close(Fin_d1);
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);
		--file_open(Fin_d1, "./data/noisy_signal.txt", read_mode);
        file_open(Fin_d1, "C:\Users\Alicia\Documents\lira_fpga\sim\FIR_system\data\noisy_signal_byte.txt", read_mode);
		uut_signal_length   <= std_logic_vector(to_unsigned(CONST_SIGNAL_LENGTH, uut_signal_length'length));
		while (not endfile(Fin_d1)) loop
        --for i in 0 to   CONST_SIGNAL_LENGTH*4 -1 loop
			readline(Fin_d1, text_line_1);
			read(text_line_1, in_d1);
			wait until rising_edge(SYSCLK);
			uart_sim_start <= '1';
			--d1 <= std_logic_vector(to_unsigned(abs(in_d1 * 2**31), d1'length));
            --int_val := to_sfixed(in_d1, int_val'left, int_val'right);
            text_data <= in_d1;
			wait for (SYSCLK_PERIOD);
			wait until falling_edge(SYSCLK);
			uart_sim_start <= '0';
            --wait until falling_edge(uart_sim_busy);
			wait for (SYSCLK_PERIOD*5);
		end loop;
		file_close(Fin_d1);
        uut_start <= '1';
        wait for SYSCLK_PERIOD;
		wait;
    end process;

    count_process : process
    begin
        wait until rising_edge(write_request);
        count <= count + 1;
    end process;
	
	p_write : process
        
		file Fout	: TEXT open WRITE_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\FIR_system\data\output_file_FIR_vhdl_sim.txt";
        file Fout2	: TEXT open WRITE_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\FIR_system\data\output_file_hex.txt";
		variable text_line, text_line2 : line;
        --variable fir_output : sfixed(CONS_INPUT_INTEGER_SIZE + CONS_COEFF_INTEGER_SIZE + SUM_BITS-1 downto -(CONS_INPUT_DECIMAL_SIZE  + CONS_COEFF_DECIMAL_SIZE));
        variable fir_output : sfixed(CONS_INTEGER_SIZE - 1 downto -CONS_DECIMAL_SIZE);
		
	begin
        for i in 0 to 3 loop
		    wait until falling_edge(tx_in_ready);
            wait for SYSCLK_PERIOD;
            shift_reg <= uut_FIR_par & shift_reg(CONS_INTEGER_SIZE + CONS_DECIMAL_SIZE - 1 downto 8);
            --shift_reg <= shift_reg(CONS_INTEGER_SIZE + CONS_DECIMAL_SIZE - 1 - 8 downto 0) & uut_FIR_par;
        end loop;
        wait for 1ps;
        --fir_output := sfixed(shift_reg);
        --fir_output := sfixed(shift_reg);
        write(text_line2, to_hex_string(shift_reg));
        write(text_line, shift_reg);
        --write(text_line, to_string(to_real(fir_output)));  
        writeline(Fout2, text_line2);
        writeline(Fout, text_line);
        --wait until rising_edge(uut_fir_ready_1);
        --fir_output := sfixed(uut_fir_output_1);
        
	end process;

    tx_busy <= not tx_busy after SYSCLK_PERIOD;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );


    -- hw_fir_top_inst : hw_fir_top
    -- port map (
    --         clk	    =>  SYSCLK,
    --         reset	=>	not(NSYSRESET),
    --         start	=>	uut_data_in_ready,
    --         ready	=>	uut_data_fir_ready,
    --         finish	=>	uut_fir_ready,
    --         return_val	=>	uut_fir_output,
    --         input_var	=>   FIR_input
    -- );

    -- FIR_system_0 : entity work.FIR_system
    -- generic map(gen_data_integer_size  => CONS_INTEGER_SIZE,
    --         gen_data_decimal_size  => CONS_DECIMAL_SIZE,
    --         --gen_signal_length      => CONST_SIGNAL_LENGTH,
    --         --gen_sum_bits           : integer := 14,
    --         gen_stages             => CONS_STAGES,
    --         gen_mem_addr_size      => CONST_MEM_ADDR_SIZE,
    --         gen_decimator_factor   => CONS_DECIMATOR_FACTOR)
    -- port map(
    --     i_clk           => SYSCLK,
    --     i_nrst          => NSYSRESET,
    --     i_enable        => '1',
    --     --i_write_request => write_request,
    --     i_signal_length => uut_signal_length,
    --     i_data_in       => d1,
    --     i_start         => write_request,
    --     o_data_out      => uut_data_out,
    --     o_data_ready    => uut_data_ready
    -- );

    -- byte_joiner_0 : entity work.byte_joiner
    --     -- generic (gen_num_bytes  : integer := 4);
    -- port map(
    --     i_clk       => SYSCLK,
    --     i_nrst      => NSYSRESET,
    --     i_ready     => rx_ready,
    --     i_data_in   => rx_data_out,
    --     o_data_out  => d1,
    --     o_finish    => write_request
    -- );

    -- data_to_bytes_0 : entity work.data_to_bytes
    -- --generic (gen_num_bytes  : integer := 4);
    -- port map(
    --     i_clk       => SYSCLK,
    --     i_nrst      => NSYSRESET,
    --     i_ready     => uut_data_ready,
    --     i_tx_busy   => tx_busy,
    --     i_data_in   => uut_data_out,
    --     o_data_out  => tx_data_in,
    --     o_finish    => tx_in_ready
    -- );

    -- uart_rx_0 : entity work.uart_rx
    -- -- generic ( baudrate : integer := 10e6;
    -- --         clk_freq : integer := 100e6);
    -- port map(
    --     i_clk           => SYSCLK,
    --     i_nrst          => NSYSRESET,
    --     i_enable 	    => '1',
    --     i_rx_ser        => uart_sim_ser,
    --     o_rx_par_valid  => rx_ready,
    --     o_rx_par        => rx_data_out
    -- );

    -- uart_tx : entity work.uart_tx
    -- -- generic ( baudrate : integer := 6e6;
    -- --         clk_freq : integer := 100e6);
    -- port map(
    --     i_clk       => SYSCLK,
    --     i_enable 	=> '1',
    --     i_nrst      => NSYSRESET,
    --     i_tx_par    => tx_data_in,
    --     i_tx_start  => tx_in_ready,
    --     o_tx_ser    => tx_ser,
    --     o_tx_busy   => tx_busy        
    -- );

    -- uart_sim_tx_0 : entity work.uart_sim_tx
    -- --generic( baud_rate : integer := 5e6);
    -- port map(
    --     i_tx_start  => uart_sim_start,
    --     i_tx_par    => text_data,
    --     o_tx_ser    => uart_sim_ser,
    --     o_tx_busy   => uart_sim_busy
    -- );

    -- uart_sim_rx_0 : entity work.uart_sim_rx
    -- --generic( baud_rate : integer := 5e6);
    -- port map(
    --     i_rx_ser        => uut_FIR_ser,
    --     o_rx_par        => uut_FIR_par,
    --     o_rx_par_ready  => uut_FIR_par_ready
    -- );

    root_0 : entity work.root 
        -- Port list
    port map(
        -- Inputs
        i_clk       => SYSCLK,
        i_nrst => NSYSRESET,
        i_d_in    => text_data,
        i_signal_length => uut_signal_length,
        i_start   => uart_sim_start,
        i_tx_busy => tx_busy,
        -- Outputs
        o_d_out   => uut_FIR_par,
        o_ready   => tx_in_ready
    );

end behavioral;
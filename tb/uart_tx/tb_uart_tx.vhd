library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity tb_uart_tx is
end tb_uart_tx;

architecture test of tb_uart_tx is 

constant SYSCLK_PERIOD : time := 10 ns; -- 100MHz
constant BAUD_RATE : integer := 6000000;
constant N_POINTS : integer := 256;
constant logging : std_logic := '0'; -- set this signal to '1' in order to allow transcript log messages


type t_tx_buff is array (0 to N_POINTS) of std_logic_vector(7 downto 0);
signal tx_buff : t_tx_buff := (others => (others => '0'));

-- declaration of the uart_rx input signals, with initial values
signal clk : std_logic :='0';
signal nreset : std_logic := '0';

signal uut_tx_par : std_logic_vector(7 downto 0) := (others => '0');
signal uut_tx_start : std_logic := '0';
signal uut_tx_busy  : std_logic;
signal uart_rx_par : std_logic_vector(7 downto 0);
signal uart_rx_par_ready : std_logic;
signal uut_tx_ser : std_logic;

-- UUT component declaration
component uart_tx
generic( 
	clk_freq : integer := 100e6;
	baudrate : integer := BAUD_RATE);
port (
	i_clk       : in std_logic;
	i_enable	: in std_logic;
    i_nrst      : in std_logic;
    i_tx_par    : in std_logic_vector(7 downto 0);
    i_tx_start  : in std_logic;

    o_tx_ser    : out std_logic;
    o_tx_busy   : out std_logic
	);
end component;


component uart_sim_rx
generic( baud_rate : integer := BAUD_RATE);
port (
    i_rx_ser        : in std_logic;
	o_rx_par        : out std_logic_vector(7 downto 0);
    o_rx_par_ready  : out std_logic
);
end component;



begin

	clk <= not clk after SYSCLK_PERIOD/2.0; -- generate clk signal

	nreset <= '1' after 10 ns; -- reset pulse

	-- fill tx_buff
	GEN_0 : for kk in 0 to N_POINTS - 1 generate
		tx_buff(kk) <= std_logic_vector(to_unsigned(kk, tx_buff(0)'length));
	end generate;
	
	-- send all data bytes over uart
	p_send : process 
	begin
		wait for 50 ns;
		for ii in 0 to N_POINTS -1 loop
			
			uut_tx_par <= tx_buff(ii); -- place uut input byte
			uut_tx_start <= '1'; 		-- begin tx_Start pulse
			wait until uut_tx_busy = '1';
			uut_tx_start <= '0'; 		-- end tx_start pulse
			wait until uut_tx_busy = '0';
		end loop;
		wait;
	end process;
	
	p_recieve : process
	variable v_err_cnt : integer := 0;
	begin

		for jj in 0 to N_POINTS -1 loop
			wait until uart_rx_par_ready = '1'; -- wait for data ready
			wait for 1 ps;
			-- inform user
			if logging then
				report "jj = " & to_String(jj);
				report "uart_rx_par = " & to_hstring(uart_rx_par);
				report "tx_buff(jj) = " & to_hstring(tx_buff(jj));
			end if;
			-- evaluate result
			if (uart_rx_par /= tx_buff(jj)) then
				v_err_cnt := v_err_cnt + 1; -- if wrong result then increment error count variable value
			end if;
			wait until uart_rx_par_ready = '0'; -- wait until uart rx module is ready to start receiving again
		end loop;
			report to_string(v_err_cnt) & " faulty points out of " & to_string(N_POINTS); -- report results
		wait;
	end process;
	
	
	uart_tx_0 : uart_tx
	port map(
		i_clk       => clk,
		i_enable	=> '1',
		i_nrst      => nreset,
		i_tx_par    => uut_tx_par, 
		i_tx_start  => uut_tx_start,
		o_tx_ser    => uut_tx_ser,
		o_tx_busy   => uut_tx_busy
	);
	
	uart_sim_rx_0 : uart_sim_rx
	port map(
		i_rx_ser        => uut_tx_ser,
		o_rx_par        => uart_rx_par,
		o_rx_par_ready  => uart_rx_par_ready
	);

end test;
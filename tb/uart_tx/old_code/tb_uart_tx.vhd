--------------------------------------------------------------------------------
-- Company: IEEC
--
-- File: tb_uart_tx.vhd
-- File history:
--      0.0.0 : 26/10/2022 : creation
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- baudrate = f_clk/32
-- Targeted device: <Family::PolarFire> <Die::MPF300T> <Package::FCG1152>
-- Author: Miquel Canal
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_uart_tx is
end tb_uart_tx;

architecture beh of tb_uart_tx is


    constant LOGGING : boolean := TRUE;
    constant SYSCLK_PERIOD : time := 5 ns;
    constant N_TEST_POINTS : integer := 5;
    signal sysclk : std_logic := '0';
    signal nrst : std_logic := '1'; 
    signal uut_tx_par : std_logic_vector(7 downto 0);
    signal uut_tx_start : std_logic := '0';
    signal uut_tx_ser, uut_tx_busy : std_logic;
    signal testing : boolean := FALSE;

    type t_array is array (0 to N_TEST_POINTS - 1) of std_logic_vector(7 downto 0);
    signal test_vector : t_array := (others => (others => '0'));

    component uart_tx
    port( 
        i_clk       : in std_logic;
        i_nrst      : in std_logic;
        i_tx_par    : in std_logic_vector(7 downto 0);
        i_tx_start  : in std_logic;

        o_tx_ser    : out std_logic;
        o_tx_busy   : out std_logic
    );
    end component uart_tx;

begin

    p_set_input_vector : process
    begin
        for ii in 0 to N_TEST_POINTS -1 loop
            test_vector(ii) <= std_logic_vector(to_unsigned(ii + 1, 8));
        end loop;
        wait;
    end process p_set_input_vector;
    p_excite : process
    begin
        -- reset pulse
        wait for SYSCLK_PERIOD * 10;
        nrst <= '0';
        wait for SYSCLK_PERIOD * 10;
        nrst <= '1';
        wait for SYSCLK_PERIOD * 10;
        -- check initial values after reset
        if uut_tx_busy = '1' then
            report "TX_BUSY initial value = '1'." severity error;
            wait;
        end if;
        if uut_tx_ser = '0' then
            report "TX_SER initial value = '0'." severity error;
            wait;
        end if;
        testing <= TRUE;
        --begin test
        for ii in 0 to N_TEST_POINTS -1 loop
            -- set input byte
            uut_tx_par <= test_vector(ii);
            -- begin start pulse
            uut_tx_start <= '1';
            wait until uut_tx_busy = '1' for 1 us;
            -- end start pulse
            uut_tx_start <= '0';
            -- report
            if LOGGING = TRUE then
                if uut_tx_busy = '0' then
                    report "TX_BUSY didn't raise in the timeout period (1 us)." severity error;
                    wait;
                else report "TX_BUSY ASSERTED IN TIME.";
                end if;
            end if;
            -- wait until end of transmission with timeout
            wait until uut_tx_busy = '0' for 10 us;
            -- report
            if LOGGING = TRUE then
                if uut_tx_busy = '1' then
                    report "TX_BUSY didn't fall in the timeout period (10 us)." severity error;
                    wait;
                else report "TX_BUSY DE-ASSERTED IN TIME.";
                end if;
            end if;
            wait for SYSCLK_PERIOD;
        end loop;
        testing <= FALSE;
        wait;
    end process p_excite;

    p_test : process
    variable v_rx_byte : std_logic_vector(7 downto 0);
    variable v_err_cnt : integer := 0;
    begin
        wait until testing = TRUE;
        for kk in 0 to N_TEST_POINTS -1 loop
            v_rx_byte := (others => '0');
            
            wait until falling_edge(uut_tx_ser) for 10 us; -- start condition

                if LOGGING = TRUE then
                    if uut_tx_ser = '1' then
                        report "START condition didn't arrive for 10 us!" severity error;
                        wait;
                    else report "START CONDITION DETECTED IN TIME.";
                    end if;
                end if;
                
                wait for SYSCLK_PERIOD * 16; -- wait bit time/2
                for ii in 0 to 7 loop
                    wait for SYSCLK_PERIOD * 32; -- wait bit time
                    v_rx_byte(ii) := uut_tx_ser;
                end loop;
                -- stop condition detect
                wait until falling_edge(uut_tx_ser) for SYSCLK_PERIOD * 16;
                if LOGGING = TRUE then
                    if uut_tx_ser = '0' then
                        report "STOP condition not issued in time.";
                    else report "STOP CONDITION DETECTED IN TIME";
                    end if;
                end if;
                report to_string(v_rx_byte);
                report to_string(test_vector(kk));
                if LOGGING = TRUE then
                    if v_rx_byte = test_vector(kk) then
                        report "SUCES on byte " & to_string(kk);
                    else report "FAIL on byte " & to_string(kk);
                    end if;
                end if;
                if v_rx_byte /= test_vector(kk) then
                    v_err_cnt := v_err_cnt + 1;
                end if;
        end loop;
        wait for 10 us;
        report "TEST ENDED: " & to_string(v_err_cnt) & " out of " & to_string(N_TEST_POINTS) & " faulty points.";
        wait;
    end process p_test;

    
    -- generate clock signal
    sysclk <= not sysclk after SYSCLK_PERIOD/2.0;



    -- instantiate uut
    uart_tx_0 : uart_tx
    port map( 
        i_clk => sysclk,
        i_nrst => nrst,
        i_tx_par => uut_tx_par,
        i_tx_start => uut_tx_start,
        
        o_tx_ser => uut_tx_ser,
        o_tx_busy => uut_tx_busy
    );

end beh;
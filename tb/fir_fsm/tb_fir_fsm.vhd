--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_fir_fsm.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.fixed_pkg.all;
use IEEE.MATH_REAL.ALL;



entity tb_fir_fsm is
end tb_fir_fsm;

architecture behavioral of tb_fir_fsm is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ
	constant CONST_MEM_ADDR_SIZE  : integer := 7;
    constant CONST_SIGNAL_LENGTH_DEC : integer := 100;
    constant CONS_SIGNAL_SIZE : integer := 6;


	-- inputs
    signal SYSCLK			: std_logic := '0';
    signal NSYSRESET		: std_logic := '0';
    signal uut_start            : std_logic := '0';
	signal uut_fir_ready: std_logic	:= '0';
    signal signal_length  : std_logic_vector(CONST_MEM_ADDR_SIZE-1 downto 0) := (others => '0');
	
	-- outputs
	signal uut_max_addr    : std_logic_vector(CONST_MEM_ADDR_SIZE-1 downto 0);
    signal uut_mem_req      : std_logic;

    signal mem_out : std_logic_vector(CONS_SIGNAL_SIZE-1 downto 0);
    signal counter, errors  : integer range 0 to CONST_SIGNAL_LENGTH_DEC := 0;
    type t_mem is array (0 to CONST_SIGNAL_LENGTH_DEC-1) of std_logic_vector(CONS_SIGNAL_SIZE-1 downto 0);
    signal mem_in : t_mem;

begin

    p_read : process		
    begin
		-- Assert Reset
		NSYSRESET <= '0';
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';

        for i in 0 to CONST_SIGNAL_LENGTH_DEC-1 loop
            mem_in(i) <= std_logic_vector(to_unsigned(i, mem_in(i)'length));
        end loop;
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);
        signal_length <= std_logic_vector(to_unsigned(CONST_SIGNAL_LENGTH_DEC-1, signal_length'length));
        uut_start <= '1';
        wait until (counter = 100);
        uut_start <= '0';
        wait for SYSCLK_PERIOD*10;
		wait;
    end process;
	
	p_write : process		
	begin
        for i in 0 to CONST_SIGNAL_LENGTH_DEC -1 loop
            wait until rising_edge(uut_mem_req) for 50 us;
            if (uut_mem_req) then
                wait for (SYSCLK_PERIOD*5);
                uut_fir_ready <= '1';
                mem_out  <= mem_in(counter);
                wait for (SYSCLK_PERIOD);
                if (mem_out /= mem_in(counter)) then
                    errors <= errors + 1;
                end if;
                counter <= counter + 1;
                uut_fir_ready <= '0';
            else
                report "Fir response did not arrive";
                errors <= errors + 1;
            end if;
        end loop;
        report to_string(errors) & " FAULTY POINTS OUT OF " & to_string(CONST_SIGNAL_LENGTH_DEC);
        wait;
	end process;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    fir_fsm_0 : entity work.fir_fsm
    -- generic map(gen_singal_length => CONST_SIGNAL_LENGTH,
    --         gen_mem_addr_size => CONST_MEM_ADDR_SIZE)
    port map(
            i_clk       =>  SYSCLK,
            i_nrst	    =>  NSYSRESET,
            i_start    =>  uut_start,
            i_signal_length => signal_length,
            i_fir_ready => uut_fir_ready,
            o_max_addr  => uut_max_addr,
            o_mem_req   => uut_mem_req
    );

end behavioral;
--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: tb_interface_in_output.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::PolarFire> <Die::MPF300TS> <Package::FCG1152>
-- Author: Alicia Manuel
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.fixed_pkg.all;
use IEEE.MATH_REAL.ALL;



entity tb_interface_in_output is
end tb_interface_in_output;

architecture behavioral of tb_interface_in_output is

    constant SYSCLK_PERIOD : time := 10 ns; -- 100MHZ

	-- inputs
    signal SYSCLK			: std_logic := '0';
    signal NSYSRESET		: std_logic := '0';
	signal uut_data_in_ready    : std_logic := '0';
    signal uut_data_in         : std_logic_vector(7 downto 0) := (others => '0');
	-- outputs
    signal uut_data_out         : std_logic_vector(7 downto 0);
    signal uut_WE   : std_logic;
    signal uut_finish   : std_logic;
    signal uut_w_addr   : std_logic_vector(7 downto 0);

begin

    p_write : process
    begin
		-- Assert Reset
		NSYSRESET <= '0';
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);

		for i in 0 to 9 loop
            uut_data_in_ready   <= '1';
            uut_data_in    <= std_logic_vector(to_unsigned(i, uut_data_in'length));
            wait until rising_edge(SYSCLK);
            uut_data_in_ready   <= '0';
            wait for SYSCLK_PERIOD*2;
        end loop;
        wait;
    end process;


    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );
    

    interface_in_output_0: entity work.interface_in_output
        generic map(
            gen_mem_addr_size => 8,
            gen_mem_addr_size_decimated => 8,
            gen_mem_data_size => 8)
        port map(
            i_clk       => SYSCLK,
            i_nrst      => NSYSRESET,
            i_w_req     => uut_data_in_ready,
            i_d_in      => uut_data_in,
            i_addr_max  => X"09",
            o_d_out     => uut_data_out,
            o_WE        => uut_WE,
            o_finish    => uut_finish,
            o_w_address => uut_w_addr
        );

    

end behavioral;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity tb_reset_sync is
end tb_reset_sync;

architecture test of tb_reset_sync is 

-- declaration of the sync1b as component
component reset_sync
port (
	i_clk   :   in  std_logic;
	i_nrst	:   in std_logic;
    o_q     :   out std_logic
	);
end component;

constant clk_period : time := 10 ns;
-- declaration of the reset_sync input signals, with initial values
signal clk : std_logic :='0';
signal reset : std_logic := '1';

-- declaration of the reset_sync output signal
signal o_q : std_logic;

begin
	clk_process: process begin --creates the clock waveform
	wait for clk_period/2.0;
	clk <= not clk;
	end process;
	
	-- create input waveforms
	-- reset <= '1' after 10 ns, '0' after 25 ns, '1' after 37 ns;
	p_stim : process
	begin
		wait for clk_period * 10;
		reset <= '0';
		wait for clk_period * 7;
		reset <= '1';
		wait;
	end process p_stim;

	
	-- tx_module instantiation
	reset_sync_inst: reset_sync port map (i_clk=>clk, i_nrst=>reset, o_q=>o_q);

end test;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity CrossCorrelation_TB is
end entity CrossCorrelation_TB;

architecture tb_arch of CrossCorrelation_TB is
    constant N : natural := 5;  -- Tamaño de la ventana de correlación
    constant CONS_GEN_DATA_LENGTH : integer := 8;
    constant CONS_GEN_OUTPUT_LENGTH : integer := 32;
    constant CONS_GEN_DATA_SIZE     : integer := 20;

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '1';
    signal data_ready   : std_logic := '0';
    signal finish   : std_logic;
    signal x : std_logic_vector(CONS_GEN_DATA_LENGTH-1 downto 0);
    signal y : std_logic_vector(CONS_GEN_DATA_LENGTH-1 downto 0);
    signal rx, result, result2 : std_logic_vector(CONS_GEN_OUTPUT_LENGTH - 1 downto 0);
    signal ready : std_logic;
    
    
    
    constant SYSCLK_PERIOD : time := 10 ns;
    constant SAMPLES : integer := 10; -- Número de muestras a enviar
    
    -- type SampleArray is array(0 to SAMPLES-1) of integer range 0 to 255;
    -- constant X_SAMPLES : SampleArray :=
    --     (
    --         1,
    --         2,
    --         3,
    --         4,
    --         5,
    --         6,
    --         7,
    --         8,
    --         9,
    --         10
    --     );
    -- constant Y_SAMPLES : SampleArray :=
    --     (
    --         5,
    --         6,
    --         7,
    --         8,
    --         9,
    --         10,
    --         11,
    --         12,
    --         13,
    --         14
    --     );
begin

    -- parallelized_xcorr_0:    entity work.parallelized_xcorr
    --         -- generic map(gen_data_size  => CONS_GEN_DATA_SIZE,
    --         --         gen_data_length =>  CONS_GEN_DATA_LENGTH,
    --         --         gen_output_length   => CONS_GEN_OUTPUT_LENGTH,
    --         --         gen_number_of_blocks    => N)
    --         port map(
    --             i_clk           =>SYSCLK,
    --             i_nrst          =>NSYSRESET,
    --             i_data1         => x,
    --             i_data2         => y,
    --             i_data_ready    => data_ready,
    --             o_finish        => finish,
    --             o_rxy           => result
    --         );

    top_xcorr_top_vhdl_0 : entity work.top_xcorr_top_vhdl
    port map (
	       clk	=>	SYSCLK,
	     reset	=>	NSYSRESET,
	     start	=>	data_ready,
	     ready	=>	ready,
	    finish	=>	finish,
	return_val	=>	result,
	         a	=>	x,
	         b	=>	y,
	       res	=>	result2
    );

    --Parallelized crosscorrelation
    -- root_0:    entity work.root
    -- port map(
    --     i_clk           =>SYSCLK,
    --     i_nrst          =>NSYSRESET,
    --     i_data1         => x,
    --     i_data2         => y,
    --     i_data_ready    => data_ready,
    --     o_finish        => finish,
    --     o_rxy           => result
    -- );

    -- CrossCorrelation root
    -- root_0:    entity work.root
    -- port map(
    --     i_clk           =>SYSCLK,
    --     i_nrst          =>NSYSRESET,
    --     i_x         => x,
    --     i_y         => y,
    --     i_data_ready    => data_ready,
    --     o_finish        => finish,
    --     o_rxy           => result
    -- );
    

    -- CrossCorrelation_0: entity work.CrossCorrelation
    -- -- generic map(
    -- --     N => 1, -- Numero de bloque
    -- --     N_total => 1,
    -- --     gen_data_length => 5, -- Longitud de los datos
    -- --     gen_samples => 20
    -- -- )
    -- port map(
    --     i_clk           => SYSCLK,
    --     i_nrst          => NSYSRESET,
    --     i_x             => x,
    --     i_y             => y,
    --     i_data_ready    => data_ready,
    --     o_finish        => finish,
    --     o_rxy           => result
    -- );

    p_read : process
        
		--C:\Users\Alicia\Documents\lira_fpga\sim\xcorr
		file Fin_d1	: TEXT open READ_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_1.txt";
        file Fin_d2	: TEXT open READ_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_2.txt";
		variable text_line_1, text_line_2 : line;
        variable in_d1, in_d2  : integer;
        --variable in_d1, in_d2 : std_logic_vector(CONS_GEN_DATA_LENGTH-1 downto 0);
		
    begin
		-- AssertNSYSRESET
		NSYSRESET <= '0';
        file_close(Fin_d1);
        file_close(Fin_d2);
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);
		--file_open(Fin_d1, "./data/noisy_signal.txt", read_mode);
        file_open(Fin_d1, "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_1.txt", read_mode);
        file_open(Fin_d2, "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_2.txt", read_mode);
		while (not endfile(Fin_d1)) loop
        --for i in 0 to   CONST_SIGNAL_LENGTH*4 -1 loop
			readline(Fin_d1, text_line_1);
			read(text_line_1, in_d1);
            readline(Fin_d2, text_line_2);
			read(text_line_2, in_d2);
			wait until rising_edge(SYSCLK);
			data_ready <= '1';
            x  <= std_logic_vector(to_signed(in_d1, x'length));
            y  <= std_logic_vector(to_signed(in_d2, y'length));
            --x   <= in_d1;
            --y   <= in_d2;
            --x_prov  := std_logic_vector(to_signed(in_d1, x_prov'length));
            --y_prov  := std_logic_vector(to_signed(in_d2, y_prov'length));
            --report "integer: " & to_string(in_d1);
            --report "bit: " & to_string(x_prov);
			--x   <= x_prov(CONS_GEN_DATA_LENGTH-1 downto 0);
            --y   <= y_prov(CONS_GEN_DATA_LENGTH-1 downto 0);
			wait for (SYSCLK_PERIOD);
            --report "bit truncated " & to_string(x);
			data_ready <= '0';
            --wait until falling_edge(uart_sim_busy);
			wait for (SYSCLK_PERIOD*5);
		end loop;
		file_close(Fin_d1);
        file_close(Fin_d2);
        wait for SYSCLK_PERIOD;
		wait;
    end process;

    -- count_process : process
    -- begin
    --     wait until rising_edge(write_request);
    --     count <= count + 1;
    -- end process;
	
	p_write : process
        
		file Fout	: TEXT open WRITE_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\output_file.txt";
		variable text_line : line;
        variable xcorr_output : std_logic_vector(CONS_GEN_OUTPUT_LENGTH - 1 downto 0);
        variable xcorr_int  : integer; 
		
	begin
        --wait until rising_edge(finish);
        if (finish = '1') then
            xcorr_output  := result;
            --xcorr_int   := to_integer(signed(result));
            write(text_line, to_string(xcorr_output));  
            writeline(Fout, text_line);
        end if;
        wait for (SYSCLK_PERIOD);
        --wait until rising_edge(uut_fir_ready_1);
        --fir_output := sfixed(uut_fir_output_1);
        
	end process;
   
    -- Clock generation process
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );
end architecture tb_arch;

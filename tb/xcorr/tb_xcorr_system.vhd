library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity tb_xcorr_system is
end entity tb_xcorr_system;

architecture tb_arch of tb_xcorr_system is
    constant N : natural := 5;  -- Tamaño de la ventana de correlación
    constant CONS_GEN_DATA_LENGTH : integer := 8;
    constant CONS_GEN_OUTPUT_LENGTH : integer := 32;
    constant CONS_GEN_DATA_SIZE     : integer := 500;
	constant CONS_MEM_ADDR_SIZE		: integer := 9;

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '1';
    signal data_ready   : std_logic := '0';
    signal finish   : std_logic;
	signal signal_length : std_logic_vector(CONS_MEM_ADDR_SIZE-1 downto 0);
    signal x : std_logic_vector(CONS_GEN_DATA_LENGTH-1 downto 0);
    signal y : std_logic_vector(CONS_GEN_DATA_LENGTH-1 downto 0);
    signal rx, result : std_logic_vector(CONS_GEN_DATA_LENGTH - 1 downto 0);
    signal tx_busy  : std_logic := '0';

    constant SYSCLK_PERIOD : time := 10 ns;

begin

    --Parallelized crosscorrelation
    root_0:    entity work.root
    port map(
        i_clk           => SYSCLK,
        i_d_in          => x,
        i_nrst          => NSYSRESET,
        i_signal_length => signal_length,
        i_start         => data_ready,
        i_tx_busy       => tx_busy,
        o_d_out         => result,
        o_ready         => finish
    );

    p_read : process
        
		--C:\Users\Alicia\Documents\lira_fpga\sim\xcorr
		file Fin_d1	: TEXT open READ_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_1.txt";
        file Fin_d2	: TEXT open READ_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_2.txt";
		variable text_line_1, text_line_2 : line;
        variable in_d1, in_d2  : integer;
        variable x_prov   : std_logic_vector(13 downto 0);
        variable y_prov   : std_logic_vector(13 downto 0);
		
    begin
		-- AssertNSYSRESET
		NSYSRESET <= '0';
        file_close(Fin_d1);
        file_close(Fin_d2);
		wait for ( SYSCLK_PERIOD * 10 );
		
		NSYSRESET <= '1';
		
		wait for SYSCLK_PERIOD *5;
		report "---------------------- START ----------------------";
		
		wait until rising_edge(SYSCLK);
		signal_length	<= std_logic_vector(to_unsigned(CONS_GEN_DATA_SIZE, signal_length'length));
        file_open(Fin_d1, "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_1.txt", read_mode);
        file_open(Fin_d2, "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\input_file_2.txt", read_mode);
		while (not endfile(Fin_d1)) loop
        --for i in 0 to   CONST_SIGNAL_LENGTH*4 -1 loop
			readline(Fin_d1, text_line_1);
			read(text_line_1, in_d1);
			wait until rising_edge(SYSCLK);
			data_ready <= '1';
            x  <= std_logic_vector(to_signed(in_d1, x'length));
			wait for (SYSCLK_PERIOD);
            --report "bit truncated " & to_string(x);
			data_ready <= '0';
            --wait until falling_edge(uart_sim_busy);
			wait for (SYSCLK_PERIOD*5);
		end loop;
		file_close(Fin_d1);
		while (not endfile(Fin_d2)) loop
				readline(Fin_d2, text_line_2);
				read(text_line_2, in_d2);
				wait until rising_edge(SYSCLK);
				data_ready <= '1';
				x  <= std_logic_vector(to_signed(in_d2, y'length));
				wait for (SYSCLK_PERIOD);
				data_ready <= '0';
				wait for (SYSCLK_PERIOD*5);
			end loop;
        file_close(Fin_d2);
        wait for SYSCLK_PERIOD;
		wait;
    end process;
	
	p_write : process
        
		file Fout	: TEXT open WRITE_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\output_file.txt";
        file Fout2	: TEXT open WRITE_MODE is "C:\Users\Alicia\Documents\lira_fpga\sim\xcorr\test_data\output_file_hex.txt";
		variable text_line, text_line2 : line;
        variable xcorr_output : std_logic_vector(CONS_GEN_OUTPUT_LENGTH - 1 downto 0);
        variable xcorr_int  : integer; 
		
	begin
        for i in 0 to 3 loop
		    wait until (finish = '1');
            wait for (SYSCLK_PERIOD);
            xcorr_output := result & xcorr_output(CONS_GEN_OUTPUT_LENGTH - 1 downto 8);
            report to_string(result);
            report to_string(xcorr_output);
            wait for (SYSCLK_PERIOD);
        end loop;
        write(text_line, to_string(xcorr_output));
        --write(text_line2, to_string(to_integer(unsigned(xcorr_output))));  
        writeline(Fout, text_line);
        --writeline(Fout2, text_line2);
        wait for (SYSCLK_PERIOD);
        --wait until rising_edge(uut_fir_ready_1);
        --fir_output := sfixed(uut_fir_output_1);
        
	end process;

    tx_busy <= not tx_busy after SYSCLK_PERIOD;
        
    -- Clock generation process
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

end architecture tb_arch;

Microchip Technology Inc. - Microchip Libero Software Release v2022.2 (Version 2022.2.0.10)

Date      :  Mon May 22 13:14:46 2023
Project   :  C:\Users\Alicia\Documents\lira_fpga\prj\FIR_fpga
Component :  CORERESET_PF_C0
Family    :  PolarFire


HDL source files for all Synthesis and Simulation tools:
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/CORERESET_PF_C0/CORERESET_PF_C0_0/core/corereset_pf.vhd
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/CORERESET_PF_C0/CORERESET_PF_C0.vhd

Stimulus files for all Simulation tools:
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/CORERESET_PF_C0/CORERESET_PF_C0_0/test/corereset_pf_tb.vhd


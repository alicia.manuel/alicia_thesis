`timescale 1 ns/100 ps
// Version: 2022.2 2022.2.0.10


module PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM(
       W_DATA,
       R_DATA,
       W_ADDR,
       R_ADDR,
       W_EN,
       R_EN,
       CLK
    );
input  [7:0] W_DATA;
output [31:0] R_DATA;
input  [15:0] W_ADDR;
input  [13:0] R_ADDR;
input  W_EN;
input  R_EN;
input  CLK;

    wire \R_DATA_TEMPR0[0] , \R_DATA_TEMPR1[0] , \R_DATA_TEMPR2[0] , 
        \R_DATA_TEMPR0[1] , \R_DATA_TEMPR1[1] , \R_DATA_TEMPR2[1] , 
        \R_DATA_TEMPR0[2] , \R_DATA_TEMPR1[2] , \R_DATA_TEMPR2[2] , 
        \R_DATA_TEMPR0[3] , \R_DATA_TEMPR1[3] , \R_DATA_TEMPR2[3] , 
        \R_DATA_TEMPR0[4] , \R_DATA_TEMPR1[4] , \R_DATA_TEMPR2[4] , 
        \R_DATA_TEMPR0[5] , \R_DATA_TEMPR1[5] , \R_DATA_TEMPR2[5] , 
        \R_DATA_TEMPR0[6] , \R_DATA_TEMPR1[6] , \R_DATA_TEMPR2[6] , 
        \R_DATA_TEMPR0[7] , \R_DATA_TEMPR1[7] , \R_DATA_TEMPR2[7] , 
        \R_DATA_TEMPR0[8] , \R_DATA_TEMPR1[8] , \R_DATA_TEMPR2[8] , 
        \R_DATA_TEMPR0[9] , \R_DATA_TEMPR1[9] , \R_DATA_TEMPR2[9] , 
        \R_DATA_TEMPR0[10] , \R_DATA_TEMPR1[10] , \R_DATA_TEMPR2[10] , 
        \R_DATA_TEMPR0[11] , \R_DATA_TEMPR1[11] , \R_DATA_TEMPR2[11] , 
        \R_DATA_TEMPR0[12] , \R_DATA_TEMPR1[12] , \R_DATA_TEMPR2[12] , 
        \R_DATA_TEMPR0[13] , \R_DATA_TEMPR1[13] , \R_DATA_TEMPR2[13] , 
        \R_DATA_TEMPR0[14] , \R_DATA_TEMPR1[14] , \R_DATA_TEMPR2[14] , 
        \R_DATA_TEMPR0[15] , \R_DATA_TEMPR1[15] , \R_DATA_TEMPR2[15] , 
        \R_DATA_TEMPR0[16] , \R_DATA_TEMPR1[16] , \R_DATA_TEMPR2[16] , 
        \R_DATA_TEMPR0[17] , \R_DATA_TEMPR1[17] , \R_DATA_TEMPR2[17] , 
        \R_DATA_TEMPR0[18] , \R_DATA_TEMPR1[18] , \R_DATA_TEMPR2[18] , 
        \R_DATA_TEMPR0[19] , \R_DATA_TEMPR1[19] , \R_DATA_TEMPR2[19] , 
        \R_DATA_TEMPR0[20] , \R_DATA_TEMPR1[20] , \R_DATA_TEMPR2[20] , 
        \R_DATA_TEMPR0[21] , \R_DATA_TEMPR1[21] , \R_DATA_TEMPR2[21] , 
        \R_DATA_TEMPR0[22] , \R_DATA_TEMPR1[22] , \R_DATA_TEMPR2[22] , 
        \R_DATA_TEMPR0[23] , \R_DATA_TEMPR1[23] , \R_DATA_TEMPR2[23] , 
        \R_DATA_TEMPR0[24] , \R_DATA_TEMPR1[24] , \R_DATA_TEMPR2[24] , 
        \R_DATA_TEMPR0[25] , \R_DATA_TEMPR1[25] , \R_DATA_TEMPR2[25] , 
        \R_DATA_TEMPR0[26] , \R_DATA_TEMPR1[26] , \R_DATA_TEMPR2[26] , 
        \R_DATA_TEMPR0[27] , \R_DATA_TEMPR1[27] , \R_DATA_TEMPR2[27] , 
        \R_DATA_TEMPR0[28] , \R_DATA_TEMPR1[28] , \R_DATA_TEMPR2[28] , 
        \R_DATA_TEMPR0[29] , \R_DATA_TEMPR1[29] , \R_DATA_TEMPR2[29] , 
        \R_DATA_TEMPR0[30] , \R_DATA_TEMPR1[30] , \R_DATA_TEMPR2[30] , 
        \R_DATA_TEMPR0[31] , \R_DATA_TEMPR1[31] , \R_DATA_TEMPR2[31] , 
        \BLKX0[0] , \BLKY0[0] , \BLKX1[0] , \BLKY1[0] , 
        \ACCESS_BUSY[0][0] , \ACCESS_BUSY[0][1] , \ACCESS_BUSY[0][2] , 
        \ACCESS_BUSY[0][3] , \ACCESS_BUSY[0][4] , \ACCESS_BUSY[0][5] , 
        \ACCESS_BUSY[0][6] , \ACCESS_BUSY[0][7] , \ACCESS_BUSY[1][0] , 
        \ACCESS_BUSY[1][1] , \ACCESS_BUSY[1][2] , \ACCESS_BUSY[1][3] , 
        \ACCESS_BUSY[1][4] , \ACCESS_BUSY[1][5] , \ACCESS_BUSY[1][6] , 
        \ACCESS_BUSY[1][7] , \ACCESS_BUSY[2][0] , \ACCESS_BUSY[2][1] , 
        \ACCESS_BUSY[2][2] , \ACCESS_BUSY[2][3] , \ACCESS_BUSY[2][4] , 
        \ACCESS_BUSY[2][5] , \ACCESS_BUSY[2][6] , \ACCESS_BUSY[2][7] , 
        VCC, GND, ADLIB_VCC;
    wire GND_power_net1;
    wire VCC_power_net1;
    assign GND = GND_power_net1;
    assign VCC = VCC_power_net1;
    assign ADLIB_VCC = VCC_power_net1;
    
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%3%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C3 (.A_DOUT({nc0, 
        nc1, nc2, nc3, nc4, nc5, nc6, nc7, nc8, nc9, nc10, nc11, nc12, 
        nc13, nc14, nc15, \R_DATA_TEMPR1[27] , \R_DATA_TEMPR1[19] , 
        \R_DATA_TEMPR1[11] , \R_DATA_TEMPR1[3] }), .B_DOUT({nc16, nc17, 
        nc18, nc19, nc20, nc21, nc22, nc23, nc24, nc25, nc26, nc27, 
        nc28, nc29, nc30, nc31, nc32, nc33, nc34, nc35}), .DB_DETECT(), 
        .SB_CORRECT(), .ACCESS_BUSY(\ACCESS_BUSY[1][3] ), .A_ADDR({
        R_ADDR[11], R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], 
        R_ADDR[6], R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], 
        R_ADDR[1], R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , 
        R_ADDR[12]}), .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND}), .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(
        VCC), .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({
        W_ADDR[13], W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], 
        W_ADDR[8], W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], 
        W_ADDR[3], W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, 
        \BLKX1[0] , W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, W_DATA[3]}), .B_REN(VCC), .B_WEN({GND, VCC})
        , .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[18]  (.A(\R_DATA_TEMPR0[18] ), .B(
        \R_DATA_TEMPR1[18] ), .C(\R_DATA_TEMPR2[18] ), .Y(R_DATA[18]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%5%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C5 (.A_DOUT({nc36, 
        nc37, nc38, nc39, nc40, nc41, nc42, nc43, nc44, nc45, nc46, 
        nc47, nc48, nc49, nc50, nc51, \R_DATA_TEMPR1[29] , 
        \R_DATA_TEMPR1[21] , \R_DATA_TEMPR1[13] , \R_DATA_TEMPR1[5] }), 
        .B_DOUT({nc52, nc53, nc54, nc55, nc56, nc57, nc58, nc59, nc60, 
        nc61, nc62, nc63, nc64, nc65, nc66, nc67, nc68, nc69, nc70, 
        nc71}), .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(
        \ACCESS_BUSY[1][5] ), .A_ADDR({R_ADDR[11], R_ADDR[10], 
        R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], R_ADDR[5], 
        R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, 
        GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]}), .A_CLK(CLK), 
        .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND}), .A_REN(VCC), 
        .A_WEN({GND, GND}), .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), 
        .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], W_ADDR[12], 
        W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], W_ADDR[7], 
        W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], W_ADDR[2], 
        W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[5]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    INV \INVBLKX0[0]  (.A(W_ADDR[14]), .Y(\BLKX0[0] ));
    OR3 \OR3_R_DATA[16]  (.A(\R_DATA_TEMPR0[16] ), .B(
        \R_DATA_TEMPR1[16] ), .C(\R_DATA_TEMPR2[16] ), .Y(R_DATA[16]));
    OR3 \OR3_R_DATA[5]  (.A(\R_DATA_TEMPR0[5] ), .B(\R_DATA_TEMPR1[5] )
        , .C(\R_DATA_TEMPR2[5] ), .Y(R_DATA[5]));
    OR3 \OR3_R_DATA[25]  (.A(\R_DATA_TEMPR0[25] ), .B(
        \R_DATA_TEMPR1[25] ), .C(\R_DATA_TEMPR2[25] ), .Y(R_DATA[25]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%4%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C4 (.A_DOUT({nc72, 
        nc73, nc74, nc75, nc76, nc77, nc78, nc79, nc80, nc81, nc82, 
        nc83, nc84, nc85, nc86, nc87, \R_DATA_TEMPR1[28] , 
        \R_DATA_TEMPR1[20] , \R_DATA_TEMPR1[12] , \R_DATA_TEMPR1[4] }), 
        .B_DOUT({nc88, nc89, nc90, nc91, nc92, nc93, nc94, nc95, nc96, 
        nc97, nc98, nc99, nc100, nc101, nc102, nc103, nc104, nc105, 
        nc106, nc107}), .DB_DETECT(), .SB_CORRECT(), .ACCESS_BUSY(
        \ACCESS_BUSY[1][4] ), .A_ADDR({R_ADDR[11], R_ADDR[10], 
        R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], R_ADDR[5], 
        R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], R_ADDR[0], GND, 
        GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]}), .A_CLK(CLK), 
        .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND}), .A_REN(VCC), 
        .A_WEN({GND, GND}), .A_DOUT_EN(VCC), .A_DOUT_ARST_N(VCC), 
        .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], W_ADDR[12], 
        W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], W_ADDR[7], 
        W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], W_ADDR[2], 
        W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[4]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    INV \INVBLKX1[0]  (.A(W_ADDR[15]), .Y(\BLKX1[0] ));
    OR3 \OR3_R_DATA[7]  (.A(\R_DATA_TEMPR0[7] ), .B(\R_DATA_TEMPR1[7] )
        , .C(\R_DATA_TEMPR2[7] ), .Y(R_DATA[7]));
    OR3 \OR3_R_DATA[6]  (.A(\R_DATA_TEMPR0[6] ), .B(\R_DATA_TEMPR1[6] )
        , .C(\R_DATA_TEMPR2[6] ), .Y(R_DATA[6]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%4%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C4 (.A_DOUT({nc108, 
        nc109, nc110, nc111, nc112, nc113, nc114, nc115, nc116, nc117, 
        nc118, nc119, nc120, nc121, nc122, nc123, \R_DATA_TEMPR0[28] , 
        \R_DATA_TEMPR0[20] , \R_DATA_TEMPR0[12] , \R_DATA_TEMPR0[4] }), 
        .B_DOUT({nc124, nc125, nc126, nc127, nc128, nc129, nc130, 
        nc131, nc132, nc133, nc134, nc135, nc136, nc137, nc138, nc139, 
        nc140, nc141, nc142, nc143}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][4] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[4]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%1%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C1 (.A_DOUT({nc144, 
        nc145, nc146, nc147, nc148, nc149, nc150, nc151, nc152, nc153, 
        nc154, nc155, nc156, nc157, nc158, nc159, \R_DATA_TEMPR2[25] , 
        \R_DATA_TEMPR2[17] , \R_DATA_TEMPR2[9] , \R_DATA_TEMPR2[1] }), 
        .B_DOUT({nc160, nc161, nc162, nc163, nc164, nc165, nc166, 
        nc167, nc168, nc169, nc170, nc171, nc172, nc173, nc174, nc175, 
        nc176, nc177, nc178, nc179}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][1] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[1]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[29]  (.A(\R_DATA_TEMPR0[29] ), .B(
        \R_DATA_TEMPR1[29] ), .C(\R_DATA_TEMPR2[29] ), .Y(R_DATA[29]));
    OR3 \OR3_R_DATA[21]  (.A(\R_DATA_TEMPR0[21] ), .B(
        \R_DATA_TEMPR1[21] ), .C(\R_DATA_TEMPR2[21] ), .Y(R_DATA[21]));
    OR3 \OR3_R_DATA[24]  (.A(\R_DATA_TEMPR0[24] ), .B(
        \R_DATA_TEMPR1[24] ), .C(\R_DATA_TEMPR2[24] ), .Y(R_DATA[24]));
    OR3 \OR3_R_DATA[9]  (.A(\R_DATA_TEMPR0[9] ), .B(\R_DATA_TEMPR1[9] )
        , .C(\R_DATA_TEMPR2[9] ), .Y(R_DATA[9]));
    OR3 \OR3_R_DATA[15]  (.A(\R_DATA_TEMPR0[15] ), .B(
        \R_DATA_TEMPR1[15] ), .C(\R_DATA_TEMPR2[15] ), .Y(R_DATA[15]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%7%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C7 (.A_DOUT({nc180, 
        nc181, nc182, nc183, nc184, nc185, nc186, nc187, nc188, nc189, 
        nc190, nc191, nc192, nc193, nc194, nc195, \R_DATA_TEMPR1[31] , 
        \R_DATA_TEMPR1[23] , \R_DATA_TEMPR1[15] , \R_DATA_TEMPR1[7] }), 
        .B_DOUT({nc196, nc197, nc198, nc199, nc200, nc201, nc202, 
        nc203, nc204, nc205, nc206, nc207, nc208, nc209, nc210, nc211, 
        nc212, nc213, nc214, nc215}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[1][7] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]})
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[7]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[19]  (.A(\R_DATA_TEMPR0[19] ), .B(
        \R_DATA_TEMPR1[19] ), .C(\R_DATA_TEMPR2[19] ), .Y(R_DATA[19]));
    OR3 \OR3_R_DATA[3]  (.A(\R_DATA_TEMPR0[3] ), .B(\R_DATA_TEMPR1[3] )
        , .C(\R_DATA_TEMPR2[3] ), .Y(R_DATA[3]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%1%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C1 (.A_DOUT({nc216, 
        nc217, nc218, nc219, nc220, nc221, nc222, nc223, nc224, nc225, 
        nc226, nc227, nc228, nc229, nc230, nc231, \R_DATA_TEMPR1[25] , 
        \R_DATA_TEMPR1[17] , \R_DATA_TEMPR1[9] , \R_DATA_TEMPR1[1] }), 
        .B_DOUT({nc232, nc233, nc234, nc235, nc236, nc237, nc238, 
        nc239, nc240, nc241, nc242, nc243, nc244, nc245, nc246, nc247, 
        nc248, nc249, nc250, nc251}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[1][1] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]})
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[1]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[11]  (.A(\R_DATA_TEMPR0[11] ), .B(
        \R_DATA_TEMPR1[11] ), .C(\R_DATA_TEMPR2[11] ), .Y(R_DATA[11]));
    OR3 \OR3_R_DATA[14]  (.A(\R_DATA_TEMPR0[14] ), .B(
        \R_DATA_TEMPR1[14] ), .C(\R_DATA_TEMPR2[14] ), .Y(R_DATA[14]));
    OR3 \OR3_R_DATA[1]  (.A(\R_DATA_TEMPR0[1] ), .B(\R_DATA_TEMPR1[1] )
        , .C(\R_DATA_TEMPR2[1] ), .Y(R_DATA[1]));
    OR3 \OR3_R_DATA[8]  (.A(\R_DATA_TEMPR0[8] ), .B(\R_DATA_TEMPR1[8] )
        , .C(\R_DATA_TEMPR2[8] ), .Y(R_DATA[8]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%2%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C2 (.A_DOUT({nc252, 
        nc253, nc254, nc255, nc256, nc257, nc258, nc259, nc260, nc261, 
        nc262, nc263, nc264, nc265, nc266, nc267, \R_DATA_TEMPR1[26] , 
        \R_DATA_TEMPR1[18] , \R_DATA_TEMPR1[10] , \R_DATA_TEMPR1[2] }), 
        .B_DOUT({nc268, nc269, nc270, nc271, nc272, nc273, nc274, 
        nc275, nc276, nc277, nc278, nc279, nc280, nc281, nc282, nc283, 
        nc284, nc285, nc286, nc287}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[1][2] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]})
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[2]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%7%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C7 (.A_DOUT({nc288, 
        nc289, nc290, nc291, nc292, nc293, nc294, nc295, nc296, nc297, 
        nc298, nc299, nc300, nc301, nc302, nc303, \R_DATA_TEMPR0[31] , 
        \R_DATA_TEMPR0[23] , \R_DATA_TEMPR0[15] , \R_DATA_TEMPR0[7] }), 
        .B_DOUT({nc304, nc305, nc306, nc307, nc308, nc309, nc310, 
        nc311, nc312, nc313, nc314, nc315, nc316, nc317, nc318, nc319, 
        nc320, nc321, nc322, nc323}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][7] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[7]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[22]  (.A(\R_DATA_TEMPR0[22] ), .B(
        \R_DATA_TEMPR1[22] ), .C(\R_DATA_TEMPR2[22] ), .Y(R_DATA[22]));
    OR3 \OR3_R_DATA[31]  (.A(\R_DATA_TEMPR0[31] ), .B(
        \R_DATA_TEMPR1[31] ), .C(\R_DATA_TEMPR2[31] ), .Y(R_DATA[31]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%0%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C0 (.A_DOUT({nc324, 
        nc325, nc326, nc327, nc328, nc329, nc330, nc331, nc332, nc333, 
        nc334, nc335, nc336, nc337, nc338, nc339, \R_DATA_TEMPR0[24] , 
        \R_DATA_TEMPR0[16] , \R_DATA_TEMPR0[8] , \R_DATA_TEMPR0[0] }), 
        .B_DOUT({nc340, nc341, nc342, nc343, nc344, nc345, nc346, 
        nc347, nc348, nc349, nc350, nc351, nc352, nc353, nc354, nc355, 
        nc356, nc357, nc358, nc359}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][0] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[0]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[23]  (.A(\R_DATA_TEMPR0[23] ), .B(
        \R_DATA_TEMPR1[23] ), .C(\R_DATA_TEMPR2[23] ), .Y(R_DATA[23]));
    OR3 \OR3_R_DATA[0]  (.A(\R_DATA_TEMPR0[0] ), .B(\R_DATA_TEMPR1[0] )
        , .C(\R_DATA_TEMPR2[0] ), .Y(R_DATA[0]));
    OR3 \OR3_R_DATA[2]  (.A(\R_DATA_TEMPR0[2] ), .B(\R_DATA_TEMPR1[2] )
        , .C(\R_DATA_TEMPR2[2] ), .Y(R_DATA[2]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%0%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C0 (.A_DOUT({nc360, 
        nc361, nc362, nc363, nc364, nc365, nc366, nc367, nc368, nc369, 
        nc370, nc371, nc372, nc373, nc374, nc375, \R_DATA_TEMPR1[24] , 
        \R_DATA_TEMPR1[16] , \R_DATA_TEMPR1[8] , \R_DATA_TEMPR1[0] }), 
        .B_DOUT({nc376, nc377, nc378, nc379, nc380, nc381, nc382, 
        nc383, nc384, nc385, nc386, nc387, nc388, nc389, nc390, nc391, 
        nc392, nc393, nc394, nc395}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[1][0] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]})
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[0]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[27]  (.A(\R_DATA_TEMPR0[27] ), .B(
        \R_DATA_TEMPR1[27] ), .C(\R_DATA_TEMPR2[27] ), .Y(R_DATA[27]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%4%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C4 (.A_DOUT({nc396, 
        nc397, nc398, nc399, nc400, nc401, nc402, nc403, nc404, nc405, 
        nc406, nc407, nc408, nc409, nc410, nc411, \R_DATA_TEMPR2[28] , 
        \R_DATA_TEMPR2[20] , \R_DATA_TEMPR2[12] , \R_DATA_TEMPR2[4] }), 
        .B_DOUT({nc412, nc413, nc414, nc415, nc416, nc417, nc418, 
        nc419, nc420, nc421, nc422, nc423, nc424, nc425, nc426, nc427, 
        nc428, nc429, nc430, nc431}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][4] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[4]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%2%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C2 (.A_DOUT({nc432, 
        nc433, nc434, nc435, nc436, nc437, nc438, nc439, nc440, nc441, 
        nc442, nc443, nc444, nc445, nc446, nc447, \R_DATA_TEMPR2[26] , 
        \R_DATA_TEMPR2[18] , \R_DATA_TEMPR2[10] , \R_DATA_TEMPR2[2] }), 
        .B_DOUT({nc448, nc449, nc450, nc451, nc452, nc453, nc454, 
        nc455, nc456, nc457, nc458, nc459, nc460, nc461, nc462, nc463, 
        nc464, nc465, nc466, nc467}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][2] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[2]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%6%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C6 (.A_DOUT({nc468, 
        nc469, nc470, nc471, nc472, nc473, nc474, nc475, nc476, nc477, 
        nc478, nc479, nc480, nc481, nc482, nc483, \R_DATA_TEMPR0[30] , 
        \R_DATA_TEMPR0[22] , \R_DATA_TEMPR0[14] , \R_DATA_TEMPR0[6] }), 
        .B_DOUT({nc484, nc485, nc486, nc487, nc488, nc489, nc490, 
        nc491, nc492, nc493, nc494, nc495, nc496, nc497, nc498, nc499, 
        nc500, nc501, nc502, nc503}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][6] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[6]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[12]  (.A(\R_DATA_TEMPR0[12] ), .B(
        \R_DATA_TEMPR1[12] ), .C(\R_DATA_TEMPR2[12] ), .Y(R_DATA[12]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%3%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C3 (.A_DOUT({nc504, 
        nc505, nc506, nc507, nc508, nc509, nc510, nc511, nc512, nc513, 
        nc514, nc515, nc516, nc517, nc518, nc519, \R_DATA_TEMPR2[27] , 
        \R_DATA_TEMPR2[19] , \R_DATA_TEMPR2[11] , \R_DATA_TEMPR2[3] }), 
        .B_DOUT({nc520, nc521, nc522, nc523, nc524, nc525, nc526, 
        nc527, nc528, nc529, nc530, nc531, nc532, nc533, nc534, nc535, 
        nc536, nc537, nc538, nc539}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][3] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[3]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[13]  (.A(\R_DATA_TEMPR0[13] ), .B(
        \R_DATA_TEMPR1[13] ), .C(\R_DATA_TEMPR2[13] ), .Y(R_DATA[13]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%7%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C7 (.A_DOUT({nc540, 
        nc541, nc542, nc543, nc544, nc545, nc546, nc547, nc548, nc549, 
        nc550, nc551, nc552, nc553, nc554, nc555, \R_DATA_TEMPR2[31] , 
        \R_DATA_TEMPR2[23] , \R_DATA_TEMPR2[15] , \R_DATA_TEMPR2[7] }), 
        .B_DOUT({nc556, nc557, nc558, nc559, nc560, nc561, nc562, 
        nc563, nc564, nc565, nc566, nc567, nc568, nc569, nc570, nc571, 
        nc572, nc573, nc574, nc575}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][7] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[7]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[17]  (.A(\R_DATA_TEMPR0[17] ), .B(
        \R_DATA_TEMPR1[17] ), .C(\R_DATA_TEMPR2[17] ), .Y(R_DATA[17]));
    OR3 \OR3_R_DATA[20]  (.A(\R_DATA_TEMPR0[20] ), .B(
        \R_DATA_TEMPR1[20] ), .C(\R_DATA_TEMPR2[20] ), .Y(R_DATA[20]));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%0%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C0 (.A_DOUT({nc576, 
        nc577, nc578, nc579, nc580, nc581, nc582, nc583, nc584, nc585, 
        nc586, nc587, nc588, nc589, nc590, nc591, \R_DATA_TEMPR2[24] , 
        \R_DATA_TEMPR2[16] , \R_DATA_TEMPR2[8] , \R_DATA_TEMPR2[0] }), 
        .B_DOUT({nc592, nc593, nc594, nc595, nc596, nc597, nc598, 
        nc599, nc600, nc601, nc602, nc603, nc604, nc605, nc606, nc607, 
        nc608, nc609, nc610, nc611}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][0] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[0]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%3%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C3 (.A_DOUT({nc612, 
        nc613, nc614, nc615, nc616, nc617, nc618, nc619, nc620, nc621, 
        nc622, nc623, nc624, nc625, nc626, nc627, \R_DATA_TEMPR0[27] , 
        \R_DATA_TEMPR0[19] , \R_DATA_TEMPR0[11] , \R_DATA_TEMPR0[3] }), 
        .B_DOUT({nc628, nc629, nc630, nc631, nc632, nc633, nc634, 
        nc635, nc636, nc637, nc638, nc639, nc640, nc641, nc642, nc643, 
        nc644, nc645, nc646, nc647}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][3] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[3]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%5%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C5 (.A_DOUT({nc648, 
        nc649, nc650, nc651, nc652, nc653, nc654, nc655, nc656, nc657, 
        nc658, nc659, nc660, nc661, nc662, nc663, \R_DATA_TEMPR2[29] , 
        \R_DATA_TEMPR2[21] , \R_DATA_TEMPR2[13] , \R_DATA_TEMPR2[5] }), 
        .B_DOUT({nc664, nc665, nc666, nc667, nc668, nc669, nc670, 
        nc671, nc672, nc673, nc674, nc675, nc676, nc677, nc678, nc679, 
        nc680, nc681, nc682, nc683}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][5] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[5]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%2%6%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R2C6 (.A_DOUT({nc684, 
        nc685, nc686, nc687, nc688, nc689, nc690, nc691, nc692, nc693, 
        nc694, nc695, nc696, nc697, nc698, nc699, \R_DATA_TEMPR2[30] , 
        \R_DATA_TEMPR2[22] , \R_DATA_TEMPR2[14] , \R_DATA_TEMPR2[6] }), 
        .B_DOUT({nc700, nc701, nc702, nc703, nc704, nc705, nc706, 
        nc707, nc708, nc709, nc710, nc711, nc712, nc713, nc714, nc715, 
        nc716, nc717, nc718, nc719}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[2][6] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, R_ADDR[13], \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, W_ADDR[15], 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[6]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[10]  (.A(\R_DATA_TEMPR0[10] ), .B(
        \R_DATA_TEMPR1[10] ), .C(\R_DATA_TEMPR2[10] ), .Y(R_DATA[10]));
    OR3 \OR3_R_DATA[28]  (.A(\R_DATA_TEMPR0[28] ), .B(
        \R_DATA_TEMPR1[28] ), .C(\R_DATA_TEMPR2[28] ), .Y(R_DATA[28]));
    OR3 \OR3_R_DATA[26]  (.A(\R_DATA_TEMPR0[26] ), .B(
        \R_DATA_TEMPR1[26] ), .C(\R_DATA_TEMPR2[26] ), .Y(R_DATA[26]));
    INV \INVBLKY0[0]  (.A(R_ADDR[12]), .Y(\BLKY0[0] ));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%2%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C2 (.A_DOUT({nc720, 
        nc721, nc722, nc723, nc724, nc725, nc726, nc727, nc728, nc729, 
        nc730, nc731, nc732, nc733, nc734, nc735, \R_DATA_TEMPR0[26] , 
        \R_DATA_TEMPR0[18] , \R_DATA_TEMPR0[10] , \R_DATA_TEMPR0[2] }), 
        .B_DOUT({nc736, nc737, nc738, nc739, nc740, nc741, nc742, 
        nc743, nc744, nc745, nc746, nc747, nc748, nc749, nc750, nc751, 
        nc752, nc753, nc754, nc755}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][2] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[2]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%1%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C1 (.A_DOUT({nc756, 
        nc757, nc758, nc759, nc760, nc761, nc762, nc763, nc764, nc765, 
        nc766, nc767, nc768, nc769, nc770, nc771, \R_DATA_TEMPR0[25] , 
        \R_DATA_TEMPR0[17] , \R_DATA_TEMPR0[9] , \R_DATA_TEMPR0[1] }), 
        .B_DOUT({nc772, nc773, nc774, nc775, nc776, nc777, nc778, 
        nc779, nc780, nc781, nc782, nc783, nc784, nc785, nc786, nc787, 
        nc788, nc789, nc790, nc791}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][1] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[1]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%1%6%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R1C6 (.A_DOUT({nc792, 
        nc793, nc794, nc795, nc796, nc797, nc798, nc799, nc800, nc801, 
        nc802, nc803, nc804, nc805, nc806, nc807, \R_DATA_TEMPR1[30] , 
        \R_DATA_TEMPR1[22] , \R_DATA_TEMPR1[14] , \R_DATA_TEMPR1[6] }), 
        .B_DOUT({nc808, nc809, nc810, nc811, nc812, nc813, nc814, 
        nc815, nc816, nc817, nc818, nc819, nc820, nc821, nc822, nc823, 
        nc824, nc825, nc826, nc827}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[1][6] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , R_ADDR[12]})
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        W_ADDR[14]}), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[6]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[4]  (.A(\R_DATA_TEMPR0[4] ), .B(\R_DATA_TEMPR1[4] )
        , .C(\R_DATA_TEMPR2[4] ), .Y(R_DATA[4]));
    INV \INVBLKY1[0]  (.A(R_ADDR[13]), .Y(\BLKY1[0] ));
    RAM1K20 #( .RAMINDEX("PF_TPSRAM_C0_0%10000-40000%32-8%SPEED%0%5%TWO-PORT%ECC_EN-0")
         )  PF_TPSRAM_C0_PF_TPSRAM_C0_0_PF_TPSRAM_R0C5 (.A_DOUT({nc828, 
        nc829, nc830, nc831, nc832, nc833, nc834, nc835, nc836, nc837, 
        nc838, nc839, nc840, nc841, nc842, nc843, \R_DATA_TEMPR0[29] , 
        \R_DATA_TEMPR0[21] , \R_DATA_TEMPR0[13] , \R_DATA_TEMPR0[5] }), 
        .B_DOUT({nc844, nc845, nc846, nc847, nc848, nc849, nc850, 
        nc851, nc852, nc853, nc854, nc855, nc856, nc857, nc858, nc859, 
        nc860, nc861, nc862, nc863}), .DB_DETECT(), .SB_CORRECT(), 
        .ACCESS_BUSY(\ACCESS_BUSY[0][5] ), .A_ADDR({R_ADDR[11], 
        R_ADDR[10], R_ADDR[9], R_ADDR[8], R_ADDR[7], R_ADDR[6], 
        R_ADDR[5], R_ADDR[4], R_ADDR[3], R_ADDR[2], R_ADDR[1], 
        R_ADDR[0], GND, GND}), .A_BLK_EN({R_EN, \BLKY1[0] , \BLKY0[0] })
        , .A_CLK(CLK), .A_DIN({GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND}), 
        .A_REN(VCC), .A_WEN({GND, GND}), .A_DOUT_EN(VCC), 
        .A_DOUT_ARST_N(VCC), .A_DOUT_SRST_N(VCC), .B_ADDR({W_ADDR[13], 
        W_ADDR[12], W_ADDR[11], W_ADDR[10], W_ADDR[9], W_ADDR[8], 
        W_ADDR[7], W_ADDR[6], W_ADDR[5], W_ADDR[4], W_ADDR[3], 
        W_ADDR[2], W_ADDR[1], W_ADDR[0]}), .B_BLK_EN({W_EN, \BLKX1[0] , 
        \BLKX0[0] }), .B_CLK(CLK), .B_DIN({GND, GND, GND, GND, GND, 
        GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, GND, 
        GND, GND, W_DATA[5]}), .B_REN(VCC), .B_WEN({GND, VCC}), 
        .B_DOUT_EN(VCC), .B_DOUT_ARST_N(GND), .B_DOUT_SRST_N(VCC), 
        .ECC_EN(GND), .BUSY_FB(GND), .A_WIDTH({GND, VCC, GND}), 
        .A_WMODE({GND, GND}), .A_BYPASS(VCC), .B_WIDTH({GND, GND, GND})
        , .B_WMODE({GND, GND}), .B_BYPASS(VCC), .ECC_BYPASS(GND));
    OR3 \OR3_R_DATA[30]  (.A(\R_DATA_TEMPR0[30] ), .B(
        \R_DATA_TEMPR1[30] ), .C(\R_DATA_TEMPR2[30] ), .Y(R_DATA[30]));
    GND GND_power_inst1 (.Y(GND_power_net1));
    VCC VCC_power_inst1 (.Y(VCC_power_net1));
    
endmodule

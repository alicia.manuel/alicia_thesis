Microchip Technology Inc. - Microchip Libero Software Release v2022.2 (Version 2022.2.0.10)

Date      :  Thu May 25 11:03:24 2023
Project   :  C:\Users\Alicia\Documents\lira_fpga\prj\FIR_fpga
Component :  PF_CCC_C0
Family    :  PolarFire


HDL source files for all Synthesis and Simulation tools:
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/PF_CCC_C0/PF_CCC_C0_0/PF_CCC_C0_PF_CCC_C0_0_PF_CCC.v
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/PF_CCC_C0/PF_CCC_C0.v

Constraint files:
    C:/Users/Alicia/Documents/lira_fpga/prj/FIR_fpga/component/work/PF_CCC_C0\PF_CCC_C0_0\PF_CCC_C0_PF_CCC_C0_0_PF_CCC.sdc

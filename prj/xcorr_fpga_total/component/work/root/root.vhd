----------------------------------------------------------------------
-- Created by SmartDesign Thu Jun 15 08:57:43 2023
-- Version: 2022.2 2022.2.0.10
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library polarfire;
use polarfire.all;
----------------------------------------------------------------------
-- root entity declaration
----------------------------------------------------------------------
entity root is
    -- Port list
    port(
        -- Inputs
        EXT_RST_N : in  std_logic;
        REF_CLK_0 : in  std_logic;
        i_rx_ser  : in  std_logic;
        -- Outputs
        o_tx_ser  : out std_logic
        );
end root;
----------------------------------------------------------------------
-- root architecture body
----------------------------------------------------------------------
architecture RTL of root is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- AND2
component AND2
    -- Port list
    port(
        -- Inputs
        A : in  std_logic;
        B : in  std_logic;
        -- Outputs
        Y : out std_logic
        );
end component;
-- CORERESET_PF_C0
component CORERESET_PF_C0
    -- Port list
    port(
        -- Inputs
        BANK_x_VDDI_STATUS : in  std_logic;
        BANK_y_VDDI_STATUS : in  std_logic;
        CLK                : in  std_logic;
        EXT_RST_N          : in  std_logic;
        FF_US_RESTORE      : in  std_logic;
        FPGA_POR_N         : in  std_logic;
        INIT_DONE          : in  std_logic;
        PLL_LOCK           : in  std_logic;
        SS_BUSY            : in  std_logic;
        -- Outputs
        FABRIC_RESET_N     : out std_logic;
        PLL_POWERDOWN_B    : out std_logic
        );
end component;
-- interface_in
component interface_in
    -- Port list
    port(
        -- Inputs
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(7 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(7 downto 0);
        o_w_address : out std_logic_vector(8 downto 0)
        );
end component;
-- interface_in_output_xcorr
component interface_in_output_xcorr
    -- Port list
    port(
        -- Inputs
        i_addr_max  : in  std_logic_vector(9 downto 0);
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(31 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(31 downto 0);
        o_finish    : out std_logic;
        o_w_address : out std_logic_vector(9 downto 0)
        );
end component;
-- interface_out_output
component interface_out_output
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(7 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(11 downto 0);
        i_start      : in  std_logic;
        i_tx_busy    : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(7 downto 0);
        o_r_address  : out std_logic_vector(11 downto 0);
        o_ready      : out std_logic
        );
end component;
-- interface_out_synth
component interface_out_synth
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(7 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(8 downto 0);
        i_r_req      : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(7 downto 0);
        o_r_address  : out std_logic_vector(8 downto 0);
        o_ready      : out std_logic
        );
end component;
-- parallelized_xcorr
component parallelized_xcorr
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_data1      : in  std_logic_vector(7 downto 0);
        i_data2      : in  std_logic_vector(7 downto 0);
        i_data_ready : in  std_logic;
        i_nrst       : in  std_logic;
        -- Outputs
        o_finish     : out std_logic;
        o_rxy        : out std_logic_vector(31 downto 0)
        );
end component;
-- PF_CCC_C0
component PF_CCC_C0
    -- Port list
    port(
        -- Inputs
        PLL_POWERDOWN_N_0 : in  std_logic;
        REF_CLK_0         : in  std_logic;
        -- Outputs
        OUT0_FABCLK_0     : out std_logic;
        PLL_LOCK_0        : out std_logic
        );
end component;
-- PF_INIT_MONITOR_C0
component PF_INIT_MONITOR_C0
    -- Port list
    port(
        -- Outputs
        AUTOCALIB_DONE             : out std_logic;
        BANK_2_VDDI_STATUS         : out std_logic;
        BANK_6_VDDI_STATUS         : out std_logic;
        DEVICE_INIT_DONE           : out std_logic;
        FABRIC_POR_N               : out std_logic;
        PCIE_INIT_DONE             : out std_logic;
        SRAM_INIT_DONE             : out std_logic;
        SRAM_INIT_FROM_SNVM_DONE   : out std_logic;
        SRAM_INIT_FROM_SPI_DONE    : out std_logic;
        SRAM_INIT_FROM_UPROM_DONE  : out std_logic;
        USRAM_INIT_DONE            : out std_logic;
        USRAM_INIT_FROM_SNVM_DONE  : out std_logic;
        USRAM_INIT_FROM_SPI_DONE   : out std_logic;
        USRAM_INIT_FROM_UPROM_DONE : out std_logic;
        XCVR_INIT_DONE             : out std_logic
        );
end component;
-- PF_TPSRAM_C0
component PF_TPSRAM_C0
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(8 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(8 downto 0);
        W_DATA : in  std_logic_vector(7 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(7 downto 0)
        );
end component;
-- PF_TPSRAM_C1
component PF_TPSRAM_C1
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(11 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(9 downto 0);
        W_DATA : in  std_logic_vector(31 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(7 downto 0)
        );
end component;
-- uart_rx
component uart_rx
    -- Port list
    port(
        -- Inputs
        i_clk          : in  std_logic;
        i_enable       : in  std_logic;
        i_nrst         : in  std_logic;
        i_rx_ser       : in  std_logic;
        -- Outputs
        o_rx_par       : out std_logic_vector(7 downto 0);
        o_rx_par_valid : out std_logic
        );
end component;
-- uart_tx
component uart_tx
    -- Port list
    port(
        -- Inputs
        i_clk      : in  std_logic;
        i_enable   : in  std_logic;
        i_nrst     : in  std_logic;
        i_tx_par   : in  std_logic_vector(7 downto 0);
        i_tx_start : in  std_logic;
        -- Outputs
        o_tx_busy  : out std_logic;
        o_tx_ser   : out std_logic
        );
end component;
-- xcorr_fsm
component xcorr_fsm
    -- Port list
    port(
        -- Inputs
        i_clk           : in  std_logic;
        i_nrst          : in  std_logic;
        i_signal_length : in  std_logic_vector(8 downto 0);
        i_start         : in  std_logic;
        -- Outputs
        o_max_addr      : out std_logic_vector(8 downto 0);
        o_mem_req       : out std_logic;
        o_mem_w_req_1   : out std_logic;
        o_mem_w_req_2   : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal AND2_0_Y                                : std_logic;
signal CORERESET_PF_C0_0_FABRIC_RESET_N        : std_logic;
signal CORERESET_PF_C0_0_PLL_POWERDOWN_B       : std_logic;
signal interface_in_0_o_d_out                  : std_logic_vector(7 downto 0);
signal interface_in_0_o_w_address              : std_logic_vector(8 downto 0);
signal interface_in_0_o_WE                     : std_logic;
signal interface_in_1_o_d_out                  : std_logic_vector(7 downto 0);
signal interface_in_1_o_w_address              : std_logic_vector(8 downto 0);
signal interface_in_1_o_WE                     : std_logic;
signal interface_in_output_xcorr_0_o_d_out     : std_logic_vector(31 downto 0);
signal interface_in_output_xcorr_0_o_finish    : std_logic;
signal interface_in_output_xcorr_0_o_w_address : std_logic_vector(9 downto 0);
signal interface_in_output_xcorr_0_o_WE        : std_logic;
signal interface_out_output_0_o_d_out          : std_logic_vector(7 downto 0);
signal interface_out_output_0_o_r_address      : std_logic_vector(11 downto 0);
signal interface_out_output_0_o_RE             : std_logic;
signal interface_out_output_0_o_ready          : std_logic;
signal interface_out_synth_0_o_d_out           : std_logic_vector(7 downto 0);
signal interface_out_synth_0_o_r_address       : std_logic_vector(8 downto 0);
signal interface_out_synth_0_o_RE              : std_logic;
signal interface_out_synth_0_o_ready           : std_logic;
signal interface_out_synth_1_o_d_out           : std_logic_vector(7 downto 0);
signal interface_out_synth_1_o_r_address       : std_logic_vector(8 downto 0);
signal interface_out_synth_1_o_RE              : std_logic;
signal interface_out_synth_1_o_ready           : std_logic;
signal o_tx_ser_net_0                          : std_logic;
signal parallelized_xcorr_0_o_finish           : std_logic;
signal parallelized_xcorr_0_o_rxy              : std_logic_vector(31 downto 0);
signal PF_CCC_C0_0_OUT0_FABCLK_0               : std_logic;
signal PF_CCC_C0_0_PLL_LOCK_0                  : std_logic;
signal PF_INIT_MONITOR_C0_0_BANK_2_VDDI_STATUS : std_logic;
signal PF_INIT_MONITOR_C0_0_BANK_6_VDDI_STATUS : std_logic;
signal PF_INIT_MONITOR_C0_0_DEVICE_INIT_DONE   : std_logic;
signal PF_INIT_MONITOR_C0_0_FABRIC_POR_N       : std_logic;
signal PF_TPSRAM_C0_0_R_DATA                   : std_logic_vector(7 downto 0);
signal PF_TPSRAM_C0_1_R_DATA                   : std_logic_vector(7 downto 0);
signal PF_TPSRAM_C1_0_R_DATA                   : std_logic_vector(7 downto 0);
signal uart_rx_0_o_rx_par                      : std_logic_vector(7 downto 0);
signal uart_rx_0_o_rx_par_valid                : std_logic;
signal uart_tx_0_o_tx_busy                     : std_logic;
signal xcorr_fsm_0_o_max_addr                  : std_logic_vector(8 downto 0);
signal xcorr_fsm_0_o_mem_req                   : std_logic;
signal xcorr_fsm_0_o_mem_w_req_1               : std_logic;
signal xcorr_fsm_0_o_mem_w_req_2               : std_logic;
signal o_tx_ser_net_1                          : std_logic;
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net                                 : std_logic;
signal i_addr_max_const_net_0                  : std_logic_vector(9 downto 0);
signal i_r_addr_max_const_net_0                : std_logic_vector(11 downto 0);
signal VCC_net                                 : std_logic;
signal i_signal_length_const_net_0             : std_logic_vector(8 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net                     <= '0';
 i_addr_max_const_net_0      <= B"1111100111";
 i_r_addr_max_const_net_0    <= B"001111100111";
 VCC_net                     <= '1';
 i_signal_length_const_net_0 <= B"111110100";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 o_tx_ser_net_1 <= o_tx_ser_net_0;
 o_tx_ser       <= o_tx_ser_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- AND2_0
AND2_0 : AND2
    port map( 
        -- Inputs
        A => interface_out_synth_0_o_ready,
        B => interface_out_synth_1_o_ready,
        -- Outputs
        Y => AND2_0_Y 
        );
-- CORERESET_PF_C0_0
CORERESET_PF_C0_0 : CORERESET_PF_C0
    port map( 
        -- Inputs
        CLK                => PF_CCC_C0_0_OUT0_FABCLK_0,
        EXT_RST_N          => EXT_RST_N,
        BANK_x_VDDI_STATUS => PF_INIT_MONITOR_C0_0_BANK_2_VDDI_STATUS,
        BANK_y_VDDI_STATUS => PF_INIT_MONITOR_C0_0_BANK_6_VDDI_STATUS,
        PLL_LOCK           => PF_CCC_C0_0_PLL_LOCK_0,
        SS_BUSY            => GND_net,
        INIT_DONE          => PF_INIT_MONITOR_C0_0_DEVICE_INIT_DONE,
        FF_US_RESTORE      => GND_net,
        FPGA_POR_N         => PF_INIT_MONITOR_C0_0_FABRIC_POR_N,
        -- Outputs
        PLL_POWERDOWN_B    => CORERESET_PF_C0_0_PLL_POWERDOWN_B,
        FABRIC_RESET_N     => CORERESET_PF_C0_0_FABRIC_RESET_N 
        );
-- interface_in_0
interface_in_0 : interface_in
    port map( 
        -- Inputs
        i_clk       => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst      => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_w_req     => xcorr_fsm_0_o_mem_w_req_1,
        i_d_in      => uart_rx_0_o_rx_par,
        -- Outputs
        o_WE        => interface_in_0_o_WE,
        o_d_out     => interface_in_0_o_d_out,
        o_w_address => interface_in_0_o_w_address 
        );
-- interface_in_1
interface_in_1 : interface_in
    port map( 
        -- Inputs
        i_clk       => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst      => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_w_req     => xcorr_fsm_0_o_mem_w_req_2,
        i_d_in      => uart_rx_0_o_rx_par,
        -- Outputs
        o_WE        => interface_in_1_o_WE,
        o_d_out     => interface_in_1_o_d_out,
        o_w_address => interface_in_1_o_w_address 
        );
-- interface_in_output_xcorr_0
interface_in_output_xcorr_0 : interface_in_output_xcorr
    port map( 
        -- Inputs
        i_clk       => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst      => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_w_req     => parallelized_xcorr_0_o_finish,
        i_d_in      => parallelized_xcorr_0_o_rxy,
        i_addr_max  => i_addr_max_const_net_0,
        -- Outputs
        o_WE        => interface_in_output_xcorr_0_o_WE,
        o_finish    => interface_in_output_xcorr_0_o_finish,
        o_d_out     => interface_in_output_xcorr_0_o_d_out,
        o_w_address => interface_in_output_xcorr_0_o_w_address 
        );
-- interface_out_output_0
interface_out_output_0 : interface_out_output
    port map( 
        -- Inputs
        i_clk        => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst       => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_start      => interface_in_output_xcorr_0_o_finish,
        i_tx_busy    => uart_tx_0_o_tx_busy,
        i_d_in       => PF_TPSRAM_C1_0_R_DATA,
        i_r_addr_max => i_r_addr_max_const_net_0,
        -- Outputs
        o_RE         => interface_out_output_0_o_RE,
        o_ready      => interface_out_output_0_o_ready,
        o_d_out      => interface_out_output_0_o_d_out,
        o_r_address  => interface_out_output_0_o_r_address 
        );
-- interface_out_synth_0
interface_out_synth_0 : interface_out_synth
    port map( 
        -- Inputs
        i_clk        => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst       => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_r_req      => xcorr_fsm_0_o_mem_req,
        i_d_in       => PF_TPSRAM_C0_0_R_DATA,
        i_r_addr_max => xcorr_fsm_0_o_max_addr,
        -- Outputs
        o_RE         => interface_out_synth_0_o_RE,
        o_ready      => interface_out_synth_0_o_ready,
        o_d_out      => interface_out_synth_0_o_d_out,
        o_r_address  => interface_out_synth_0_o_r_address 
        );
-- interface_out_synth_1
interface_out_synth_1 : interface_out_synth
    port map( 
        -- Inputs
        i_clk        => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst       => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_r_req      => xcorr_fsm_0_o_mem_req,
        i_d_in       => PF_TPSRAM_C0_1_R_DATA,
        i_r_addr_max => xcorr_fsm_0_o_max_addr,
        -- Outputs
        o_RE         => interface_out_synth_1_o_RE,
        o_ready      => interface_out_synth_1_o_ready,
        o_d_out      => interface_out_synth_1_o_d_out,
        o_r_address  => interface_out_synth_1_o_r_address 
        );
-- parallelized_xcorr_0
parallelized_xcorr_0 : parallelized_xcorr
    port map( 
        -- Inputs
        i_clk        => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst       => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_data_ready => AND2_0_Y,
        i_data1      => interface_out_synth_0_o_d_out,
        i_data2      => interface_out_synth_1_o_d_out,
        -- Outputs
        o_finish     => parallelized_xcorr_0_o_finish,
        o_rxy        => parallelized_xcorr_0_o_rxy 
        );
-- PF_CCC_C0_0
PF_CCC_C0_0 : PF_CCC_C0
    port map( 
        -- Inputs
        REF_CLK_0         => REF_CLK_0,
        PLL_POWERDOWN_N_0 => CORERESET_PF_C0_0_PLL_POWERDOWN_B,
        -- Outputs
        OUT0_FABCLK_0     => PF_CCC_C0_0_OUT0_FABCLK_0,
        PLL_LOCK_0        => PF_CCC_C0_0_PLL_LOCK_0 
        );
-- PF_INIT_MONITOR_C0_0
PF_INIT_MONITOR_C0_0 : PF_INIT_MONITOR_C0
    port map( 
        -- Outputs
        FABRIC_POR_N               => PF_INIT_MONITOR_C0_0_FABRIC_POR_N,
        PCIE_INIT_DONE             => OPEN,
        USRAM_INIT_DONE            => OPEN,
        SRAM_INIT_DONE             => OPEN,
        DEVICE_INIT_DONE           => PF_INIT_MONITOR_C0_0_DEVICE_INIT_DONE,
        BANK_2_VDDI_STATUS         => PF_INIT_MONITOR_C0_0_BANK_2_VDDI_STATUS,
        BANK_6_VDDI_STATUS         => PF_INIT_MONITOR_C0_0_BANK_6_VDDI_STATUS,
        XCVR_INIT_DONE             => OPEN,
        USRAM_INIT_FROM_SNVM_DONE  => OPEN,
        USRAM_INIT_FROM_UPROM_DONE => OPEN,
        USRAM_INIT_FROM_SPI_DONE   => OPEN,
        SRAM_INIT_FROM_SNVM_DONE   => OPEN,
        SRAM_INIT_FROM_UPROM_DONE  => OPEN,
        SRAM_INIT_FROM_SPI_DONE    => OPEN,
        AUTOCALIB_DONE             => OPEN 
        );
-- PF_TPSRAM_C0_0
PF_TPSRAM_C0_0 : PF_TPSRAM_C0
    port map( 
        -- Inputs
        W_EN   => interface_in_0_o_WE,
        R_EN   => interface_out_synth_0_o_RE,
        CLK    => PF_CCC_C0_0_OUT0_FABCLK_0,
        W_DATA => interface_in_0_o_d_out,
        W_ADDR => interface_in_0_o_w_address,
        R_ADDR => interface_out_synth_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C0_0_R_DATA 
        );
-- PF_TPSRAM_C0_1
PF_TPSRAM_C0_1 : PF_TPSRAM_C0
    port map( 
        -- Inputs
        W_EN   => interface_in_1_o_WE,
        R_EN   => interface_out_synth_1_o_RE,
        CLK    => PF_CCC_C0_0_OUT0_FABCLK_0,
        W_DATA => interface_in_1_o_d_out,
        W_ADDR => interface_in_1_o_w_address,
        R_ADDR => interface_out_synth_1_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C0_1_R_DATA 
        );
-- PF_TPSRAM_C1_0
PF_TPSRAM_C1_0 : PF_TPSRAM_C1
    port map( 
        -- Inputs
        W_EN   => interface_in_output_xcorr_0_o_WE,
        R_EN   => interface_out_output_0_o_RE,
        CLK    => PF_CCC_C0_0_OUT0_FABCLK_0,
        W_DATA => interface_in_output_xcorr_0_o_d_out,
        W_ADDR => interface_in_output_xcorr_0_o_w_address,
        R_ADDR => interface_out_output_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C1_0_R_DATA 
        );
-- uart_rx_0
uart_rx_0 : uart_rx
    port map( 
        -- Inputs
        i_clk          => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst         => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_enable       => VCC_net,
        i_rx_ser       => i_rx_ser,
        -- Outputs
        o_rx_par_valid => uart_rx_0_o_rx_par_valid,
        o_rx_par       => uart_rx_0_o_rx_par 
        );
-- uart_tx_0
uart_tx_0 : uart_tx
    port map( 
        -- Inputs
        i_clk      => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_enable   => VCC_net,
        i_nrst     => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_tx_start => interface_out_output_0_o_ready,
        i_tx_par   => interface_out_output_0_o_d_out,
        -- Outputs
        o_tx_ser   => o_tx_ser_net_0,
        o_tx_busy  => uart_tx_0_o_tx_busy 
        );
-- xcorr_fsm_0
xcorr_fsm_0 : xcorr_fsm
    port map( 
        -- Inputs
        i_clk           => PF_CCC_C0_0_OUT0_FABCLK_0,
        i_nrst          => CORERESET_PF_C0_0_FABRIC_RESET_N,
        i_start         => uart_rx_0_o_rx_par_valid,
        i_signal_length => i_signal_length_const_net_0,
        -- Outputs
        o_mem_w_req_1   => xcorr_fsm_0_o_mem_w_req_1,
        o_mem_w_req_2   => xcorr_fsm_0_o_mem_w_req_2,
        o_mem_req       => xcorr_fsm_0_o_mem_req,
        o_max_addr      => xcorr_fsm_0_o_max_addr 
        );

end RTL;

----------------------------------------------------------------------
-- Created by SmartDesign Mon Jun 26 10:31:26 2023
-- Version: 2022.2 2022.2.0.10
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library polarfire;
use polarfire.all;
----------------------------------------------------------------------
-- root entity declaration
----------------------------------------------------------------------
entity root is
    -- Port list
    port(
        -- Inputs
        i_clk           : in  std_logic;
        i_d_in          : in  std_logic_vector(7 downto 0);
        i_nrst          : in  std_logic;
        i_signal_length : in  std_logic_vector(13 downto 0);
        i_start         : in  std_logic;
        i_tx_busy       : in  std_logic;
        -- Outputs
        o_d_out         : out std_logic_vector(7 downto 0);
        o_ready         : out std_logic
        );
end root;
----------------------------------------------------------------------
-- root architecture body
----------------------------------------------------------------------
architecture RTL of root is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- fir_fsm
component fir_fsm
    -- Port list
    port(
        -- Inputs
        i_clk           : in  std_logic;
        i_fir_ready     : in  std_logic;
        i_nrst          : in  std_logic;
        i_signal_length : in  std_logic_vector(13 downto 0);
        i_start         : in  std_logic;
        -- Outputs
        o_max_addr      : out std_logic_vector(13 downto 0);
        o_mem_req       : out std_logic;
        o_mem_w_req     : out std_logic
        );
end component;
-- FIR_system_2
component FIR_system_2
    -- Port list
    port(
        -- Inputs
        i_clk              : in  std_logic;
        i_data_in          : in  std_logic_vector(31 downto 0);
        i_nrst             : in  std_logic;
        i_start            : in  std_logic;
        -- Outputs
        o_data_out         : out std_logic_vector(31 downto 0);
        o_data_ready       : out std_logic;
        o_finish_first_fir : out std_logic
        );
end component;
-- interface_in_output
component interface_in_output
    -- Port list
    port(
        -- Inputs
        i_addr_max  : in  std_logic_vector(13 downto 0);
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(31 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(31 downto 0);
        o_finish    : out std_logic;
        o_w_address : out std_logic_vector(13 downto 0)
        );
end component;
-- interface_in_synth
component interface_in_synth
    -- Port list
    port(
        -- Inputs
        i_addr_max  : in  std_logic_vector(13 downto 0);
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(7 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(7 downto 0);
        o_w_address : out std_logic_vector(15 downto 0)
        );
end component;
-- interface_out_output
component interface_out_output
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(7 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(15 downto 0);
        i_start      : in  std_logic;
        i_tx_busy    : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(7 downto 0);
        o_r_address  : out std_logic_vector(15 downto 0);
        o_ready      : out std_logic
        );
end component;
-- interface_out_synth
component interface_out_synth
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(31 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(13 downto 0);
        i_r_req      : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(31 downto 0);
        o_r_address  : out std_logic_vector(13 downto 0);
        o_ready      : out std_logic
        );
end component;
-- PF_TPSRAM_C0
component PF_TPSRAM_C0
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(13 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(15 downto 0);
        W_DATA : in  std_logic_vector(7 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(31 downto 0)
        );
end component;
-- PF_TPSRAM_C1
component PF_TPSRAM_C1
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(15 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(13 downto 0);
        W_DATA : in  std_logic_vector(31 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(7 downto 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal fir_fsm_0_o_max_addr               : std_logic_vector(13 downto 0);
signal fir_fsm_0_o_mem_req                : std_logic;
signal fir_fsm_0_o_mem_w_req              : std_logic;
signal FIR_system_2_0_o_data_out          : std_logic_vector(31 downto 0);
signal FIR_system_2_0_o_data_ready        : std_logic;
signal FIR_system_2_0_o_finish_first_fir  : std_logic;
signal interface_in_output_0_o_d_out      : std_logic_vector(31 downto 0);
signal interface_in_output_0_o_finish     : std_logic;
signal interface_in_output_0_o_w_address  : std_logic_vector(13 downto 0);
signal interface_in_output_0_o_WE         : std_logic;
signal interface_in_synth_0_o_d_out       : std_logic_vector(7 downto 0);
signal interface_in_synth_0_o_w_address   : std_logic_vector(15 downto 0);
signal interface_in_synth_0_o_WE          : std_logic;
signal interface_out_output_0_o_r_address : std_logic_vector(15 downto 0);
signal interface_out_output_0_o_RE        : std_logic;
signal interface_out_synth_0_o_d_out      : std_logic_vector(31 downto 0);
signal interface_out_synth_0_o_r_address  : std_logic_vector(13 downto 0);
signal interface_out_synth_0_o_RE         : std_logic;
signal interface_out_synth_0_o_ready      : std_logic;
signal o_d_out_net_0                      : std_logic_vector(7 downto 0);
signal o_ready_net_0                      : std_logic;
signal PF_TPSRAM_C0_0_R_DATA              : std_logic_vector(31 downto 0);
signal PF_TPSRAM_C1_1_R_DATA              : std_logic_vector(7 downto 0);
signal o_ready_net_1                      : std_logic;
signal o_d_out_net_1                      : std_logic_vector(7 downto 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal i_addr_max_const_net_0             : std_logic_vector(13 downto 0);
signal i_r_addr_max_const_net_0           : std_logic_vector(15 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 i_addr_max_const_net_0   <= B"00000001100100";
 i_r_addr_max_const_net_0 <= B"0000000001100100";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 o_ready_net_1       <= o_ready_net_0;
 o_ready             <= o_ready_net_1;
 o_d_out_net_1       <= o_d_out_net_0;
 o_d_out(7 downto 0) <= o_d_out_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- fir_fsm_0
fir_fsm_0 : fir_fsm
    port map( 
        -- Inputs
        i_clk           => i_clk,
        i_nrst          => i_nrst,
        i_start         => i_start,
        i_fir_ready     => FIR_system_2_0_o_finish_first_fir,
        i_signal_length => i_signal_length,
        -- Outputs
        o_mem_w_req     => fir_fsm_0_o_mem_w_req,
        o_mem_req       => fir_fsm_0_o_mem_req,
        o_max_addr      => fir_fsm_0_o_max_addr 
        );
-- FIR_system_2_0
FIR_system_2_0 : FIR_system_2
    port map( 
        -- Inputs
        i_clk              => i_clk,
        i_nrst             => i_nrst,
        i_start            => interface_out_synth_0_o_ready,
        i_data_in          => interface_out_synth_0_o_d_out,
        -- Outputs
        o_finish_first_fir => FIR_system_2_0_o_finish_first_fir,
        o_data_ready       => FIR_system_2_0_o_data_ready,
        o_data_out         => FIR_system_2_0_o_data_out 
        );
-- interface_in_output_0
interface_in_output_0 : interface_in_output
    port map( 
        -- Inputs
        i_clk       => i_clk,
        i_nrst      => i_nrst,
        i_w_req     => FIR_system_2_0_o_data_ready,
        i_d_in      => FIR_system_2_0_o_data_out,
        i_addr_max  => i_addr_max_const_net_0,
        -- Outputs
        o_WE        => interface_in_output_0_o_WE,
        o_finish    => interface_in_output_0_o_finish,
        o_d_out     => interface_in_output_0_o_d_out,
        o_w_address => interface_in_output_0_o_w_address 
        );
-- interface_in_synth_0
interface_in_synth_0 : interface_in_synth
    port map( 
        -- Inputs
        i_clk       => i_clk,
        i_nrst      => i_nrst,
        i_w_req     => fir_fsm_0_o_mem_w_req,
        i_d_in      => i_d_in,
        i_addr_max  => fir_fsm_0_o_max_addr,
        -- Outputs
        o_d_out     => interface_in_synth_0_o_d_out,
        o_WE        => interface_in_synth_0_o_WE,
        o_w_address => interface_in_synth_0_o_w_address 
        );
-- interface_out_output_0
interface_out_output_0 : interface_out_output
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_start      => interface_in_output_0_o_finish,
        i_tx_busy    => i_tx_busy,
        i_d_in       => PF_TPSRAM_C1_1_R_DATA,
        i_r_addr_max => i_r_addr_max_const_net_0,
        -- Outputs
        o_RE         => interface_out_output_0_o_RE,
        o_ready      => o_ready_net_0,
        o_d_out      => o_d_out_net_0,
        o_r_address  => interface_out_output_0_o_r_address 
        );
-- interface_out_synth_0
interface_out_synth_0 : interface_out_synth
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_r_req      => fir_fsm_0_o_mem_req,
        i_d_in       => PF_TPSRAM_C0_0_R_DATA,
        i_r_addr_max => fir_fsm_0_o_max_addr,
        -- Outputs
        o_RE         => interface_out_synth_0_o_RE,
        o_ready      => interface_out_synth_0_o_ready,
        o_d_out      => interface_out_synth_0_o_d_out,
        o_r_address  => interface_out_synth_0_o_r_address 
        );
-- PF_TPSRAM_C0_0
PF_TPSRAM_C0_0 : PF_TPSRAM_C0
    port map( 
        -- Inputs
        W_EN   => interface_in_synth_0_o_WE,
        R_EN   => interface_out_synth_0_o_RE,
        CLK    => i_clk,
        W_DATA => interface_in_synth_0_o_d_out,
        W_ADDR => interface_in_synth_0_o_w_address,
        R_ADDR => interface_out_synth_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C0_0_R_DATA 
        );
-- PF_TPSRAM_C1_1
PF_TPSRAM_C1_1 : PF_TPSRAM_C1
    port map( 
        -- Inputs
        W_EN   => interface_in_output_0_o_WE,
        R_EN   => interface_out_output_0_o_RE,
        CLK    => i_clk,
        W_DATA => interface_in_output_0_o_d_out,
        W_ADDR => interface_in_output_0_o_w_address,
        R_ADDR => interface_out_output_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C1_1_R_DATA 
        );

end RTL;

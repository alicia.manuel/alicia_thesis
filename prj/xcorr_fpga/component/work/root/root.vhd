----------------------------------------------------------------------
-- Created by SmartDesign Tue Jun 13 14:26:14 2023
-- Version: 2022.2 2022.2.0.10
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library polarfire;
use polarfire.all;
----------------------------------------------------------------------
-- root entity declaration
----------------------------------------------------------------------
entity root is
    -- Port list
    port(
        -- Inputs
        i_clk           : in  std_logic;
        i_d_in          : in  std_logic_vector(7 downto 0);
        i_nrst          : in  std_logic;
        i_signal_length : in  std_logic_vector(8 downto 0);
        i_start         : in  std_logic;
        i_tx_busy       : in  std_logic;
        -- Outputs
        o_d_out         : out std_logic_vector(7 downto 0);
        o_ready         : out std_logic
        );
end root;
----------------------------------------------------------------------
-- root architecture body
----------------------------------------------------------------------
architecture RTL of root is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- AND2
component AND2
    -- Port list
    port(
        -- Inputs
        A : in  std_logic;
        B : in  std_logic;
        -- Outputs
        Y : out std_logic
        );
end component;
-- interface_in
component interface_in
    -- Port list
    port(
        -- Inputs
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(7 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(7 downto 0);
        o_w_address : out std_logic_vector(8 downto 0)
        );
end component;
-- interface_in_output_xcorr
component interface_in_output_xcorr
    -- Port list
    port(
        -- Inputs
        i_addr_max  : in  std_logic_vector(9 downto 0);
        i_clk       : in  std_logic;
        i_d_in      : in  std_logic_vector(31 downto 0);
        i_nrst      : in  std_logic;
        i_w_req     : in  std_logic;
        -- Outputs
        o_WE        : out std_logic;
        o_d_out     : out std_logic_vector(31 downto 0);
        o_finish    : out std_logic;
        o_w_address : out std_logic_vector(9 downto 0)
        );
end component;
-- interface_out_output
component interface_out_output
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(7 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(11 downto 0);
        i_start      : in  std_logic;
        i_tx_busy    : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(7 downto 0);
        o_r_address  : out std_logic_vector(11 downto 0);
        o_ready      : out std_logic
        );
end component;
-- interface_out_synth
component interface_out_synth
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_d_in       : in  std_logic_vector(7 downto 0);
        i_nrst       : in  std_logic;
        i_r_addr_max : in  std_logic_vector(8 downto 0);
        i_r_req      : in  std_logic;
        -- Outputs
        o_RE         : out std_logic;
        o_d_out      : out std_logic_vector(7 downto 0);
        o_r_address  : out std_logic_vector(8 downto 0);
        o_ready      : out std_logic
        );
end component;
-- parallelized_xcorr
component parallelized_xcorr
    -- Port list
    port(
        -- Inputs
        i_clk        : in  std_logic;
        i_data1      : in  std_logic_vector(7 downto 0);
        i_data2      : in  std_logic_vector(7 downto 0);
        i_data_ready : in  std_logic;
        i_nrst       : in  std_logic;
        -- Outputs
        o_finish     : out std_logic;
        o_rxy        : out std_logic_vector(31 downto 0)
        );
end component;
-- PF_TPSRAM_C0
component PF_TPSRAM_C0
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(8 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(8 downto 0);
        W_DATA : in  std_logic_vector(7 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(7 downto 0)
        );
end component;
-- PF_TPSRAM_C1
component PF_TPSRAM_C1
    -- Port list
    port(
        -- Inputs
        CLK    : in  std_logic;
        R_ADDR : in  std_logic_vector(11 downto 0);
        R_EN   : in  std_logic;
        W_ADDR : in  std_logic_vector(9 downto 0);
        W_DATA : in  std_logic_vector(31 downto 0);
        W_EN   : in  std_logic;
        -- Outputs
        R_DATA : out std_logic_vector(7 downto 0)
        );
end component;
-- xcorr_fsm
component xcorr_fsm
    -- Port list
    port(
        -- Inputs
        i_clk           : in  std_logic;
        i_nrst          : in  std_logic;
        i_signal_length : in  std_logic_vector(8 downto 0);
        i_start         : in  std_logic;
        -- Outputs
        o_max_addr      : out std_logic_vector(8 downto 0);
        o_mem_req       : out std_logic;
        o_mem_w_req_1   : out std_logic;
        o_mem_w_req_2   : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal AND2_0_Y                                : std_logic;
signal interface_in_0_o_d_out                  : std_logic_vector(7 downto 0);
signal interface_in_0_o_w_address              : std_logic_vector(8 downto 0);
signal interface_in_0_o_WE                     : std_logic;
signal interface_in_1_o_d_out                  : std_logic_vector(7 downto 0);
signal interface_in_1_o_w_address              : std_logic_vector(8 downto 0);
signal interface_in_1_o_WE                     : std_logic;
signal interface_in_output_xcorr_0_o_d_out     : std_logic_vector(31 downto 0);
signal interface_in_output_xcorr_0_o_finish    : std_logic;
signal interface_in_output_xcorr_0_o_w_address : std_logic_vector(9 downto 0);
signal interface_in_output_xcorr_0_o_WE        : std_logic;
signal interface_out_output_0_o_r_address      : std_logic_vector(11 downto 0);
signal interface_out_output_0_o_RE             : std_logic;
signal interface_out_synth_0_o_d_out           : std_logic_vector(7 downto 0);
signal interface_out_synth_0_o_r_address       : std_logic_vector(8 downto 0);
signal interface_out_synth_0_o_RE              : std_logic;
signal interface_out_synth_0_o_ready           : std_logic;
signal interface_out_synth_1_o_d_out           : std_logic_vector(7 downto 0);
signal interface_out_synth_1_o_r_address       : std_logic_vector(8 downto 0);
signal interface_out_synth_1_o_RE              : std_logic;
signal interface_out_synth_1_o_ready           : std_logic;
signal o_d_out_net_0                           : std_logic_vector(7 downto 0);
signal o_ready_net_0                           : std_logic;
signal parallelized_xcorr_0_o_finish           : std_logic;
signal parallelized_xcorr_0_o_rxy              : std_logic_vector(31 downto 0);
signal PF_TPSRAM_C0_0_R_DATA                   : std_logic_vector(7 downto 0);
signal PF_TPSRAM_C0_1_R_DATA                   : std_logic_vector(7 downto 0);
signal PF_TPSRAM_C1_0_R_DATA                   : std_logic_vector(7 downto 0);
signal xcorr_fsm_0_o_max_addr                  : std_logic_vector(8 downto 0);
signal xcorr_fsm_0_o_mem_req                   : std_logic;
signal xcorr_fsm_0_o_mem_w_req_1               : std_logic;
signal xcorr_fsm_0_o_mem_w_req_2               : std_logic;
signal o_ready_net_1                           : std_logic;
signal o_d_out_net_1                           : std_logic_vector(7 downto 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal i_addr_max_const_net_0                  : std_logic_vector(9 downto 0);
signal i_r_addr_max_const_net_0                : std_logic_vector(11 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 i_addr_max_const_net_0   <= B"1111100111";
 i_r_addr_max_const_net_0 <= B"001111100111";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 o_ready_net_1       <= o_ready_net_0;
 o_ready             <= o_ready_net_1;
 o_d_out_net_1       <= o_d_out_net_0;
 o_d_out(7 downto 0) <= o_d_out_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- AND2_0
AND2_0 : AND2
    port map( 
        -- Inputs
        A => interface_out_synth_0_o_ready,
        B => interface_out_synth_1_o_ready,
        -- Outputs
        Y => AND2_0_Y 
        );
-- interface_in_0
interface_in_0 : interface_in
    port map( 
        -- Inputs
        i_clk       => i_clk,
        i_nrst      => i_nrst,
        i_w_req     => xcorr_fsm_0_o_mem_w_req_1,
        i_d_in      => i_d_in,
        -- Outputs
        o_WE        => interface_in_0_o_WE,
        o_d_out     => interface_in_0_o_d_out,
        o_w_address => interface_in_0_o_w_address 
        );
-- interface_in_1
interface_in_1 : interface_in
    port map( 
        -- Inputs
        i_clk       => i_clk,
        i_nrst      => i_nrst,
        i_w_req     => xcorr_fsm_0_o_mem_w_req_2,
        i_d_in      => i_d_in,
        -- Outputs
        o_WE        => interface_in_1_o_WE,
        o_d_out     => interface_in_1_o_d_out,
        o_w_address => interface_in_1_o_w_address 
        );
-- interface_in_output_xcorr_0
interface_in_output_xcorr_0 : interface_in_output_xcorr
    port map( 
        -- Inputs
        i_clk       => i_clk,
        i_nrst      => i_nrst,
        i_w_req     => parallelized_xcorr_0_o_finish,
        i_d_in      => parallelized_xcorr_0_o_rxy,
        i_addr_max  => i_addr_max_const_net_0,
        -- Outputs
        o_WE        => interface_in_output_xcorr_0_o_WE,
        o_finish    => interface_in_output_xcorr_0_o_finish,
        o_d_out     => interface_in_output_xcorr_0_o_d_out,
        o_w_address => interface_in_output_xcorr_0_o_w_address 
        );
-- interface_out_output_0
interface_out_output_0 : interface_out_output
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_start      => interface_in_output_xcorr_0_o_finish,
        i_tx_busy    => i_tx_busy,
        i_d_in       => PF_TPSRAM_C1_0_R_DATA,
        i_r_addr_max => i_r_addr_max_const_net_0,
        -- Outputs
        o_RE         => interface_out_output_0_o_RE,
        o_ready      => o_ready_net_0,
        o_d_out      => o_d_out_net_0,
        o_r_address  => interface_out_output_0_o_r_address 
        );
-- interface_out_synth_0
interface_out_synth_0 : interface_out_synth
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_r_req      => xcorr_fsm_0_o_mem_req,
        i_d_in       => PF_TPSRAM_C0_0_R_DATA,
        i_r_addr_max => xcorr_fsm_0_o_max_addr,
        -- Outputs
        o_RE         => interface_out_synth_0_o_RE,
        o_ready      => interface_out_synth_0_o_ready,
        o_d_out      => interface_out_synth_0_o_d_out,
        o_r_address  => interface_out_synth_0_o_r_address 
        );
-- interface_out_synth_1
interface_out_synth_1 : interface_out_synth
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_r_req      => xcorr_fsm_0_o_mem_req,
        i_d_in       => PF_TPSRAM_C0_1_R_DATA,
        i_r_addr_max => xcorr_fsm_0_o_max_addr,
        -- Outputs
        o_RE         => interface_out_synth_1_o_RE,
        o_ready      => interface_out_synth_1_o_ready,
        o_d_out      => interface_out_synth_1_o_d_out,
        o_r_address  => interface_out_synth_1_o_r_address 
        );
-- parallelized_xcorr_0
parallelized_xcorr_0 : parallelized_xcorr
    port map( 
        -- Inputs
        i_clk        => i_clk,
        i_nrst       => i_nrst,
        i_data_ready => AND2_0_Y,
        i_data1      => interface_out_synth_0_o_d_out,
        i_data2      => interface_out_synth_1_o_d_out,
        -- Outputs
        o_finish     => parallelized_xcorr_0_o_finish,
        o_rxy        => parallelized_xcorr_0_o_rxy 
        );
-- PF_TPSRAM_C0_0
PF_TPSRAM_C0_0 : PF_TPSRAM_C0
    port map( 
        -- Inputs
        W_EN   => interface_in_0_o_WE,
        R_EN   => interface_out_synth_0_o_RE,
        CLK    => i_clk,
        W_DATA => interface_in_0_o_d_out,
        W_ADDR => interface_in_0_o_w_address,
        R_ADDR => interface_out_synth_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C0_0_R_DATA 
        );
-- PF_TPSRAM_C0_1
PF_TPSRAM_C0_1 : PF_TPSRAM_C0
    port map( 
        -- Inputs
        W_EN   => interface_in_1_o_WE,
        R_EN   => interface_out_synth_1_o_RE,
        CLK    => i_clk,
        W_DATA => interface_in_1_o_d_out,
        W_ADDR => interface_in_1_o_w_address,
        R_ADDR => interface_out_synth_1_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C0_1_R_DATA 
        );
-- PF_TPSRAM_C1_0
PF_TPSRAM_C1_0 : PF_TPSRAM_C1
    port map( 
        -- Inputs
        W_EN   => interface_in_output_xcorr_0_o_WE,
        R_EN   => interface_out_output_0_o_RE,
        CLK    => i_clk,
        W_DATA => interface_in_output_xcorr_0_o_d_out,
        W_ADDR => interface_in_output_xcorr_0_o_w_address,
        R_ADDR => interface_out_output_0_o_r_address,
        -- Outputs
        R_DATA => PF_TPSRAM_C1_0_R_DATA 
        );
-- xcorr_fsm_0
xcorr_fsm_0 : xcorr_fsm
    port map( 
        -- Inputs
        i_clk           => i_clk,
        i_nrst          => i_nrst,
        i_start         => i_start,
        i_signal_length => i_signal_length,
        -- Outputs
        o_mem_w_req_1   => xcorr_fsm_0_o_mem_w_req_1,
        o_mem_w_req_2   => xcorr_fsm_0_o_mem_w_req_2,
        o_mem_req       => xcorr_fsm_0_o_mem_req,
        o_max_addr      => xcorr_fsm_0_o_max_addr 
        );

end RTL;
